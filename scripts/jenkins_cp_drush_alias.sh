#!/bin/bash

SITE_DIR=`pwd`
DRUSH_DIR=$HOME"/.drush"
SCRIPT_DIR=$SITE_DIR"/scripts"
DRUSH_ALIAS_FILE="/wholefoods.aliases.drushrc.php"

if [ ! -d "$DRUSH_DIR" ]; then
  echo $DRUSH_DIR" dir NOT found, creating it"
  mkdir $DRUSH_DIR
else
  echo $DRUSH_DIR" dir found, removing it"
  rm -rf $DRUSH_DIR
  echo $DRUSH_DIR" dir deleted"
  mkdir $DRUSH_DIR
  echo $DRUSH_DIR" re-created"
fi

if cp -f $SCRIPT_DIR$DRUSH_ALIAS_FILE $DRUSH_DIR; then
  echo "copied "$SCRIPT_DIR$DRUSH_ALIAS_FILE" to "$DRUSH_DIR
else
  echo "FAILED to copy "$SCRIPT_DIR$DRUSH_ALIAS_FILE" to "$DRUSH_DIR
fi

