-- SUMMARY --

The API Tokens module provides an input filter that allows to replace custom
parametric tokens with rendered content. Each module is able to define own
parametric tokens (via hook_api_tokens_info) and token render function. Module
provides flexible caching mechanism.

For a full description of the module, visit the project page:
  http://drupal.org/project/api_tokens

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/api_tokens


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see https://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Go to /admin/config/content/formats and enable the API Tokens filter for any
  of your existing text formats or create a new one.

* List of all registered tokens available at admin/config/content/api-tokens.


-- CONTACT --

Current maintainers:
* Alex Zhulin (Alex Zhulin) - https://drupal.org/user/2659881

This project has been sponsored by:
* Blink Reaction
  A leader in Enterprise Drupal Development, delivering robust,
  high-performance websites for dynamic companies. Blink creates scalable and
  flexible web solutions that provide the best in customer experience and meet
  brand, marketing, and business goals.
