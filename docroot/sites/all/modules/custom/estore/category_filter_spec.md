# Category Filter Feature

We'd like to add a method to identify categories that will be filtered from the main navigation,
and presented on a page of their own.

For example, a category may be marked with the string "sports" to indicate
that is a sports catering category.

Any category with this flag would be excluded from the main navigation.

We will then create a secondary interface for the store where ONLY items
marked as sports would be displayed.

To achieve this, we'd like to add a string field to the Spinternet category admin
screen. This field should allow alpha characters only and should be optional.
It should default to null.

In addition, this value should be added to the results of the CATREQ api endpoint.

The web front end will be responsible for filtering the results of CATNAVREQ and CATREQ
based on this new field. That is, categories should not be included or excluded
from these APIs based on the value of this new field.
