<?php

class ESTORE_CACHE{
  
  public function set($key, $value, $ttl = 300){
    cache_set($key,$value);
    cache_set('cache_expire_' . $key,(time() + $ttl));
    return FALSE;
  }

  public function get($key){
    $expires = cache_get('cache_expire_' . $key);

    if ($expires !== FALSE) {
      $expires = intval($expires->data);
    } else {
      $expires = 0;
    }
    $value = cache_get($key);
    if ($value !== FALSE) {
      $value = $value->data;
    }

    if (@$expires && (@$expires > time()) && (@$value != FALSE)) {
      return $value;
    }

    return FALSE;
  }
}