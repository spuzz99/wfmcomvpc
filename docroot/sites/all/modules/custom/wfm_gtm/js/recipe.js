(function wfm_gtm_recipe($) {
  'use strict';

  var recipe = {};

  function recipeClickHandler(event) {
    var obj = recipe.buildDataObject();

    if (obj && window.dataLayer){
      window.dataLayer.push(obj);
    }
  }


  /**
   * Build an object for pushing into the datalayer
   */
  recipe.buildDataObject = function() {
    var recipeName = recipe.getRecipeName();
    var category = recipe.getCategory();
    var recipeType = recipe.getType();

    // No valid recipe name, category or recipeType
    if (!recipeName || !category || !recipeType) {
      return false;
    }


    return {
      event: 'recipe-printed',
      recipeName: recipeName,
      recipeCategory: category,
      recipeType: recipeType
    };
  };

   // Variable specialDiet added to global scope via php in
   // recipe_comments.tpl.php
   // category expects the special diet field
  recipe.getCategory = function() {
    if (!specialDiet.length) {
      return false;
    }
    return specialDiet;
  };

  // variable productTitle was added to global scope via php in
  // recipe_comments.tpl.php
  // Recipe Type actually uses the course field
  recipe.getType = function() {
    if (!course.length) {
      return false;
    }
    return course;
  };

  // variable productTitle was added to global scope via php in
  // recipe_comments.tpl.php
  recipe.getRecipeName = function() {
    if (!productTitle.length) {
     return false;
    }
    return productTitle;
  };

  recipe.addEventListeners = function() {
    $('.recipe-print').click(recipeClickHandler);
  };

  /**
   * Attach to Drupal behaviors
   */
  Drupal.behaviors.wfm_gtm_recipe = {
    attach: function (context, settings) {
      $('body', context).once('wfm_gtm_recipe', function () {
        recipe.addEventListeners();
      });
    }
  };
})(jQuery);