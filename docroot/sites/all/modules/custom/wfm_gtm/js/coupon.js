(function wfm_gtm_coupon($) {
  'use strict';

  var coupon = {};

  /**
   * getSelectedCouponIds
   * finds all checked coupons, grabs their
   * respective ID (which is the coupon's PLU) from the parent element,
   * and returns a comma-separated list of IDs
   *
   * @returns {Array}
   */
  coupon.getSelectedCouponIds = function() {
    var idArray = [];
    var coupons = $('.print-checkbox:checked').closest('.views-row');

    coupons.each(function(key, element) {
      // grab the first class in the list that contains coupon-
      var couponClass = $(element).attr('class').match(/coupon-[0-9]+/)[0];

      // remove the "coupon-" part so we're left with just the PLU
      // and append that to our array of coupon IDs
      idArray.push(couponClass.replace('coupon-', ''));
    });

    return idArray;
  };

  /**
   * sumCouponSavings
   * loops through each selected coupon, parses the discount amount, adds them all together,
   * and returns the total savings in dollar amount
   *
   * @param   {Array} couponIds
   * @returns {Number}
   */
  coupon.sumCouponSavings = function(couponIds) {
    var totalSavings = 0;
    $.each(couponIds, function(key, couponId) {
      var savingString = $('.coupon-'+ couponId).find('.views-field-field-coupon-offer .field-content').text();
      // unformat currency to get 'cents' value for each coupon and add to the running total
      // this regex removes everything except the actual integers
      totalSavings += parseInt(savingString.replace(/[^0-9-.]|\./g, ''));
    });

    // divide total cents by 100 to get dollar amount and cast to float
    return parseFloat(totalSavings / 100);
  };

  /**
   * getStore
   * get the store address and nid from the selected dropdown.
   * If no store is selected, return false.
   *
   * @returns {Object} or false
   */
  coupon.getStore = function() {
    var storeSelect = $('.store-select :selected');
    // if there is no store selected
    if (!storeSelect.val()) {
      return false;
    }

    return {
      nid: storeSelect.val(),
      address: storeSelect.attr('label')
    };
  };

  /**
   * buildDataObject
   * constructs the object we need to append to window.dataLayer
   *
   * @returns {Object}
   */
  coupon.buildDataObject = function() {
    var couponIds = coupon.getSelectedCouponIds();
    var storeInfo = coupon.getStore();
    var couponSavings = coupon.sumCouponSavings(couponIds);

    // do not proceed if we did not get any store information
    if (!storeInfo) {
      return false;
    }

    return {
      event: 'coupon-printed',
      couponIds: couponIds.join(', '),
      couponStoreName: storeInfo.address,
      couponStoreNumber: storeInfo.nid,
      couponQuantity: couponIds.length,
      totalSavings: couponSavings
    };
  };

  coupon.addEventListeners = function() {
    // When the user clicks "Print Selected"
    $('#edit-form-submit').click(function() {
      var obj = coupon.buildDataObject();

      // confirming that our data object and window.dataLayer
      // are both truthy values before appending the event
      if (obj && window.dataLayer) {
        window.dataLayer.push(obj);
      }
    });
  };

  /**
   * Attach to Drupal behaviors
   */
  Drupal.behaviors.wfm_gtm_coupon = {
    attach: function (context, settings) {
      $('body', context).once('wfm_gtm_coupon', function () {
        coupon.addEventListeners();
      });
    }
  };
})(jQuery);