<?php

/**
 * @file
 * Contains WFMShare.
 */

/**
 * The WFMShare class is the base class for shares.
 * It provides a common interface to control shares.
 *
 * The purpose of this class is to setup a way to generate share codes
 * for various tools throughout the application.
 *
 * This class stores and relays data, but it does not create the pages
 * that the data is shared on.
 */
class WFMShare {

  /**
   * Class WFMShare instatiation function.
   */
  public function __construct() {
  }

  /**
   * Creates the initial share. Returns the shortcode.
   *
   * The sharevars parameter accepts the following values:
   *   type - (string) The type of share this is.
   *   title - (string) The title of the share if you so choose.
   *   urlalias - (string) The Drupal URL alias that points to the content.
   *   data - (mixed) A data variable you want saved with the share.
   *   expires - (int) A Unix timestamp of when you want the share to expire.
   *
   * @param array $share_vars
   *   Array of values.
   * 
   * @return mixed
   *   FALSE or Share ID
   */
  static public function create($share_vars = array()) {
    // Verify required fields.
    $required_fields = array('recipient_email');

    foreach ($required_fields as $field) {
      if (!isset($share_vars[$field]) || (isset($share_vars[$field]) && empty($share_vars[$field]))) {
        return FALSE;
      }
    }

    $data = self::buildDataFields($share_vars);
    $share_id = db_insert('wfm_share')->fields($data)->execute();

    return (!empty($share_id) ? $share_id : FALSE);
  }

  /**
   * Returns the share.
   *
   * @param int $share_id
   *   ID of the share
   * @param array $conditions
   *   Other conditions for the share
   *
   * Possible values:
   *   recipient_item_id
   *   recipient_email
   *   shared_item_id
   *   uid
   *   type
   *   title
   *   status
   *   created
   *   changed
   *   expires
   *   visited
   *   urlalias
   */
  static public function read($share_id = NULL, $conditions = array(), $share_accepted = FALSE) {
    $query = db_select('wfm_share', 'ws');
    $cnt_query = db_select('wfm_share', 'ws');
    $filter_field = $filter_value = '';
    $allowed_fields = array(
      'recipient_item_id',
      'recipient_email',
      'shared_item_id',
      'uid',
      'type',
      'title',
      'status',
      'created',
      'changed',
      'expires',
      'visited',
      'urlalias'
    );
    $condition = array();

    // Add any passed in conditions that are valid.
    foreach ($conditions as $field_name => $field_value) {
      if (in_array($field_name, $allowed_fields) && !empty($field_value)) {
        $cnt_query->condition($field_name, $field_value);
        $query->condition($field_name, $field_value);
      }
    }

    // For when we are just loading by share id.
    if (!empty($share_id)) {
      $cnt_query->condition('shareid', (int) $share_id);
      $query->condition('shareid', (int) $share_id);
    }

    // Make sure that the share has been accepted by the recipient.
    if ($share_accepted) {
      $cnt_query->condition('recipient_item_id', '[none given]', '!=');
      $query->condition('recipient_item_id', '[none given]', '!=');
    }

    // Make sure it exists.
    $num_rows = $cnt_query->countQuery()->execute()->fetchField();

    if ($num_rows >= 1) {
      $results = $query->fields('ws')->execute();
      // Allows for multiple values to be returned.
      $share = $results->fetchAll();

      foreach ($share as $idx => $item) {
        if (isset($item->data) && !empty($item->data) && is_string($item->data)) {
          $item->data = unserialize($item->data);
          $share[$idx] = $item;
        }
      }

      if (!empty($share_id) && is_array($share)) {
        return $share[0];
      }
      else {
        return $share;
      }
    }

    return FALSE;
  }

  /**
   * Update record function.
   */
  static public function update($share_id = NULL, $share_vars = array()) {
    $data = self::buildDataFields($share_vars, TRUE);

    // Return FALSE if we don't have an ID or any data to save.
    if (empty($share_id) || empty($data)) {
      return FALSE;
    }

    $affected_rows = db_update('wfm_share')->fields($data)
      ->condition('shareid', $share_id)
      ->execute();
    return (!empty($affected_rows) ? TRUE : FALSE);
  }

  /**
   * Delete record function.
   */
  static public function delete($share_id = NULL) {
    if (!empty($share_id) && is_int($share_id)) {
      $affected_rows = db_delete('wfm_share')->condition('shareid', $share_id)->execute();
      return (!empty($affected_rows) ? TRUE : FALSE);
    }

    return FALSE;
  }

  /**
   * Read by recipient.
   */
  static public function readByRecipient($item_id = NULL, $email = NULL) {
    $fields = array(
      'recipient_item_id' => $item_id,
      'recipient_email' => $email,
    );

    return self::read(NULL, $fields);
  }

  /**
   * Read by sharer.
   */
  static public function readBySharer($item_id = NULL, $uid = NULL) {
    $fields = array(
      'shared_item_id' => $item_id,
      'uid' => $uid,
    );

    return self::read(NULL, $fields, TRUE);
  }

  /**
   * Read by sharer UID.
   */
  static public function readBySharerUID($uid = NULL) {
    return self::read(NULL, NULL, NULL, $uid);
  }

  /**
   * Shared by me.
   */
  static public function sharedByMe() {
    global $user;
    $results = WFMShare::read(NULL, array('uid' => $user->uid), TRUE);
    $shared_by_me = new stdClass();

    if (empty($results)) {
      return FALSE;
    }

    foreach ($results as $share) {
      $shared_by_me->shares[] = $share;
      $shared_by_me->ids[$share->shared_item_id] = $share->shared_item_id;
    }

    return $shared_by_me;
  }

  /**
   * Shared to me.
   */
  static public function sharedToMe() {
    global $user;
    $results = WFMShare::read(NULL, array('recipient_email' => $user->mail), TRUE);
    $shared_to_me = new stdClass();

    if (empty($results)) {
      return FALSE;
    }

    foreach ($results as $share) {
      $shared_to_me->shares[] = $share;
      $shared_to_me->ids[$share->recipient_item_id] = $share->recipient_item_id;
    }

    return $shared_to_me;
  }

  /**
   * This is for just using the short code functionality and not the sharing functionality.
   */
  static public function makeShortCode($id = NULL) {
    if (empty($id)) {
      return FALSE;
    }

    return self::shortCode($id);
  }

  /**
   * This is for just using the short code functionality and not the sharing functionality.
   */
  static public function decodeShortCode($id = NULL) {
    if (empty($id)) {
      return FALSE;
    }

    return self::shortCode($id, TRUE);
  }

  /**
   * Update records that was visited.
   */
  static public function recordVisit($share_id = NULL, $visited = NULL) {
    if (empty($visited) || !is_int($visited)) {
      $visited = time();
    }

    return self::update($share_id, array('visited', $visited));
  }

  /**
   * Tells you whether or not the share is expired.
   * 
   * Apply your own rules from here.
   */
  static public function isExpired($share_id = NULL) {
    $share = self::read($share_id);

    if ($share->expires >= time()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Return short code.
   */
  static public function getShortCode($share_id = NULL) {
    $share = self::read($share_id);

    if (!empty($share)) {
      return self::shortCode($share_id);
    }

    return FALSE;
  }

  /**
   * Return share from short code.
   */
  static public function getShareFromShortCode($short_code = NULL) {
    $share_id = self::shortCode($short_code, TRUE);
    $share = self::read($share_id);

    return $share;
  }

  /**
   * Build data fields.
   */
  static protected function buildDataFields($share_vars = array(), $update = FALSE) {
    $string_vars = array(
      'recipient_email',
      'type',
      'title',
      'urlalias',
      'shared_item_id',
      'recipient_item_id'
    );
    $int_vars = array('uid', 'status', 'visited', 'expires');

    // Build the list of fields that can be added to the insert/update data.
    $fields = array_merge($string_vars, $int_vars, array(
      'changed',
      'created',
      'data'
    ));

    // Initialize all the variables.
    foreach ($fields as $var_name) {
      $$var_name = NULL;
    }

    $timestamp = time();
    // Defaults changed to right now.
    $changed = $timestamp;

    if (!$update) {
      // Defaults to the current user's uid.
      global $user;
      $uid = $user->uid;

      // Defaults type to generic.
      $type = 'generic';
      // Defaults status to 1 (active).
      $status = 1;
      // Defaults timestamps to right now.
      $created = $timestamp;
    }
    else {
      // Then we are in update mode.
    }

    // Now blend in the passed in variables.
    if (!empty($share_vars)) {
      // Check the string vars.
      foreach ($string_vars as $var_name) {
        if (isset($share_vars[$var_name]) && !empty($share_vars[$var_name])) {
          $$var_name = (string) $share_vars[$var_name];
        }
      }

      // Check the int vars.
      foreach ($int_vars as $var_name) {
        if (isset($share_vars[$var_name]) && !empty($share_vars[$var_name])) {
          $$var_name = (int) $share_vars[$var_name];
        }
      }

      if (isset($share_vars['data']) && !empty($share_vars['data'])) {
        $data = serialize($share_vars['data']);
      }
    }

    // Build data set to insert/update.
    $fields_and_values = array();

    // Insert values only if set above somewhere.
    foreach ($fields as $field_name) {
      if (isset($$field_name) && !empty($$field_name)) {
        $fields_and_values[$field_name] = $$field_name;
      }
    }

    return $fields_and_values;
  }

  /**
   * Translates a number to a short alhanumeric version.
   * 
   * Lovingly copied from...
   *   http://kvz.io/blog/2009/06/10/create-short-ids-with-php-like-youtube-or-tinyurl/
   *
   * Translated any number up to 9007199254740992
   * to a shorter version in letters e.g.:
   * 9007199254740989 --> PpQXn7COf
   *
   * specifiying the second argument TRUE, it will
   * translate back e.g.:
   * PpQXn7COf --> 9007199254740989
   *
   * this function is based on any2dec && dec2any by
   * fragmer[at]mail[dot]ru
   * see: http://nl3.php.net/manual/en/function.base-convert.php#52450
   *
   * If you want the alphaID to be at least 3 letter long, use the
   * $pad_up = 3 argument
   *
   * In most cases this is better than totally random ID generators
   * because this can easily avoid duplicate ID's.
   * For example if you correlate the alpha ID to an auto incrementing ID
   * in your database, you're done.
   *
   * The reverse is done because it makes it slightly more cryptic,
   * but it also makes it easier to spread lots of IDs in different
   * directories on your filesystem. Example:
   * $part1 = substr($alpha_id,0,1);
   * $part2 = substr($alpha_id,1,1);
   * $part3 = substr($alpha_id,2,strlen($alpha_id));
   * $destindir = "/".$part1."/".$part2."/".$part3;
   * // by reversing, directories are more evenly spread out. The
   * // first 26 directories already occupy 26 main levels
   *
   * more info on limitation:
   * - http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-talk/165372
   *
   * if you really need this for bigger numbers you probably have to look
   * at things like: http://theserverpages.com/php/manual/en/ref.bc.php
   * or: http://theserverpages.com/php/manual/en/ref.gmp.php
   * but I haven't really dugg into this. If you have more info on those
   * matters feel free to leave a comment.
   *
   * The following code block can be utilized by PEAR's Testing_DocTest
   * <code>
   * // Input //
   * $number_in = 2188847690240;
   * $alpha_in = "SpQXn7Cb";
   *
   * // Execute //
   * $alpha_out = alphaID($number_in, FALSE, 8);
   * $number_out = alphaID($alpha_in, TRUE, 8);
   *
   * if ($number_in != $number_out) {
   *   echo "Conversion failure, ".$alpha_in." returns ".$number_out." instead of the ";
   *   echo "desired: ".$number_in."\n";
   * }
   * if ($alpha_in != $alpha_out) {
   *   echo "Conversion failure, ".$number_in." returns ".$alpha_out." instead of the ";
   *   echo "desired: ".$alpha_in."\n";
   * }
   *
   * // Show //
   * echo $number_out." => ".$alpha_out."\n";
   * echo $alpha_in." => ".$number_out."\n";
   * echo alphaID(238328, FALSE)." => ".alphaID(alphaID(238328, FALSE), TRUE)."\n";
   *
   * // expects:
   * // 2188847690240 => SpQXn7Cb
   * // SpQXn7Cb => 2188847690240
   * // aaab => 238328
   *
   * </code>
   *
   * @author  Kevin van Zonneveld <kevin@vanzonneveld.net>
   * @author  Simon Franz
   * @author  Deadfish
   * @copyright 2008 Kevin van Zonneveld (http://kevin.vanzonneveld.net)
   * @license  http://www.opensource.org/licenses/bsd-license.php New BSD Licence
   * @version  SVN: Release: $Id: alphaID.inc.php 344 2009-06-10 17:43:59Z kevin $
   * @link   http://kevin.vanzonneveld.net/
   *
   * @param mixed $in
   *   String or long input to translate.
   * @param bool $to_num
   *   Reverses translation when TRUE.
   * @param mixed  $pad_up
   *   Number or boolean padds the result up to a specified length.
   * @param string $pass_key
   *   Supplying a password makes it harder to calculate the original ID.
   *
   * @return mixed
   *   string or long
   */
  static protected function shortCode($in, $to_num = FALSE, $pad_up = FALSE, $pass_key = NULL) {
    $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    if ($pass_key !== NULL) {
      // Although this function's purpose is to just make the
      // ID short - and not so much secure,
      // with this patch by Simon Franz (http://blog.snaky.org/)
      // you can optionally supply a password to make it harder
      // to calculate the corresponding numeric ID.

      for ($n = 0; $n < strlen($index); $n++) {
        $i[] = substr($index, $n, 1);
      }

      $passhash = hash('sha256', $pass_key);
      $passhash = (strlen($passhash) < strlen($index)) ? hash('sha512', $pass_key) : $passhash;

      for ($n = 0; $n < strlen($index); $n++) {
        $p[] = substr($passhash, $n, 1);
      }

      array_multisort($p, SORT_DESC, $i);
      $index = implode($i);
    }

    $base = strlen($index);

    if ($to_num) {
      // Digital number <<-- alphabet letter code.
      $in = strrev($in);
      $out = 0;
      $len = strlen($in) - 1;

      for ($t = 0; $t <= $len; $t++) {
        $bcpow = bcpow($base, $len - $t);
        $out = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
      }

      if (is_numeric($pad_up)) {
        $pad_up--;

        if ($pad_up > 0) {
          $out -= pow($base, $pad_up);
        }
      }
      $out = sprintf('%F', $out);
      $out = substr($out, 0, strpos($out, '.'));
    }
    else {
      // Digital number -->> alphabet letter code.
      if (is_numeric($pad_up)) {
        $pad_up--;

        if ($pad_up > 0) {
          $in += pow($base, $pad_up);
        }
      }

      $out = "";

      for ($t = floor(log($in, $base)); $t >= 0; $t--) {
        $bcp = bcpow($base, $t);
        $a = floor($in / $bcp) % $base;
        $out = $out . substr($index, $a, 1);
        $in = $in - ($a * $bcp);
      }

      // Reverse.
      $out = strrev($out);
    }

    return $out;
  }

}
