<?php

/**
 * @file
 * Custom module wfm_mobile_recipe_box.
 */

/**
 * Implements hook_menu().
 */
function wfm_mobile_recipe_box_menu() {
  $items = array();

  $items['myrecipebox/add/%node'] = array(
    'title' => 'Add Recipe to a Recipe Box',
    'page callback' => 'wfm_mobile_recipe_box_add_callback',
    'page arguments' => array(2),
    'access callback' => 'user_is_logged_in',
    'type' => MENU_CALLBACK,
  );

  $items['myrecipebox/remove/%'] = array(
    'title' => 'Remove Recipe from a Recipe Box',
    'page callback' => 'wfm_mobile_recipe_box_delete_callback',
    'page arguments' => array(2),
    'access callback' => 'user_is_logged_in',
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Implements hook_ctools_plugin_directory().
 */
function wfm_mobile_recipe_box_ctools_plugin_directory($module, $plugin) {
  $dir = '';
  if ('ctools' == $module) {
    $dir = 'plugins/' . $plugin;
  }

  return $dir;
}


/**
 * Add recipe to a Recipe Box callback.
 */
function wfm_mobile_recipe_box_add_callback($node) {
  if ($node && 'recipe' == $node->type) {
    wfm_mobile_recipe_box_add($node);
  }
  drupal_goto();
}


/**
 * Delete recipe from a Recipe Box callback.
 */
function wfm_mobile_recipe_box_delete_callback($id) {
  wfm_mobile_recipe_box_delete($id);
  drupal_goto();
}


/**
 * Returns a list of recipes.
 */
function wfm_mobile_recipe_box_get() {
  global $user;
  try {
    $recipeListApi = new WFMApiRecipeList();
    $response = $recipeListApi->getList($user->uid);
    $query = db_select('taxonomy_term_data', 'D');
    $query->join('taxonomy_vocabulary', 'V', 'V.vid = D.vid');
    $cats = $query->fields('D', array('tid', 'name'))
      ->condition('V.machine_name', 'recipe_type_dish')
      ->orderBy('D.name')
      ->execute()
      ->fetchAllKeyed();
    $data = array();
    foreach ($response as $item) {
      $tid = $item['node']->field_recipe_course ? $item['node']->field_recipe_course[LANGUAGE_NONE][0]['tid'] : 0;
      $name = isset($cats[$tid]) ? $cats[$tid] : t('Other');
      $data[$name][] = array(
        'nid' => $item['node']->nid,
        'title' => $item['node']->title,
      );
    }
    ksort($data);
  }
  catch (Exception $e) {
    drupal_set_message(t('An error occurred while trying to get a recipe list.'), 'warning');
    $data = array();
  }
  return $data;
}


/**
 * Returns a list of recipes.
 */
function wfm_mobile_recipe_box_in_list($nid) {
  global $user;
  try {
    $recipeListApi = new WFMApiRecipeList();
    $response = $recipeListApi->isInList($user->uid, $nid);
  }
  catch (Exception $e) {
    $response = FALSE;
  }
  return $response;
}


/**
 * Addes a recipe.
 */
function wfm_mobile_recipe_box_add($node = NULL) {
  global $user;
  try {
    $value = $node->nid;
    $recipeListApi = new WFMApiRecipeList();
    $recipeListApi->addItem($user->uid, $value);
    $status = TRUE;
  }
  catch (Exception $e) {
    drupal_set_message(t('An error occurred while trying to add a recipe.'), 'warning');
    $status = FALSE;
  }
  return $status;
}


/**
 * Deletes a recipe.
 */
function wfm_mobile_recipe_box_delete($rid = 0) {
  global $user;
  try {
    $recipeListApi = new WFMApiRecipeList();
    $recipeListApi->deleteItem($user->uid, $rid);
    $status = TRUE;
  }
  catch (Exception $e) {
    drupal_set_message(t('An error occurred while trying to delete a recipe.'), 'warning');
    $status = FALSE;
  }
  return $status;
}
