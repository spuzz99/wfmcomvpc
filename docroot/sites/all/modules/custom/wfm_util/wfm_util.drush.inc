<?php
/**
 * @file
 * Provide Drush integration for removing redundant video nodes.
 */
/**
 * Implements hook_drush_command().
 */
function wfm_util_drush_command() {

  $items['video-dedupe'] = array(
    'description' => 'Remove redundant video nodes with identical titles.',
    'aliases' => array('vdd'),
    'callback' => '_videodedupe_delete',
  );

  return $items;
}


/**
 * Callback for Drush video de-dupe command.
 */
function _videodedupe_delete() {
  wfm_util_videodedupe('video');
}