<?php
/**
 * @file
 * Contains WFMFacebook.
 */

// Include the wfmsocial.inc to get the WFMSocial abstract class.
include_once 'wfmsocial.inc';

class WFMFacebook extends WFMSocial {
  /**
   * Get Facebook posts by store.
   *
   * @param int $limit
   * @param string $store
   * @param bool $exclusive
   *
   * @retval object
   *   A
   */
  public function getPosts($limit = 3, $store = NULL, $exclusive = FALSE) {
    try {
      $store_id = $this->_getStoreId($store, $exclusive);
    }
    catch (Exception $e) {
      return;
    }

    $cid = $this->_buildCid($store_id, 'Facebook');
    $cid_time = $this->_buildTCid($store_id, 'Facebook');

    $cached = $this->_getCache($cid);
    $cached_time = $this->_getCache($cid_time);

    if ($cached == NULL || $cached_time < REQUEST_TIME) {
      $posts_obj = $this->_getPostsNotFromCache($limit, $store_id);

      $this->_setCache($cid, $posts_obj);
      $this->_setCache($cid_time, REQUEST_TIME + 3600);
    }
    else {
      $posts_obj = $cached;
    }
    $count = count($posts_obj->data);
    for ($i = 0; $i < $count; $i++) {
      //Trim before we make links so that they don't get chopped off.
      $posts_obj->data[$i]->message = $this->trimMessage($posts_obj->data[$i]->message, 150);
      $posts_obj->data[$i]->message = $this->makeLinks($posts_obj->data[$i]->message);
    }
    return $posts_obj;
  }

  /**
   *
   */
  protected function _getPostsNotFromCache($limit, $store_id) {
    $posts_to_fetch = $limit;
    $fetch_count = variable_get('facebook_post_fetcher_post_count', 0);
    do {
      // Fetch wall posts.
      $posts_obj = $this->_fetchPosts($store_id, $posts_to_fetch);

      if (isset($posts_obj->data)) {
        // Filter posts.
        $posts_obj = $this->_feedProcess($posts_obj);

        // Determine how many posts to request next iteration (if necessary).
        if (count($posts_obj->data) < $fetch_count) {
          $posts_to_fetch
            = (int) $fetch_count + $posts_to_fetch - count($posts_obj->data);
        }
      }
      else {
        watchdog('facebook_post_fetcher',
                 'No wall post data returned from Facebook.',
                 array(),
                 WATCHDOG_NOTICE);

        if (isset($post_obj->error)) {
          watchdog('facebook_post_fetcher',
                   'Facebook error: ' . $post_obj->error->message,
                   array(),
                   WATCHDOG_NOTICE);
        }

        $posts_obj->data = array();
        break;
      }
    }
    while (isset($posts_obj->data) &&
           !empty($posts_obj->data) &&
           count($posts_obj->data) < $fetch_count);

    // Re-index array.
    $posts_obj->data = array_values($posts_obj->data);
    return $posts_obj;
  }

  /**
   */
  protected function _fetchPosts($store_id, $post_count) {
    // Define the Facebook Graph API URL to request posts.
    $app_id = WFMVariables::get(WFMVariables::SOCIAL_FACEBOOK_APP_ID);
    $secret = WFMVariables::get(WFMVariables::SOCIAL_FACEBOOK_APP_SECRET);

    $token = $this->_getAccessToken($app_id, $secret);

    $graph_url = FACEBOOK_GRAPH_URL . '/' . $store_id . '/posts?access_token=' . $token . '&limit=' . (isset($post_count) ? $post_count : 3) . '&date_format=U';

    watchdog('Facebook Request', $graph_url, array(), WATCHDOG_NOTICE);
    // Request posts JSON from Facebook.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $graph_url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

    $posts_json = curl_exec($ch);
    curl_close($ch);
    if ($posts_json !== FALSE) {
      // Decode JSON into PHP object.
      $posts_obj = json_decode($posts_json);
    }
    else {
      $posts_obj = array();
      watchdog('facebook_post_fetcher', 'Failed to request obtain Facebook posts using: ' . $graph_url, array(), WATCHDOG_ERROR);
    }
    return $posts_obj;
  }

  /**
   */
  protected function _feedProcess($posts_obj) {
    foreach ($posts_obj->data as $key => $post) {
      // Remove Facebook Story wall posts.
      if (isset($post->story)) {
        unset($posts_obj->data[$key]);
        continue;
      }

      $this->_getUserpic($posts_obj->data[$key]);
    }

    return $posts_obj;
  }

  /**
   */
  protected function _getAccessToken($app_id, $app_secret) {
    // Define the Facebook Graph API URL to request access token.
    $graph_url = FACEBOOK_GRAPH_URL . '/oauth/access_token?grant_type=client_credentials&client_id=' . $app_id . '&client_secret=' . $app_secret;
    watchdog('Facebook Token', 'Getting Facebook Token', array(), WATCHDOG_NOTICE);

    // Request access token from Facebook.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $graph_url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

    $access_token = curl_exec($ch);

    if (curl_errno($ch)) {
      watchdog('curl', 'FB Access Token Curl Error: ' . curl_error($ch), array(), WATCHDOG_ERROR);
    }

    curl_close($ch);
    if ($access_token !== FALSE) {
      // Trim access_token key from response.
      $access_token = str_replace('access_token=', '', $access_token);
    }
    else {
      watchdog('facebook_post_fetcher', 'Failed to request obtain Facebook access token using: ' . $graph_url, array(), WATCHDOG_ERROR);
    }

    return $access_token;
  }

  /**
   */
  protected function _getStoreId($store, $exclusive) {
    try {
      $val = $this->_getStoreFieldValue($store, $exclusive, 'field_facebook_url', 'url');

      $val = str_replace(array('https://', 'http://'), array('', ''), $val);
      $exploded_val = explode('/', $val, 2);
      $val = $exploded_val[1];
      $exploded_val = explode('?', $val);
      $exploded_val = explode('#', $exploded_val[0]);
      $exploded_val = explode('/', $exploded_val[0]);
      $val = array_pop($exploded_val);
      return $val;
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   */
  protected function _getUserpic(&$post) {
    $graph_url = FACEBOOK_GRAPH_URL . '/' . $post->from->id . '/picture';
    $headers = get_headers($graph_url, 1);
    if (isset($headers['Location'])) {
      $post->from->picture = $headers['Location'];
    }
    else {
      $post->from->picture = NULL;
    }
  }
}
