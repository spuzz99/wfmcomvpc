<?php
/**
* @file
* Default variables file for social media - Facebook
*
*/

$variables[self::SOCIAL_FACEBOOK_APP_ID] = array(
  'title' => 'Facebook App ID',
  'category' => self::CATEGORY_SOCIAL,
  'help' => '',
  'default' => '189541064455755',
);

$variables[self::SOCIAL_FACEBOOK_APP_SECRET] = array(
  'title' => 'Facebook App Secret',
  'category' => self::CATEGORY_SOCIAL,
  'help' => '',
  'default' => '1ab02a2a70e9269771679b6395454100',
);

$variables[self::SOCIAL_TWITTER_CONSUMER_KEY] = array(
  'title' => 'Twitter Consumer Key',
  'category' => self::CATEGORY_SOCIAL,
  'help' => '',
  'default' => 'x3NsHErWlw41Rz0OkH61UA',
);

$variables[self::SOCIAL_TWITTER_CONSUMER_SECRET] = array(
  'title' => 'Twitter Consumer Secret',
  'category' => self::CATEGORY_SOCIAL,
  'help' => '',
  'default' => 'CJ7j4MF7UG1TXbMc4fB3RGn0adH8XSMf17ZZWBhpWFE',
);

$variables[self::SOCIAL_TWITTER_OAUTH_ACCESS_TOKEN] = array(
  'title' => 'Twitter OAuth Access Token',
  'category' => self::CATEGORY_SOCIAL,
  'help' => '',
  'default' => '15131310-g2Cq7mutXvvMh1bXft4pq4U5uWSdkkuzlJIB9Q6mf',
);

$variables[self::SOCIAL_TWITTER_OAUTH_ACCESS_TOKEN_SECRET] = array(
  'title' => 'Twitter OAuth Access Token Secret',
  'category' => self::CATEGORY_SOCIAL,
  'help' => '',
  'default' => 'ef7PyeJ03UhVCf1B2f5nNLr1fUS6Pm6MfSJIcvVp8AU',
);