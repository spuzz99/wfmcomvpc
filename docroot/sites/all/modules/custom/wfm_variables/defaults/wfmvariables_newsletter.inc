<?php
/**
* @file
* Default variables file for newsletter
*
*/

$variables[self::NEWSLETTER_UNSUBSCRIBE_LINK] = array(
  'title' => 'Newsletter Unsubscribe Link',
  'category' => self::CATEGORY_NEWSLETTER,
  'help' => '',
  'default' => 'http://communications.wholefoodsmarket.com/pub/sf/ResponseForm',
);

$variables[self::NEWSLETTER_RI] = array(
  'title' => 'Newsletter RI Value',
  'category' => self::CATEGORY_NEWSLETTER,
  'help' => '',
  'default' => 'X0Gzc2X%3DWQpglLjHJlTQGmFkwj7knwL3pW7WIUiX1tzajEi1cVXMtX%3DWQpglLjHJlTQGnSqOdp53zavuFR9Y14zeTcWAPChlG',
);

$variables[self::NEWSLETTER_EI] = array(
  'title' => 'Newsletter EI Value',
  'category' => self::CATEGORY_NEWSLETTER,
  'help' => '',
  'default' => 'EmKX1i-iU9oM5Emz8kQjo6Y',
);

$variables[self::NEWSLETTER_RI_PREF] = array(
  'title' => 'Newsletter RI Value - Preferences',
  'category' => self::CATEGORY_NEWSLETTER,
  'help' => '',
  'default' => 'X0Gzc2X%3DWQpglLjHJlTQGmFkwj7knwL3pW7WIUiX1tzajEi1cVXMtX%3DWQpglLjHJlTQGnSqOdp53zavuFR9Y14zeTcWAPChlG',
);

$variables[self::NEWSLETTER_EI_PREF] = array(
  'title' => 'Newsletter EI Value - Preferences',
  'category' => self::CATEGORY_NEWSLETTER,
  'help' => '',
  'default' => 'EhD67L35CWQqkTBBOptvy60',
);

