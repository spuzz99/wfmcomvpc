<?php
/**
* @file
* Default variables file for Google Maps
*
*/

$variables[self::GMAPS_JS_API_URL] = array(
  'title' => 'Google Maps JavaScript API URL',
  'category' => self::CATEGORY_GMAPS,
  'help' => '',
  'default' => 'http://maps.googleapis.com/maps/api/js',
);

$variables[self::GMAPS_JSON_API_URL] = array(
  'title' => 'Google Maps JSON API URL',
  'category' => self::CATEGORY_GMAPS,
  'help' => '',
  'default' => 'http://maps.googleapis.com/maps/api/geocode/json',
);

$variables[self::GMAPS_CRYPTO_KEY] = array(
  'title' => 'Google Maps Cryptographic Signing Key',
  'category' => self::CATEGORY_GMAPS,
  'help' => '',
  'default' => 'kY7SNuzT-dgpF5XnbAF9BB2SKNE=',
);
