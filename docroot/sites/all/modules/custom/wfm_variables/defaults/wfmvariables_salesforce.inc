<?php
/**
* @wfmvariables_salesforce.inc
* Default variables file for Salesforce
*
*/

$variables[self::SALESFORCE_ORGANIZATION_ID] = array(
  'title' => 'Salesforce Organization ID',
  'category' => self::CATEGORY_SALESFORCE,
  'help' => '',
  'default' => '00DF0000000gR7X',
);
$variables[self::SALESFORCE_POST_URL] = array(
  'title' => 'Salesforce POST URL',
  'category' => self::CATEGORY_SALESFORCE,
  'help' => '',
  'default' => 'https://www.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8',
);
$variables[self::SALESFORCE_CATEGORY_FIELD] = array(
  'title' => 'Salesforce Category Field',
  'category' => self::CATEGORY_SALESFORCE,
  'help' => '',
  'default' => '00NF000000CThMH',
);
$variables[self::SALESFORCE_STORE_FIELD] = array(
  'title' => 'Salesforce store Field',
  'category' => self::CATEGORY_SALESFORCE,
  'help' => '',
  'default' => '00NF000000CThMY',
);
$variables[self::SALESFORCE_UPC_FIELD] = array(
  'title' => 'Salesforce UPC Field',
  'category' => self::CATEGORY_SALESFORCE,
  'help' => '',
  'default' => '00NF000000CThMc',
);
$variables[self::SALESFORCE_ZIP_FIELD] = array(
  'title' => 'Salesforce zip Field',
  'category' => self::CATEGORY_SALESFORCE,
  'help' => '',
  'default' => '00NF000000CThMZ',
);
$variables[self::SALESFORCE_TLC_FIELD] = array(
  'title' => 'Salesforce TLC Field',
  'category' => self::CATEGORY_SALESFORCE,
  'help' => '',
  'default' => '00NF000000D0Eps',
);
$variables[self::SALESFORCE_UA_FIELD] = array(
  'title' => 'Salesforce User Agent Field',
  'category' => self::CATEGORY_SALESFORCE,
  'help' => 'Please provide the field id for the production salesforce instance.',
  'default' => '00NF000000D0G5v',
);