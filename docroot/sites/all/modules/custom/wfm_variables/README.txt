WFM_VARIABLES
=======================================================
The WFM_VARIABLES module provides a single admin UI for runtime variables.  
This is handy for saving variables that might change in the future or as 
the instance moves from DEV to STAGE to PROD. This module does nothing on 
it's own, but is used by other WFM custom modules .

TO MODIFY A RUNTIME VARIABLE ==========================
(admin task)
1. In the Administration UI select 
   Configuration : Whole Foods Market : Whole Foods Market Site Variables. 
   (admin/config/wfm/variables).
2. Locate and modify value.
3. Press Save.

TO ADD A VARIABLE TO THE LIST =========================
(Developer Task)
1. In the wfmvariables.inc file add your new variable and new variable class 
(if any) to the WFMVariables class object.

IF YOU HAVE A NEW VARIABLE CLASS:
2. In the defaults/wfmvariables_default.inc file add a requireOnce statement 
for your (yet to be created) variable include file.  For example:

    require_once 'wfmvariables_mymodule.inc';

3. Create a new variables include file.  For example:

    defaults/wfmvariables_mymodule.inc
    
4. In this file add to the $variables array.  

    <?php
    /**
    * @file
    * Default variables file for eStore
    */
    
    $variables[self::MYMODULE_NEW_VARIABLE] = array(
      'title' => 'eStore Top Categories URL',
      'category' => self::CATEGORY_MYMODULE,
      'help' => 'Information about this variable',
      'default' => 'default_value',
    );

5. Save files and clear cache.

TO CONSUME A VARIABLE WITHIN YOUR MODULE ================
(developer task)
1. In your mymodule.info file add wfm_variables as a dependancy.

    dependencies[] = wfm_variables

2. In your code grab the value of the shared variable.

    $localvar = WFMVariables::get(WFMVariables::MYMODULE_NEW_VARIABLE);

The missing database part ================
(developer task)
1. You will want to have these variables saved to the variables table but they will not save if they are use their default value.
2. navigate to admin/config/wfm/variables
3. change your values by inserting a character like z at the front of each variable
4. Click save
5. Drupal will report something like this for each variable:
Updating variable sf_org_id to 00DF0000000gR7X
6. Make certain each variable is listed in this message
7. Remove the z's you inserted in step 3
8. click save
9. Now your variables are saved to the variables table
10. You can verify with
drush vget wfm_variable__zzzz
Note. replace the zzzz with the first few letters of your new variables
11. If you see: "No matching variable found" your variables have not been saved.
12. You probably want your variables to be in a feature so add them in the strongarm section.
