<?php

/**
 * @file
 * Custom blocks for WholeFoods site
 */

include_once 'wfm_blocks.form.inc';

/**
 * Implements hook_block_info().
 */
function wfm_blocks_block_info() {
  $blocks = array();
  $blocks['wfm_sharelinks'] = array(
    'info' => t('wfm_blocks: Promo share links'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  $blocks['wfm_breadcrumbs'] = array(
    'info' => t('wfm_blocks: Breadcrumbs'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  $blocks['wfm_blocks_search_banner'] = array(
    'info' => t('wfm_blocks: Search Banner'),
    'visibility' => BLOCK_VISIBILITY_LISTED,
    'pages' => 'site_search/*',
    'region' => 'content',
    'weight' => '-10',
    'status' => TRUE,
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  $blocks['wfm_blocks_subscription'] = array(
    'info' => t('wfm_blocks: Newsletter Subscription'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['wfm_blocks_vendors'] = array(
    'info' => t('WFM Blocks Vendor Select'),
    'status' => 1,
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['wfm_blocks_footer'] = array(
    'info' => t('wfm_blocks: Editable Footer'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  // Commenting out these blocks but leaving them in case we want to restore this functionality.
  // $blocks['wfm_blocks_oo_header'] = array(
  //   'info' => t('wfm_blocks: Online Ordering Header'),
  // );
  // $blocks['online_ordering_body'] = array(
  //   'info' => t('Online Ordering Body'),
  //   'status' => 1,
  // );
  $blocks['wfm_blocks_no_search_results'] = array(
    'info' => t('wfm_blocks: No Search Results'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  $blocks['product_lines_sidebar'] = array(
    'info' => t('Product Lines Sidebar'),
    'status' => 1,
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  $blocks['wfm_blocks_coupon_filters'] = array(
    'info' => t('wfm_blocks: Coupon Filters'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['wfm_store_social_widgets'] = array(
    'info' => t('wfm_blocks: Store Social Widgets'),
    'pages' => "stores/*",
    'region' => 'sidebar_second',
    'status' => TRUE,
    'visibility' => BLOCK_VISIBILITY_LISTED,
    'weight' => 28,
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  $blocks['wfm_uk_dept_social_widgets'] = array(
    'info' => t('wfm_blocks: UK Dept Social Widgets'),
    'pages' => "department/article/*-uk\ndepartment/*-uk",
    'region' => 'sidebar_second',
    'status' => TRUE,
    'visibility' => BLOCK_VISIBILITY_LISTED,
    'weight' => 36,
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  return $blocks;
}

/**
 * Implements hook_block_info_alter().
 */
function wfm_blocks_block_info_alter(&$blocks, $theme, $code_blocks) {
  // Opt-in modules we want to force per role caching.
  $cache_modules = array(
    'block',
  );
  foreach ($blocks as $name => &$module) {
    if (in_array($name, $cache_modules)) {
      foreach ($module as &$block) {
        // Cache per role.
        $block['cache'] = DRUPAL_CACHE_PER_ROLE;
      }
    }
  }
}

/**
 * Implements hook_block_cid_parts_alter().
 */
function wfm_blocks_block_cid_parts_alter(&$cid_parts, $block) {
  // Add the user's store to block cache id.
  if ($store = store_get_user_store()) {
    $cid_parts[] = $store->nid;
  }
}

/**
 * Implements hook_block_configure().
 */
function wfm_blocks_block_configure($delta = '') {
  $form = array();

  switch ($delta) {
    case 'wfm_blocks_search_banner':
      $form['wfm_blocks_search_banner_image'] = array(
        '#name' => 'banner_image',
        '#type' => 'managed_file',
        '#title' => t('Search Banner Image'),
        '#description' => t('Select to override default search banner image - *.gif, *.png, *.jpg, and *.jpeg allowed.'),
        '#default_value' => variable_get('wfm_blocks_search_banner_fid', ''),
        '#upload_location' => 'public://media/Global/Banners/',
        '#upload_validators' => array(
          'file_validate_extensions' => array('gif png jpg jpeg'),
        ),
      );

      $form['wfm_blocks_search_banner_estore_item'] = array(
        '#type' => 'textfield',
        '#title' => t('eStore Category'),
        '#size' => 60,
        '#description' => t('Enter to specify an estore category, if any.'),
        '#default_value' => variable_get('wfm_blocks_search_banner_estore_item', ''),
      );
      break;

    case 'wfm_blocks_subscription':
      $form['subscriptions'] = array(
        '#type' => 'fieldset',
      );
      $form['subscriptions']['description'] = array(
        '#type' => 'textarea',
        '#name' => 'description',
        '#default_value' => variable_get('wfm_blocks_subscription_description'),
        '#title' => 'Description (Input HTML)',
      );
      break;

    case 'wfm_blocks_footer':
      $form['wfm_blocks_footer_body'] = array(
        '#type' => 'textarea',
        '#title' => t('Footer block contents'),
        '#description' => t('Enter HTML to override footer default markup.'),
        '#default_value' => variable_get('wfm_blocks_footer_body'),
      );
      break;

    case 'wfm_blocks_no_search_results':
      $form['wfm_blocks_no_search_results'] = array(
        '#type' => 'text_format',
        '#name' => 'html',
        '#default_value' => variable_get('wfm_blocks_no_search_results'),
        '#title' => t('Enter HTML for no search results page.'),
      );
      break;

    case 'product_lines_sidebar':
      $form['wfm_blocks_product_lines_sidebar'] = array(
        '#type' => 'text_format',
        '#title' => t('Product Lines Sidebar Content'),
        '#default_value' => variable_get('wfm_blocks_product_lines_sidebar'),
      );
      break;
  }
  return $form;
}

/**
 * Implements hook_block_save().
 *
 * This hook declares how the configured options for a block
 * provided by this module are saved.
 */
function wfm_blocks_block_save($delta = '', $edit = array()) {
  if ($delta == 'wfm_blocks_search_banner') {
    variable_set('wfm_blocks_search_banner_estore_item', $edit['wfm_blocks_search_banner_estore_item']);
    if ($edit['wfm_blocks_search_banner_image']['value'] != 0) {
      // If a file has been selected.
      $file = file_load($edit['wfm_blocks_search_banner_image']);
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      $block = block_load('wfm_blocks', $delta);
      file_usage_add($file, 'wfm_blocks', 'block', $block->bid);
      variable_set('wfm_blocks_search_banner_fid', $file->fid);
    }
    else {
      variable_set('wfm_blocks_search_banner_fid', 0);
    };
  }
  elseif ($delta == 'wfm_blocks_subscription') {
    variable_set('wfm_blocks_subscription_description', $edit['description']);
  }
  elseif ($delta == 'wfm_blocks_footer') {
    variable_set('wfm_blocks_footer_body', $edit['wfm_blocks_footer_body']);
  }
  elseif ($delta == 'wfm_blocks_no_search_results') {
    variable_set('wfm_blocks_no_search_results', $edit['wfm_blocks_no_search_results']['value']);
  }
  elseif ($delta === 'product_lines_sidebar') {
    $content = $edit['wfm_blocks_product_lines_sidebar']['value'];
    variable_set('wfm_blocks_product_lines_sidebar', check_markup($content, 'source_editor'));
  }
}

/**
 * Implements wfm_blocks_block_view().
 */
function wfm_blocks_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'wfm_sharelinks':
      $block['content'] = wfm_sharelinks_content();
      break;

    case 'wfm_breadcrumbs':
      $block['content'] = wholefoods_display_breadcrumb();
      break;

    case 'wfm_blocks_vendors':
      $block['content'] = wfm_blocks_vendors_callback();
      break;

    case 'wfm_blocks_search_banner':
      $block['content'] = wfm_blocks_search_banner_contents();
      break;

    case 'wfm_blocks_subscription':
      $block['content'] = array(
        '#markup' => wfm_blocks_subscription_contents(),
        '#attached' => array(
          'js' => array(
            drupal_get_path('module', 'wfm_blocks') . '/wfm_blocks.js' => array(
              'type' => 'file',
              'scope' => 'footer',
            ),
          ),
        ),
      );
      break;

    case 'wfm_blocks_footer':
      $block['subject'] = NULL;
      $block['content'] = array(
        '#title' => ' ',
        '#markup' => wfm_blocks_footer_contents(),
        '#prefix' => " ",
        '#suffix' => " ",
        '#attached' => array(
          'js' => array(
            drupal_get_path('module', 'wfm_blocks') . '/wfm_blocks.js' => array(
              'type' => 'file',
              'scope' => 'footer',
            ),
            drupal_get_path('module', 'wfm_blocks') . '/store-select.js' => array(
              'type' => 'file',
              'scope' => 'header',
            ),
            drupal_get_path('module', 'store') . '/js/store-select-footer.js' => array(
              'type' => 'file',
              'scope' => 'footer',
            ),
          ),
        ),
      );
      break;

    case 'wfm_blocks_oo_header':
      $path = drupal_get_path('module', 'wfm_blocks');
      $block['content'] = array(
        '#markup' => wfm_blocks_online_ordering_header_content(),
      );
      break;

    case 'online_ordering_body':
      $block['content'] = array(
        '#markup' => wfm_blocks_online_ordering_content(),
      );
      break;

    case 'wfm_blocks_no_search_results':
      $content = variable_get('wfm_blocks_no_search_results');
      $block['content'] = array(
        '#markup' => check_markup($content, 'source_editor'),
      );
      break;

    case 'product_lines_sidebar':
      $content = variable_get('wfm_blocks_product_lines_sidebar');
      $block['content'] = array(
        '#markup' => check_markup($content, 'source_editor'),
      );
      break;

    case 'wfm_blocks_coupon_filters':
      $block['content'] = array(
        '#markup' => wfm_blocks_coupon_filters_content(),
        '#attached' => array(
          'js' => array(
            drupal_get_path('module', 'wfm_coupons') . '/js/wfm_coupons.js' => array(
              'scope' => 'footer',
            ),
          ),
        ),
      );
      break;

    case 'wfm_store_social_widgets':
      $block['content'] = wfm_blocks_store_social_widgets();
      break;

    case 'wfm_uk_dept_social_widgets':
      $block['content'] = wfm_blocks_uk_dept_social_widgets();
      break;
  }
  return $block;
}

/**
 * Implements hook_api_tokens_info().
 */
function wfm_blocks_api_tokens_info() {
  // Token: [api:block["module", "delta"]/]
  $tokens['block'] = array(
    'title' => t('WFM Block'),
    'description' => t('Renders wfm_blocks by module name and delta.'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $tokens;
}

/**
 * Defines "block" API Token handler.
 */
function wfm_blocks_apitoken_block($module, $delta) {
  $block = block_load($module, $delta);
  $block = _block_get_renderable_array(_block_render_blocks(array($block)));
  return drupal_render($block);
}

/**
 * Define Site Footer content function.
 *
 * Provides default content and override via Block UI.
 */
function wfm_blocks_footer_contents() {
  $html = '';
  $body = variable_get('wfm_blocks_footer_body');
  if ($body) {
    // Custom override.
    $html .= check_markup($body, 'full_html');
  }
  else {
    // Default content.
    $html .= "<h3 style='color: #ff0000'>wfm_blocks: Editable Footer - Default Content.  Did you run DB UPDATE?</h3>";
  }
  return $html;
}

/**
 * Define Share Links content used on store page.
 */
function wfm_sharelinks_content() {
  $_url = request_path();
  $_get = $_GET;
  unset($_get['q']);
  $options = array(
    'absolute' => TRUE,
    'query' => $_get,
  );
  $url = url($_url, $options);

  $node = node_load(arg(1));
  $pin_img = '';
  if ($node->type == 'recipe') {
    $pin_img = file_create_url($node->field_hero_image[LANGUAGE_NONE][0]['uri']);
    $pin_img_parts = parse_url($pin_img);
    $pin_img = $pin_img_parts['scheme'] . '://www.wholefoodsmarket.com' . $pin_img_parts['path'];
    $pin_descr = strip_tags(field_view_field('node', $node, 'body'));
  }

  $html = <<<EOF
  <div class="social-links clearfix">
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#version=v2.0&xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <div class="recipe-fb">
    <fb:like send="false" layout="button_count" width="450" show_faces="false"></fb:like>
  </div>

  <div class="recipe-twitter">
    <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
  </div>
EOF;

  if ($pin_img) {
    $html .= <<<EOF
  <div class="pinit">
    <a data-pin-config="beside"
       href="//pinterest.com/pin/create/button/?url=<?php print urlencode($url); ?>&media=<?php print urlencode($pin_img); ?>&description=<?php print urlencode($pin_descr); ?>"
       data-pin-do="buttonPin" ><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>
  </div>
EOF;
  }

  return $html;
}

/**
 * Define search banner content function.
 *
 * Dependent upon wfm_marque module function.
 */
function wfm_blocks_search_banner_contents() {
  // Capture the image file path and form into HTML with attributes.
  $image_file = file_load(variable_get('wfm_blocks_search_banner_fid', ''));

  if (isset($image_file->uri)) {
    // If image has been selected.
    $image_path = file_create_url($image_file->uri);
  }
  else {
    // Show default image.
    $image_path = '/' . drupal_get_path('module', 'wfm_blocks') . '/default_searchbanner.png';
  };

  $banner = "<img id='wfm_blocks_search_banner' class='banner search' src='$image_path' alt='Whole Foods banner image'>";
  $item = variable_get('wfm_blocks_search_banner_estore_item', '');
  $type = 'category';
  // @TODO - Chris - Category is hard coded at this time.  Future iterations may permit search terms to be provided to point to specific item
  $markup = wfm_blocks_search_banner_markup($type, $item, $banner);

  return array('#markup' => $markup);
}

/**
 * Utility function modeled after wfm_marque_destination_url, which I couldn't use because it defines a button.
 */
function wfm_blocks_search_banner_markup($estore_item_type = NULL, $estore_item = NULL, $banner = NULL) {
  $estore_base_url = variable_get('wfm_estore_url', 'shop.wholefoodsmarket.com') . "/store/shopAt.aspx";
  $uri = NULL;
  $ref = NULL;

  if (isset($_COOKIE['local_store'])) {
    // Store NID.
    $store = $_COOKIE['local_store'];
  }
  else {
    $store = NULL;
  };

  if (isset($_COOKIE['local_store_id'])) {
    // Store code.
    $store_id = $_COOKIE['local_store_id'];
  }
  else {
    $store_id = NULL;
  };

  switch ($estore_item_type) {
    case 'item':
    case 'Item':
      $destination = "<a href='http://" . $estore_base_url . "?pname=" . $estore_item . "&s=" . $store_id . "' target='top'> " . $banner . "</a>";
      $ref = "pname=" . $estore_item . "&s=" . $store_id;
      break;

    case 'category':
    case 'Category':
      $destination = "<a href='http://" . $estore_base_url . "?cname=" . $estore_item . "&s=" . $store_id . "' target='top'> " . $banner . "</a>";
      $ref = "cname=" . $estore_item . "&s=" . $store_id;
      break;

    default:
      // If category and item have not been defined.
      $destination = "<a href='http://" . $estore_base_url . "?s=" . $store_id . "' target='top'> " . $banner . "</a>";
      $ref = "cname=" . $estore_item . "&s=" . $store_id;
      break;
  }

  if (is_null($store_id) && ($estore_item_type != NULL)) {
    $destination = "<a class='ctools-use-modal' href='/modals/nojs/find_store/$ref' onclick=modredirect='" . json_encode("http://" . $estore_base_url . "?" . $ref) . "'> $banner </a>";
    // @TODO - CHRIS - had trouble getting $ref value from modal location so I created less slick onclick, which is functional. Maybe optimize in future.
  }

  return $destination;
}

/**
 * Subscription block contents.
 */
function wfm_blocks_subscription_contents() {
  $html = '';
  $description = variable_get('wfm_blocks_subscription_description');
  if ($description) {
    $html .= '<div class="form-description">' . $description . '</div>';
  }
  $form = drupal_get_form('wfm_blocks_subsciption_form');
  $html .= render($form);
  $html .= '<div class="error-area"></div>';
  return '<div class="nl-subscription-form">' . $html . '</div>';
}


/**
 * Subscription block form.
 */
function wfm_blocks_subsciption_form() {
  $form = array(
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'wfm_blocks') . '/wfm_blocks.js' => array(
          'type' => 'file',
          'scope' => 'footer',
        ),
      ),
    ),
    'subscription_email' => array(
      '#type' => 'textfield',
      '#attributes' => array(
        'class' => array('email'),
        'placeholder' => t('Email Address...'),
      ),
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    ),
  );
  return $form;
}

/**
 * Validation handler for subscription form.
 */
function wfm_blocks_subsciption_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['subscription_email'])) {
    form_set_error('subscription_email', t('Please, enter a valid email address'));
  }
}

/**
 * Submit handler for subscription form.
 */
function wfm_blocks_subsciption_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['subscription_email'])) {
    unset($_SESSION['newsletter_email']);
    $_SESSION['newsletter_email'] = $form_state['values']['subscription_email'];
  }
  drupal_goto('newsletters');
}

/**
 * Callback function for wfm_blocks_vendors().
 *
 * @return string
 *   Rendered HTML for select form on vendor/LPLP profile pages.
 */
function wfm_blocks_vendors_callback() {
  $form = drupal_get_form('wfm_blocks_vendor_select_form');
  $html = wholefoods_display_breadcrumb();
  $html .= render($form);
  return $html;
}

/**
 * Creates select form for vendor/LPLP pages.
 *
 * @return array
 *   Renderable array for form
 */
function wfm_blocks_vendor_select_form() {
  $lplp = FALSE;
  $path = current_path();
  $path_alias = drupal_lookup_path('alias', $path);
  if (strpos($path_alias, 'profiles-loan-recipients') === 0) {
    $lplp = TRUE;
  }
  $form['header'] = array(
    '#markup' => '<h2>' . wfm_blocks_get_title() . '</h2>',
  );
  $form['select_state'] = array(
    '#type' => 'select',
    '#title' => t('View Profiles By Location'),
    '#options' => wfm_blocks_get_vendor_select_options($lplp),
    '#attributes' => array(
      'id' => 'vendor-select-form',
    ),
  );
  $form['path'] = array(
    '#type' => 'hidden',
    '#value' => $path_alias,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#attributes' => array(
      'id' => 'vendor-form-submit',
    ),
  );
  $form['#submit'] = array('wfm_blocks_vendor_select_form_submit');
  return $form;
}

/**
 * Submit handler for vendor_select_form.
 */
function wfm_blocks_vendor_select_form_submit($form, $form_state) {
  $path = $form_state['input']['path'];
  $state = $form_state['input']['select_state'];
  if (strpos($path, 'profiles-loan-recipients') === 0) {
    $redirect_path = 'profiles-loan-recipients/' . $state;
  }
  if (strpos($path, 'local-vendor-profiles') === 0 || strpos($path, 'locally-grown') === 0) {
    $redirect_path = 'local-vendor-profiles/' . $state;
  }
  if (isset($redirect_path) && strlen($state) == 2) {
    drupal_goto($redirect_path);
  }
  else {
    drupal_set_message(t('Please select a location.'), 'warning');
  }
}

/**
 * Creates array to be used for select options.
 *
 * @param bool $lplp
 *   Distinguished whether to render LPLP or vendor form.
 *
 * @return array
 *   Array to be used for select options
 */
function wfm_blocks_get_vendor_select_options($lplp = FALSE) {
  $options = array();
  $statenames = _local_get_state_list(TRUE);
  $query = db_select('field_data_field_postal_address', 'a');
  $query->leftJoin('node', 'n', 'n.nid = a.entity_id');
  if ($lplp = TRUE) {
    // If vendor has an LPLP tag.
    $query->leftJoin('field_data_field_lplp_recipient', 'lplp', 'lplp.entity_id = a.entity_id');
    $query->condition('lplp.field_lplp_recipient_value', 1, '=');
  }
  $query
    ->fields('a', array('field_postal_address_administrative_area',
      'field_postal_address_country'))
    ->condition('a.bundle', 'local_vendor')
    ->condition('n.status', 1)
    ->distinct()
    ->orderBy('a.field_postal_address_country', 'ASC')
    ->orderBy('a.field_postal_address_administrative_area', 'ASC');
  $result = $query->execute();

  $countries = array(
    // Annoyingly, we have both CA and CN in the database for Canada.
    'CA' => 'Canada',
    'CN' => 'Canada',
    'UK' => 'United Kingdom',
    'US' => 'United States'
  );
  foreach ($result as $row) {
    $state = $row->field_postal_address_administrative_area;
    $country = $row->field_postal_address_country;
    if ($country == 'GB') {
      $country = 'UK';
      $state = 'UK';
    }
    if ($country !== '' && $state !== '') {
      $options[$countries[$country]][$state] = $statenames[$state];
    }
  }
  ksort($options);
  foreach ($options as $k => $v) {
    if (is_array($options[$k])) {
      asort($options[$k]);
    }
  }
  $default = array(0 => t('Select a Location'));
  $out_array = array_merge($default, $options);
  return $out_array;
}

/**
 * Creates page title.
 *
 * @return string
 *   HTML for page title.
 */
function wfm_blocks_get_title() {
  $node = menu_get_object();
  return $node->title;
}

/**
 * Renders HTML for /online-ordering header block.
 *
 * @return string
 *   rendered html
 */
function wfm_blocks_online_ordering_header_content() {
  $form = drupal_get_form('online_ordering_header_form');
  $title = wfm_blocks_get_title();
  $blurb = wfm_blocks_online_ordering_header_blurb();
  // Renderable array.
  $ra_oo_header = array(
    'header' => array(
      '#markup' => '<h2>' . $title . '</h2>',
    ),
    'blurb' => array(
      '#markup' => '<p class="oo-summary">' . $blurb . '</p>',
    ),
    'store_state_fields' => array(
      '#type' => 'container',
      'selects' => $form,
      '#attributes' => array(
        'class' => array('store-select-wrapper'),
      ),
    ),
  );
  return render($ra_oo_header);
}

/**
 * Checks if a users is in the UK. If not it returns body content.
 *
 * @return string
 *   HTML body content from body field.
 */
function wfm_blocks_online_ordering_content() {
  $content = '';
  $node = menu_get_object();
  if (isset($node)) {
    $language = $node->language;
    $users_global_node = wholefoods_get_global_node();
    if ($users_global_node->nid !== WFMVariables::get(WFMVariables::UK_OFFICE)) {
      $content = check_markup($node->body[$language][0]['value'], 'source_editor');
    }
  }
  return $content;
}

/**
 * Get content for /online-ordering header blurb.
 *
 * @return string
 *   text from node summary field
 */
function wfm_blocks_online_ordering_header_blurb() {
  $node = menu_get_object();
  $language = $node->language;
  if ($node->body[$language][0]['summary']) {
    return check_plain($node->body[$language][0]['summary']);
  }
}

/**
 * Form submit callback function.
 *
 * Redirect to location on shop.wholefoodsmarket.com based on submitted value.
 */
function online_ordering_header_submit_callback($form, &$form_state) {
  header('Cache-Control: private, no-cache, no-store, must-revalidate, max-age=0');
  header('Pragma: no-cache');
  // Disable server-side caching.
  drupal_page_is_cacheable(FALSE);

  $storenid = $form_state['input']['store'];
  $tlc = store_get_tlc_by_nid($storenid);
  $url = 'shop/' . $tlc;
  drupal_goto($url);
}

/**
 * Renders HTML for /coupons header block.
 *
 * @return string
 *   rendered html
 */
function wfm_blocks_coupon_filters_content() {
  if (module_exists('wfm_template_text')) {
    $submit_label = wfm_template_text_get_text('wfm_template_text_sales_coupons_store_select_submit');
  }
  else {
    $submit_label = t('View Sales & Coupons');
  }
  $form = drupal_get_form('coupons_store_select_form');
  $form['submit']['#value'] = $submit_label;
  $rendered_form = render($form);

  // Renderable array.
  $ra_filter = array(
    'header' => array(
      '#markup' => $rendered_form,
    ),
  );
  return render($ra_filter);
}

/**
 * Form submit callback function.
 *
 * Redirect to location on coupons with contextual filter arg.
 *
 * @see wfm_blocks_form_alter
 */
function coupons_store_submit_callback($form, &$form_state) {
  $storenid = $form_state['input']['store'];
  $user_store = store_get_user_store();
  global $theme_key;

  // Conditionally set user store.
  if (!$user_store || $user_store->type !== 'store') {
    // It could be national office.
    _store_set_user_store($storenid);
  }

  // Determine redirect URL.
  if (is_numeric($storenid)) {
    $storename = store_get_storename_from_nid($storenid);
    $url = trim(request_uri() . '/' . $storename, '/');

    if ($theme_key == 'wholefoods') {
      // If form submission is from the /coupons page.
      // A return of 0 could equal false.
      if (strpos(request_uri(), 'coupons') !== FALSE) {
        $url = 'coupons/' . $storename;
      }

      // If form submission is from the /sales-flyer page.
      elseif (strpos(request_uri(), 'sales-flyer') !== FALSE) {
        $url = 'sales-flyer/' . $storename;
      }
    }

    // Do the redirect.
    if ($url) {
      $form_state['redirect'] = $url;
    }
  }
  else {
    drupal_set_message(t('Please select a store to view coupons.'), 'error');
  }
}

/**
 * Renders HTML for right sidebar with Store Social Widgets.
 *
 * @return string
 *   rendered html
 */
function wfm_blocks_store_social_widgets() {
  if (arg(0) == 'node' && is_numeric(arg(1)) && !arg(2)) {
    $node = node_load(arg(1));
    if ($node->type == "store") {
      $twitter_id = (isset($node->field_store_twitter[LANGUAGE_NONE][0]['value']) && $node->field_store_twitter[LANGUAGE_NONE][0]['value'] != '') ? trim($node->field_store_twitter[LANGUAGE_NONE][0]['value']) : '';
      $fb_id = '';
      if (isset($node->field_facebook_url[LANGUAGE_NONE][0]['url']) && $node->field_facebook_url[LANGUAGE_NONE][0]['url'] != '') {
        $fb_id = trim($node->field_facebook_url[LANGUAGE_NONE][0]['url']);
        if (substr($fb_id, -1) == '/') {
          // Remove trailing slash if present.
          $fb_id = substr($fb_id, 0, strlen($fb_id) - 1);
        }
        $fb_id = substr($fb_id, strrpos($fb_id, '/') + 1);
      }
      if ($fb_id != '') {
        $html = <<<HTML
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like-box" data-href="http://www.facebook.com/$fb_id" data-width="252" data-show-faces="true" data-stream="true" data-header="true" style="margin-left:-20px;"></div>
HTML;
      }
      if ($twitter_id != '') {
        if (class_exists('WFMTwitter')) {
          $tweets_html = '';
          $twitter = new WFMTwitter();
          $posts = $twitter->getPosts(3, $node, TRUE);
          if ($posts) {
            foreach ($posts as $tweet) {
              $tweets_html .= '<div class="tweet-content">' . $tweet->text . '</div>';
            }
            $twitter_html = <<<HTML
              <style type="text/css">
                #store_twitter_title {
                  opacity: 0.75;
                  background: url("/sites/all/themes/wholefoods/images/icons/icon-twitter-bird.png") no-repeat scroll 0 5px transparent;
                  color: #EC7A37;
                  font-family: 'WFMSketchCartonBold' !important;
                  font-size: 36px !important;
                  height: 41px;
                  margin: 20px 0 10px 0;
                  padding: 0 0 5px 52px !important;
                  border:none !important;
                }
                #store_twitter_widget div {
                  padding:10px 0 15px 0;
                  border-top:1px #999 solid;
                }
              </style>
              <div id="store_twitter_widget">
              <div id="store_twitter_title">TWEETS</div>
                $tweets_html
              </div>
              <a href="https://twitter.com/$twitter_id" class="twitter-follow-button" data-show-count="false" data-size="large" data-dnt="true">Follow @$twitter_id</a>
              <script>!function(d,s,id){
                var js,fjs=d.getElementsByTagName(s)[0];
                if(!d.getElementById(id)){
                  js=d.createElement(s);
                  js.id=id;
                  js.src="//platform.twitter.com/widgets.js";
                  fjs.parentNode.insertBefore(js,fjs);
                }
              }(document,"script","twitter-wjs");</script>
HTML;
            $html .= $twitter_html;
          }
        }
      }
    }
  }
  return $html;
}

/**
 * Renders HTML for right sidebar with social widgets on UK department articles.
 *
 * @return string
 *   rendered html
 */
function wfm_blocks_uk_dept_social_widgets() {
  $node = menu_get_object();
  $fb_id = 'WholeFoodsLondon';
  $twitter_id = 'WFMLondon';
  $html = <<<HTML
  <style type="text/css">
    #store_facebook_title {
      background: url("/sites/all/themes/wholefoods/images/social-media/social-sprite.png") 0 -430px no-repeat;
      color: #3b5998;
      font-family: "Serifa Bold","Lucida Sans","Lucida Grande",sans-serif;
      font-size: 28px;
      height: 31px;
      margin: 20px 0 10px 0;
      padding: 10px 0 5px 52px;
    }
    #store_twitter_widget div {
      padding: 10px 0 15px 0;
    }

    div#store_twitter_title {
      background: url("/sites/all/themes/wholefoods/images/icons/icon-twitter-bird.png") no-repeat scroll 0 5px transparent;
      color: #2daae1;
      font-family: "Serifa Bold","Lucida Sans","Lucida Grande",sans-serif;
      font-size: 28px;
      height: 31px;
      margin: 20px 0 10px 0;
      padding: 10px 0 5px 52px;
    }
  </style>
  <div id="store_facebook_title">FACEBOOK</div>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
  <div class="fb-like-box" data-href="http://www.facebook.com/${fb_id}" data-width="252" data-show-faces="true" data-stream="true" data-header="true" style="margin-left:-20px;"></div>

HTML;

  if (class_exists('WFMTwitter')) {
    $tweets_html = '';
    $twitter = new WFMTwitter();
    $posts = $twitter->getPosts(5, NULL, TRUE);
    if ($posts) {
      foreach ($posts as $tweet) {
        $tweets_html .= '<div class="tweet-content">' . $tweet->text . '</div>';
      }
      $twitter_html = <<<HTML
  <div id="store_twitter_widget">
    <div id="store_twitter_title">TWEETS</div>
    $tweets_html
  </div>
  <a href="https://twitter.com/$twitter_id" class="twitter-follow-button" data-show-count="false" data-size="large" data-dnt="true">Follow @$twitter_id</a>
  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
HTML;
      $html .= $twitter_html;
    }
  }

  return $html;
}
