<?php

/**
 * @file
 * Code for the wfm_blocks forms
 */

/**
 * Form constructor for store select form.
 *
 * @param array $form
 *   Form array
 * @param array $form_state
 *   Form state array
 * @param array $callback
 *   Array of submit callback functions.
 * @param array $javascript
 *   Array of javascript files to attach.
 */
function store_select_form($form, &$form_state, $callback = array(), $javascript = array()) {
  global $user;
  $scripts = array(
    drupal_get_path('module', 'wfm_blocks') . '/store-select.js',
    drupal_get_path('module', 'wfm_gtm') . '/js/gtm-store-select.js',
  );
  $attached_scripts = array_merge($scripts, $javascript);
  $defaults = store_select_get_user_defaults($user);
  $form['state'] = array(
    '#type' => 'select',
    '#title' => t('Select Your Location'),
    '#options' => _wfm_blocks_get_state_options(),
    '#default_value' => $defaults['state'],
    '#attributes' => array(
      'class' => array('state-select'),
    ),
  );
  $form['store'] = array(
    '#type' => 'select',
    '#title' => t('Select Your Store'),
    '#options' => _wfm_blocks_get_store_options(),
    '#default_value' => $defaults['store'],
    '#attributes' => array(
      'class' => array('store-select'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#attributes' => array(
      'id' => 'store-select-submit',
      'class' => array('store-select-submit'),
    ),
  );
  $form['#attached']['js'] = $attached_scripts;
  $form['#attributes'] = array(
    'class' => array('store-select-form'),
  );

  if (count($callback) > 0) {
    $form['#submit'] = $callback;
  }
  return $form;
}

/**
 * Get default values for a logged-in user.
 *
 * @param object $user
 *   Drupal user object
 *
 * $return array
 *   $array(
 *     'state' => 'string',
 *     'store' => 'string'
 *   )
 */
function store_select_get_user_defaults($user) {
  $defaults = array();
  $defaults['state'] = NULL;
  $defaults['store'] = NULL;
  if ($user->uid > 0) {
    if ($store = store_get_user_store()) {
      $language = $store->language;
      $defaults['state'] = $store->field_postal_address[$language][0]['administrative_area'];
      $defaults['store'] = $store->nid;
    }
  }
  return $defaults;
}

/**
 * Function generate select options for state select.
 *
 * @return array
 *   Form option values to be merged with form API array
 */
function _wfm_blocks_get_state_options() {
  $cache = cache_get('wfm_state_select_options', 'cache');
  if ($cache) {
    $state_options = $cache->data;
  }
  else {
    $state_options = array('0' => 'Please Select Your Location');
    $statenames = _local_get_state_list(TRUE);

    $statequery = db_select('field_data_field_postal_address', 'a');
    $statequery
      ->fields('a', array('field_postal_address_country', 'field_postal_address_administrative_area'))
      ->condition('a.bundle', 'store')
      ->distinct()
      ->orderBy('a.field_postal_address_country', 'ASC')
      ->orderBy('a.field_postal_address_administrative_area', 'ASC');
    $stateresult = $statequery->execute();

    $countries = array(
      'CA' => 'Canada',
      'UK' => 'United Kingdom',
      'US' => 'United States'
    );
    foreach ($stateresult as $row) {
      $country = $row->field_postal_address_country;
      $state = $row->field_postal_address_administrative_area;
      if ($country == 'GB') {
        $country = 'UK';
        $state = 'UK';
      }
      $state_options[$countries[$country]][$state] = $statenames[$state];
    }
    ksort($state_options);
    asort($state_options['United States']);
    cache_set('wfm_state_select_options', $state_options, 'cache');
  }
  return $state_options;
}

/**
 * Function generate select options for store select.
 *
 * @param array $query_options
 *   Array of options to modify query. Valid options are:
 *   $options['stores_in_development'] = TRUE, Set TRUE to include stores in development in results
 *
 * @return array
 *   Form option values to be merged with form API array
 */
function _wfm_blocks_get_store_options($query_options = array()) {
  $default_options = array(
    'stores_in_development' => FALSE,
  );
  // Build options from array passes to function and defaults.
  $query_options = array_merge($default_options, $query_options);
  $cache_id = _wfm_blocks_store_options_cache_name($query_options);
  $cache = cache_get($cache_id, 'cache');
  if ($cache) {
    $store_options = $cache->data;
  }
  else {
    $store_options = array('' => 'Please Select a Store');
    $statenames = _local_get_state_list(TRUE);

    $query = db_select('node', 'n');

    $query->leftJoin('field_data_field_postal_address', 'a', 'a.entity_id = n.nid');
    $query->leftJoin('field_data_field_opening_soon', 'o', 'o.entity_id = n.nid');
    $query
      ->condition('n.type', 'store')
      ->condition('n.status', '1')
      ->fields('n', array('title', 'nid'))
      ->fields('a', array(
        'field_postal_address_administrative_area',
        'field_postal_address_country',
        'field_postal_address_locality',
        'field_postal_address_thoroughfare',
      ))
      ->orderBy('title', 'ASC');
    // Iterate over query options to adjust query as needed.
    foreach ($query_options as $key => $value) {
      switch ($key) {
        case 'stores_in_development':
          // Adjust query condition depending on if we want stores in development or not.
          if ($value === TRUE) {
            $query->condition('o.field_opening_soon_value', '0', '>=');
          }
          else {
            $query->condition('o.field_opening_soon_value', '0');
          }
          break;
      }
    }
    $result = $query->execute();

    foreach ($result as $row) {
      $country = $row->field_postal_address_country;
      $state = $row->field_postal_address_administrative_area;
      $name = $row->title;
      $nid = $row->nid;
      $city = $row->field_postal_address_locality;
      $street = $row->field_postal_address_thoroughfare;
      if ($country == 'GB') {
        $country = 'UK';
        $state = 'UK';
      }
      $store_options[$state . $city][$nid] = $name . ' - ' . $street;
    }
    ksort($store_options);
    cache_set($cache_id, $store_options, 'cache');
  }
  return $store_options;
}

/**
 * Build cache id name from query options.
 *
 * @param array $options
 *   $options = array (
 *    'key' => 'value',
 *    'key2' => 'value2',
 *   );
 *
 * @return string
 *   Cache id name
 *
 * @see _wfm_blocks_get_store_options()
 */
function _wfm_blocks_store_options_cache_name($options) {
  $id = 'wfm_store_select_options';
  $mapped_array = array_map('_wfm_blocks_stringify_array', array_keys($options), $options);
  foreach ($mapped_array as $line) {
    $id .= '|' . $line;
  }
  return $id;
}

/**
 * Callback function for array_map.
 *
 * @param string $array_key
 *   Array Key.
 * @param string||bool||int $array_value
 *   Array Value.
 *
 * @return string
 *   Key=>value pair of an array as a string separated by a "|"
 */
function _wfm_blocks_stringify_array($array_key, $array_value) {
  return $array_key . '|' . trim(var_export($array_value, TRUE), '\'');
}

/**
 * Form constructor function for the /online-ordering page.
 */
function online_ordering_header_form($form, &$form_state) {
  $callbacks = array('store_make_users_store_submit', 'online_ordering_header_submit_callback');
  $null = array();
  $form = store_select_form($null, $null, $callbacks);
  return $form;
}

/**
 * Form constructor for /coupons page.
 */
function coupons_store_select_form($form, &$form_state) {
  $null = array();
  $callbacks = array('coupons_store_submit_callback');
  $base_form = store_select_form($null, $null, $callbacks);
  return $base_form;
}
