/**
 * @file
 * Javascript file for holidays2013.
 */
(function( $ ){
  $.fn.WFMholidays = function(options) {
    // Some default selectors
    var holidaynav = {
      options: $.extend({
        'navToggleSelector': 'a.togglenav',
        'navSelector': 'ul.topmenu',
      }, options),

      /**
      * Add Listeners
      */
      addListeners: function() {
        $('body').delegate(this.options.navToggleSelector, 'click', function() {
          holidaynav.toggleNav();
          return false;
        });
        if (holidaynav.ismobile()) {
          holidaynav.addMobileLinks();
          $('body').delegate('.expandable', 'click', function() {
            holidaynav.mobileSubMenuExpand(this);
            return false;
          });
        }
      },

      /**
      * Adds expand link to mobile navigation.
      */
      addMobileLinks: function() {
        var expandlink = holidaynav.options.navSelector + ' h3';
        var linkhtml = '<a class="expandable" href="#">' + Drupal.t('Expand') + '</a>';
        $(expandlink).append(linkhtml);
      },
      
      /**
      * Hackish fix to force a page redraw to fix a Safari rendering issue.
      */
      forceRedraw: function() {
        /**
         * Method for repaint appears ineffective:
         * content = document.getElementById('content');
         * var redraw = content.offsetHeight;
         */
        $('#content').css('background', '#fffffe');
        setTimeout(function() {
          $('#content').css('background', '#ffffff');
        }, 100);
      },

      /**
      * Get everything ready.
      */
      init: function() {
        this.addListeners();
        if(!holidaynav.ismobile()) {
          holidaynav.forceRedraw();
        }
      },

      /**
      * Determine if we're on the mobile site.
      */
      ismobile: function() {
        if (Drupal.settings.ajaxPageState.theme === 'wholefoods_mobile') {
          return true;
        } 
        else {
          return false;
        }
      },

      /**
      * Mobile menu toggle.
      */      
      mobileSubMenuExpand: function(a) {
        //$(a).parent('h3').next('ul').toggleClass('expanded');
        $(a).parent('h3').next('ul').slideToggle(400, 'swing', function(a) {
          $(this).prev('h3').children('.expandable').toggleClass('expanded');
        });
      },

      /**
      * Desktop menu toggle.
      */        
      toggleNav: function() {
        $(this.options.navSelector).slideToggle(400, 'swing', function() {
          $(holidaynav.options.navToggleSelector).toggleClass('exposed');
        });
      },
    };

    var holidayMenus = {
      options: $.extend({
        'hoverSelector': '.trigger',
        'targetSelector': '.recipe-image-replace',
      }, options),

      /**
      * Add Listeners
      */
      addListeners: function() {
        $(holidayMenus.options.hoverSelector).delegate('a', 'mouseover', function() {
          holidayMenus.switchImage(this);
          return false;
        });
      },

      /**
      * Get everything ready.
      */
      init: function() {
        holidayMenus.addListeners();
        holidayMenus.preloadImages();
      },

      /**
      * Preload images for holiday menus page.
      */    
      preloadImages: function() {
        $(holidayMenus.options.hoverSelector + ' a').each(function(i) {
          new Image().src = $(this).attr('data-image');
        });
      },

      /**
      * Switch image src on hover.
      */       
      switchImage: function(a) {
        var href = $(a).attr('href');
        var image = $(a).attr('data-image');
        var title = $(a).text();
        var imgdiv = $(a).parents('li.clearfix').find(holidayMenus.options.targetSelector);
        $(imgdiv).find('a').attr('href', href);
        $(imgdiv).find('img').attr('alt', title);
        $(imgdiv).find('img').attr('src', image);
      }
    };

    var servingsCalc = {
      // Some default values
      options: $.extend({
        'tableSelector': '#holiday_calc',
        'targetSelector': 'span.target',
        'doneTypingInterval': 300,
      }, options),

      /**
      * Add Listeners
      */            
      addListeners: function() {
        $(servingsCalc.options.tableSelector).delegate('input', 'focus', function(event) {
          $(this).val('');
          servingsCalc.timeout();
        });  
        $(servingsCalc.options.tableSelector).delegate('input', 'change', function(event) {
          servingsCalc.timeout();
        });
        $(servingsCalc.options.tableSelector).delegate('input', 'keypress', function(event) {
          if (servingsCalc.validateNumber(event)) {
            servingsCalc.timeout();
          }
          else {
            $(this).val('');
            alert(Drupal.t('Please enter numbers only.'));
          }
        });
      },

      /**
      * Do calculations on user input
      */
      calculate: function() {
        $(servingsCalc.options.tableSelector + ' input').each(function(index) {
          if ($(this).val() !== '0' || $(this).val() !== '') {
            var entered = $(this).val();
            var multiplier = $(this).attr('data-multiplier');
            var total = entered * multiplier;
            servingsCalc.setValue(total, this);
          } else {
            servingsCalc.setValue(0, this);
          }
        });
      },

      /**
      * Get everything ready
      */
      init: function() {
        if ($(servingsCalc.options.tableSelector).length > 0) {
          servingsCalc.addListeners();
        }
      },

      /**
      * Set calculated total
      */
      setValue: function(total, input) {
        $(input).parents().siblings('td').last().find(servingsCalc.options.targetSelector).text(total);
      },

      /**
      * First, clear timeout.  Then set new timout counter
      */
      timeout: function() {
        clearTimeout(servingsCalc.typingTimer);
        servingsCalc.typingTimer = setTimeout(servingsCalc.calculate, servingsCalc.options.doneTypingInterval);
      },

      /**
      * Make sure a number is typed.
      */
      validateNumber: function(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39) {
          return true;
        }
        else if ( key < 48 || key > 57 ) {
          return false;
        }
        else return true;
      },
    }     

    //Kick it off.
    holidaynav.init();
    holidayMenus.init();
    servingsCalc.init();
  };
})( jQuery );

jQuery(window).load(function(){
  jQuery('body').WFMholidays();
});
