<?php
/**
* @file
* WFM Static module file.
*/

include_once('wfm_static_holiday_html.inc');

/**
 * Implements hook_menu().
 */
function wfm_static_menu() {
  $items = array();

  $items['site-map'] = array(
    'title' => 'Site Map',
    'page callback' => 'wfm_static_site_map_page',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Outputs the site-map page
 */
function wfm_static_site_map_page() {

  function sitemap_read_array($array) {
    $out = '';
    foreach ($array as $menu_tree) {
      $out .= sitemap_render_menu($menu_tree);
    }
    return $out;
  }

  function sitemap_render_menu($menu) {
    $output = '<ul class="sitemap">';
    foreach ($menu as $item) {
      $link = $item['link'];
      if ($link['hidden']) {
        continue;
      }
      $id = str_replace('/', '', $link['link_path']);
      $output .= '<li id="' . $id . '"><a href="' . check_url(url($link['href'], $link['options']))
          . '">' . $link['title'] . '</a></li>';
      if ($item['below'] && $link['depth'] <= 4) {
        $output .= sitemap_render_menu($item['below']);
      }
    }
    $output .= '</ul>';
    return $output;
  }

  drupal_add_css('.view-empty{display:none}ul.sitemap{margin:0}', array('type' => 'inline'));
  $menu_arrays = array();
  $menus = array(
    0 => 'main-menu',
    1 => 'menu-footer',
    2 => 'menu-footer-2',
    3 => 'menu-footer-3',
  );
  foreach ($menus as $menu) {
    $menu_arrays[] = menu_tree_all_data($menu);
  }
  $content = '<h2>' . t('Site Map') . '</h2>' . sitemap_read_array($menu_arrays) . '<p>&nbsp;</p>';
  return $content;
}

/**
* Implements hook_preprocess_html().
* Add classes to <body> tag based on metatag keywords.
*
* Previous version used hook_node_view() to make keywords accessible to
* this function to avoid a node_load().  However, this hook is not available
* on mobile due to Panels so this node_load() is necessary to work for
* both themes.
*/
function wfm_static_preprocess_html(&$vars) {
  if ($vars['page']['#type'] == 'page') {
    $node = node_load(arg(1));
    if (isset($node->metatags['keywords'])) {
      $keywordstring = $node->metatags['keywords']['value'];
      $tags = explode(',', $keywordstring);
      foreach ($tags as $tag) {
        $vars['classes_array'][] = drupal_html_class(trim($tag));
      }
    }
  }
}

/**
 * Implements hook_api_tokens_info().
 */
function wfm_static_api_tokens_info() {
  $tokens = array();

  $tokens['holiday_nav'] = array(
    'title' => t('Holidays Navigation'),
    'description' => t('Renders holiday nav menu.'),
    'handler' => 'wfm_static_apitoken_holiday_nav',
    //No cache because it causes drupal_add_js not to function
  );
  $tokens['holiday_footer'] = array(
    'title' => t('Holidays Footer'),
    'description' => t('Renders holiday footer.'),
    'handler' => 'wfm_static_apitoken_holiday_footer',
  );
  $tokens['holiday_recipe_nav'] = array(
    'title' => t('Holidays Recipe Nav'),
    'description' => t('Renders holiday recipe nav.'),
    'handler' => 'wfm_static_apitoken_recipe_holiday_nav',
  );
  $tokens['holiday_recipe_header'] = array(
    'title' => t('Holidays Recipe Header'),
    'description' => t('Renders holiday recipe header.'),
    'handler' => 'wfm_static_apitoken_recipe_holiday_header',
  );
  $tokens['holiday_gifts_header'] = array(
    'title' => t('Holidays Gifts Header'),
    'description' => t('Renders holiday gifts header.'),
    'handler' => 'wfm_static_apitoken_gifts_holiday_nav',
  );
  return $tokens;
}

/**
 * Defines Holiday nav token handler.
 */
function wfm_static_apitoken_holiday_nav() {
  $path = drupal_get_path('module', 'wfm_static');
  drupal_add_js($path . '/js/holidays2013.js', 'file');
  $html = _wfm_static_holiday_nav_html();
  return $html;
}

/**
 * Defines Holiday footer token handler.
 */
function wfm_static_apitoken_holiday_footer() {
  $html = _wfm_static_holiday_footer_html();
  return $html;
}

/**
 * Defines Holiday recipe nav token handler.
 */
function wfm_static_apitoken_recipe_holiday_nav() {
  $html = _wfm_static_holiday_recipe_nav_html();
  return $html;
}

/**
 * Defines Holiday recipe nav token handler.
 */
function wfm_static_apitoken_recipe_holiday_header() {
  $html = _wfm_static_holiday_recipe_header_html();
  return $html;
}

/**
 * Defines Holiday gifts nav token handler.
 */
function wfm_static_apitoken_gifts_holiday_nav() {
  $html = _wfm_static_holiday_gift_header_html();
  return $html;
}
