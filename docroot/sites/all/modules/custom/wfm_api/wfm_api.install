<?php

/**
 * @file
 * Install, update and uninstall functions for the WFM API module.
 */

/**
 * Implements hook_schema
 */
function wfm_api_schema() {
  $schemas['cache_wfm_api'] = drupal_get_schema_unprocessed('system', 'cache');
  $schemas['cache_wfm_api_sales_flyers'] = drupal_get_schema_unprocessed('system', 'cache');
  $schemas['cache_wfm_api_shopping_lists'] = drupal_get_schema_unprocessed('system', 'cache');
  $schemas['cache_wfm_api_recipe_lists'] = drupal_get_schema_unprocessed('system', 'cache');
  return $schemas;
}

/**
 * Implements hook_update_dependencies().
 *
 * Creates update hook dependencies for SAGE v2 / Recipe migrate deploy.
 */
function wfm_api_update_dependencies() {
  $dependencies = array();
  $migrate_updates = array(
    'migrate' => 7202,
    'migrate' => 7203,
    'migrate' => 7204,
    'migrate' => 7205,
    'migrate' => 7206,
    'migrate' => 7207
  );

  $dependencies['migrate']['7202'] = array(
    'core' => 7095,
    'core' => 7096
  );
  $dependencies['wfm_api']['7001'] = $migrate_updates;
  $dependencies['wfm_api']['7002'] = $migrate_updates;
  $dependencies['wfm_api']['7003'] = $migrate_updates;
  return $dependencies;
}

/**
 * Implements hook_update_n. Creates cache bins.
 */
function wfm_api_update_7001() {
  // Enable module.
  module_enable(array('composer_manager', 'wfm_migrate'));

  // Register migration.
  migrate_static_registration(array('recipev2'));
  drush_migrate_deregister_migration('recipes');
}

/**
 * Setup for recipev2 migration.
 */
function wfm_api_update_7002() {
  // Disable & uninstall old migration module.
  if (drupal_is_cli()) {
    echo "Disabling & uninstalling old wfm migration module.\n";
    if (module_exists('wfm')) {
      module_disable(array('wfm'));
      drupal_uninstall_modules(array('wfm'));
    }
  }

  // Delete unused tables.
  if (drupal_is_cli()) {
    echo "Removing old migration tables.\n";
    db_drop_table('migrate_map_flyer');
    db_drop_table('migrate_map_johnmackeysblogattachment');
    db_drop_table('migrate_map_johnmackeysblogauthor');
    db_drop_table('migrate_map_johnmackeysblogblogentry');
    db_drop_table('migrate_map_johnmackeysblogcategory');
    db_drop_table('migrate_map_johnmackeysblogcomment');
    db_drop_table('migrate_map_johnmackeysblogpage');
    db_drop_table('migrate_map_playlist');
    db_drop_table('migrate_map_recipe');
    db_drop_table('migrate_map_stores');
    db_drop_table('migrate_map_video');
    db_drop_table('migrate_map_wholestoryattachment');
    db_drop_table('migrate_map_wholestoryauthor');
    db_drop_table('migrate_map_wholestoryblogentry');
    db_drop_table('migrate_map_wholestorycategory');
    db_drop_table('migrate_map_wholestorycomment');
    db_drop_table('migrate_map_wholestorypage');
    db_drop_table('migrate_message_flyer');
    db_drop_table('migrate_message_johnmackeysblogattachment');
    db_drop_table('migrate_message_johnmackeysblogauthor');
    db_drop_table('migrate_message_johnmackeysblogblogentry');
    db_drop_table('migrate_message_johnmackeysblogcategory');
    db_drop_table('migrate_message_johnmackeysblogcomment');
    db_drop_table('migrate_message_johnmackeysblogpage');
    db_drop_table('migrate_message_playlist');
    db_drop_table('migrate_message_recipe');
    db_drop_table('migrate_message_stores');
    db_drop_table('migrate_message_video');
    db_drop_table('migrate_message_wholestoryattachment');
    db_drop_table('migrate_message_wholestoryauthor');
    db_drop_table('migrate_message_wholestoryblogentry');
    db_drop_table('migrate_message_wholestorycategory');
    db_drop_table('migrate_message_wholestorycomment');
    db_drop_table('migrate_message_wholestorypage');
  }

  if (drupal_is_cli()) {
    echo "Enabling modules: wfm_migrate & composer_manager.\n";
  }
  module_enable(array('wfm_migrate', 'composer_manager'), TRUE);

  // Register our new migration.
  if (drupal_is_cli()) {
    echo "Registering RecipeV2.\n";
  }
  // Migration::registerMigration('RecipeV2Migration', 'recipev2');
  migrate_static_registration();
}

/**
 * Update variables table with new API keys.
 */
function wfm_api_update_7003() {
  $configs = array(
    'wfmapi_host' => 'api.wholefoodsmarket.com',
    'wfmapi_key' => 'uZCNDfRt4tdRARzgKN92OPp1FYZXCNMp',
    'wfmapi_secret' => 'd9SQCxgysBTHTh6Z',
    'wfmapi_version' => 2,
  );

  foreach ($configs as $key => $value) {
    variable_set('wfm_variable__' . $key, $value);
  }
  variable_del('wfm_variable__wfmpai_version');
}

/**
 * Delete recipe nodes not in "Published" status.
 */
function wfm_api_update_7004() {
  if (!class_exists('\Wfm\Api\SageClient\Recipe')) {
    include_once $_SERVER["DOCUMENT_ROOT"] . '../vendor/autoload.php';
  }

  $recipe_api = new \Wfm\Api\SageClient\Recipe(
    WFMVariables::get('wfmapi_key'),
    WFMVariables::get('wfmapi_secret'),
    'https://' . WFMVariables::get('wfmapi_host')
  );

  $recipe_api->recipeIterator(function($recipe) {
    if ($recipe['status'] != 'Published') {
      $mongo_id = $recipe['_id'];
      $node_id = db_select('field_data_field_mongo_id', 'm')
        ->fields('m', array('entity_id'))
        ->condition('field_mongo_id_value', $mongo_id, '=')
        ->execute()
        ->fetchField();
      if ($node_id) {
        drupal_set_message('Deleting node: ' . $node_id . '  ' . $recipe['title']);
        node_delete($node_id);
      }
    }
  });
  _wfm_migrate_mongo_mapper();
}

/**
 * Update reverse_proxy_addresses variable.
 */
function wfm_api_update_7005() {
  if (!function_exists('_wfm_api_update_ip_whitelist')) {
    module_load_include('inc', 'wfm_api', 'wfm_api.drush');
  }
  _wfm_api_update_ip_whitelist();
}