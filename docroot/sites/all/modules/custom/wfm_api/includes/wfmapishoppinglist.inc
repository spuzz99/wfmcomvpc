<?php
/**
 * @file
 * Contains WFMApiShoppingList
 */

use Wfm\Api\SageClient\User;
use Wfm\Api\SageClient\Recipe;

class WFMApiShoppingList extends WFMApi {

  /**
   * @var object implementation of Wfm\Api\SageClient\User
   */
  protected $userApi = '';

  /**
   * @var object implementation of Wfm\Api\SageClient\Recipe
   */
  protected $recipeApi = '';

  /**
   * @var string Cache key for shopping lists.
   */
  protected $cache_get_lists_id;

  /**
   * @var string Cache bin to store shopping list data.
   */
  protected $cache_bin;

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct();
    $this->cache_bin = 'cache_wfm_api_shopping_lists';
    $this->cache_get_lists_id = 'getShoppingLists';

    $user_cache = new WfmApiDrupalCache(array(
      'cache_bin' => 'cache_wfm_api_shopping_lists'
    ));
    $recipe_cache = new WfmApiDrupalCache();

    $this->userApi = new User(
      $this->api_key,
      $this->api_secret,
      $this->api_url,
      $this->api_version,
      $user_cache
    );
    $this->recipeApi = new Recipe(
      $this->api_key,
      $this->api_secret,
      $this->api_url,
      $this->api_version,
      $recipe_cache
    );
  }

  /**
   * Rename an existing shopping list.
   *
   * NOTE: This method will expire the self::getLists() cache.
   *
   * @param string $uid
   *   A valid Drupal User ID.
   * @param string $list_id
   *   A valid list id as returned from this::getLists().
   * @param string $new_name
   *   A string containing the new name of the shopping list.
   *
   * @retval
   *  The response sent back from the WFM SAGE API.
   */
  public function renameList($uid, $list_id, $new_name) {
    // Clear cache.
    $cid = $this->_buildCid($this->cache_get_lists_id, array($uid));
    $this->_expireCache($cid, $this->cache_bin);

    // Make request.
    try {
      $uuid = $this->getJUIDFromUid($uid);
      $this->userApi->setJanrainUuid($uuid);
      $new_name = $this->sanitizeValues($new_name);
      $result = $this->userApi->updateShoppingListName($list_id, $new_name);
      return $result;
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Get all items in a shopping list.
   *
   * Note: This method WILL return cached data from the SAGE API if available.
   *
   * @param string $uid
   *   Drupal user id
   * @param string $list_id
   *   A valid list _id as returned from this::getLists(). Mongo id, not numeric id.
   *   The numeric id is not always present.
   *
   * @retval
   *  An array containing items in the shopping list.
   */
  public function getItems($uid, $list_id) {
    $lists = $this->getLists($uid);
    foreach ($lists as $list) {
      if ($list->_id == $list_id) {
        return $list->items;
      }
    }
  }

  /**
   * Get all lists for a user.
   *
   * Note: This method WILL return cached data from the SAGE API if available.
   *
   * @param string $uid
   *   A valid Drupal User ID.
   *
   * @return array
   *   An array containing shopping lists for the user.
   */
  public function getLists($uid) {
    $lists = &drupal_static(__FUNCTION__);
    if (!$lists) {
      $cid = $this->_buildCid($this->cache_get_lists_id, array($uid));
      $lists = $this->_getCache($cid, $this->cache_bin);
      if (!$lists) {
        try {
          $uuid = $this->getJUIDFromUid($uid);
          $this->userApi->setJanrainUuid($uuid);
          $lists = $this->userApi->getShoppingLists();
          if ($lists && $uid) {
            $lists = $this->sanitizeValues($lists);
            $this->_setCache($cid, $lists, CACHE_TEMPORARY, $this->cache_bin);
          }
        }
        catch (Exception $e) {
          watchdog('wfmapi', $e);
        }
      }
      // Quick convert to an object.
      $lists = json_decode(json_encode($lists));
    }
    return $lists;
  }

  /**
   * Sort a list by it's name in a given direction.
   *
   * @param array $list
   *   A list of shopping lists from self::getLists().
   *   (optional) The direction the sorting algrotihim should work.
   *   Possible values are:
   *    - asc (default)
   *    - desc
   *
   * @retval
   *   $list after being sorted.
   */
  static protected function _listSortByAlpha($list, $direction = 'asc') {
    if (drupal_strtolower($direction) == 'desc') {
      usort($list, array('WFMApiShoppingList', '::_listSortByAlpha_lambdaDesc'));
    }
    else {
      usort($list, array('WFMApiShoppingList', '_listSortByAlpha_lambdaAsc'));
    }
    return $list;
  }

  /**
   * Ascending alphabetical sorting algorithim for items with $name property.
   *
   * @param object $a
   *   Item 1 to compare.
   * @param object $b
   *   Item 2 to compare.
   *
   * @retval -1
   *   $a->name comes after $b->name.
   * @retval 1
   *   $a->name comes before $b->name.
   */
  static protected function _listSortByAlpha_lambdaAsc($a, $b) {
    $a = $a->name;
    $b = $b->name;
    return ($a < $b) ? -1 : 1;
  }

  /**
   * Descending sorting algorithim for items with $name property.
   *
   * @param object $a
   *   Item 1 to compare.
   * @param object $b
   *   Item 2 to compare.
   *
   * @retval -1
   *   $a->name comes before $b->name.
   * @retval 1
   *   $a->name comes after $b->name.
   */
  static protected function _listSortByAlpha_lambdaDesc($a, $b) {
    $a = $a->name;
    $b = $b->name;
    return ($a > $b) ? -1 : 1;
  }

  /**
   * Sort a list by it's created time in a given direction.
   *
   * @param array $list
   *   A list of shopping lists from self::getLists().
   *   (optional) The direction the sorting algrotihim should work.
   *   Possible values are:
   *    - asc (default)
   *    - desc
   *
   * @retval
   *   $list after being sorted.
   */
  static protected function _listSortByCreated($list, $direction = 'asc') {
    if (drupal_strtolower($direction) == 'desc') {
      usort($list, array('WFMApiShoppingList', '_listSortByCreated_lambdaDesc'));
    }
    else {
      usort($list, array('WFMApiShoppingList', '_listSortByCreated_lambdaAsc'));
    }

    return $list;
  }

  /**
   * Descending sorting algorithim for items with $created_at property.
   *
   * @param object $a
   *   Item 1 to compare.
   * @param object $b
   *   Item 2 to compare.
   *
   * @retval -1
   *   $a->created_at comes before $b->created_at.
   * @retval 1
   *   $a->created_at comes after $b->created_at.
   */
  static protected function _listSortByCreated_lambdaDesc($a, $b) {
    $a = strtotime($a->created_at);
    $b = strtotime($b->created_at);
    return ($a > $b) ? 1 : -1;
  }

  /**
   * Ascending sorting algorithim for items with $created_at property.
   *
   * @param object $a
   *   Item 1 to compare.
   * @param object $b
   *   Item 2 to compare.
   *
   * @retval -1
   *   $a->created_at comes before $b->created_at.
   * @retval 1
   *   $a->created_at comes after $b->created_at.
   */
  static protected function _listSortByCreated_lambdaAsc($a, $b) {
    $a = strtotime($a->created_at);
    $b = strtotime($b->created_at);
    return ($a < $b) ? 1 : -1;
  }

  /**
   * Add a new shopping list for a user.
   *
   * NOTE: This method will expire the self::getLists() cache.
   *
   * @param string $uid
   *   A valid Drupal User ID.
   * @param string $name
   *   A string containing a name for the new shopping list.
   *
   * @return array
   *   The response sent back from the WFM SAGE API.
   */
  public function addList($uid, $name) {
    // Expire shopping list cache.
    $name = $this->sanitizeValues($name);
    $cid = $this->_buildCid($this->cache_get_lists_id, array($uid));
    $this->_expireCache($cid, $this->cache_bin);

    // Make request to SAGE.
    try {
      $uuid = $this->getJUIDFromUid($uid);
      $this->userApi->setJanrainUuid($uuid);
      $result = $this->userApi->createShoppingList($name, array());
      return $result;
    }
    catch (Exception $e) {
      watchdog('wfmapi', $e);
    }
  }

  /**
   * Add a new item to a shopping list.
   *
   * NOTE: This method will expire the self::getLists() cache.
   * NOTE: This method will expire the self::getItems() cache for this $list_id.
   *
   * @param string $uid
   *   A valid Drupal User ID.
   * @param string $list_id
   *   A valid list id as returned from this::getLists().
   * @param string $name
   *   A string containing the name of the item..
   * @param string $description
   *   (optional) A string containing a description of the item.
   *
   * @return array
   *   The response sent back from the WFM SAGE API.
   */
  public function addItemToList($uid, $list_id, $name, $description = '', $category = NULL) {
    $shopping_lists = $this->getLists($uid);
    $cid = $this->_buildCid($this->cache_get_lists_id, array($uid));
    $this->_expireCache($cid, $this->cache_bin);
    foreach ($shopping_lists as $shopping_list) {
      if ($shopping_list->_id == $list_id) {
        $the_list = $shopping_list;
      }
    }

    if ($the_list) {
      try {
        $new_item = array(
          'name' => $name,
          'note' => $description,
          'category_id' => $category
        );
        $new_item = $this->sanitizeValues($new_item);
        $uuid = $this->getJUIDFromUid($uid);
        $this->userApi->setJanrainUuid($uuid);
        $response = $this->userApi->addShoppingListItems($the_list->_id, array($new_item));
      }
      catch (Exception $e) {
        watchdog('wfmapi', $e);
      }
    }
    return $response;
  }

  /**
   * Add a new item to a shopping list.
   *
   * NOTE: This method will expire the self::getLists() cache.
   * NOTE: This method will expire the self::getItems() cache for this $list_id.
   *
   * @param string $uid
   *   A valid Drupal User ID.
   * @param string $list_id
   *   A valid list id as returned from this::getLists().
   * @param string $recipe_id
   *   A valid Recipe ID.
   *
   * @return array
   *   The response sent back from the WFM SAGE API.
   */
  public function addRecipeToList($uid, $list_id, $recipe_id) {
    $cid = $this->_buildCid($this->cache_get_lists_id, array($uid));
    $this->_expireCache($cid, $this->cache_bin);

    $recipe = $this->recipeApi->getRecipe($recipe_id);
    $items = $this->getRecipeIngredientsAsShoppingListItems($recipe);

    try {
      $uuid = $this->getJUIDFromUid($uid);
      $this->userApi->setJanrainUuid($uuid);
      $items = $this->sanitizeValues($items);
      return $this->userApi->addShoppingListItems($list_id, $items);
    }
    catch (BadResponseException $e) {
      watchdog('wfmapi', $e->getResponse()->getBody(TRUE));
    }
  }

  /**
   * Remove an existing from a shopping list.
   *
   * NOTE: This method will expire the self::getLists() cache.
   * NOTE: This method will expire the self::getItems cache for this $list_id.
   *
   * @param string $uid
   *   A valid Drupal User ID.
   * @param string $list_id
   *   A valid list id as returned from this::getLists().
   * @param string $item_id
   *   A valid item id as returend from this::getItems().
   *
   * @retval FALSE
   *  The $list_id or $item_id isn't valid.
   * @retval
   *  The response sent back from the WFM SAGE API.
   *  'real_item' is added to this array which contains the deleted item.
   */
  public function deleteItemFromList($uid, $list_id, $item_id) {
    $cid = $this->_buildCid($this->cache_get_lists_id, array($uid));
    $this->_expireCache($cid, $this->cache_bin);
    try {
      $uuid = $this->getJUIDFromUid($uid);
      $this->userApi->setJanrainUuid($uuid);
      return $this->userApi->deleteShoppingListItem($list_id, $item_id);
    }
    catch (BadResponseException $e) {
      watchdog('wfmapi', $e->getResponse()->getBody(TRUE));
    }
  }

  /**
   * Edit an existing item in a shopping list.
   *
   * NOTE: This method will expire the self::getLists() cache.
   * NOTE: This method will expire the self::getItems() cache for this $list_id.
   *
   * @param string $uid
   *   A valid Drupal User ID.
   * @param string $list_id
   *   A valid list id as returned from this::getLists().
   * @param string $item_id
   *   A valid item id as returned from this::getItems().
   * @param string $name
   *   A string containing the name of the item..
   * @param string $description
   *   A string containing a description of the item.
   *
   * @retval
   *  The response sent back from the WFM SAGE API.
   */
  public function editItemInList($uid, $list_id, $item_id, $name, $description = '', $category = NULL) {
    $cid = $this->_buildCid($this->cache_get_lists_id, array($uid));
    $this->_expireCache($cid, $this->cache_bin);
    $item = array(
      array(
        'name' => $name,
        'note' => $description,
        'category_id' => $category,
      ),
    );
    $item = $this->sanitizeValues($item);
    try {
      $uuid = $this->getJUIDFromUid($uid);
      $this->userApi->setJanrainUuid($uuid);
      return $this->userApi->updateShoppingListItem($list_id, $item_id, $item);
    }
    catch (BadResponseException $e) {
      watchdog('wfmapi', $e->getResponse()->getBody(TRUE));
    }
  }

  /**
   * Delete an existing shopping list.
   *
   * NOTE: This method will expire the self::getLists() cache.
   *
   * @param string $uid
   *   A valid Drupal User ID.
   * @param string $list_id
   *   A valid list id as returned from this::getLists().
   */
  public function deleteList($uid, $list_id) {
    $cid = $this->_buildCid($this->cache_get_lists_id, array($uid));
    $this->_expireCache($cid, $this->cache_bin);
    try {
      $uuid = $this->getJUIDFromUid($uid);
      $this->userApi->setJanrainUuid($uuid);
      return $this->userApi->deleteShoppingList($list_id);
    }
    catch (BadResponseException $e) {
      watchdog('wfmapi', $e->getResponse()->getBody(TRUE));
    }
  }

}
