<?php

/**
 * @file
 * Inc file with VariableDB class implementation.
 */

class VariableDB {
  protected $_db = array();

  /**
   * Class VariableDB instatiation function.
   */
  public function __construct() {
    $this->_updateFromVariable();
  }

  /**
   * Return all rows.
   */
  public function getAllRows() {
    return $this->_db;
  }

  /**
   * Print table.
   */
  public function printTable() {
    echo "<pre>";
    print_r($this->_db);
    echo "</pre>";
  }

  /**
   * Truncate function.
   */
  public function truncate() {
    $this->_db = array();
    $this->_pushToVariable();
  }

  /**
   * Add new record.
   */
  public function newRecord(array $columns) {
    $columns = array_merge($this->_defaultOptions, $columns);
    $keys = array_keys($this->_db);
    $this->_db[] = $columns;
    $index = array_pop(array_diff(array_keys($this->_db), $keys));
    $columns['index'] = $index;
    $this->_db[$index] = $columns;
    $this->_pushToVariable();

    return $index;
  }

  /**
   * Return record by key.
   */
  public function getRecordByKey($key, $key_name = 'index') {
    if ($key_name == 'index') {
      if (isset($this->_db[$key])) {
        return $this->_db[$key];
      }
      return NULL;
    }

    $key_name = drupal_strtolower($key_name);
    foreach ($this->_db as $id => $record) {
      if (isset($record[$key_name])) {
        if ($record[$key_name] == $key) {
          return $record;
        }
      }
    }

    return NULL;
  }

  /**
   * Return milti record by array of keys.
   */
  public function getRecordByKeyMulti(array $keys) {
    foreach ($this->_db as $id => $record) {
      $is_record = TRUE;
      foreach ($keys as $k => $v) {
        if (!isset($record[$k]) || $record[$k] != $v) {
          $is_record = FALSE;
        }
      }

      if ($is_record) {
        return $record;
      }
    }

    return NULL;
  }

  /**
   * Delete record by key.
   */
  public function deleteRecordByKey($key, $key_name = 'index') {
    $record = $this->getRecordByKey($key, $key_name);
    if ($record == NULL) {
      return FALSE;
    }

    unset($this->_db[$record['index']]);
    $this->_pushToVariable();
    return TRUE;
  }

  /**
   * Update record by key.
   */
  public function updateRecord($key, $columns) {
    $record = $this->getRecordByKey($key);
    if ($record == NULL) {
      return FALSE;
    }

    $columns = array_merge($record, $columns);
    $this->_db[$columns['index']] = $columns;
    $this->_pushToVariable();
  }

  /**
   * Helper function _updateFromVariable.
   */
  protected function _updateFromVariable() {
    $this->_db = unserialize(variable_get($this->_dbName, array()));
  }

  /**
   * Helper function _pushToVariable.
   */
  protected function _pushToVariable() {
    variable_set($this->_dbName, serialize($this->_db));
    $this->_db = unserialize(variable_get($this->_dbName, array()));
  }
}
