<?php

/**
 * @file
 * Inc file with WFMEventsNonceDb, WFMEventsTokenDb and WFMEventsOAuthToken class implementation.
 */

class WFMEventsNonceDb extends VariableDb {

  public static $_singleton = NULL;
  protected $_defaultOptions = array(
    'consumer_id' => '',
    'timestamp' => '',
    'nonce' => '0',
  );
  protected $_dbName = 'WFM_EVENTS_CONSUMER_NONCE';

  /**
   * Singleton function.
   */
  static public function singleton() {
    if (self::$_singleton === NULL) {
      self::$_singleton = new WFMEventsNonceDb();
    }

    return self::$_singleton;
  }

}

class WFMEventsTokenDb extends VariableDb {

  public static $_singleton = NULL;
  protected $_defaultOptions = array(
    'consumer_id' => '',
    'token' => '',
    'token_secret' => '',
    'callback_url' => '',
    'verifier' => '',
    'type' => 0,
  );
  protected $_dbName = 'WFM_EVENTS_TOKENS';

  /**
   * Singleton function.
   */
  static public function singleton() {
    if (self::$_singleton === NULL) {
      self::$_singleton = new WFMEventsTokenDb();
    }

    return self::$_singleton;
  }

}

class WFMEventsOAuthToken {

  protected $id;
  protected $type;
  protected $consumer;
  protected $token;
  protected $token_secret;
  protected $callback;
  protected $verifier;
  protected $user;
  public static $_tokenDb;

  /**
   * Build DB.
   */
  public static function buildDb() {
    if (self::$_tokenDb == NULL) {
      self::$_tokenDb = WFMEventsTokenDb::singleton();
    }
  }

  // Static functions.

  /**
   * Create request token.
   */
  public static function createRequestToken($consumer, $token, $tokensecret, $callback) {
    $token_list = array(
      'consumer_id' => $consumer->getId(),
      'token' => $token,
      'token_secret' => $tokensecret,
      'callback_url' => $callback,
      'verifier' => '',
      'type' => 0,
    );

    self::buildDb();
    $record = self::$_tokenDb->newRecord($token_list);
    return $record;
  }

  /**
   * Find by token.
   */
  public static function findByToken($token) {
    self::buildDb();
    $record = self::$_tokenDb->getRecordByKey($token, 'token');

    if ($record !== NULL) {
      return new self($record['index']);
    }

    return NULL;
  }

  /**
   * Class WFMEventsOAuthToken instatiation function.
   */
  public function __construct($id = -1) {
    if ($id != -1) {
      $this->id = $id;
      $this->load();
    }
  }

  /**
   * Load function.
   */
  private function load() {
    self::buildDb();
    $info = self::$_tokenDb->getRecordByKey($this->id);

    $this->token = $info['token'];
    $this->type = $info['type'];
    $this->token_secret = $info['token_secret'];
    $this->consumer = new WFMEventsOAuthConsumer($info['consumer_id']);
    $this->callback = $info['callback_url'];
    $this->verifier = $info['verifier'];
  }

  /**
   * Change to access token.
   */
  public function changeToAccessToken($token, $secret) {
    if ($this->isRequest()) {
      $token_list['type'] = 2;
      $token_list['verifier'] = '';
      $token_list['callback_url'] = '';
      $token_list['token'] = $token;
      $token_list['token_secret'] = $secret;

      self::buildDb();
      $info = self::$_tokenDb->updateRecord($this->id, $token_list);

      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  // Some setters.

  /**
   * Set verifier.
   */
  public function setVerifier($verifier) {
    $token_list['verifier'] = $verifier;

    self::buildDb();
    $info = self::$_tokenDb->updateRecord($this->id, $token_list);
  }

  // Some getters.

  /**
   * Is request.
   */
  public function isRequest() {
    return $this->type == 1;
  }

  /**
   * Is access.
   */
  public function isAccess() {
    return !$this->isRequest();
  }

  /**
   * Get callback.
   */
  public function getCallback() {
    return $this->callback;
  }

  /**
   * Get verifier.
   */
  public function getVerifier() {
    return $this->verifier;
  }

  /**
   * Get type.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get secret.
   */
  public function getSecret() {
    return $this->token_secret;
  }

  /**
   * Get user.
   */
  public function getUser() {
    return $this->user;
  }

}
