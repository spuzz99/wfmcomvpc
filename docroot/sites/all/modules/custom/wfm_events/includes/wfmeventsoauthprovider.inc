<?php

/**
 * @file
 * Inc file with WFMEventsOAuthProvider class implementation.
 */

class WFMEventsOAuthProvider {

  private $oauth;
  private $consumer;
  private $oauth_error;
  private $user;
  private $issue;

  /**
   * Create consumer.
   */
  public function createConsumer($note) {
    $key = sha1(OAuthProvider::generateToken(20, TRUE));
    $secret = sha1(OAuthProvider::generateToken(20, TRUE));
    return WFMEventsOAuthConsumer::create($key, $secret, $note);
  }

  /**
   * Class WFMEventsOAuthProvider instatiation function.
   */
  public function __construct() {
    /* create our instance */
    $this->oauth = new OAuthProvider();

    /* setup check functions */
    $this->oauth->consumerHandler(array($this, 'checkConsumer'));
    $this->oauth->timestampNonceHandler(array($this, 'checkNonce'));
    $this->oauth->tokenHandler(array($this, 'checkToken'));
  }

  /**
   * Run the checks.
   */
  public function checkRequest() {
    // Now that everything is setup we run the checks.
    try {
      $this->oauth->checkOAuthRequest();
    }
    catch (OAuthException $e) {
      $this->issue = OAuthProvider::reportProblem($e);
      $this->oauth_error = TRUE;
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get issue.
   */
  public function getIssue() {
    return $this->issue;
  }

  /**
   * Set request token query.
   */
  public function setRequestTokenQuery() {
    $this->oauth->isRequestTokenEndpoint(TRUE);
  }

  /**
   * Generate request token.
   */
  public function generateRequestToken() {
    if ($this->oauth_error) {
      return FALSE;
    }

    $token = sha1(OAuthProvider::generateToken(20, TRUE));
    $token_secret = sha1(OAuthProvider::generateToken(20, TRUE));

    $callback = $this->oauth->callback;

    WFMEventsOAuthToken::createRequestToken($this->consumer, $token, $token_secret, $callback);

    return array('token' => $token, 'secret' => $token_secret);
  }

  /**
   * Generate access token.
   */
  public function generateAccessToken() {
    if ($this->oauth_error) {
      return FALSE;
    }

    $access_token = sha1(OAuthProvider::generateToken(20, TRUE));
    $secret = sha1(OAuthProvider::generateToken(20, TRUE));

    $token = WFMEventsOAuthToken::findByToken($this->oauth->token);

    $token->changeToAccessToken($access_token, $secret);
    return "oauth_token=" . $access_token . "&oauth_token_secret=" . $secret;
  }

  /**
   * Generate verifier.
   */
  public function generateVerifier() {
    $verifier = sha1(OAuthProvider::generateToken(20, TRUE));
    return $verifier;
  }

  /**
   * Check consumer.
   */
  public function checkConsumer($provider) {
    $return = OAUTH_CONSUMER_KEY_UNKNOWN;

    $a_consumer = WFMEventsOAuthConsumer::findByKey($provider->consumer_key);

    if (is_object($a_consumer)) {
      if (!$a_consumer->isActive()) {
        $return = OAUTH_CONSUMER_KEY_REFUSED;
      }
      else {
        $this->consumer = $a_consumer;
        $provider->consumer_secret = $this->consumer->getSecretKey();
        $return = OAUTH_OK;
      }
    }

    return $return;
  }

  /**
   * Check token.
   */
  public function checkToken($provider) {
    $token = WFMEventsOAuthToken::findByToken($provider->token);

    // Token not found.
    if (is_null($token)) {
      return OAUTH_TOKEN_REJECTED;
    }
    // Bad verifier for request token.
    elseif ($token->getType() == 1 && $token->getVerifier() != $provider->verifier) {
      return OAUTH_VERIFIER_INVALID;
    }
    else {
      if ($token->getType() == 2) {

      }
      $provider->token_secret = $token->getSecret();
      return OAUTH_OK;
    }
  }

  /**
   * Check nonce.
   */
  public function checkNonce($provider) {
    if ($this->oauth->timestamp < time() - 5 * 60) {
      return OAUTH_BAD_TIMESTAMP;
    }
    elseif ($this->consumer->hasNonce($provider->nonce, $this->oauth->timestamp)) {
      return OAUTH_BAD_NONCE;
    }
    else {
      $this->consumer->addNonce($this->oauth->nonce);
      return OAUTH_OK;
    }
  }

}
