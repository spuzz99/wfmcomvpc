<?php

/**
 * @file
 * Inc file with WFMEventsConsumerListDb and WFMEventsOAuthConsumer class implementation.
 */

class WFMEventsConsumerListDb extends VariableDb {

  public static $_singleton = NULL;
  protected $_defaultOptions = array(
    'consumer_key' => '',
    'consumer_secret' => '',
    'active' => '0',
    'note' => '',
  );
  protected $_dbName = 'WFM_EVENTS_CONSUMER_LIST';

  /**
   * Singleton function.
   */
  static public function singleton() {
    if (WFMEventsConsumerListDb::$_singleton === NULL) {
      WFMEventsConsumerListDb::$_singleton = new WFMEventsConsumerListDb();
    }

    return WFMEventsConsumerListDb::$_singleton;
  }

}

class WFMEventsOAuthConsumer {

  private $id;
  private $key;
  private $secret;
  private $active;
  public static $_consumerDb = NULL;
  public static $_nonceDb = NULL;

  /**
   * Building DB.
   */
  public static function buildDb() {
    if (self::$_consumerDb == NULL) {
      self::$_consumerDb = WFMEventsConsumerListDb::singleton();
    }

    if (self::$_nonceDb == NULL) {
      self::$_nonceDb = WFMEventsNonceDb::singleton();
    }
  }

  /**
   * Find by key.
   */
  public static function findByKey($key) {
    self::buildDb();
    $record = self::$_consumerDb->getRecordByKey($key, 'consumer_key');

    if ($record !== NULL) {
      return new self($record['index']);
    }
  }

  /**
   * Class WFMEventsOAuthConsumer instatiation function.
   */
  public function __construct($id = -1) {
    if ($id != -1) {
      $this->id = $id;
      $this->load();
    }
  }

  /**
   * Load function.
   */
  private function load() {
    self::buildDb();
    $info = self::$_consumerDb->getRecordByKey($this->id);

    $this->key = $info['consumer_key'];
    $this->secret = $info['consumer_secret'];
    $this->active = $info['active'];
  }

  /**
   * Create function.
   */
  public static function create($key, $secret, $note = '') {
    self::buildDb();

    $consumer = array(
      'consumer_key' => $key,
      'consumer_secret' => $secret,
      'note' => $note,
      'active' => TRUE,
    );

    $record = self::$_consumerDb->newRecord($consumer);
    $consumer = new self($record);
    return $consumer;
  }

  /**
   * Check is active.
   */
  public function isActive() {
    return $this->active;
  }

  /**
   * Get key.
   */
  public function getKey() {
    return $this->key;
  }

  /**
   * Get secret key.
   */
  public function getSecretKey() {
    return $this->secret;
  }

  /**
   * Get ID.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Function hasNonce.
   */
  public function hasNonce($nonce, $timestamp) {
    self::buildDb();

    $info = self::$_nonceDb->getRecordByKeyMulti(array(
      'timestamp' => $timestamp,
      'nonce' => $nonce,
      'consumer_id' => $this->id
    ));
    if ($info !== NULL) {
      return TRUE;
    }
  }

  /**
   * Function addNonce.
   */
  public function addNonce($nonce) {
    self::buildDb();

    $consumer_nonce = array(
      'consumer_id' => $this->id,
      'timestamp' => REQUEST_TIME,
      'nonce' => $nonce,
    );

    $record = self::$_nonceDb->newRecord($consumer_nonce);
    return $record;
  }

  /* setters */

  /**
   * Set key.
   */
  public function setKey($key) {
    $this->key = $key;
  }

  /**
   * Set secret.
   */
  public function setSecret($secret) {
    $this->secret = $secret;
  }

  /**
   * Set active.
   */
  public function setActive($active) {
    $this->active = $active;
  }

  /**
   * Set ID.
   */
  public function setId($id) {
    $this->id = $id;
  }

}
