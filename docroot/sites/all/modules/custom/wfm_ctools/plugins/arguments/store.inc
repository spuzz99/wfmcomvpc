<?php

/**
 * @file
 * Plugin to provide an argument handler for a Store Name.
 */

$plugin = array(
  'title' => t('Store: Name'),
  'keyword' => 'store',
  'description' => t('Creates a node context from a Store Name argument.'),
  'context' => 'wfm_ctools_argument_store_context',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the Store Name of a Store node for this argument'),
  ),
);

/**
 * Discover if this argument gives us the node we crave.
 */
function wfm_ctools_argument_store_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  if ($empty) {
    return ctools_context_create_empty('node');
  }
  if (is_object($arg)) {
    return ctools_context_create('node', $arg);
  }
  $query = 'SELECT entity_id FROM {field_data_field_store_name} WHERE'
    . " field_store_name_value = :name AND bundle = 'store' LIMIT 1";
  $arg = db_query($query, array(':name' => $arg))->fetchColumn();
  if (!is_numeric($arg)) {
    return FALSE;
  }
  $node = node_load($arg);
  if (!$node) {
    return FALSE;
  }
  return ctools_context_create('node', $node);
}
