<?php

/**
 * @file
 * Plugin to provide Special Diets Shopping List block.
 */

$plugin = array(
  'title' => t('Store Special Diets'),
  'description' => t('Special Diets Shopping List block'),
  'single' => TRUE,
  'content_types' => array('store_special_diets'),
  'render callback' => 'wfm_ctools_store_special_diets_ct_render',
  'required context' => new ctools_context_required(t('Store: Name'), 'node'),
  'edit form' => 'wfm_ctools_store_special_diets_ct_edit_form',
  'category' => array(t('Custom'), 0),
);

/**
 * Render function.
 */
function wfm_ctools_store_special_diets_ct_render($subtype, $conf, $args = array(), $context = NULL) {
  $block = new stdClass();
  $block->title = '';
  $block->content = '';

  if (!empty($context->data->nid) && $context->data->type == 'store') {
    $block->title = 'Special Diets Shopping List';
    $block->content = store_page_content_mobile_special_diets_block($context->data);
  }
  return $block;
}

/**
 * Settings function.
 */
function wfm_ctools_store_special_diets_ct_edit_form($form) {
  return $form;
}
