<?php

/**
 * @file
 * Custom plugin Shopping List Details.
 */

$plugin = array(
  'title' => t('Shopping List Details'),
  'description' => t('Shopping List Details widget'),
  'single' => TRUE,
  'content_types' => array('shopping_list_details'),
  'render callback' => 'wfm_mobile_shopping_list_details_ct_render',
  'edit form' => 'wfm_mobile_shopping_list_details_ct_edit_form',
  'category' => array(t('Custom'), 0),
);


/**
 * Render function.
 */
function wfm_mobile_shopping_list_details_ct_render($subtype, $conf, $args = array()) {
  $block = new stdClass();
  $block->title = '';
  $block->content = '';
  if (!empty($args[0])) {
    $id = check_plain($args[0]);
    $name = wfm_mobile_shopping_lists_get_list($id);
    if ($name) {
      $block->title = $name;
      $content = drupal_get_form('wfm_mobile_shopping_list_details_ct_edit_list_form', $id, $name);
      $block->content = drupal_render($content);
      ctools_add_js('shopping_list_details', 'wfm_mobile_shopping_lists',
          'plugins/content_types/shopping_list_details');
    }
  }
  return $block;
}


/**
 * Settings function.
 */
function wfm_mobile_shopping_list_details_ct_edit_form($form) {
  return $form;
}


/**
 * Form constructor for Create Shopping List form.
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form($form, &$form_state, $id, $name) {
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );

  $form['items'] = array(
    '#type' => 'value',
    '#value' => $items = wfm_mobile_shopping_lists_get_items($id),
  );

  $form['back'] = array(
    '#markup' => '<div class="back">' . l(t('Back'), wfm_mobile_shopping_lists_get_path()) . '</div>',
  );

  $form['list'] = array(
    '#type' => 'container',

    'list_rename' => array(
      '#markup' => '<div class="rename-list">' . t('Rename List') . '</div>',
    ),

    'list_delete_submit' => array(
      '#markup' => '<div id="list-delete">'
      . l(t('Delete List'), 'mobile/shopping-lists/' . $id . '/remove') . '</div>',
    ),

    'list_email' => array(
      '#markup' => wfm_mobile_shopping_lists_format_email($name, $items),
    ),
  );

  $form['rename'] = array(
    '#type' => 'container',

    'rename_name' => array(
      '#type' => 'textfield',
      '#title' => t('Rename List'),
      '#default_value' => '',
      '#maxlength' => 128,
    ),

    'rename_submit' => array(
      '#type' => 'submit',
      '#value' => t('Save Rename'),
      '#name' => 'rename_submit',
      '#validate' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_rename_list_validate'),
      '#submit' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_rename_list_submit'),
    ),
  );

  $date = t('Latest Added');
  $date_link = l($date, '', array(
    'external' => TRUE,
    'fragment' => FALSE,
    'attributes' => array('class' => array('date')),
  ));
  $cat = t('Category');
  $cat_link = l($cat, '', array(
    'external' => TRUE,
    'fragment' => FALSE,
    'attributes' => array('class' => array('cat')),
  ));

  $form['item'] = array(
    '#type' => 'container',

    'item_add' => array(
      '#type' => 'item',
      '#markup' => l(t('+ Add Item'), '', array('external' => TRUE, 'fragment' => FALSE)),
    ),
  );

  if (1 < count($items)) {
    $form['item']['item_sort'] = array(
      '#type' => 'item',
      '#markup' => t('Sort by:') . ' <span><b class="cat">' . $date . '</b>' . $date_link . '</span>'
      . '<span><b class="date">' . $cat . '</b>' . $cat_link . '</span>',
    );
  }

  $cats = wfm_mobile_shopping_lists_get_categories();
  $form['new'] = array(
    '#type' => 'container',

    'new_note' => array(
      '#type' => 'textfield',
      '#title' => t('Item*'),
      '#default_value' => '',
      '#maxlength' => 128,
      '#description' => t('ex: Apple'),
      '#attributes' => array('class' => array('note')),
    ),

    'new_desc' => array(
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => '',
      '#maxlength' => 128,
      '#description' => t('ex: Nice ones for pie'),
      '#attributes' => array('class' => array('desc')),
    ),

    'new_cat' => array(
      '#type' => 'select',
      '#title' => t('Category'),
      '#options' => $cats,
      '#default_value' => '',
    ),

    'new_submit' => array(
      '#type' => 'submit',
      '#value' => t('Add Item'),
      '#name' => 'new_submit',
      '#validate' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_add_item_validate'),
      '#submit' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_add_item_submit'),
    ),
  );

  if (count($items)) {
    $form['delete_items_top'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Selected'),
      '#name' => 'delete_items_top',
      '#attributes' => array('class' => array('button-as-link')),
      '#submit' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_delete_items_submit'),
    );
  }

  $form['list_items'] = array(
    '#type' => 'container',
  );

  foreach ($items as $id => $item) {
    $form['list_items']['item_' . $id] = array(
      '#type' => 'container',
      '#attributes' => array(
        'order-cat' => $item['order_cat'],
      ),

      'item_' . $id => array(
        '#type' => 'checkbox',
        '#title' => $item['note'],
      ),

      'category' => array(
        '#markup' => '<div class="category">' . $item['cat'] . '</div>',
      ),

      'desc' => array(
        '#markup' => '<div class="desc">' . $item['desc'] . '</div>',
      ),

      'form_' . $id => array(
        '#type' => 'container',
        '#attributes' => array('class' => array('form-edit')),

        'note_' . $id => array(
          '#type' => 'textfield',
          '#title' => t('Item*'),
          '#default_value' => $item['note'],
          '#maxlength' => 128,
          '#description' => t('ex: Apple'),
          '#attributes' => array('class' => array('note')),
        ),

        'desc_' . $id => array(
          '#type' => 'textfield',
          '#title' => t('Description'),
          '#default_value' => $item['desc'],
          '#maxlength' => 128,
          '#description' => t('ex: Nice ones for pie'),
          '#attributes' => array('class' => array('desc')),
        ),

        'cat_' . $id => array(
          '#type' => 'select',
          '#title' => t('Category'),
          '#options' => $cats,
          '#default_value' => $item['cat_id'],
        ),

        'update_' . $id => array(
          '#type' => 'submit',
          '#value' => t('Update Item'),
          '#name' => 'update_' . $id,
          '#attributes' => array('class' => array('item-update')),
          '#validate' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_update_item_validate'),
          '#submit' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_update_item_submit'),
        ),

        'delete_' . $id => array(
          '#type' => 'submit',
          '#value' => t('Remove Item from List'),
          '#name' => 'delete_' . $id,
          '#attributes' => array('class' => array('item-delete')),
          '#submit' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_delete_item_submit'),
        ),
      ),

      'toggle' => array(
        '#markup' => l('+', '', array(
          'external' => TRUE,
          'fragment' => FALSE,
          'attributes' => array('class' => array('toggle', 'collapsed'))
        )),
      ),

    );
  }

  if (count($items)) {
    $form['delete_items_bottom'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Selected'),
      '#name' => 'delete_items_bottom',
      '#attributes' => array('class' => array('button-as-link')),
      '#submit' => array('wfm_mobile_shopping_list_details_ct_edit_list_form_delete_items_submit'),
    );
  }

  return $form;
}


/**
 * Validation handler for "Rename List" in wfm_mobile_shopping_list_details_ct_edit_list_form().
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form_rename_list_validate($form, &$form_state) {
  if (!trim($form_state['values']['rename_name'])) {
    form_set_error('rename_name', t('New list name field is required.'));
  }
}


/**
 * Validation handler for "Add Item" in wfm_mobile_shopping_list_details_ct_edit_list_form().
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form_add_item_validate($form, &$form_state) {
  if (!trim($form_state['values']['new_note'])) {
    form_set_error('new_note', t('New list item name field is required.'));
  }
}


/**
 * Validation handler for "Update Item" in wfm_mobile_shopping_list_details_ct_edit_list_form().
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form_update_item_validate($form, &$form_state) {
  $item = wfm_mobile_shopping_lists_get_id($form_state);
  if (!trim($form_state['values']['note_' . $item])) {
    form_set_error('note_' . $item, t('List item name field is required.'));
  }
}


/**
 * Submission handler for "Rename List" in wfm_mobile_shopping_list_details_ct_edit_list_form().
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form_rename_list_submit($form, &$form_state) {
  wfm_mobile_shopping_lists_rename_list($form_state['values']['id'], $form_state['values']['rename_name']);
}


/**
 * Submission handler for "Add Item" in wfm_mobile_shopping_list_details_ct_edit_list_form().
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form_add_item_submit($form, &$form_state) {
  $data = $form_state['values'];
  wfm_mobile_shopping_lists_create_item($data['id'], $data['new_note'], $data['new_desc'], $data['new_cat']);
}


/**
 * Submission handler for "Remove Item from List" in wfm_mobile_shopping_list_details_ct_edit_list_form().
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form_delete_item_submit($form, &$form_state) {
  $id = wfm_mobile_shopping_lists_get_id($form_state);
  wfm_mobile_shopping_lists_delete_item($form_state['values']['id'], $id);
}


/**
 * Submission handler for "Update Item" in wfm_mobile_shopping_list_details_ct_edit_list_form().
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form_update_item_submit($form, &$form_state) {
  $data = $form_state['values'];
  $id = wfm_mobile_shopping_lists_get_id($form_state);
  wfm_mobile_shopping_lists_update_item($data['id'], $id, $data['note_' . $id],
      $data['desc_' . $id], $data['cat_' . $id]);
}


/**
 * Submission handler for "Delete Selected" in wfm_mobile_shopping_list_details_ct_edit_list_form().
 */
function wfm_mobile_shopping_list_details_ct_edit_list_form_delete_items_submit($form, &$form_state) {
  $data = $form_state['values'];
  foreach ($data['items'] as $id => $null) {
    if ($data['item_' . $id]) {
      wfm_mobile_shopping_lists_delete_item($data['id'], $id);
    }
  }
}
