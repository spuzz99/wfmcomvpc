<?php
/**
 * @file
 * blog_moderation.features.inc
 */

/**
 * Implements hook_views_api().
 */
function blog_moderation_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function blog_moderation_flag_default_flags() {
  $flags = array();
  // Exported flag: "Closed".
  $flags['blog_comment_closed'] = array(
    'content_type' => 'comment',
    'title' => 'Closed',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Close Comment',
    'flag_long' => 'Close the comment',
    'flag_message' => 'Comment closed',
    'unflag_short' => 'Closed',
    'unflag_long' => 'Remove Closed flag',
    'unflag_message' => 'Closed flag removed',
    'unflag_denied_text' => 'You do not have permission to close this comment',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
      'unflag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
    ),
    'weight' => '-18',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Emailed".
  $flags['blog_comment_emailed'] = array(
    'content_type' => 'comment',
    'title' => 'Emailed',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Emailed',
    'flag_long' => 'Commentor has been contacted directly',
    'flag_message' => 'Marked as Emailed',
    'unflag_short' => 'Emailed',
    'unflag_long' => 'Remove Emailed Flag',
    'unflag_message' => 'Email Flag Removed',
    'unflag_denied_text' => 'You do not have permission to mark this "Direct Response"',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
      'unflag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
    ),
    'weight' => '-16',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "First Touch Moderator 1".
  $flags['blog_comment_first_touch_1'] = array(
    'content_type' => 'comment',
    'title' => 'First Touch Moderator 1',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Moderator-1',
    'flag_long' => 'Make Moderator 1 first touch',
    'flag_message' => 'Moderator 1 is first touch',
    'unflag_short' => 'Moderator-1',
    'unflag_long' => 'Remove first touch from Moderator 1',
    'unflag_message' => 'First touch status removed',
    'unflag_denied_text' => 'Only blog admins may send to moderation',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
      'unflag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "First Touch Moderator 2".
  $flags['blog_comment_first_touch_2'] = array(
    'content_type' => 'comment',
    'title' => 'First Touch Moderator 2',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Moderator-2',
    'flag_long' => 'Make Moderator 2 first touch',
    'flag_message' => 'Moderator 2 is first touch',
    'unflag_short' => 'Moderator-2',
    'unflag_long' => 'Remove first touch from Moderator 2',
    'unflag_message' => 'First touch status removed',
    'unflag_denied_text' => 'Only blog admins may send to moderation',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
      'unflag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Assign Comment to Moderator 1".
  $flags['blog_comment_moderate'] = array(
    'content_type' => 'comment',
    'title' => 'Assign Comment to Moderator 1',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Moderator-1',
    'flag_long' => 'Assign for moderation',
    'flag_message' => 'Assigned for moderation',
    'unflag_short' => 'Moderator-1',
    'unflag_long' => 'Remove from moderation queue',
    'unflag_message' => 'Removed from moderation queue',
    'unflag_denied_text' => 'Only blog admins may send to moderation',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'weight' => '-14',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Assign Comment to Moderator 2".
  $flags['blog_comment_moderate_2'] = array(
    'content_type' => 'comment',
    'title' => 'Assign Comment to Moderator 2',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Moderator-2',
    'flag_long' => 'Assign for moderation',
    'flag_message' => 'Assigned for moderation',
    'unflag_short' => 'Moderator-2',
    'unflag_long' => 'Remove from moderation queue',
    'unflag_message' => 'Removed from moderation queue',
    'unflag_denied_text' => 'Only blog admins may send to moderation',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'weight' => '-8',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Pending".
  $flags['blog_comment_pending'] = array(
    'content_type' => 'comment',
    'title' => 'Pending',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Pending',
    'flag_long' => 'Mark comment as pending',
    'flag_message' => 'Comment is pending',
    'unflag_short' => 'Pending',
    'unflag_long' => 'Remove Pending flag',
    'unflag_message' => 'Pending flag removed',
    'unflag_denied_text' => 'You do not have permission to mark this "Pending"',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
      'unflag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
    ),
    'weight' => '-17',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Approve &amp;amp; Publish Comment".
  $flags['blog_comment_publish'] = array(
    'content_type' => 'comment',
    'title' => 'Approve & Publish Comment',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Approve',
    'flag_long' => 'Approve and publish this comment',
    'flag_message' => 'Comment has been published',
    'unflag_short' => 'Unpublish',
    'unflag_long' => 'Unpublish an approved comment',
    'unflag_message' => 'Comment has been returned to moderation queue',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '4',
        1 => '3',
      ),
      'unflag' => array(
        0 => '4',
        1 => '3',
      ),
    ),
    'weight' => '-12',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Replied".
  $flags['blog_comment_replied'] = array(
    'content_type' => 'comment',
    'title' => 'Replied',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Replied',
    'flag_long' => 'Flag as a Reply',
    'flag_message' => 'Flagged as Reply',
    'unflag_short' => 'Replied',
    'unflag_long' => 'Remove Reply flag',
    'unflag_message' => 'Reply flag removed',
    'unflag_denied_text' => 'You do not have permission to mark this "Reply"',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
      'unflag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
        3 => '3',
      ),
    ),
    'weight' => '-15',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Comment is Off Topic".
  $flags['off_topic'] = array(
    'content_type' => 'comment',
    'title' => 'Comment is Off Topic',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Off-Topic',
    'flag_long' => 'Mark this comment as \'off-topic\'',
    'flag_message' => 'Marked Off-Topic',
    'unflag_short' => 'Off-Topic',
    'unflag_long' => 'Remove the Off-Topic flag',
    'unflag_message' => 'Removed "off-Topic" flag',
    'unflag_denied_text' => 'You do not have permission to mark this "Off-Topic"',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
      ),
      'unflag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
      ),
    ),
    'weight' => '-7',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Comment Violates policy".
  $flags['violates_policy'] = array(
    'content_type' => 'comment',
    'title' => 'Comment Violates policy',
    'global' => '1',
    'types' => array(
      0 => 'comment_node_blog',
      1 => 'comment_node_blog_post',
    ),
    'flag_short' => 'Violates-Policy',
    'flag_long' => 'Mark this comment as \'violating policy\'',
    'flag_message' => 'Marked Violates Policy',
    'unflag_short' => 'Violates-Policy',
    'unflag_long' => 'Remove the Violates Policy flag',
    'unflag_message' => 'Removed "Violates Policy" flag',
    'unflag_denied_text' => 'You do not have permission to mark this "Violates Policy"',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
      ),
      'unflag' => array(
        0 => '5',
        1 => '4',
        2 => '6',
      ),
    ),
    'weight' => '-6',
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'blog_moderation',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;

}
