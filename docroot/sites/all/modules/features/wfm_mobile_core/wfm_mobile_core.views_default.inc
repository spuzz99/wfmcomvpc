<?php
/**
 * @file
 * wfm_mobile_core.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wfm_mobile_core_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_global';
  $view->human_name = 'Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'panels_fields';
  $handler->display->display_options['row_options']['regions'] = array(
    'type' => 'left',
    'title' => 'left',
    'body' => 'left',
    'view_node' => 'right',
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no results found.';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Indexed Content: Content type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'search_api_index_global';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['type']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['type']['format_name'] = 1;
  /* Field: Indexed Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_global';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Field: Indexed Content: The main body text */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'search_api_index_global';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'wfm_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = '>';
  /* Sort criterion: Search: Relevance */
  $handler->display->display_options['sorts']['search_api_relevance']['id'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['table'] = 'search_api_index_global';
  $handler->display->display_options['sorts']['search_api_relevance']['field'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['order'] = 'DESC';
  /* Filter criterion: Indexed Content: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'search_api_index_global';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['filters']['status']['group'] = 1;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_global';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['group'] = 1;
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Keywords';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: Indexed Content: Content type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'search_api_index_global';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Content type';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['type']['group_info']['label'] = 'Content type';
  $handler->display->display_options['filters']['type']['group_info']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['group_info']['optional'] = FALSE;
  $handler->display->display_options['filters']['type']['group_info']['default_group'] = '1';
  $handler->display->display_options['filters']['type']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'All Results',
      'operator' => '=',
      'value' => array(
        'blog' => 'blog',
        'blog_post' => 'blog_post',
        'core_value' => 'core_value',
        'department' => 'department',
        'department_article' => 'department_article',
        'local_vendor' => 'local_vendor',
        'product' => 'product',
        'product_line' => 'product_line',
        'recipe' => 'recipe',
        'service' => 'service',
        'special_diet' => 'special_diet',
        'coupon' => 0,
        'event' => 0,
        'faq' => 0,
        'job' => 0,
        'marquee' => 0,
        'metro' => 0,
        'mobile_marquee' => 0,
        'national_offices' => 0,
        'newsletter' => 0,
        'page' => 0,
        'person' => 0,
        'product_certification' => 0,
        'product_recall' => 0,
        'promo' => 0,
        'region' => 0,
        'special_diet_shopping_list' => 0,
        'sale_item' => 0,
        'store' => 0,
        'video' => 0,
        'webform' => 0,
      ),
    ),
    2 => array(
      'title' => 'Recipes',
      'operator' => '=',
      'value' => array(
        'recipe' => 'recipe',
        'blog' => 0,
        'blog_post' => 0,
        'core_value' => 0,
        'coupon' => 0,
        'department' => 0,
        'department_article' => 0,
        'event' => 0,
        'faq' => 0,
        'job' => 0,
        'local_vendor' => 0,
        'marquee' => 0,
        'metro' => 0,
        'mobile_marquee' => 0,
        'national_offices' => 0,
        'newsletter' => 0,
        'page' => 0,
        'person' => 0,
        'product' => 0,
        'product_certification' => 0,
        'product_line' => 0,
        'product_recall' => 0,
        'promo' => 0,
        'region' => 0,
        'service' => 0,
        'special_diet' => 0,
        'store' => 0,
        'video' => 0,
        'webform' => 0,
      ),
    ),
    3 => array(
      'title' => 'Blogs',
      'operator' => '=',
      'value' => array(
        'blog_post' => 'blog_post',
        'blog' => 0,
        'core_value' => 0,
        'coupon' => 0,
        'department' => 0,
        'department_article' => 0,
        'event' => 0,
        'faq' => 0,
        'job' => 0,
        'local_vendor' => 0,
        'marquee' => 0,
        'metro' => 0,
        'mobile_marquee' => 0,
        'national_offices' => 0,
        'newsletter' => 0,
        'page' => 0,
        'person' => 0,
        'product' => 0,
        'product_certification' => 0,
        'product_line' => 0,
        'product_recall' => 0,
        'promo' => 0,
        'recipe' => 0,
        'region' => 0,
        'service' => 0,
        'special_diet' => 0,
        'store' => 0,
        'video' => 0,
        'webform' => 0,
      ),
    ),
    4 => array(
      'title' => 'Articles',
      'operator' => '=',
      'value' => array(
        'blog' => 'blog',
        'core_value' => 'core_value',
        'department' => 'department',
        'department_article' => 'department_article',
        'local_vendor' => 'local_vendor',
        'product' => 'product',
        'product_line' => 'product_line',
        'service' => 'service',
        'special_diet' => 'special_diet',
        'blog_post' => 0,
        'coupon' => 0,
        'event' => 0,
        'faq' => 0,
        'job' => 0,
        'marquee' => 0,
        'metro' => 0,
        'mobile_marquee' => 0,
        'national_offices' => 0,
        'newsletter' => 0,
        'page' => 0,
        'person' => 0,
        'product_certification' => 0,
        'product_recall' => 0,
        'promo' => 0,
        'recipe' => 0,
        'region' => 0,
        'special_diet_shopping_list' => 0,
        'sale_item' => 0,
        'store' => 0,
        'video' => 0,
        'webform' => 0,
      ),
    ),
  );

  /* Display: Global */
  $handler = $view->new_display('panel_pane', 'Global', 'global');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $export['search'] = $view;

  return $export;
}
