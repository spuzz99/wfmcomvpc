(function($) {

  function MobileStoresMap() {
    if ('mobileStores' in Drupal.settings) {
      this.map = null;
      this.center = null;
      this.canvas = '#mapCanvas';
      this.toggleMapSubling = '#edit-field-geo-data-latlon-wrapper';
      this.toggleMap = '#toggle-map';
      this.viewContent = '.stores-list-map .view-content';
      this.fields = this.viewContent + ' .views-row';
      this.formWrapper = '#views-exposed-form-store-lookup-by-address-mobile';
    }
  }


  MobileStoresMap.prototype.init = function() {
    if ('map' in this && $(this.viewContent).length) {
      this.initCenter();
      this.initMapToggle();
      this.initCanvas();
      this.initMap();
      this.setCurrent();
      this.setMarkers();
    }
  };


  MobileStoresMap.prototype.getLocation = function() {
    if (!$.cookie('geo_location') && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(pos) {
        $.cookie('geo_location', pos.coords.latitude + ',' + pos.coords.longitude);
        !$('#edit-field-geo-data-latlon-address').val() && MobileStoresMap.prototype.refreshStoresList(pos.coords.latitude, pos.coords.longitude);
      }, function() {
        $.cookie('geo_location', '');
      }, {
        timeout: 5000,
        enableHighAccuracy: true
      });
    }
  };


  MobileStoresMap.prototype.initCenter = function() {
    this.center = Drupal.settings.mobileStores.center;
    if ($.cookie('geo_location')) {
      var position = $.cookie('geo_location').split(",");
      this.center.lat = position[0];
      this.center.lon = position[1];
    }
  };

  MobileStoresMap.prototype.initMapToggle = function() {
    var self = this
      , toggleMap = $(this.toggleMap);
    toggleMap.length || (toggleMap = $('<a/>').attr({
      id: 'toggle-map',
      href: ''
    }).text(Drupal.t('Hide Map')).appendTo($(this.formWrapper)));
    toggleMap.unbind('click').click(function() {
      var target = $(self.canvas);
      if ($(this).data('expanded')) {
        target.stop(true, true).slideUp();
        $(this).addClass('expanded').text(Drupal.t('Show Map')).data('expanded', false);
      }
      else {
        target.stop(true, true).slideDown();
        $(this).removeClass('expanded').text(Drupal.t('Hide Map')).data('expanded', true);
      }
      return false;
    }).data('expanded', true);
  };


  MobileStoresMap.prototype.initCanvas = function() {
    var canvas = $(this.canvas);
    canvas.length || (canvas = $('<div/>').attr('id', this.canvas.substring(1)).insertBefore(this.viewContent));
    canvas.height(canvas.width() * .7);
  };


  MobileStoresMap.prototype.initMap = function() {
    var bounds = Drupal.settings.mobileStores.bounds
      , sw = new google.maps.LatLng(bounds.bottom, bounds.left)
      , ne = new google.maps.LatLng(bounds.top, bounds.right);
    var options = {
      center: new google.maps.LatLng(this.center.lat, this.center.lon),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      draggable: true
    };
    this.map = new google.maps.Map($(this.canvas)[0], options);
    this.map.fitBounds(new google.maps.LatLngBounds(sw, ne));
  };


  MobileStoresMap.prototype.setCurrent = function() {
    if (Drupal.settings.mobileStores.showUser) {
      var position = new google.maps.LatLng(this.center.lat, this.center.lon);
      var marker = {
        map: this.map,
        position: position,
        title: Drupal.t('Your Location'),
        icon: new google.maps.MarkerImage(this.getMarker(.64, '0000FF', 12, '')),
        zIndex: 1000
      };
      new google.maps.Marker(marker);
    }
  };


  MobileStoresMap.prototype.setMarkers = function() {
    var self = this
      , markers = Drupal.settings.mobileStores.markers
      , fields = $(this.fields)
      , i = 0;
    var infoBubble = null;
    var bounds = new google.maps.LatLngBounds();
    markers.forEach(function(value) {
      var position = new google.maps.LatLng(value.lat, value.lon);
      bounds.extend(position);
      var ord = Math.floor(i / 26);
      var c = ord ? String.fromCharCode(64 + ord) : '';
      c += String.fromCharCode(65 + i % 26);
      var marker = new google.maps.Marker({
        map: self.map,
        position: position,
        icon: new google.maps.MarkerImage(self.getMarker(.64, 'FC7B70', 13, c))
      });
      $('.marker', fields).eq(i++).empty().append('<img src="' + self.getMarker(2, 'FC7B70', 24, c) + '"/>');
      google.maps.event.addListener(marker, 'click', function() {
        if (infoBubble) {
          infoBubble.close();
        }
        infoBubble = new google.maps.InfoWindow({
          content: value.content
        });
        infoBubble.open(self.map,marker);
      });
    });
    this.map.fitBounds(bounds);
  };


  MobileStoresMap.prototype.getMarker = function(scale, color, fontSize, text) {
    var marker = 'https://chart.googleapis.com/chart?chst=d_map_spin&chld='
      + scale + '|0|' + color + '|' + fontSize + '|b|' + text;
    return marker;
  };


  Drupal.behaviors.refreshStoresMap = {
    attach: function() {
      new MobileStoresMap().init();
    }
  };


  $(function() {
    MobileStoresMap.prototype.getLocation();
  });


  MobileStoresMap.prototype.refreshStoresList = function(lat, lon) {
    $('#edit-field-geo-data-latlon-lat').val(lat);
    $('#edit-field-geo-data-latlon-lng').val(lon);
    $('#edit-submit-store-lookup-by-address').click();
  }

})(jQuery);
