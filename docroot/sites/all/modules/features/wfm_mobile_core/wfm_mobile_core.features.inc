<?php
/**
 * @file
 * wfm_mobile_core.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wfm_mobile_core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wfm_mobile_core_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function wfm_mobile_core_image_default_styles() {
  $styles = array();

  // Exported image style: mobile_100x100.
  $styles['mobile_100x100'] = array(
    'label' => 'mobile_100x100',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: mobile_120x60.
  $styles['mobile_120x60'] = array(
    'label' => 'mobile_120x60',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 120,
          'height' => 60,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: mobile_300x190.
  $styles['mobile_300x190'] = array(
    'label' => 'mobile_300x190',
    'effects' => array(
      53 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 190,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: mobile_585x390.
  $styles['mobile_585x390'] = array(
    'label' => 'mobile_585x390',
    'effects' => array(
      54 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 585,
          'height' => 390,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: mobile_60x60.
  $styles['mobile_60x60'] = array(
    'label' => 'mobile_60x60',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 60,
          'height' => 60,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_styles_default_styles().
 */
function wfm_mobile_core_styles_default_styles() {
  $styles = array();

  // Exported style: mobile_100x100

  $styles['file']['styles']['mobile_100x100'] = array(
    'label' => 'mobile_100x100',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'mobile_100x100' => array(
          'default preset' => 'original',
          'preset' => 'mobile_100x100',
        ),
      ),
      'audio' => array(
        'mobile_100x100' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'mobile_100x100' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'mobile_100x100' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'mobile_100x100' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: mobile_120x60

  $styles['file']['styles']['mobile_120x60'] = array(
    'label' => 'mobile_120x60',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'mobile_120x60' => array(
          'default preset' => 'original',
          'preset' => 'mobile_120x60',
        ),
      ),
      'audio' => array(
        'mobile_120x60' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'mobile_120x60' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'mobile_120x60' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'mobile_120x60' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: mobile_300x190

  $styles['file']['styles']['mobile_300x190'] = array(
    'label' => 'mobile_300x190',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'mobile_300x190' => array(
          'default preset' => 'original',
          'preset' => 'mobile_300x190',
        ),
      ),
      'audio' => array(
        'mobile_300x190' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'mobile_300x190' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'mobile_300x190' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'mobile_300x190' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: mobile_585x390

  $styles['file']['styles']['mobile_585x390'] = array(
    'label' => 'mobile_585x390',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'mobile_585x390' => array(
          'default preset' => 'original',
          'preset' => 'mobile_585x390',
        ),
      ),
      'audio' => array(
        'mobile_585x390' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'mobile_585x390' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'mobile_585x390' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'mobile_585x390' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: mobile_60x60

  $styles['file']['styles']['mobile_60x60'] = array(
    'label' => 'mobile_60x60',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'mobile_60x60' => array(
          'default preset' => 'original',
          'preset' => 'mobile_60x60',
        ),
      ),
      'audio' => array(
        'mobile_60x60' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'mobile_60x60' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'mobile_60x60' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'mobile_60x60' => array(
          'default preset' => 'unlinked_thumbnail',
          'preset' => 'linked_mobile_60x60',
        ),
      ),
    ),
  );
  return $styles;
}

/**
 * Implements hook_styles_default_presets_alter().
 */
function wfm_mobile_core_styles_default_presets_alter(&$presets) {
  $styles = styles_default_styles();
  if ($styles['file']['styles']['mobile_100x100']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['mobile_100x100'] = array(
      'default preset' => 'original',
      'preset' => 'mobile_100x100',
    );

    $presets['file']['containers']['audio']['styles']['mobile_100x100'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['mobile_100x100'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['mobile_100x100'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['mobile_100x100'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['file']['styles']['mobile_120x60']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['mobile_120x60'] = array(
      'default preset' => 'original',
      'preset' => 'mobile_120x60',
    );

    $presets['file']['containers']['audio']['styles']['mobile_120x60'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['mobile_120x60'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['mobile_120x60'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['mobile_120x60'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['file']['styles']['mobile_300x190']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['mobile_300x190'] = array(
      'default preset' => 'original',
      'preset' => 'mobile_300x190',
    );

    $presets['file']['containers']['audio']['styles']['mobile_300x190'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['mobile_300x190'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['mobile_300x190'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['mobile_300x190'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['file']['styles']['mobile_585x390']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['mobile_585x390'] = array(
      'default preset' => 'original',
      'preset' => 'mobile_585x390',
    );

    $presets['file']['containers']['audio']['styles']['mobile_585x390'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['mobile_585x390'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['mobile_585x390'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['mobile_585x390'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['file']['styles']['mobile_60x60']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['mobile_60x60'] = array(
      'default preset' => 'original',
      'preset' => 'mobile_60x60',
    );

    $presets['file']['containers']['audio']['styles']['mobile_60x60'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['mobile_60x60'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['mobile_60x60'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['mobile_60x60'] = array(
      'default preset' => 'unlinked_thumbnail',
      'preset' => 'linked_mobile_60x60',
    );

  }
}
