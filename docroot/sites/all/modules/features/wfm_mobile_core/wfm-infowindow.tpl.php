<h3><?php print $variables['store_name']; ?></h3>
<?php if($variables['thoroughfare']): ?>
  <?php print $variables['thoroughfare']; ?>
<?php endif; ?>
<?php if($variables['premise']): ?>
  <br><?php print $variables['premise']; ?>
<?php endif; ?>
  <br><?php print $variables['locality'] . ', '. $variables['administrative_area'] . ' ' . $variables['postal_code']; ?>
<?php if($variables['field_phone_number']): ?>
  <br><b><?php print t('Phone'); ?>: </b><?php print $variables['field_phone_number']; ?>
<?php endif; ?>
<?php if($variables['field_store_hours']): ?>
  <br><b><?php print t('Hours'); ?>: </b><?php print $variables['field_store_hours']; ?>
<?php endif; ?>