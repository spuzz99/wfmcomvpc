<?php


/**
 * Form constructor for the Blogs RSS Headers form.
 */
function blogs_rss_headers_form($form, &$form_state) {
  $settings = variable_get('blogs_rss_feeds_settings', array());
  $url = isset($settings['default']['url']) ? $settings['default']['url'] : '[blog-id]';
  $url = explode('[blog-id]', $url);

  $form['default'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['default']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Feed title'),
    '#default_value' => isset($settings['default']['title']) ? $settings['default']['title'] : '',
    '#size' => 67,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('e.g "Whole Foods Market Blog RSS"'),
  );

  $form['default']['label'] = array(
    '#type' => 'item',
    '#title' => t('Blog feed URL'),
    '#value' => '',
  );

  $form['default']['url_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#title_display' => 'invisible',
    '#default_value' => $url[0],
    '#size' => 28,
    '#maxlength' => 64,
    '#required' => TRUE,
  );

  $form['default']['blog_id'] = array(
    '#markup' => '<div class="form-item form-item-blog-id"><span>[blog-id]</span></div>',
  );

  $form['default']['url_suffix'] = array(
    '#type' => 'textfield',
    '#default_value' => $url[1],
    '#size' => 28,
    '#maxlength' => 64,
  );

  $form['default']['description'] = array(
    '#markup' => '<div class="blog-url-description description">'
      . t('e.g "http://wholefoodsmarket.com/blogs/[blog-id]/all/rss.xml"') . '</div>',
  );

  $blogs = blogs_blog_list();

  $form['blogs'] = array(
    '#type' => 'value',
    '#value' => $blogs,
  );

  foreach ($blogs as $id => $title) {
    $form['container-' . $id] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('blog-wrapper'),
      ),
    );

    $form['container-' . $id]['override-' . $id] = array(
      '#type' => 'checkbox',
      '#title' => $title,
      '#default_value' => isset($settings[$id]) ? TRUE : FALSE,
    );

    $form['container-' . $id]['default-' . $id] = array(
      '#type' => 'item',
      '#title' => t('Default'),
      '#states' => array(
        'visible' => array('input[name="override-' . $id . '"]' => array('checked' => FALSE)),
      ),
    );

    $form['container-' . $id]['title-' . $id] = array(
      '#type' => 'textfield',
      '#title' => 'Feed title',
      '#default_value' => isset($settings[$id]['title']) ? $settings[$id]['title'] : '',
      '#size' => 40,
      '#maxlength' => 128,
      '#attributes' => array(
        'class' => array('blog-feed-title'),
      ),
      '#states' => array(
        'visible' => array('input[name="override-' . $id . '"]' => array('checked' => TRUE)),
      ),
    );

    $form['container-' . $id]['url-' . $id] = array(
      '#type' => 'textfield',
      '#title' => 'Feed URL',
      '#default_value' => isset($settings[$id]['url']) ? $settings[$id]['url'] : '',
      '#size' => 40,
      '#maxlength' => 128,
      '#attributes' => array(
        'class' => array('blog-feed-url'),
      ),
      '#states' => array(
        'visible' => array('input[name="override-' . $id . '"]' => array('checked' => TRUE)),
      ),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'blogs') . '/includes/blogs.admin.css',
  );

  return $form;
}


/**
 * Form validation handler for blogs_rss_headers_form().
 */
function blogs_rss_headers_form_validate($form, &$form_state) {
  $data = $form_state['values'];
  foreach ($data['blogs'] as $id => $title) {
    if ($data['override-' . $id]) {
      if (!$data['title-' . $id]) {
        form_set_error('title-' . $id, t('Feed title field is required for "!blog" blog.', array(
          '!blog' => $title,
        )));
      }
      if (!$data['url-' . $id]) {
        form_set_error('url-' . $id, t('Feed URL field is required for "!blog" blog.', array(
          '!blog' => $title,
        )));
      }
    }
  }
}


/**
 * Form submission handler for blogs_rss_headers_form().
 */
function blogs_rss_headers_form_submit($form, &$form_state) {
  $data = $form_state['values'];
  $settings = array(
    'default' => array(
      'title' => $data['title'],
      'url' => $data['url_prefix'] . '[blog-id]' . $data['url_suffix'],
    ),
  );

  foreach ($data['blogs'] as $id => $title) {
    if ($data['override-' . $id]) {
      $settings[$id] = array(
        'title' => $data['title-' . $id],
        'url' => $data['url-' . $id],
      );
    }
  }

  variable_set('blogs_rss_feeds_settings', $settings);
  drupal_set_message(t('Blog RSS feeds settings have been saved.'));
}


/**
 * Returns a list of national offices.
 */
function blogs_blog_list() {
  $data = db_select('node', 'N')
    ->fields('N', array('nid', 'title'))
    ->condition('N.type', 'blog')
    ->condition('N.status', 1)
    ->orderBy('N.title', 'ASC')
    ->execute()
    ->fetchAllKeyed();

  return $data;
}
