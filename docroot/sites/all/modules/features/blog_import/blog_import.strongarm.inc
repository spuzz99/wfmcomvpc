<?php
/**
 * @file
 * blog_import.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function blog_import_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_attachment_field';
  $strongarm->value = 'field_blog_media';
  $export['wordpress_migrate_attachment_field'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_category_vocabulary';
  $strongarm->value = 'tags';
  $export['wordpress_migrate_category_vocabulary'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_drush';
  $strongarm->value = '';
  $export['wordpress_migrate_drush'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_generate_redirects';
  $strongarm->value = 0;
  $export['wordpress_migrate_generate_redirects'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_import_method';
  $strongarm->value = '0';
  $export['wordpress_migrate_import_method'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_notification';
  $strongarm->value = '0';
  $export['wordpress_migrate_notification'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_notification_body';
  $strongarm->value = 'Your WordPress import is complete! Any messages generated are below.

!output';
  $export['wordpress_migrate_notification_body'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_notification_failure_body';
  $strongarm->value = 'Your WordPress import failed. Any messages generated are below.\\n\\n!output';
  $export['wordpress_migrate_notification_failure_body'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_notification_subject';
  $strongarm->value = 'Wordpress import status';
  $export['wordpress_migrate_notification_subject'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_page_type';
  $strongarm->value = 'page';
  $export['wordpress_migrate_page_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_path_action';
  $strongarm->value = '1';
  $export['wordpress_migrate_path_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_podcast_field';
  $strongarm->value = '';
  $export['wordpress_migrate_podcast_field'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_post_type';
  $strongarm->value = 'blog_post';
  $export['wordpress_migrate_post_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_tag_vocabulary';
  $strongarm->value = '';
  $export['wordpress_migrate_tag_vocabulary'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_text_format';
  $strongarm->value = 'full_html';
  $export['wordpress_migrate_text_format'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'wordpress_migrate_blog_class';
  $strongarm->value = 'BlogImportBlog';
  $export['wordpress_migrate_blog_class'] = $strongarm;

  return $export;
}
