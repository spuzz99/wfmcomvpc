<?php
/**
 * @file
 * gift_boxes.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function gift_boxes_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_gift_box_basics|node|gift_box|form';
  $field_group->group_name = 'group_gift_box_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'gift_box';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_gift_box';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '12',
    'children' => array(
      0 => 'body',
      1 => 'field_order_url',
      2 => 'field_sold_out',
      3 => 'field_video',
      4 => 'locations_acl',
      5 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_gift_box_basics|node|gift_box|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_gift_box|node|gift_box|form';
  $field_group->group_name = 'group_gift_box';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'gift_box';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Gift Box',
    'weight' => '0',
    'children' => array(
      0 => 'group_gift_box_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_gift_box|node|gift_box|form'] = $field_group;

  return $export;
}
