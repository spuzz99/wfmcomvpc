<?php
/**
 * @file
 * related_content_views.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function related_content_views_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_related_image__file_field_styles_file_related_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_related_image__file_field_styles_file_related_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_square_55x55__file_field_styles_file_square_55x55';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_square_55x55__file_field_styles_file_square_55x55'] = $file_display;

  return $export;
}
