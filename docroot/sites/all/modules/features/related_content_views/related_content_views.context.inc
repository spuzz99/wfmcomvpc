<?php
/**
 * @file
 * related_content_views.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function related_content_views_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'hsh_sidebar_blog_posts';
  $context->description = 'Places recent HSH blog posts in the HSH page sidebars.';
  $context->tag = 'Healthy Eating';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'healthy-eating/health-starts-here*' => 'healthy-eating/health-starts-here*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-related_content_views-block' => array(
          'module' => 'views',
          'delta' => 'related_content_views-block',
          'region' => 'sidebar_second',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Healthy Eating');
  t('Places recent HSH blog posts in the HSH page sidebars.');
  $export['hsh_sidebar_blog_posts'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'parents_kids_sidebar';
  $context->description = '';
  $context->tag = 'Healthy Eating';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'healthy-eating/parents-kids*' => 'healthy-eating/parents-kids*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-related_content_views-block_1' => array(
          'module' => 'views',
          'delta' => 'related_content_views-block_1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Healthy Eating');
  $export['parents_kids_sidebar'] = $context;

  return $export;
}
