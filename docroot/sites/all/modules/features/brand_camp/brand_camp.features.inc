<?php
/**
 * @file
 * brand_camp.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function brand_camp_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "video_embed_field" && $api == "default_video_embed_styles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function brand_camp_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function brand_camp_image_default_styles() {
  $styles = array();

  // Exported image style: bc_image_340x270.
  $styles['bc_image_340x270'] = array(
    'label' => 'bc_image_340x270',
    'effects' => array(
      53 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 340,
          'height' => 270,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: bc_image_340x340.
  $styles['bc_image_340x340'] = array(
    'label' => 'bc_image_340x340',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 340,
          'height' => 340,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: bc_image_640x510.
  $styles['bc_image_640x510'] = array(
    'label' => 'bc_image_640x510',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 510,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: bc_image_640x640.
  $styles['bc_image_640x640'] = array(
    'label' => 'bc_image_640x640',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 640,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function brand_camp_node_info() {
  $items = array(
    'bc_article' => array(
      'name' => t('Brand Camp Article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bc_connect' => array(
      'name' => t('Brand Camp Special'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bc_coupon' => array(
      'name' => t('Brand Camp Coupon'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bc_image' => array(
      'name' => t('Brand Camp Image'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bc_ugc_image' => array(
      'name' => t('Brand Camp UGC image'),
      'base' => 'node_content',
      'description' => t('This content type used for downloading images data from Chute via API.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bc_video' => array(
      'name' => t('Brand Camp Video'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'chute_url' => array(
      'name' => t('Chute URL'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
