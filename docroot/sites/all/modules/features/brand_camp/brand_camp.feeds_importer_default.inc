<?php
/**
 * @file
 * brand_camp.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function brand_camp_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = TRUE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'chute_image_imprter';
  $feeds_importer->config = array(
    'name' => 'Chute image importer',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '$.data.*',
        'sources' => array(
          'jsonpath_parser:0' => 'id',
          'jsonpath_parser:2' => 'caption',
          'jsonpath_parser:1' => 'caption',
          'jsonpath_parser:3' => 'username',
          'jsonpath_parser:4' => 'service',
          'jsonpath_parser:5' => 'source.import_url',
          'jsonpath_parser:6' => 'tags',
          'jsonpath_parser:7' => 'thumbnail',
          'jsonpath_parser:8' => 'source.source_url',
          'jsonpath_parser:9' => 'hearts',
          'jsonpath_parser:10' => 'album_id',
          'jsonpath_parser:11' => 'id',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:0' => 0,
            'jsonpath_parser:2' => 0,
            'jsonpath_parser:1' => 0,
            'jsonpath_parser:3' => 0,
            'jsonpath_parser:4' => 0,
            'jsonpath_parser:5' => 0,
            'jsonpath_parser:6' => 0,
            'jsonpath_parser:7' => 0,
            'jsonpath_parser:8' => 0,
            'jsonpath_parser:9' => 0,
            'jsonpath_parser:10' => 0,
            'jsonpath_parser:11' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'body',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'title',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'field_bc_src_username',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'jsonpath_parser:4',
            'target' => 'field_bc_src_service',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'jsonpath_parser:5',
            'target' => 'field_bc_src_import_url:url',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'jsonpath_parser:6',
            'target' => 'field_bc_chute_tags',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          7 => array(
            'source' => 'jsonpath_parser:7',
            'target' => 'field_bc_src_thumbnail_url:url',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'jsonpath_parser:8',
            'target' => 'field_bc_src_image_url:url',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'jsonpath_parser:9',
            'target' => 'field_bc_src_likes_count',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'jsonpath_parser:10',
            'target' => 'field_bc_album_id',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'jsonpath_parser:11',
            'target' => 'field_bc_asset_id',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'bc_ugc_image',
      ),
    ),
    'content_type' => 'chute_url',
    'update' => 0,
    'import_period' => '900',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['chute_image_imprter'] = $feeds_importer;

  return $export;
}
