<?php
/**
 * @file
 * brand_camp.box.inc
 */

/**
 * Implements hook_default_box().
 */
function brand_camp_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'bc_mobile_back_to_top';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Brand Camp: Mobile - Back to Top';
  $box->options = array(
    'body' => array(
      'value' => '<p class="backtotop"><a href="#page-wrapper">Back To Top</a></p>',
      'format' => 'source_editor',
    ),
    'additional_classes' => '',
  );
  $export['bc_mobile_back_to_top'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'top_text_bc_ugc_image_mobile';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Brand Camp: Top text for bc_ugc_image page mobile';
  $box->options = array(
    'body' => array(
      'value' => '<h2>VALUES MATTER<br />YOUR STORIES</h2>
<div class="isoFilters mobile">
  <div class="ugc isoFilters-widget">
    <div class="title">View</div>
    <div class="filters" data-filter="ugc">
      <div class="filter" data-filter="0"><a href="values-matter">Our Stories</a></div>
      <div class="filter" data-filter="1"><a href="values-matter/your-stories">Your Stories</a></div>
    </div>
  </div>
  <div class="ugc-search">
    <div class="title">seach by @name</div>
    <form action="values-matter/your-stories" id="ugc-search-form" method="get"><input id="ugc-search-value" name="name" type="text" value=""><input id="ugc-single-search-submit" type="submit" value="FIND IT!">&nbsp;</form>
  </div>
  <p class="backbutton"><a href="/values-matter/your-stories" onclick="history.go(-1);return false;">Go Back</a></p>
</div>
<style type="text/css">
  #boxes-box-top_text_bc_ugc_image_mobile { background: url("//assets.wholefoodsmarket.com/www/misc/brand/your-stories-background-640x640.jpg") top left no-repeat; 
  }
</style>  
',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['top_text_bc_ugc_image_mobile'] = $box;

  return $export;
}
