<?php
/**
 * @file
 * brand_camp.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function brand_camp_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'brand_camp_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Brand Camp List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Values Matter';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'field_bc_showme_value' => array(
      'bef_format' => 'bef',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => 'All',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'field_bc_iwantto_value' => array(
      'bef_format' => 'bef',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => 'Both',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'field_bc_ihave_value' => array(
      'bef_format' => 'bef',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => 'All',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'full_html';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Weight (field_bc_weight) */
  $handler->display->display_options['sorts']['field_bc_weight_value']['id'] = 'field_bc_weight_value';
  $handler->display->display_options['sorts']['field_bc_weight_value']['table'] = 'field_data_field_bc_weight';
  $handler->display->display_options['sorts']['field_bc_weight_value']['field'] = 'field_bc_weight_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'bc_article' => 'bc_article',
    'bc_coupon' => 'bc_coupon',
    'bc_image' => 'bc_image',
    'bc_video' => 'bc_video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Choose filter options for &quot;Show Me&quot; category (field_bc_showme) */
  $handler->display->display_options['filters']['field_bc_showme_value']['id'] = 'field_bc_showme_value';
  $handler->display->display_options['filters']['field_bc_showme_value']['table'] = 'field_data_field_bc_showme';
  $handler->display->display_options['filters']['field_bc_showme_value']['field'] = 'field_bc_showme_value';
  $handler->display->display_options['filters']['field_bc_showme_value']['group'] = 1;
  $handler->display->display_options['filters']['field_bc_showme_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['operator_id'] = 'field_bc_showme_value_op';
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['label'] = 'Show Me';
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['operator'] = 'field_bc_showme_value_op';
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['identifier'] = 'field_bc_showme_value';
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: Choose filter for &quot;I Want To&quot; category (field_bc_iwantto) */
  $handler->display->display_options['filters']['field_bc_iwantto_value']['id'] = 'field_bc_iwantto_value';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['table'] = 'field_data_field_bc_iwantto';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['field'] = 'field_bc_iwantto_value';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['group'] = 1;
  $handler->display->display_options['filters']['field_bc_iwantto_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['operator_id'] = 'field_bc_iwantto_value_op';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['label'] = 'I Want To';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['operator'] = 'field_bc_iwantto_value_op';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['identifier'] = 'field_bc_iwantto_value';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    6 => 0,
    3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'bc_article' => 'bc_article',
    'bc_coupon' => 'bc_coupon',
    'bc_image' => 'bc_image',
    'bc_ugc_image' => 'bc_ugc_image',
    'bc_video' => 'bc_video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['path'] = 'values-matter';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['not'] = TRUE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'bc_article' => 'bc_article',
    'bc_coupon' => 'bc_coupon',
    'bc_image' => 'bc_image',
    'bc_video' => 'bc_video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Block 2 */
  $handler = $view->new_display('block', 'Block 2', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['relationship'] = 'field_bc_nodes_nid';
  $handler->display->display_options['row_options']['links'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<div class="custom-load-more">
<a href="#">Load More</a>
</div>';
  $handler->display->display_options['footer']['area']['format'] = 'source_editor';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Nodes (field_bc_nodes) */
  $handler->display->display_options['relationships']['field_bc_nodes_nid']['id'] = 'field_bc_nodes_nid';
  $handler->display->display_options['relationships']['field_bc_nodes_nid']['table'] = 'field_data_field_bc_nodes';
  $handler->display->display_options['relationships']['field_bc_nodes_nid']['field'] = 'field_bc_nodes_nid';
  $handler->display->display_options['relationships']['field_bc_nodes_nid']['required'] = TRUE;
  $handler->display->display_options['relationships']['field_bc_nodes_nid']['delta'] = '-1';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Nodes (field_bc_nodes:delta) */
  $handler->display->display_options['sorts']['delta']['id'] = 'delta';
  $handler->display->display_options['sorts']['delta']['table'] = 'field_data_field_bc_nodes';
  $handler->display->display_options['sorts']['delta']['field'] = 'delta';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'field_bc_nodes_nid';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['relationship'] = 'field_bc_nodes_nid';
  $handler->display->display_options['filters']['type']['value'] = array(
    'bc_article' => 'bc_article',
    'bc_coupon' => 'bc_coupon',
    'bc_image' => 'bc_image',
    'bc_video' => 'bc_video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Mobile */
  $handler = $view->new_display('page', 'Mobile', 'mobile');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'load_more';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['effects']['speed'] = 'slow';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Choose filter options for &quot;Show Me&quot; category (field_bc_showme) */
  $handler->display->display_options['filters']['field_bc_showme_value']['id'] = 'field_bc_showme_value';
  $handler->display->display_options['filters']['field_bc_showme_value']['table'] = 'field_data_field_bc_showme';
  $handler->display->display_options['filters']['field_bc_showme_value']['field'] = 'field_bc_showme_value';
  $handler->display->display_options['filters']['field_bc_showme_value']['group'] = 1;
  $handler->display->display_options['filters']['field_bc_showme_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['operator_id'] = 'field_bc_showme_value_op';
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['label'] = 'Show Me';
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['operator'] = 'field_bc_showme_value_op';
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['identifier'] = 'field_bc_showme_value';
  $handler->display->display_options['filters']['field_bc_showme_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: Choose filter for &quot;I Want To&quot; category (field_bc_iwantto) */
  $handler->display->display_options['filters']['field_bc_iwantto_value']['id'] = 'field_bc_iwantto_value';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['table'] = 'field_data_field_bc_iwantto';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['field'] = 'field_bc_iwantto_value';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['group'] = 1;
  $handler->display->display_options['filters']['field_bc_iwantto_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['operator_id'] = 'field_bc_iwantto_value_op';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['label'] = 'I Want To';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['operator'] = 'field_bc_iwantto_value_op';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['identifier'] = 'field_bc_iwantto_value';
  $handler->display->display_options['filters']['field_bc_iwantto_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    6 => 0,
    3 => 0,
  );
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'bc_article' => 'bc_article',
    'bc_coupon' => 'bc_coupon',
    'bc_image' => 'bc_image',
    'bc_ugc_image' => 'bc_ugc_image',
    'bc_video' => 'bc_video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['type']['group_info']['label'] = 'View';
  $handler->display->display_options['filters']['type']['group_info']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['group_info']['widget'] = 'radios';
  $handler->display->display_options['filters']['type']['group_info']['default_group'] = '1';
  $handler->display->display_options['filters']['type']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'All',
      'operator' => 'in',
      'value' => array(
        'bc_article' => 'bc_article',
        'bc_coupon' => 'bc_coupon',
        'bc_image' => 'bc_image',
        'bc_ugc_image' => 'bc_ugc_image',
        'bc_video' => 'bc_video',
        'all' => 0,
        'blog' => 0,
        'blog_post' => 0,
        'bc_connect' => 0,
        'chute_url' => 0,
        'core_value' => 0,
        'department' => 0,
        'department_article' => 0,
        'event' => 0,
        'faq' => 0,
        'job' => 0,
        'local_vendor' => 0,
        'marquee' => 0,
        'metro' => 0,
        'national_offices' => 0,
        'newsletter' => 0,
        'page' => 0,
        'person' => 0,
        'product' => 0,
        'product_certification' => 0,
        'product_line' => 0,
        'product_recall' => 0,
        'promo' => 0,
        'recipe' => 0,
        'region' => 0,
        'service' => 0,
        'special_diet' => 0,
        'store' => 0,
        'video' => 0,
        'webform' => 0,
      ),
    ),
    2 => array(
      'title' => 'Our Stories',
      'operator' => 'in',
      'value' => array(
        'bc_article' => 'bc_article',
        'bc_coupon' => 'bc_coupon',
        'bc_image' => 'bc_image',
        'bc_video' => 'bc_video',
        'all' => 0,
        'blog' => 0,
        'blog_post' => 0,
        'bc_connect' => 0,
        'bc_ugc_image' => 0,
        'chute_url' => 0,
        'core_value' => 0,
        'department' => 0,
        'department_article' => 0,
        'event' => 0,
        'faq' => 0,
        'job' => 0,
        'local_vendor' => 0,
        'marquee' => 0,
        'metro' => 0,
        'national_offices' => 0,
        'newsletter' => 0,
        'page' => 0,
        'person' => 0,
        'product' => 0,
        'product_certification' => 0,
        'product_line' => 0,
        'product_recall' => 0,
        'promo' => 0,
        'recipe' => 0,
        'region' => 0,
        'service' => 0,
        'special_diet' => 0,
        'store' => 0,
        'video' => 0,
        'webform' => 0,
      ),
    ),
    3 => array(
      'title' => 'Your Stories',
      'operator' => 'in',
      'value' => array(
        'bc_ugc_image' => 'bc_ugc_image',
        'all' => 0,
        'blog' => 0,
        'blog_post' => 0,
        'bc_article' => 0,
        'bc_coupon' => 0,
        'bc_image' => 0,
        'bc_connect' => 0,
        'bc_video' => 0,
        'chute_url' => 0,
        'core_value' => 0,
        'department' => 0,
        'department_article' => 0,
        'event' => 0,
        'faq' => 0,
        'job' => 0,
        'local_vendor' => 0,
        'marquee' => 0,
        'metro' => 0,
        'national_offices' => 0,
        'newsletter' => 0,
        'page' => 0,
        'person' => 0,
        'product' => 0,
        'product_certification' => 0,
        'product_line' => 0,
        'product_recall' => 0,
        'promo' => 0,
        'recipe' => 0,
        'region' => 0,
        'service' => 0,
        'special_diet' => 0,
        'store' => 0,
        'video' => 0,
        'webform' => 0,
      ),
    ),
  );
  $handler->display->display_options['path'] = 'no/path';
  $export['brand_camp_list'] = $view;

  return $export;
}
