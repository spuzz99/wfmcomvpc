<?php

$lists = $list;
$listShown = FALSE;
$classRightList = '';
$itemListSortBy = (isset($_GET['sort_by']) && $_GET['sort_by'] == 'category' ? 'category' : 'latest');

if (!isset($_GET['show_list'])) {
  $default_card = 'none';
  $classRightList = 'default';
}
else {
  $default_card = $_GET['show_list'];
}

$emailLink = '/modals/no_js/email_shopping_list/email/_id/' . $itemListSortBy;
$printLink = '/print/action/shopping_list/print/_id/' . $itemListSortBy;
$shareLink = '/modals/no_js/email_shopping_list/share/_id/' . $itemListSortBy;
$smsLink = '/modals/no_js/shopping_list/sms/_id/' . $itemListSortBy;

//List message
$noneMsg = t('Select a list to edit or share');

if (empty($lists)) {
  $noneMsg = t('You have no lists.');
}

$sharedByMe = WFMShare::sharedByMe();
$sharedToMe = WFMShare::sharedToMe();
$listsSharedWithMe = array();
?>
<div class="left">
  <h2><?php print t('My Lists') ?></h2>
  <ul class="list_links">
    <?php
    foreach ($lists as $list):
      if (isset($sharedByMe->ids[$list->_id])) {
        $shared = ' (' . t('shared') . ')';
      }
      else {
        $shared = '';
      }

      if (isset($sharedToMe->ids[$list->_id])) {
        $listsSharedWithMe[] = $list;
        continue; //Don't output this here
      }

      $linkPath = $_SERVER['REQUEST_URI'] . '#list';
      $linkName = check_plain($list->name . $shared);
    ?>
    <li>
      <a href="<?php print $linkPath; ?>" ref="<?php print $list->_id; ?>" class="list_link"><?php print $linkName; ?></a>
    </li>
    <?php endforeach; ?>
  </ul>

  <?php print l('+ ' . t('Create a new list'), '', array('attributes' => array('ref' => 'add', 'class' => array('list_link')))); ?>

  <?php if (!empty($listsSharedWithMe)): ?>
  <h2><?php print t('Shared With Me') ?></h2>
  <ul class="list_links">
    <?php
    foreach ($listsSharedWithMe as $list):
      $linkPath = $_SERVER['REQUEST_URI'] . '#list';
      $linkName = check_plain($list->name);
    ?>
    <li>
      <a href="<?php print $linkPath; ?>" ref="<?php print $list->id; ?>" class="list_link"><?php print $linkName; ?></a>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
</div>

<div class="right">
  <div class="card">
    <div class="top">
      <h2>
        <div class="edit-link-title">
          <?php print t('Edit List'); ?>
        </div>

        <!-- SubTitle List BEGIN -->
        <div class="edit-link-subtitle">
          <?php
          foreach ($lists as $list) {
            $defClass = '';

            if (isset($list->_id) && $list->_id == $default_card) {
              $defClass = ' default';
              $listShown = $list->id;
            }

            $delAttr = array(
              'attributes' => array(
                'title' => t('Delete This List'),
                'alt' => t('Delete This List'),
                'class' => array('delete_link'),
              ),
            );

            $URLAttr = array(
              'query' => array(
                'destination' => "user/shopping_lists?show_list={$list->_id}",
              ),
              'absolute' => TRUE,
            );
            $baseURL = "/user/shopping_lists?show_list={$list->_id}";

            if (isset($_GET['sort_by']) && $_GET['sort_by']) {
              $baseURL .= "&sort_by=$itemListSortBy";
            }

            $renameLink = "<a href='$baseURL' ref='{$list->_id}' id='rename_link' class='rename_link'>Rename</a>";
            $deleteURL = url('action/shopping_list/remove/' . $list->_id, $URLAttr);
            $subtitle = '';

            //Share idenfitication
            if (isset($sharedToMe->ids[$list->_id])) {
              foreach ($sharedToMe->shares as $share) {
                if ($share->recipient_item_id == $list->_id) {
                  $subtitle = $share->title;
                  break;
                }
              }
            }
            else if (isset($sharedByMe->ids[$list->_id])) {
              foreach ($sharedByMe->shares as $share) {
                if ($share->shared_item_id == $list->id) {
                  $otherUser = user_load_by_mail($share->recipient_email);
                  $subtitle .= 'Shared to ' . format_username($otherUser) . "\n<br>";
                  //Allow a list of these to build
                }
              }
            }
            ?>
            <!-- list-title-item-BEGIN -->
            <div ref='<?php print $list->_id; ?>' id='list-title-<?php print $list->_id; ?>' class='title<?php print $defClass; ?>'>
              <span class='list-title'>: <?php print $list->name; ?></span>

              <span class='options'>
                <input class='rename_field' name='rename_list' type='text' id='rename_list_<?php print $list->_id; ?>' />
                <?php print "$renameLink | " . l(t('Delete'), $deleteURL, $delAttr); ?>
              </span>

              <?php if (!empty($subtitle)): ?>
                <span class='list-subtitle'><?php print $subtitle; ?></span>
              <?php endif; ?>
            </div>
            <!-- list-title-item-END -->
            <?php
          }
          ?>
        </div><!-- SubTitle List END -->
        <?php
        $printURL = $printLink;
        $emailURL = $emailLink;
        $shareURL = $shareLink;
        $smsURL = $smsLink;

        if ($listShown) {
          $printURL = str_replace('_id', $listShown, $printLink);
          $emailURL = str_replace('_id', $listShown, $emailLink);
          $shareURL = str_replace('_id', $listShown, $shareLink);
          $smsURL = str_replace('_id', $listShown, $smsLink);
        }
        ?>
        <div class="right-header">
          <span class="email_link">
            <a href="<?php print $emailURL; ?>" class="email-list ctools-use-modal ctools-modal-user-medium-modal" ref="<?php print $emailLink; ?>" target="_blank"><?php print t('Email'); ?></a>
          </span>
          <span class="print_link">
            <a href="<?php print $printURL; ?>" target="_blank" ref="<?php print $printLink; ?>" target="_blank"><?php print t('Print'); ?></a>
          </span>
          <!-- Commenting out because SAGE v2 requires a Janrain access token of the particular user to access user endpoints.
          The result is that users can't share lists.
          <span class="sms_link">
            <a href="<?php print $smsURL; ?>" class="sms-list ctools-use-modal ctools-modal-user-medium-modal" ref="<?php print $smsLink; ?>" target="_blank"><?php print t('SMS'); ?></a>
          </span>
          -->
          <!--
          <span class="share_link">
            <a href="<?php print $shareURL; ?>" class="share-list ctools-use-modal" ref="<?php print $shareLink; ?>" target="_blank"><?php print t('Share'); ?></a>
          </span>
          -->
        </div>

        <div id="list-add" class="content">
          <div class="list-add-header">
            <?php print ': ' . t('Add Shopping List'); ?>
          </div>
          <?php print drupal_render($add_form); ?>
        </div>
      </h2>
    </div>
    <div id="list-none" class="content <?php print $classRightList; ?>">
      <div class="list_top">
        <p><?php print $noneMsg; ?></p>
      </div>
    </div>
    <div id="shopping-list">
      <div id="list-add-item-form">
        <?php print drupal_render($add_item_form); ?>
      </div>
<?php
foreach ($lists as $list):
  //Load and sort the shopping list items
  $items = users_get_and_sort_shopping_list_items($user->uid, $list->_id, $itemListSortBy);
  $defClass = '';

  if (isset($list->_id) && $list->_id == $default_card) {
    $defClass = ' default';
  }
  ?>

  <div ref="<?php print $list->_id; ?>" id="list-<?php print $list->_id; ?>" class="content<?php print $defClass; ?>">
    <div class="list-top-text">
      <div class="top-text-left">
        <div class="list-message">
          <?php
          if (!empty($items)) {
            echo t('Click on item name to edit');
          }
          ?>
        </div>
        <div class="list-add-item">
          <?php print l('+ ' . t('Add item'), '', array('attributes' => array('class' => 'add_item_to_list'))); ?>
        </div>
      </div>
      <div class="top-text-right">
        <div class="list-sorter">
          <?php if (!empty($items)): ?>
          <div class="sort-msg"><?php print t('Sort by:') . '&nbsp;'; ?></div>
          <div class="sort-controls">
            <?php

            $latestURL = "/user/shopping_lists?show_list={$list->_id}&sort_by=latest";
            $categoryURL = "/user/shopping_lists?show_list={$list->_id}&sort_by=category";

            $latestLink = "<a href='$latestURL' class='item-list-sort-by-latest'>Latest added</a>";
            $categoryLink = "<a href='$categoryURL' class='item-list-sort-by-category'>Category</a>";

            $latestOut = "<span class='item-list-sort-by-latest'>Latest added</span>";
            $categoryOut = "<span class='item-list-sort-by-dept'>Category</span>";

            if ($itemListSortBy == 'category') {
              $latestOut = $latestLink;
            }
            else {
              $categoryOut = $categoryLink;
            }

            print $latestOut . " | " . $categoryOut;
            ?>
          </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="list_items">
      <ul class="list_item_list">
      <?php
      foreach($items as $item) {
        $noteLink = l($item->name, '/', array('html' => TRUE, 'attributes' => array('class' => array('list_item_edit'), 'ref' => $item->_id)));
        $deleteItemIcon = "<span class='delete-item-icon'><a href='#' ref='$item->_id'>Delete</a></span>";
        $item->output = "<div class='list-item'><div class='list-item-name-desc'>";
        $item->output .= "$deleteItemIcon<div class='list-item-name'>{$noteLink}</div>";

        if ($item->note != '-1' && $item->note != '') {
          $item->output .= "<div class='list-item-desc'>{$item->note}</div>";
        }

        $item->output .= '</div><div class="list-item-category">' . $item->category . '</div></div>';
        $form = drupal_get_form('users_add_shopping_list_item_form_' . $item->_id, $item, $list->_id);
        print '<li>' . $item->output . '<div class="list_item_edit_form" ref="'
            . $item->_id . '">' . drupal_render($form) . '</div></li>';
      }
      ?>
      </ul>
    </div>
  </div>
<?php
endforeach;
?>
    </div>
  </div>
</div>
<div class="clearfix"></div>