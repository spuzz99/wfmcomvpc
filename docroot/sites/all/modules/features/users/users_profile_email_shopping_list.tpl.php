<?php
/**
 * NOTES
 */
?>
<div class="shopping-list-model-box email-modal">
  <div class="modal-title">Email list</div>
  <form id="email-box">
    <div class="email-info">
      Emailing a list sends a plain text version via email. If you would like the recipient to view and edit your list on wholefoodsmarket.com, use the Share List option instead.
    </div>

    <div class="email-to-myself">
      <span class="checkbox-col">
        <input type="checkbox" name="email_myself" value="true" />
      </span>
      <span class="email-myself-email">To: myself (<?php print $user->mail; ?>)</span>
    </div>

    <div>
      <span class="checkbox-col">
        <input type="checkbox" id="email-to-chkbx" name="email_to" value="true" />
      </span>
      <span class="email-to-email">To:&nbsp;</span>
      <span class="email-to"><input type="text" name="email_to_email" value="" /></span>
    </div>
<!--
    <div class="email-from">
      <span class="checkbox-col"></span>
      <span class="email-from-email">From:&nbsp;</span>
      <input type="text" name="email_from_email" value="<?php print $user->mail; ?>" />
    </div>
-->
    <br />
    <div class="email-buttons">
      <a href="#" class="orange-link send-mail" onclick="Drupal.CTools.Modal.dismiss();return true;"target="_blank">Send email</a>
      <a href="#" class="simple-link cancel" onclick="Drupal.CTools.Modal.dismiss();">Cancel</a>
    </div>
  </form>
</div>

<script>
jQuery(document).ready(function($) {
  var eSubject = '<?php print t("Whole Foods Market Shopping List"); ?>';
  var eTo = "";
  var eToMe = "";
  var eFrom = "<?php print $user->mail; ?>";
  update_email();

  $(".email-to-myself input[type='checkbox']").change(function(){
    eToMe = '';

    if ($(this).is(':checked') == true) {
      eToMe = "<?php print $user->mail; ?>";
    }

    update_email();
  });

  $(".email-to input[type='checkbox'], .email-to input[type='text']").change(function(){
    eTo = '';

    if ($("#email-to-chkbx").is(':checked') == true) {
      eTo = $(".email-to input[type='text']").val();
    }

    update_email();
  });

  $(".email-from input[type='text']'").change(function(){
    eFrom = $(this).val();
    update_email();
  });

  function update_email() {
    var to = '';

    if (eToMe != '' && eTo != '') {
      to = eToMe + ',' + eTo;
    }
    else if (eToMe != '') {
      to = eToMe;
    }
    else {
      to = eTo;
    }

    var ref = Drupal.settings.WholeFoods.ShoppingList.current;
    var body = Drupal.settings.WholeFoods.ShoppingList[ref]["email"] + Drupal.settings.WholeFoods.ShoppingList[ref]["items"]; <?php // set in users.module -> users_block_view ?>

    $('.email-buttons a.send-mail').attr( 'href', 'mailto:' + encodeURIComponent(to) + '?subject=' + encodeURIComponent(eSubject) + '&body=' + encodeURIComponent(body) );
	//$('.email-buttons a.send-mail').attr( 'href', 'mailto:' + encodeURIComponent(to) + '?subject=' + encodeURIComponent(eSubject) + '&body=' + encodeURIComponent(body) + '&from=' + encodeURIComponent(eFrom) );
  }
});
</script>
