<?php
$listing = array();
foreach ($nodes as $item) {
  if (!isset($item['node'])) { continue; }
  $node = $item['node'];
  $cats = field_get_items('node', $node, 'field_recipe_course', $node->language);
  foreach ($cats as $term_item) { $term = taxonomy_term_load($term_item['tid']); }
  $listing[$term->name]['dishes'][$node->title] = array(
    'item_link' => l($node->title, drupal_lookup_path('alias', 'node/' . $node->nid)),
    'remove_link' => l(t('Remove'), 'action/recipe/remove/' . $node->nid, array('query' => array('destination' => 'user/recipe_box'))),
  );
}
ksort($listing);
?>

<h3><?php print t('My Recipe Box'); ?></h3>
<?php print t('To save recipes, just click "add to recipe box" when you see a recipe you want to try.'); ?>
<?php foreach ($listing as $name => $data): ?>
  <h4><?php print $name; ?></h4>
  <ul>
  <?php ksort($data['dishes']); ?>
  <?php foreach ($data['dishes'] as $dishes): ?>
    <li>
      <?php print $dishes['item_link']; ?> <span class="remove_link">[<?php print $dishes['remove_link']; ?>]</span>
    </li>
  <?php endforeach; ?>
  </ul>
<?php endforeach; ?>