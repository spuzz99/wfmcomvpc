<?php
/**
 * @file
 * users.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function users_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function users_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_default_profile2_type().
 */
function users_default_profile2_type() {
  $items = array();
  $items['drupal'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "drupal",
    "label" : "drupal",
    "weight" : "0",
    "data" : { "registration" : 0 },
    "metatags" : [],
    "rdf_mapping" : []
  }');
  return $items;
}
