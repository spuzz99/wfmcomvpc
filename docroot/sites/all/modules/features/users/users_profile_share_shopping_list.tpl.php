<?php
/**
 * NOTES
 */
?>
<div class="shopping-list-model-box email-modal share-modal">
  <div class="modal-title">Share list</div>
  <form id="email-box">
    <div class="email-info">
      Sharing a list allows the recipient to view and edit your list on wholefoodsmarket.com. To send a plain text version of the list, use the Email list option instead.
    </div>

    <div class="email-to">
      <span class="email-to-email">To (email):&nbsp;</span>
      <input type="text" name="email_to_email" value="" />
    </div>
<!--
    <div class="email-from">
      <span class="email-from-email">From:&nbsp;</span>
      <input type="text" name="email_from_email" value="<?php print $user->mail; ?>" />
    </div>
-->
    <div class="email-buttons">
      <a href="#" class="orange-link send-mail" target="_blank">Send invitation</a>
      <a href="#" class="simple-link cancel" onclick="Drupal.CTools.Modal.dismiss();">Cancel</a>
    </div>
  </form>
</div>

<script>
jQuery(document).ready(function($) {
  var eSubject = '<?php print t("Whole Foods Market Shopping List"); ?>';
  <?php // set in users.module -> users_block_view ?>
  var eBody = Drupal.settings.WholeFoods.ShoppingList[Drupal.settings.WholeFoods.ShoppingList.current]["share"];
  eBody += Drupal.settings.WholeFoods.ShoppingList[Drupal.settings.WholeFoods.ShoppingList.current]["items"]; 
  var eTo = "";
  var eFrom = "<?php print $user->mail; ?>";
  update_email();

  $('a.send-mail').click(function() {
    if (eTo == '') {
      alert("You must enter an email address!");
      return false;
    }

    $.ajax({
      url: "/action/shopping_list/generate_share/<?php print $list->id; ?>/" + eTo,
      async: false,
      cache: false,
      success: function(url){
        eBody = eBody.replace('[ACCEPT_SHARE_LINK]', url);
        update_email();
		Drupal.CTools.Modal.dismiss();
      }
    });
  });

  $(".email-from input").change(function(){
    eFrom = $(this).val();
    update_email();
  });

  $(".email-to input").change(function(){
    eTo = $(this).val();
    update_email();
  });

  function update_email() {
  
	  $('.email-buttons a.send-mail').attr( 'href', 'mailto:' + encodeURIComponent(eTo) + '?subject=' + encodeURIComponent(eSubject) + '&body=' + encodeURIComponent(eBody) );
    //$('.email-buttons a.send-mail').attr( 'href', 'mailto:' + encodeURIComponent(eTo) + '?subject=' + encodeURIComponent(eSubject) + '&body=' + encodeURIComponent(eBody) + '&from=' + encodeURIComponent(eFrom) );
  }
});
</script>
