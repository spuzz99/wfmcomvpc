<?php
/**
 * @file
 * users.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function users_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'header_nav';
  $context->description = '';
  $context->tag = 'Global';
  $context->conditions = array(
    'user' => array(
      'values' => array(
        'administrator' => 'administrator',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'content-manager',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Global');
  $export['header_nav'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_profile_other';
  $context->description = '';
  $context->tag = 'Users';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'other',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'users-user_header_other' => array(
          'module' => 'users',
          'delta' => 'user_header_other',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'quicktabs-user_profile_other' => array(
          'module' => 'quicktabs',
          'delta' => 'user_profile_other',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Users');
  $export['user_profile_other'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_profile_self';
  $context->description = '';
  $context->tag = 'Users';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'current',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'users-user_header_self' => array(
          'module' => 'users',
          'delta' => 'user_header_self',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'quicktabs-user_profile_self' => array(
          'module' => 'quicktabs',
          'delta' => 'user_profile_self',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Users');
  $export['user_profile_self'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_shopping_list_self';
  $context->description = '';
  $context->tag = 'Users';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user/shopping_lists' => 'user/shopping_lists',
        'user/shopping_lists*' => 'user/shopping_lists*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'users-user_shopping_self' => array(
          'module' => 'users',
          'delta' => 'user_shopping_self',
          'region' => 'content',
          'weight' => NULL,
        ),
        'users-users_profile_header' => array(
          'module' => 'users',
          'delta' => 'users_profile_header',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Users');
  $export['user_shopping_list_self'] = $context;

  return $export;
}
