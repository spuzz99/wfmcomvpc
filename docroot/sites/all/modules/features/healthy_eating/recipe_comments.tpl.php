<?php
/**
 * @file
 * Theme file for the recipe comments.
 *
 * Available Variables:
 *  - $recipe - The recipe node object.
 */

$field = field_get_items('node', $recipe, 'field_rid');
$rid_field = array_pop($field);
$rid = (int)$rid_field['value'];

// Build up all Recipe category fields for google analytics use.
// Special Diet.
$special_diet = array();
for ($i = 0; $i < count($recipe->field_special_diet['und']); $i++) {
  $special_diet[] = $recipe->field_special_diet['und'][$i]['node']->title;
}
sort($special_diet);
$special_diet_string = check_plain(implode(", ", $special_diet));

// Course.
$course = array();
$tid = 0;
$course_string = "";
for ($i = 0; $i < count($recipe->field_recipe_course['und']); $i++) {
  $tid = $recipe->field_recipe_course['und'][$i]['tid'];
  $term = taxonomy_term_load($tid);
  if (strlen($term->name)) {
    $course[] = $term->name;
  }
}
$course_string = check_plain(implode(", ", $course));

// Category
$category = array();
$tid = 0;
$term = "";
$category_string = "";
if (!empty($recipe->field_recipe_category)) {
  for ($i = 0; $i < count($recipe->field_recipe_category['und']); $i++) {
    $tid = $recipe->field_recipe_category['und'][$i]['tid'];
    $term = taxonomy_term_load($tid);
    if (strlen($term->name)) {
      $category[] = $term->name;
    }
  }
}
$category_string = check_plain(implode(", ", $category));

drupal_add_js(
  'var commentId="' . $rid . '"; ' .
  'var recipeId="' . $rid . '"; ' .
  'var specialDiet="' . $special_diet_string . '"; ' .
  'var recipeCategory="' . $category_string . '"; ' .
  'var course="' . $course_string . '"; ' .
  'var productTitle="' . $recipe->title . '";', 'inline');
?>

<h3 id="comments-section-head"><?php print t('Recipe Discussion'); ?></h3>

<a id="comments"></a>
<div id="pluck_comments">
  <div id="pluckComments">
    <script type="text/javascript">
	    var opts = {}
	    pluckAppProxy.embedApp("pluck_comments", {
			plckCommentOnKeyType: "article", //required
			plckCommentOnKey: "<?php print $rid; ?>", //required
	        plckItemsPerPage: "15",
	        plckSort: "TimeStampDescending"
	    }, opts);
    </script>
  </div>
</div>