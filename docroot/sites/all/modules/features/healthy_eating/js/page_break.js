jQuery(document).ready(function($) {
  var container = $('<div class="recipe-print-container"></div>');
  var h2 = $('.node-recipe h2');
  h2.detach();
  container.append(h2);

  var recipe = $('.node-recipe .content');

  var $ingredient_ul = $('ul.ingredient_ul');
  var $new_ul = null;
  $ingredient_ul.children().each(function() {
    var $li = $(this);
    if ($li.has('.page-break').length > 0) {
      $new_ul = $('<div class="page-break">&nbsp;</div><div class="field field-name-field-ingredients field-type-text-with-summary field-label-above clearfix"><div itemscope itemtype="http://schema.org/Ingredients" class="field-items" itemprop="ingredients"><ul class="ingredient_ul"></ul></div></div>');
      $('.field.field-name-field-ingredients.field-type-text-with-summary.field-label-above').last().after($new_ul);
      $new_ul = $('.field.field-name-field-ingredients.field-type-text-with-summary.field-label-above').last();
      $new_ul = $('ul', $new_ul).last();
    }

    if ($new_ul != null) {
      $li.appendTo($new_ul);
    }
  });

  recipe.children().each(function() {
    var p_break = $(this).hasClass('page-break');
    if (p_break) {
      $(this).detach();
      if (container != null) {
        // Add it above everything.
        recipe.append(container);
        container = null;
      }
    }
    else {
      // Look for a container.
      if (container == null) {
        container = jQuery('<div class="recipe-print-container"></div>');
      }
      $(this).detach();
      container.append($(this));
    }
  });
  if (container != null) {
    // Add it above everything.
    recipe.append(container);
    container = null;
  }
});