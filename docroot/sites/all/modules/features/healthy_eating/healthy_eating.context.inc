<?php
/**
 * @file
 * healthy_eating.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function healthy_eating_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'recipe_landing';
  $context->description = '';
  $context->tag = 'Recipes';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'recipes' => 'recipes',
        'recipe' => 'recipe',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-recipe_promo-block' => array(
          'module' => 'views',
          'delta' => 'recipe_promo-block',
          'region' => 'marquee',
          'weight' => NULL,
        ),
        'healthy_eating-recipe_search' => array(
          'module' => 'healthy_eating',
          'delta' => 'recipe_search',
          'region' => 'content_top',
          'weight' => NULL,
        ),
        'quicktabs-recipes' => array(
          'module' => 'quicktabs',
          'delta' => 'recipes',
          'region' => 'content',
          'weight' => '-10',
        ),
        'newsletters-newsletters_sidebar_form' => array(
          'module' => 'newsletters',
          'delta' => 'newsletters_sidebar_form',
          'region' => 'content',
          'weight' => '-5',
        ),
        'healthy_eating-recipe_landing_footer' => array(
          'module' => 'healthy_eating',
          'delta' => 'recipe_landing_footer',
          'region' => 'content',
          'weight' => '0',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Recipes');
  $export['recipe_landing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'recipe_page';
  $context->description = '';
  $context->tag = 'Recipes';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'recipe' => 'recipe',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'healthy_eating-recipe_individual_page_header' => array(
          'module' => 'healthy_eating',
          'delta' => 'recipe_individual_page_header',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'healthy_eating-recipe_comments' => array(
          'module' => 'healthy_eating',
          'delta' => 'recipe_comments',
          'region' => 'content_bottom',
          'weight' => '',
        ),
        'healthy_eating-recipe_search' => array(
          'module' => 'healthy_eating',
          'delta' => 'recipe_search',
          'region' => 'sidebar_second',
          'weight' => '0',
        ),
        'newsletters-newsletters_sidebar_form' => array(
          'module' => 'newsletters',
          'delta' => 'newsletters_sidebar_form',
          'region' => 'sidebar_second',
          'weight' => '5',
        ),
        'views-related_recipes-featured' => array(
          'module' => 'views',
          'delta' => 'related_recipes-featured',
          'region' => 'sidebar_second',
          'weight' => '10',
        ),
        'views-related_recipes-related' => array(
          'module' => 'views',
          'delta' => 'related_recipes-related',
          'region' => 'sidebar_second',
          'weight' => '15',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Recipes');
  $export['recipe_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'recipe_promo_block';
  $context->description = 'Promo block for the recipe landing page';
  $context->tag = 'Recipes';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'recipes' => 'recipes',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-recipe_promo-block' => array(
          'module' => 'views',
          'delta' => 'recipe_promo-block',
          'region' => 'marquee',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Promo block for the recipe landing page');
  t('Recipes');
  $export['recipe_promo_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'recipe_search';
  $context->description = '';
  $context->tag = 'Recipes';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'recipe/search/*' => 'recipe/search/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'healthy_eating-recipe_search' => array(
          'module' => 'healthy_eating',
          'delta' => 'recipe_search',
          'region' => 'content_top',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Recipes');
  $export['recipe_search'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'special_diet';
  $context->description = '';
  $context->tag = 'Special Diets';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'special_diet' => 'special_diet',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'theme' => array(
      'values' => array(
        'wholefoods' => 'wholefoods',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-special_diets-shopping_list_pdf' => array(
          'module' => 'views',
          'delta' => 'special_diets-shopping_list_pdf',
          'region' => 'content',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Special Diets');
  $export['special_diet'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'special_diets_interior_header';
  $context->description = 'header for specific special diet';
  $context->tag = 'Special Diets';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'special_diet' => 'special_diet',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-79b7d58970fbf37e698e9a6609ccd2f5' => array(
          'module' => 'views',
          'delta' => '79b7d58970fbf37e698e9a6609ccd2f5',
          'region' => 'content_top',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Special Diets');
  t('header for specific special diet');
  $export['special_diets_interior_header'] = $context;

  return $export;
}
