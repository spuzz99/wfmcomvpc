<?php
/**
 * @file
 * Healthy Eating feature common functions.
 */

/**
 * Returns count of recipes content.
 */
function healthy_eating_get_recipes_count() {
  // This count query is going to be cached
  $query = new EntityFieldQuery;
  return $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'recipe')
    ->propertyCondition('status', 1)
    ->count()
    ->execute();
}

/**
 * Returns Special Diet options array for search form.
 */
function healthy_eating_get_special_diet_options() {
  static $special_diet_options;

  if (!$special_diet_options && $cache = cache_get('wfm_healthy_eating:special_diet_options')) {
    $special_diet_options = $cache->data;
  }
  else {
    $query = db_select('node', 'n');
    $query->fields('n', array('nid', 'title'));
    $query->leftJoin('field_data_field_display_in_advanced_search', 'sc', 'sc.entity_id = n.nid');
    $query->condition('n.status', 1);
    $query->condition('n.type', 'special_diet');

    $db_or = db_or();
    $db_or->isNull('sc.field_display_in_advanced_search_value');
    $db_or->condition('sc.field_display_in_advanced_search_value', 1, '!=');

    $query->condition($db_or);
    $query->orderBy('n.title');

    $special_diet_options = $query->execute()->fetchAllKeyed();

    cache_set('wfm_healthy_eating:special_diet_options', $special_diet_options);
  }

  return $special_diet_options;
}
