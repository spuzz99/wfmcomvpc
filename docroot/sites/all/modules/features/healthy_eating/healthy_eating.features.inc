<?php
/**
 * @file
 * healthy_eating.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function healthy_eating_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function healthy_eating_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function healthy_eating_image_default_styles() {
  $styles = array();

  // Exported image style: header_recipe.
  $styles['header_recipe'] = array(
    'effects' => array(
      52 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 425,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'header_recipe',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function healthy_eating_node_info() {
  $items = array(
    'recipe' => array(
      'name' => t('Recipe'),
      'base' => 'node_content',
      'description' => t('A full recipe for a meal.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'special_diet' => array(
      'name' => t('Special Diet'),
      'base' => 'node_content',
      'description' => t('Informational page regarding special diet.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_styles_default_styles().
 */
function healthy_eating_styles_default_styles() {
  $styles = array();

  // Exported style: related_image

  $styles['file']['styles']['related_image'] = array(
    'label' => 'related_image',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'related_image' => array(
          'default preset' => 'original',
          'preset' => 'related_image',
        ),
      ),
      'audio' => array(
        'related_image' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'related_image' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'related_image' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'related_image' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  return $styles;
}

/**
 * Implements hook_styles_default_presets_alter().
 */
function healthy_eating_styles_default_presets_alter(&$presets) {
  $styles = styles_default_styles();
  if ($styles['file']['styles']['related_image']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['related_image'] = array(
      'default preset' => 'original',
      'preset' => 'related_image',
    );

    $presets['file']['containers']['audio']['styles']['related_image'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['related_image'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['related_image'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['related_image'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
}
