<?php
/**
 * @file
 * healthy_eating.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function healthy_eating_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_bottom|node|recipe|default';
  $field_group->group_name = 'group_bottom';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'bottom',
    'weight' => '3',
    'children' => array(
      0 => 'field_video_node',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
      ),
    ),
  );
  $export['group_bottom|node|recipe|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_diet_infotab|node|special_diet|form';
  $field_group->group_name = 'group_diet_infotab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'special_diet';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_diet_tabgroup';
  $field_group->data = array(
    'label' => 'Diet Info',
    'weight' => '12',
    'children' => array(
      0 => 'body',
      1 => 'field_image',
      2 => 'field_subtitle',
      3 => 'field_display_in_advanced_search',
      4 => 'field_body_mobile',
      5 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_diet_infotab|node|special_diet|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_diet_relatedtab|node|special_diet|form';
  $field_group->group_name = 'group_diet_relatedtab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'special_diet';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_diet_tabgroup';
  $field_group->data = array(
    'label' => 'Related',
    'weight' => '13',
    'children' => array(
      0 => 'field_recipes',
      1 => 'field_related_content',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_diet_relatedtab|node|special_diet|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_diet_tabgroup|node|special_diet|form';
  $field_group->group_name = 'group_diet_tabgroup';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'special_diet';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Diet Tabs',
    'weight' => '0',
    'children' => array(
      0 => 'locations_acl',
      1 => 'group_diet_infotab',
      2 => 'group_diet_relatedtab',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_diet_tabgroup|node|special_diet|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_food_guide_basics|node|food_guide|form';
  $field_group->group_name = 'group_food_guide_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'food_guide';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_food_guide';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '11',
    'children' => array(
      0 => 'body',
      1 => 'field_image',
      2 => 'field_recipes',
      3 => 'field_subtitle',
      4 => 'locations_acl',
      5 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_food_guide_basics|node|food_guide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_food_guide|node|food_guide|form';
  $field_group->group_name = 'group_food_guide';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'food_guide';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Food Guide',
    'weight' => '0',
    'children' => array(
      0 => 'group_food_guide_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_food_guide|node|food_guide|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left|node|recipe|default';
  $field_group->group_name = 'group_left';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'left',
    'weight' => '1',
    'children' => array(
      0 => 'field_ingredients',
      1 => 'field_recipe_directions',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'left',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_left|node|recipe|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_marquee|node|recipe|form';
  $field_group->group_name = 'group_marquee';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_recipe_tabs';
  $field_group->data = array(
    'label' => 'Marquee',
    'weight' => '26',
    'children' => array(
      0 => 'field_heading_background',
      1 => 'field_marquee_image',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_marquee|node|recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_recipe_imported|node|recipe|form';
  $field_group->group_name = 'group_recipe_imported';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_recipe_tabs';
  $field_group->data = array(
    'label' => 'Imported Data',
    'weight' => '27',
    'children' => array(
      0 => 'body',
      1 => 'field_cook_time',
      2 => 'field_difficulty',
      3 => 'field_hero_image',
      4 => 'field_ingredients',
      5 => 'field_prep_time',
      6 => 'field_recipe_directions',
      7 => 'field_serve_count',
      8 => 'field_special_diet',

      9 => 'field_nutritional_info',
      10 => 'field_rid',
      11 => 'field_recipe_created_time',
      12 => 'field_recipe_rating',
      13 => 'field_recipe_category',
      14 => 'field_recipe_course',
      15 => 'field_recipe_cusine',
      16 => 'field_recipe_featured_in',
      17 => 'field_recipe_main_ingredient',
      18 => 'field_recipe_occasions',
      19 => 'field_serves',
      20 => 'field_mongo_id',
      21 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_recipe_imported|node|recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_recipe_info|node|recipe|form';
  $field_group->group_name = 'group_recipe_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_recipe_tabs';
  $field_group->data = array(
    'label' => 'Editable Info',
    'weight' => '24',
    'children' => array(
      0 => 'field_is_featured',
      1 => 'field_is_promoted_featured',
      2 => 'metatags',
      3 => 'path',
      4 => 'redirect',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_recipe_info|node|recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_recipe_related|node|recipe|form';
  $field_group->group_name = 'group_recipe_related';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_recipe_tabs';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '25',
    'children' => array(
      0 => 'field_related_content',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_recipe_related|node|recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_recipe_tabs|node|recipe|form';
  $field_group->group_name = 'group_recipe_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Recipe Tabs',
    'weight' => '0',
    'children' => array(
      0 => 'locations_acl',
      1 => 'remove_from_search_field',
      2 => 'group_marquee',
      3 => 'group_recipe_imported',
      4 => 'group_recipe_info',
      5 => 'group_recipe_related',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_recipe_tabs|node|recipe|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_right|node|recipe|default';
  $field_group->group_name = 'group_right';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'right',
    'weight' => '2',
    'children' => array(
      0 => 'field_special_diet',
      1 => 'field_nutritional_info',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'right',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_right|node|recipe|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sdsl_basics|node|special_diet_shopping_list|form';
  $field_group->group_name = 'group_sdsl_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'special_diet_shopping_list';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_sdsl';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '8',
    'children' => array(
      0 => 'field_shopping_list',
      1 => 'locations_acl',
      2 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_sdsl_basics|node|special_diet_shopping_list|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sdsl|node|special_diet_shopping_list|form';
  $field_group->group_name = 'group_sdsl';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'special_diet_shopping_list';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Special Diet Shopping List',
    'weight' => '0',
    'children' => array(
      0 => 'group_sdsl_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_sdsl|node|special_diet_shopping_list|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_top|node|recipe|default';
  $field_group->group_name = 'group_top';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'recipe';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'top',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_hero_image',
      2 => 'field_serves',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'top',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_top|node|recipe|default'] = $field_group;

  return $export;
}
