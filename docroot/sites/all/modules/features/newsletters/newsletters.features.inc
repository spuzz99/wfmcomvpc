<?php
/**
 * @file
 * newsletters.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function newsletters_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function newsletters_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function newsletters_image_default_styles() {
  $styles = array();

  // Exported image style: newsletters_155x100.
  $styles['newsletters_155x100'] = array(
    'label' => 'newsletters_155x100',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 155,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function newsletters_node_info() {
  $items = array(
    'newsletter' => array(
      'name' => t('Newsletter'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_styles_default_styles().
 */
function newsletters_styles_default_styles() {
  $styles = array();

  // Exported style: newsletter_image

  $styles['file']['styles']['newsletter_image'] = array(
    'label' => 'newsletter_image',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'newsletter_image' => array(
          'default preset' => 'original',
          'preset' => 'newsletter_image',
        ),
      ),
      'audio' => array(
        'newsletter_image' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'newsletter_image' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'newsletter_image' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'newsletter_image' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: newsletter_image

  $styles['file']['styles']['newsletter_image'] = array(
    'label' => 'newsletter_image',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'newsletter_image' => array(
          'default preset' => 'original',
        ),
      ),
      'audio' => array(
        'newsletter_image' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'newsletter_image' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'newsletter_image' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'newsletter_image' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  return $styles;
}

/**
 * Implements hook_styles_default_presets_alter().
 */
function newsletters_styles_default_presets_alter(&$presets) {
  $styles = styles_default_styles();
  if ($styles['file']['styles']['newsletter_image']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['newsletter_image'] = array(
      'default preset' => 'original',
      'preset' => 'newsletter_image',
    );

    $presets['file']['containers']['audio']['styles']['newsletter_image'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['newsletter_image'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['newsletter_image'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['newsletter_image'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['media']['styles']['newsletter_image']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['media']['containers']['image']['styles']['newsletter_image'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['audio']['styles']['newsletter_image'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['video']['styles']['newsletter_image'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['default']['styles']['newsletter_image'] = array(
      'default preset' => 'original',
    );

    $presets['media']['containers']['media_youtube']['styles']['newsletter_image'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
}
