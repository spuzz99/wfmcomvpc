<?php
/**
 * @file
 * Code for the Newsletters blocks.
 */

/**
 * Implements hook_block_info().
 */
function newsletters_block_info() {
  $blocks = array();
  $blocks['newsletters_sidebar_form'] = array(
    'info' => t('Newsletters Sidebar Subscription Form'),
    'cache' => DRUPAL_NO_CACHE, //We have some dynamic stuff happening in this block
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function newsletters_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'newsletters_sidebar_form':
      $form_vars = variable_get('wfm_newsletter_sidebar_blocks');
      if (newsletter_path_contains_form_in_settings($form_vars)) {
        $form = drupal_get_form('newsletters_sidebar_subscription_form', $form_vars);
        $block['content']['#markup'] = render($form);
        $block['content']['#attached']['js'] = array(
          drupal_get_path('module', 'newsletters') . '/newsletters.sidebar.js' => array(
            'type' => 'file',
            'scope' => 'footer',
          ),
        );
      } 
      else {
        $block['content'] = '';
      }
      break;
  }
  return $block;
}
