<?php

/**
 * @file
 * Custom Tokens using "Tokens API" API (as opposed to the Tokens API).
 */

/**
 * Implements hook_api_tokens_info().
 */
function newsletters_api_tokens_info() {
  $tokens = array();

  $tokens['newsletter_subscription'] = array(
    'title' => 'Newsletter Subscription Token',
    'description' => 'Renders newsletter subscription form like what is found on /newsletters'
  );

  return $tokens;
}

function newsletters_apitoken_newsletter_subscription() {
  $first_form = drupal_get_form('newsletter_header_form');
  if (isset($first_form['after_info'])) {
    unset($first_form['after_info']);
  }
  $second_form = drupal_get_form('newsletters_form_dbu_update');
  return drupal_render($first_form) . drupal_render($second_form);

}