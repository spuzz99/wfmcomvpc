(function WFMcustomerServiceForm($){
  'use strict';
  var customerServiceForm = {

    //Properties
    components_to_toggle: [
      '.form-item-submitted-topic-block-state-select',
      '.form-item-submitted-topic-block-store-select',
      '#webform-component-name',
      '#webform-component-phone',
      '#webform-component-email',
      '#webform-component-product-upc',
      '#webform-component-store-zip-code',
      '#webform-component-body',
      '#phone-number-box-wrapper'
    ],
    tlc: 'input[name="submitted[tlc]"]',
    state_select: '#edit-submitted-topic-block-state-select',
    store_select: '#edit-submitted-topic-block-store-select',
    topic_select: '#edit-submitted-choose-topic',
    submit: '#edit-submit',
    webform: '#webform-client-form-7477',
    paragraph: '#webform-client-form-7477 .header-content',
    store_select_box: '#webform-client-form-7477 .store-select-box',
    infobox: '#phone-number-box',
    storebox: '#edit-submitted-topic-block',
    instaemail: '<a href="mailto:wholefoods@instacart.com">wholefoods@instacart.com</a>',
      /**
       * @TODO we should move all of this template text into the wfm_template_text module and put it into
       * a javascript setting to make it accessible to this script.
       */
    instamessage: Drupal.t('For questions about Instacart orders, delivery, or returns please contact '),
    store_not_selected_message: Drupal.t('Many issues can be solved by a quick call to your local store. Please select your store from the above menu to see phone number.'),
    store_selected_message: Drupal.t('This store can be reached directly at'),
    instacart_paragraph: Drupal.t('<ol><li>All online catering and holiday meals are handled by your local store. If you have a change to an order that you are picking up in the next 24 hours please call the store for the fastest response. Select the store from the list below to see the phone number.</li>'
      + '<li>Instacart order questions are best handled by e-mailing <a href="mailto:happycustomers@instacart.com">happycustomers@instacart.com</a></li>'
      + '<li>For questions about gift cards call 1-800-WFM-CARD</li></ol>'),
    product_request_paragraph: Drupal.t('Individual stores are responsible for their own product sourcing. Select your store below to send your product request along to your local store team.'),
    store_experience_paragraph: Drupal.t('Local store feedback is sent directly to the store you select below. If you want to send feedback to our corporate team, please select Company Feedback instead in the menu above.'),
    edv_365_paragraph: Drupal.t('The more information you can provide to us about a specific product, the more quickly we can help you. If you don’t have the 6 digits from your product label, please be as specific as possible about the product name in the comments.'),
    company_policy_paragraph: Drupal.t('Even though company feedback is handled by our corporate team, it helps us to resolve your need faster if we know which store you visit most regularly.'),
    potential_vendors_paragraph: Drupal.t('New vendors are evaluated at the regional level. Vendors of all types - including service providers - should follow the process detailed on our '
      + '<a href="' + location.protocol + '//' +location.hostname + '/company-info/information-potential-suppliers">Potential Supplier page</a>' 
      +'. We can’t guarantee a response to all inquiries.'),
    quality_standards_paragraph: Drupal.t('Product standards include animal welfare, unacceptable ingredients, organic certification, and other similar topics. Feedback about store-prepared products and holiday meals should be sent to your local store. Select Store Feedback in the menu above to contact them.'),
    website_issues_paragraph: Drupal.t('Please include a link to the page on our site about which you have a question or comment in the Message section. For questions about store sales and flyers please contact your local store. Select Store Feedback in the menu above to contact them.'),
    store_options: {},

    /**
     * Add event listeners
     */
    addListeners: function() {
      $(customerServiceForm.topic_select).change(function topicChangeListenerCallback(){
        customerServiceForm.topicChangeCallback();
      });
      $(customerServiceForm.state_select).change(function stateChangeListenerCallback(){
        customerServiceForm.stateChangeCallback();
      });
      $(customerServiceForm.store_select).change(function storeChangeListenerCallback(){
        customerServiceForm.storeChangeCallback();
      });
      $(customerServiceForm.submit).click(function formSubmitClickListenerCallback() {
        customerServiceForm.submitClickCallback();
        return false;
      });
    },

    /**
     * Build option DOM element from object
     *
     * @param object option_object
     * @return object
     *   DOM option object
     */
    buildOptionElement: function(option_object) {
      var option = document.createElement('option');
      $(option).val(option_object.value).text(option_object.label).prop('selected', option_object.selected);
      return option;
    },

    /**
     * Create object representing store select object, save in customerServiceForm.store_options
     */
    createStoreOptionsObject: function() {
      var options = {};
      $(customerServiceForm.store_select).first().children('optgroup').each(function(){
        var label = $(this).attr('label');
        options[label] = {};
        $(this).children('option').each( function(){
          var value = $(this).val();
          options[label][value] = {};
          options[label][value]['name'] = $(this).text();
          options[label][value]['selected'] = false;
          if($(this).prop('selected') === true) {
            options[label][value]['selected'] = true;
          }
        });
      });
      customerServiceForm.store_options = options;
    },

    /**
     * Disable Store Select
     */
    disableStoreSelect: function() {
      $(customerServiceForm.store_select).prop('disabled', true).addClass('disabled');
    },

    /**
     * Enable Store Select
     */
    enableStoreSelect: function() {
      $(customerServiceForm.store_select).prop('disabled', false).removeClass('disabled');
    },

    /**
     * Filter store select options based on selected state.
     */
    filterStoreOptions: function() {
      var state_abbreviation = $(customerServiceForm.state_select).val(),
          store_options = customerServiceForm.store_options,
          default_option = '<option value=0>' + Drupal.t('Select a Store') + '</option>';
      $(customerServiceForm.store_select + ' option').detach();
      $(customerServiceForm.store_select + ' optgroup').detach();
      $(customerServiceForm.store_select).append(default_option);
      $.each(store_options, function(index, value) {
        var abbr = index.substring(0, 2),
            optgroup = {};
        if (abbr == state_abbreviation) {
          optgroup = document.createElement('optgroup');
          optgroup.label = index.substring(2);
          $(customerServiceForm.store_select).append(optgroup);
          $.each(value, function(optindex, optvalue) {
            var option_values = {
              label: optvalue.name,
              text: optvalue.name,
              value: optindex,
              selected: optvalue.selected
            },
            opt = customerServiceForm.buildOptionElement(option_values);
            $(customerServiceForm.store_select).find('optgroup').last().append(opt);
          });
        }
      });
    },

    /**
     * @param object element
     *   object representing option DOM element
     * @return object
     *   Object represented necessary values of select element
     */
    getOptionAsObject: function(element) {
      var optobj = {},
          value_string = $(element).val(),
          value_array = value_string.split('__');

      optobj.phone = $(element).attr('number');
      optobj.tlc = $(element).attr('tlc');
      optobj.stateabbr = value_array[0];
      optobj.city = value_array[1];
      optobj.nid = value_array[2];
      optobj.value = value_string;
      optobj.display = $(element).text();
      return optobj;
    },

    /**
     * Callback function for AJAX request to /ajax/stores
     *
     * @param store object
     *   Object of store info
     */
    setDefaultStoreCallback: function(store) {
      store = store[Object.keys(store)[0]];
      if (store) {
        $(customerServiceForm.state_select).val(store.location.stateabbr).change();
        $(customerServiceForm.store_select + ' option[value="' + store.nid + '"]').prop('selected', true).change();
      }
    },

    /**
     * Return array of form elements to be displayed based on chosen value in topic select
     *
     * @param string topic
     * @return array
     *   array of element ids
     */
    getWebformComponents: function(topic) {
      var componentIds = [];
      switch(topic) {
        case '365_edv':
          componentIds = [
            '.form-item-submitted-topic-block-state-select',
            '.form-item-submitted-topic-block-store-select',
            '#webform-component-name',
            '#webform-component-phone',
            '#webform-component-email',
            '#webform-component-body',
            '#webform-component-product-upc',
            '.form-item-mollom-captcha'
          ];
          break;

        case 'store_location_request':
          componentIds = [
            '#webform-component-name',
            '#webform-component-phone',
            '#webform-component-email',
            '#webform-component-body',
            '#webform-component-store-zip-code',
            '.form-item-mollom-captcha'
          ];  
          break;

        case 'potential_vendors':
          componentIds = [
            '#webform-component-name',
            '#webform-component-phone',
            '#webform-component-email',
            '#webform-component-body',
            '.form-item-mollom-captcha'
          ];
          break;

        case 'quality_standards':
          componentIds = [
            '#webform-component-name',
            '#webform-component-phone',
            '#webform-component-email',
            '#webform-component-body',
            '.form-item-mollom-captcha'
          ];
          break;

        case 'website_issues':
          componentIds = [
            '#webform-component-name',
            '#webform-component-phone',
            '#webform-component-email',
            '#webform-component-body',
            '.form-item-mollom-captcha'
          ];
          break;

        case 'company_policy':
          componentIds = [
            '.form-item-submitted-topic-block-state-select',
            '.form-item-submitted-topic-block-store-select',
            '#webform-component-name',
            '#webform-component-phone',
            '#webform-component-email',
            '#webform-component-body',
            '.form-item-mollom-captcha'
          ];
          break;

        case 'product_request':
          componentIds = [
            '.form-item-submitted-topic-block-state-select',
            '.form-item-submitted-topic-block-store-select',
            '#webform-component-name',
            '#webform-component-phone',
            '#webform-component-email',
            '#webform-component-body',
            '.form-item-mollom-captcha'
          ];
          break;

        default:
          componentIds = [
            '.form-item-submitted-topic-block-state-select',
            '.form-item-submitted-topic-block-store-select',
            '#webform-component-name',
            '#webform-component-phone',
            '#webform-component-email',
            '#webform-component-body',
            '.form-item-mollom-captcha',
            '#phone-number-box-wrapper'
          ];
      }
      return componentIds;
    },

    /**
     * Initialization function
     */
    init: function() {
      customerServiceForm.createStoreOptionsObject();
      customerServiceForm.addListeners();
      customerServiceForm.disableStoreSelect();
      customerServiceForm.setDefaultValues();
      if (Drupal.settings.WholeFoods.theme == 'wholefoods_mobile'){
        customerServiceForm.enableStoreSelect();
      }
    },

    /**
     * Set default selected store. Email is handled server-side.
     */
    setDefaultValues: function() {
      var user_store = null;

      if (user_store = $.cookie('local_store')) {
        Drupal.WholeFoods.getStoreInfo(user_store, customerServiceForm.setDefaultStoreCallback);
      }
    },

    /**
     * Set value on hidden input store tlc.
     */
    setHiddenStoreValue: function() {
      var storenid = '',
          storetlc = '';
      if (Drupal.settings.WholeFoods.theme == 'wholefoods_mobile'){
        storetlc = $('#edit-phone-number-store-list').val();
        $('input[name="submitted[tlc]"]').val(storetlc);
      }
      else {
        storenid = $(customerServiceForm.store_select + ' option:selected').val();
        Drupal.WholeFoods.getStoreInfo(storenid, function(storedata){
          storedata = Drupal.WholeFoods.removeStoreNidKey(storedata);
          $('input[name="submitted[tlc]"]').val(storedata.tlc);
        })
      }
    },

    /**
     * Callback function to state select change listener.
     */
    stateChangeCallback: function() {
      customerServiceForm.disableStoreSelect();
      var selected_state = $(customerServiceForm.state_select).val();
      if (selected_state != false) {
        customerServiceForm.enableStoreSelect();
      }
      customerServiceForm.filterStoreOptions();
      $('#phone-number-box').text(customerServiceForm.store_not_selected_message);
      $('.phone-number').text('');
    },

    /**
     * Callback function to store select change listener.
     */
    storeChangeCallback: function() {
      var nid = $(customerServiceForm.store_select).find(':selected').val();

      if (nid) {
        Drupal.WholeFoods.getStoreInfo(nid, function(storedata){
          storedata = Drupal.WholeFoods.removeStoreNidKey(storedata);
          $(customerServiceForm.infobox).text(customerServiceForm.store_selected_message).show();
          $('.phone-number').text(storedata.phone);
        })
      }
      else {
        $(customerServiceForm.paragraph).text(customerServiceForm.store_not_selected_message).show();
        $('.phone-number').text('');
      }

      customerServiceForm.setHiddenStoreValue();
    },

    /**
     * Callback function for customer service form submit.
     */
    submitClickCallback: function() {
      var topic = $(customerServiceForm.topic_select).val(),
          selectedStore ='',
          message = Drupal.t('This topic requires you to select a store to make sure your message is delivered to the right person. Please choose a store to continue.'),
          zipmessage = Drupal.t('This topic requires you to enter a zip code. Please enter a zip code to continue');

      var requires_store_selection = [
        'store_experience',
        'instacart',
        'product_request',
        'company_policy',
        '365_edv'
      ];
  
      if (Drupal.settings.WholeFoods.theme == 'wholefoods_mobile') {
        selectedStore = $('#edit-phone-number-store-list').val();
      } else {
        selectedStore = $(customerServiceForm.store_select).val();
      }

      if ($.inArray(topic, requires_store_selection) >= 0) {
        if (selectedStore == false) {
          alert(message);
          if ($(customerServiceForm.state_select).val() == false) {
            $(customerServiceForm.state_select).focus();
          }
          else if ($(customerServiceForm.store_select).val() == false) {
            $(customerServiceForm.store_select).focus();
          }
          return false;
        }
      }
      if (topic == 'store_location_request') {
        if (!$('#edit-submitted-store-zip-code').val()) {
          $('#edit-submitted-store-zip-code').focus();
          alert(zipmessage);
          return false;
        }
      }
      $(customerServiceForm.submit).prop('disabled', true);
      $(customerServiceForm.submit).addClass('disabled');
      $(customerServiceForm.submit).val('Sending…');
      $(customerServiceForm.webform).submit();
    },

    /**
     * Add HTML class to #phone-number-box-wrapper based on selected topic.
     * @param topic string
     *   topic from topic select on customer service form
     */
    toggleInfoboxClass: function(topic) {
      var classes = $('#phone-number-box-wrapper').attr('class'),
          class_list = [];
      if (classes) {
        class_list = classes.split(/\s+/);
      }
      if (class_list) {
        $.each(class_list, function (index, class_name) {
          if (class_name.indexOf('topic-') > -1) {
            $('#phone-number-box-wrapper').removeClass(class_name);
          }
        });
      }
      $('#phone-number-box-wrapper').addClass('topic-' + topic);
    },

    /**
     * Callback function to topic select change listener.
     */
    topicChangeCallback: function() {
      var topic = $(customerServiceForm.topic_select).val(),
          components = customerServiceForm.getWebformComponents(topic);
      if (topic == false) {
        $(customerServiceForm.paragraph).html('');
          customerServiceForm.components_to_toggle.forEach(function forEachShowCallback(component){
          $(component).hide();
        });
      }
      else {
        $(customerServiceForm.storebox).show();
        customerServiceForm.components_to_toggle.forEach(function forEachHideCallback(component){
          $(component).hide();
        });
        components.forEach(function forEachShowCallback(component){
           $(component).show();
        });
        $(customerServiceForm.paragraph).html('');
        customerServiceForm.toggleInfoboxClass(topic);
        $(customerServiceForm.infobox).hide();
      }
      if (topic === 'instacart') {
        $(customerServiceForm.paragraph).html(customerServiceForm.instacart_paragraph);
        customerServiceForm.store_not_selected_message = 'Immediate online catering and holiday meal issues are best solved by a quick call to the store where you placed your order. Select your store from the menu to the left to see the store\'s phone number.';
        $(customerServiceForm.infobox).show();
        if ($(customerServiceForm.store_select).val() == false) {
          $(customerServiceForm.infobox).text(customerServiceForm.store_not_selected_message);
        }
      }
      if (topic === 'product_request') {
        $(customerServiceForm.paragraph).html(customerServiceForm.product_request_paragraph);
      }
      if (topic === 'store_experience') {
        $(customerServiceForm.paragraph).html(customerServiceForm.store_experience_paragraph);
        customerServiceForm.store_not_selected_message = 'This feedback will be sent directly to your local store. Many issues can be resolved with a quick phone call. Select your store from the menu to the left to see their phone number.';
        $(customerServiceForm.infobox).show();
        if ($(customerServiceForm.store_select).val() == false) {
          $(customerServiceForm.infobox).text(customerServiceForm.store_not_selected_message);
        }
        else {
          $(customerServiceForm.infobox).text(customerServiceForm.store_selected_message);
        }
      }
      if (topic === '365_edv') {
        $(customerServiceForm.paragraph).html(customerServiceForm.edv_365_paragraph);
      }
      if (topic === 'company_policy') {
        $(customerServiceForm.paragraph).html(customerServiceForm.company_policy_paragraph);
      }
      if (topic === 'potential_vendors') {
        $(customerServiceForm.paragraph).html(customerServiceForm.potential_vendors_paragraph);
      }
      if (topic === 'quality_standards') {
        $(customerServiceForm.paragraph).html(customerServiceForm.quality_standards_paragraph);
      }
      if (topic === 'store_location_request') {
        $('#webform-component-store-zip-code label').append('<span class="form-required" title="This field is required.">*</span>');
      }
      if (topic === 'website_issues') {
        $(customerServiceForm.paragraph).html(customerServiceForm.website_issues_paragraph);
      }
    },

  };

  //Attach to Drupal behaviors
  Drupal.behaviors.customerServiceForm = {
    attach: function() {
      $('body').once('customerServiceForm', function(){
        customerServiceForm.init();
        if (!Drupal.settings.store.new_submit) {
          customerServiceForm.topicChangeCallback();
        }
      });
    }
  };

})(jQuery);