/**
 * @file Controls the Google Maps and Directins for the store locator maps.
 *
 * For this file to be useful, the view 'store_map' and the block 'map' needs
 * to be setup on a page. The template file for the block 'map' needs to be
 * themed to insert correct JavaScript variables and make the map container.
 * The theme is currently setup in the file
 * 'views-view-table--store-map--map.tpl.php' under the Whole Foods theme.
 *
 * Useful Variables:
 *   - markers_list
 *     List of markers (stores) to add to the map. Should be an entry for every store.
 *     If this variable is not set (typeof == 'undefined'), we're not on a map page.
 *     Layout:
 *       array (
 *         'id_for_store_1' => array(
 *           'lat' => string of the latitude coordinates,
 *           'lon' => string of the longitude coordinates,
 *           'title' => string of the HTML title of the store,
 *           'raw_title' => string of the NON-HTML title of the store,
 *           'description' => description for the popup when clicking on the marker,
 *           'address' => string of the address of the store (for directions),
 *           'links' => string of links in HTML format to be inserted in the popup,
 *          ),
 *         etc...
 *       )
 *
 *   - current_store (only set when a $_GET parameter of 'store' is available)
 *     An array containing 'lat' and 'lon' of the current store. Useful for
 *     positioning the map.
 *
 *
 * Reference: http://code.google.com/apis/maps/documentation/javascript/reference.html
 *   It's a great resource for working with Google's Map API.
 */


/**
 * Inserts the address of a location into the map_directions form.
 *
 * TODO: Change the name of this method. That makes no sense.
 */
function insertMapDirections(id) {
  jQuery('#map_directions .content').slideDown();
  var form = jQuery('#directions_form');
  var destination = jQuery('#destination_box', form);
  destination.val(markers_list[id]['address']);
  //  event.preventDefault();
  return false;
}

(function ($) {
  $(document).ready(function() {
    // Find all the 'nearest' store displays and hide them.
    $('.nearest_stores').hide();
    $('.nearest_stores:first').show();

    // Add handlers to 'nearest' links.
    $('.nearest_store_button').click(function() {
      $('.nearest_stores').hide();
      $('#nearest_' + $(this).attr('within') + '_miles').show();
      return false;
    });

    // Hide map directions.
    $('#map_directions .content').hide();

    // Make the map directions header clickable.
    $('#map_directions h2').click(function() {
      $('#map_directions .content').slideToggle();
      return false;
    });

    if (typeof markers_list != 'undefined') {
      /**
       * If we have markers_list, that's an indicator we need to setup a map.
       */

      /*
       * We start by setting map options. If there is a current_store
       * set (via the template file for the store view), then we need to center
       * the map on that store. Otherwise center on the continental United States.
       *
       * Options Reference: http://code.google.com/apis/maps/documentation/javascript/reference.html#MapOptions
       */
      var myOptions = new Array();
      if (current_store != null) {
        // Center on a store, zoom in to 8. (You can see most of Kansas City)
	      myOptions = {
	        center: new google.maps.LatLng(current_store['lat'], current_store['lon']),
			    zoom: 11,
			    mapTypeId: google.maps.MapTypeId.ROADMAP
	      };
      }
      else {
        // Center on US center, zoom to 3. (The US should fill the map)
        myOptions = {
	        center: new google.maps.LatLng(39.5222, -98.3511),
			    zoom: 3,
			    mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      }

      // Create the map using the options we defined.
      // http://code.google.com/apis/maps/documentation/javascript/reference.html#Map
		  var map = new google.maps.Map(document.getElementById("map_canvas"),
																	  myOptions);

      // Create the Directions service.
      // http://code.google.com/apis/maps/documentation/javascript/reference.html#DirectionsService
      var directions = new google.maps.DirectionsService();

      // Add a submit handler to the Directions form.
      $('#directions_form').submit(function() {

        // Create the new request object
        // http://code.google.com/apis/maps/documentation/javascript/reference.html#DirectionsRequest
        var request = new Object();
        request['destination'] = $('#destination_box').val();
        request['origin'] = $('#origin_box').val();

        // TODO: Add a Walking, Driving or Bicycling button.
        // http://code.google.com/apis/maps/documentation/javascript/reference.html#TravelMode
        request['travelMode'] = google.maps.TravelMode.DRIVING;

        directions.route(request, function(result, status) {
          // Callback to handle the result.
          var renderer = new google.maps.DirectionsRenderer({
            directions : result,
            map : map,
          })

          // Create some HTML to display the directions and insert it into the theme.
          var legs = renderer.getDirections().routes[0].legs[0];
          var html = '' +
            '<div class="map-start"><span class="map-label">' + Drupal.t('Start:') + '</span>' + legs.start_address + '</div>' +
            '<div class="map-end"><span class="map-label">' + Drupal.t('End:') + '</span>' + legs.end_address + '</div>';
          html += '<div class="map-distance">' + legs.distance.text + ' - ' + Drupal.t('about') + ' ' + legs.duration.text + '</div>';
          // Loop through the legs of the journey and add those to the directions...
          for (var i in legs.steps) {
            var step = legs.steps[i];
            html += '<div class="map-step">' + i + ') ' + step.instructions + '</div>';
          }

          // Finally display the results in the theme.
          $('#map_directions_result').html(html);

        });
        return false;
      })

      /**
       * infoWindow contains all the information popups for every store.
       * It is keyed by the store result number (basically it's ID for
       * these purposes).
       */
      var infoWindow = new Array();

      /**
       * The currenly opened infoWindow. We're storing this so we can
       * close the infoWindow when we open a new one. Don't want to
       * clutter the map.
       */
      var openWindow = null;

      /**
       * The template for all markers on the screen.
       * http://code.google.com/apis/maps/documentation/javascript/reference.html#MarkerOptions
       *
       * TODO: Move the image to somewhere in the theme or something.
       */
      var marker = {
        map : map,
        visible : true,
        icon : new google.maps.MarkerImage('http://' + document.domain + '/sites/all/themes/wholefoods/images/store.png'),
        clickable : true,
      };

      var home_marker = {
        map : map,
        visible : true,
        icon : new google.maps.MarkerImage('http://' + document.domain + '/sites/all/themes/wholefoods/images/grocery.png'),
        clickable : true,
      };

      // For every marker in the list of markers, add it to the map and add an infoWindow.
      for (var i in markers_list) {
        var value = markers_list[i];
        var the_marker = marker;

        if (value['id'] == current_store['id'])
          the_marker = home_marker;

        // Add the title and position to the marker options template.
        the_marker['position'] = new google.maps.LatLng(value['lat'], value['lon']);
        the_marker['title'] = value['raw_title'];

        // Create the new marker (icon)
        // Add an if here.

          icon = new google.maps.Marker(the_marker);

        icon['id'] = i;

        // We'll add the title, description and links to the popup window.
        // TODO: Format this better.
        infoWindow[i] = new google.maps.InfoWindow({
          // CSS for the store-popup has to be defined on the tag itself, not in styles since the
          // objects are generated on the fly by Google. Apparently.
          content : '<div class="store-popop" style="font-size: 13px">' + value['description'] + value['telephone_number'] + value['links'] + '</div>',
        });

        google.maps.event.addListener(icon, 'click', function(the_item) {
          // On click of a marker, close the current popup and open the new one.
          if (openWindow != null) {
            openWindow.close();
            openWindow = null;
          }
          infoWindow[this['id']].open(map, this);
          openWindow = infoWindow[this['id']];
        });
      }
    }
  });
})(jQuery);
