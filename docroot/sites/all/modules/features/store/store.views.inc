<?php


/**
 * Implements hook_views_plugins().
 */
function store_views_plugins() {
  $plugins = array(
    'argument default' => array(
      'store_location' => array(
        'title' => t('Store Tid from User Store'),
        'handler' => 'views_plugin_argument_get_user_store_tid',
      ),
      'store_id_from_tid' => array(
        'title' => t('Store Nid from User Store Tid'),
        'handler' => 'views_plugin_argument_get_user_store_nid_from_tid',
      ),
    ),
  );
  return $plugins;
}

/**
 * @file
 * Views definitions for 'Store make users store form'
 */

/**
 * Implements hook_views_data().
 */
function store_views_data() {
  $data = array();
  $data['store']['table']['group'] = t('Store Locations - By State');
  $data['store']['table']['join'] = array(
    '#global' => array(),
  );
  // Add Store make users store field
  $data['store']['store_make_users_store'] = array(
    'title' => t('Store make users store.'),
    'help' => t('Provides a store make users store form.'),
    'field' => array(
      'handler' => 'store_handler_store_make_users_store',
    ),
  );
  // Add Alias code by State field
  $data['store']['alias_code_by_state'] = array(
    'title' => t('Alias code by State.'),
    'help' => t('Provides a alias code by State.'),
    'field' => array(
      'handler' => 'store_handler_alias_code_by_state',
    ),
  );
  return $data;
}
