<?php
/**
 * @file
 * store.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function store_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function store_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function store_image_default_styles() {
  $styles = array();

  // Exported image style: event_page.
  $styles['event_page'] = array(
    'effects' => array(
      52 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 280,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'event_page',
  );

  // Exported image style: event_thumbnail.
  $styles['event_thumbnail'] = array(
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 170,
          'height' => 105,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'event_thumbnail',
  );

  // Exported image style: locations_small.
  $styles['locations_small'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 226,
          'height' => 160,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'locations_small',
  );

  // Exported image style: promo.
  $styles['promo'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 430,
          'height' => 286,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'promo',
  );

  // Exported image style: promo_pod_large.
  $styles['promo_pod_large'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 418,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'promo_pod_large',
  );

  // Exported image style: stores_list_by_state_90x65.
  $styles['stores_list_by_state_90x65'] = array(
    'label' => 'stores_list_by_state 90x65',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 90,
          'height' => 65,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vertical_header.
  $styles['vertical_header'] = array(
    'label' => 'vertical_header',
    'effects' => array(
      53 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 310,
          'height' => 385,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function store_node_info() {
  $items = array(
    'department' => array(
      'name' => t('Department'),
      'base' => 'node_content',
      'description' => t('Departments within a store. These departments default to be global, but store admins may create unique departments for their store.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'department_article' => array(
      'name' => t('Department Article'),
      'base' => 'node_content',
      'description' => t('Articles specific to a department. These articles can be specific to a store by adding a Store to the content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Store Events (e.g. cooking classes, dinners, tastings, etc.). These events are specific to a store.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'metro' => array(
      'name' => t('Metro'),
      'base' => 'node_content',
      'description' => t('Represents a metro area.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'promo' => array(
      'name' => t('Promo'),
      'base' => 'node_content',
      'description' => t('Promotional pods. Order-able by administrators; can be associated with global, regional, metro or store level display.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'region' => array(
      'name' => t('Region'),
      'base' => 'node_content',
      'description' => t('Regional business units and regional offices.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'service' => array(
      'name' => t('Service'),
      'base' => 'node_content',
      'description' => t('Store services. Defaults to global content, but local admin may create a unique servie for their store.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'store' => array(
      'name' => t('Store'),
      'base' => 'node_content',
      'description' => t('A local store.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
