<?php

/**
 * @file
 * Contains the Store Tid argument default plugin.
 */

/**
 * Default argument plugin to use the Store Tid value from the User Store.
 *
 * @ingroup views_argument_default_plugins
 */
class views_plugin_argument_get_user_store_tid extends views_plugin_argument_default {


  function option_definition() {
    $options = parent::option_definition();
    $options['fields_conditions'] = array('default' => FALSE, 'bool' => TRUE);
    $options['nearby'] = array('default' => FALSE, 'bool' => TRUE);
    $options['fields'] = array('default' => '', 'string' => TRUE);
    $options['types'] = array('default' => 'store, national_offices, region, metro', 'string' => TRUE);
    $options['find_all'] = array('default' => FALSE);
    $options['include_global'] = array('default' => FALSE, 'bool' => TRUE);
    $options['include_parents'] = array('default' => FALSE, 'bool' => TRUE);
    $options['include_children'] = array('default' => FALSE, 'bool' => TRUE);

    return $options;
  }


  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['fields_conditions'] = array(
      '#type' => 'checkbox',
      '#title' => t('Find no empty fields'),
      '#default_value' => $this->options['fields_conditions'],
      '#description' => t('Look at user store and store parents to find filled fields to avoid empty data.'),
    );
    $form['fields'] = array(
      '#type' => 'textfield',
      '#title' => t('Fields to find.'),
      '#default_value' => $this->options['fields'],
      '#description' => t('Find inputed fields in user\'s and parent stores. Explode field names with comas. For example: field_pod_1_image, field_pod_1_content, field_pod_1_cta.'),
      '#process' => array('form_process_checkbox', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-argument-default-store-location-fields-conditions' => array(1),
      ),
    );
    $form['types'] = array(
      '#type' => 'textfield',
      '#title' => t('Content types.'),
      '#default_value' => $this->options['types'],
      '#description' => t('If filled the following content types will be load to check no empty fields. Explode content types with comas. For example: blog.'),
      '#process' => array('form_process_checkbox', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-argument-default-store-location-fields-conditions' => array(1),
      ),
    );
    $form['find_all'] = array(
      '#type' => 'checkbox',
      '#title' => t('Get all suitable parents'),
      '#default_value' => $this->options['find_all'],
      '#description' => t('If checked, script will not brake on first result, all suitable parents will be used. Please check "Allow multiple values" this feature to work properly.'),
      '#process' => array('form_process_checkbox', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-argument-default-store-location-fields-conditions' => array(1),
      ),
    );
    $form['nearby'] = array(
      '#type' => 'checkbox',
      '#title' => t('Nearby Stores'),
      '#default_value' => $this->options['nearby'],
      '#description' => t('Filter content for nearby stores.'),
      '#process' => array('form_process_checkbox', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-argument-default-store-location-fields-conditions' => array(0),
      ),
    );
    $form['include_global'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include Global Store TID'),
      '#default_value' => $this->options['include_global'],
      '#description' => t('Please check "Allow multiple values" this feature to work properly.'),
      '#process' => array('form_process_checkbox', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-argument-default-store-location-fields-conditions' => array(0),
      ),
    );
    $form['include_parents'] = array(
      '#type' => 'checkbox',
      '#title' => t('Also Include All Parents'),
      '#default_value' => $this->options['include_parents'],
      '#description' => t('Also include all parents of the store (metros, regions, national offices).'),
      '#process' => array('form_process_checkbox', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-argument-default-store-location-fields-conditions' => array(0),
        'edit-options-argument-default-store-location-include-global' => array(1),
      ),
      '#dependency_count' => 2,
    );
    $form['include_children'] = array(
      '#type' => 'checkbox',
      '#title' => t('Also Include All Children'),
      '#default_value' => $this->options['include_children'],
      '#description' => t('Also include all children of the location (regions, metros, stores).'),
      '#process' => array('form_process_checkbox', 'ctools_dependent_process'),
      '#dependency' => array(
        'edit-options-argument-default-store-location-fields-conditions' => array(0),
        'edit-options-argument-default-store-location-include-parents' => array(0),
        'edit-options-argument-default-store-location-include-global' => array(0),
      ),
      '#dependency_count' => 3,
    );
  }


  function get_argument() {
    $result = array();
    // Get Term TID from query string.
    if (isset($_GET['store'])) {
      $result[] = $_GET['store'];
    }
    else {
      // Try to receive Term TID from User Store.
      $store = store_get_user_store();
      if (isset($store->locations_acl[LANGUAGE_NONE][0]['tid'])) {
        $result[] = $store->locations_acl[LANGUAGE_NONE][0]['tid'];
      }
    }
    // Find no empty selected fields in current store or parent elements and
    // return the first found Term TID that is satisfies the conditions.
    if ($this->options['fields_conditions'] && !empty($this->options['fields'])) {
      if (!count($result)) {
        $result = array(wholefoods_get_global_node()->locations_acl[LANGUAGE_NONE][0]['tid']);
      }
      $tid = reset($result);
      if ($tid) {
        $result = array();
        $types = empty($this->options['types']) ? array('store') : drupal_explode_tags($this->options['types']);
        $fields = drupal_explode_tags($this->options['fields']);
        $parents = taxonomy_get_parents_all($tid);

        foreach ($parents as $term) {
          $store = store_get_store_from_tid($term->tid, $types);
          if (!is_null($store)) {
            foreach ($fields as $field_name) {
              if (field_get_items('node', $store, $field_name)) {
                $result[] = $term->tid;
                if ($this->options['find_all']) {
                  break;
                }
                else {
                  break 2;
                }
              }
            }
          }
        }
      }
    }
    else {
      // Also include Global Term TID.
      if ($this->options['include_global'] || !count($result)) {
        // Also include all parent term TIDs.
        if ($this->options['include_parents'] && count($result)) {
          $result += _store_taxonomy_get_children($result);
        }
        else {
          $result[] = core_get_global_term()->tid;
        }
      }
      if ($this->options['include_children']) {
        $result_children = store_taxonomy_get_children_ids($result[0]);
        $result = array_merge($result, $result_children);
      }
      if ($this->options['nearby']) {
        $result_nearby = array();
        $bundles = array('store', 'national_offices', 'metro', 'region');
        // get nids and locations for current stores.
        $query = db_select('field_data_field_geo_data', 'geo')->distinct();
        $query->addField('geo', 'entity_id', 'nid');
        $query->addExpression("CONCAT(geo.field_geo_data_lat, ':', geo.field_geo_data_lon)", 'loc');
        $query->innerJoin('field_data_locations_acl', 'l', 'geo.entity_id = l.entity_id AND geo.bundle = l.bundle');
        $query->condition('geo.bundle', $bundles, 'IN');
        $query->condition('l.locations_acl_tid', $result, 'IN');
        $query_result = $query->execute()->fetchAll();
        if (count($query_result)) {
          // Find nearby stores for each store
          $nids = array();
          foreach ($query_result as $store) {
            $new_view = views_get_view('store_lookup_by_address');
            $new_view->set_display('within_range');
            $new_view->preview = TRUE;
            $new_view->pre_execute(array($store->loc, 35, $store->nid));
            $new_view->execute();
            $new_view->post_execute();
            // Create an array with all nearby stores.
            foreach ($new_view->result as $v) {
              $nids[] = $v->entity;
            }
          }
          // Gether locations TIDs from nearby stores.
          if (count($nids)) {
            $stores = node_load_multiple($nids);
            foreach ($stores as $store) {
              if (isset($store->locations_acl[LANGUAGE_NONE][0]['tid'])) {
                $result_nearby[] = $store->locations_acl[LANGUAGE_NONE][0]['tid'];
              }
            }
          }
        }
        // Change current location TIDs with nearby locations TIDs.
        if ($this->options['include_parents']) {
          $result_nearby += _store_taxonomy_get_children($result_nearby);
          $result_nearby = array_unique($result_nearby);
        }
        $result = $result_nearby;
      }
    }
    if (count($result)) {
      return implode('+', array_unique($result));
    }
  }
}
