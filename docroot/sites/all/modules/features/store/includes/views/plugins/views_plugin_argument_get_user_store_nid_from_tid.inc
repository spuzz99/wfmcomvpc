<?php

/**
 * @file
 * Contains the Store Tid argument default plugin to get store id.
 */

/**
 * Default argument plugin to use the Store Tid value from the User Store or from url.
 *
 * @ingroup views_argument_default_plugins
 */
class views_plugin_argument_get_user_store_nid_from_tid extends views_plugin_argument_default {

  function get_argument() {
    // Get Term TID from query string.

    if (isset($_GET['tid']) && is_numeric($_GET['tid'])) {
      $nid =  store_get_store_from_tid($_GET['tid'], array('store'), TRUE);
    }
    else {
      // Try to receive Term TID from User Store.
      $store = store_get_user_store();
      if (!empty($store)) {
        $nid =  $store->nid;
      }
    }
    return $nid;
  }

}

