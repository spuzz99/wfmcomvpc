<?php
/**
 * Create a subclass of SearchApiViewsHandlerFilterLocation as to suit our
 * purposes with some custom code.
 */
class SearchApiViewsHandlerFilterLocation_store extends SearchApiLocationMapViewsHandlerFilter {
  /**
   * Add this filter to the query.
   */
  public function query() {

    // Set the radius of the search from the view's options.
    $spatial['radius'] = $radius = $this->options['radius'];
    $spatial['radius_measure'] = $this->options['radius_measure'] ? $this->options['radius_measure'] : 'km';
    $spatial['field'] = $this->real_field;
    // If args are passed, attempt to use those arguments as the lat/lon, rad
    // and possibly a store NID to ignore.

    // Args are passed as lat:long[/radius/nid]
    if (!empty($this->view->args)) {
      $view_args = $this->view->args[0];

      if (is_string($view_args)) {
        $view_args = explode(':', $view_args);
      }
      else {
        $this->view->args[0] = '';
      }

      // Make sure both lat and long are actualy passed.
      if (count($view_args) == 2) {
        $spatial['lat'] = (float) $view_args[0];
        $spatial['lon'] = (float) $view_args[1];
      }

      // If an extra arg is present, it should be radius. Always measured
      // in the unit defined by the view.
      if (isset($this->view->args[1])) {
        $spatial['radius'] = $this->view->args[1];
      }

      // Finally if a third argument is passed, it should be the NID of a store to ignore.
      if (isset($this->view->args[2])) {
        $this->query->condition('nid', $this->view->args[2], '<>');
      }
    }

    if (isset($_GET['address'])) {
      $this->view->exposed_input['address'] = check_plain($_GET['address']);
    }

    $address = isset($this->view->exposed_input['address']) ? $this->view->exposed_input['address'] : '';
    setcookie('store_last_search_address', $address, 0, '/');

    if (!empty($address)) {
      $holder = _get_loc_from_string($address);
      if ($holder) {
        $this->view->exposed_input['field_geo_data_latlon']['lat'] = $spatial['lat'] = (string) $holder['lat'];
        $this->view->exposed_input['field_geo_data_latlon']['lng'] = $spatial['lon'] = (string) $holder['lon'];

      }
      else {
        $spatial['radius'] = 0;
      }
    }
    else {
      if (isset($this->view->exposed_input['field_geo_data_latlon']['lat']) && $this->view->exposed_input['field_geo_data_latlon']['lat'] != 1
          && isset($this->view->exposed_input['field_geo_data_latlon']['lng']) && $this->view->exposed_input['field_geo_data_latlon']['lng'] != 1) {
        $spatial['lat'] = $this->view->exposed_input['field_geo_data_latlon']['lat'];
        $spatial['lon'] = $this->view->exposed_input['field_geo_data_latlon']['lng'];
      }
    }

    $this->query->setOption('search_api_location', array($spatial));

    // ToDo: test solr changing names and use this command instead of store_search_api_solr_query_alter
    //$this->query->sort($spatial['field'], 'ASC');
  }
}
