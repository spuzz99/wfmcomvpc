<?php
/**
 * @file
 * feature_marquee.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function feature_marquee_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'marquee_page';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Marquee - Page';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Heading Copy */
  $handler->display->display_options['fields']['field_heading_copy']['id'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['table'] = 'field_data_field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['field'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['label'] = '';
  $handler->display->display_options['fields']['field_heading_copy']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['validate']['fail'] = 'ignore';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['footer'] = FALSE;
  /* Footer: Heading Image */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['ui_name'] = 'Heading Image';
  $handler->display->display_options['footer']['area']['empty'] = TRUE;
  $handler->display->display_options['footer']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['empty'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Heading Copy */
  $handler->display->display_options['fields']['field_heading_copy']['id'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['table'] = 'field_data_field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['field'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['label'] = '';
  $handler->display->display_options['fields']['field_heading_copy']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_heading_copy']['hide_empty'] = TRUE;

  /* Display: Heading Image */
  $handler = $view->new_display('block', 'Heading Image', 'heading_image');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Heading Image */
  $handler->display->display_options['fields']['field_heading_background']['id'] = 'field_heading_background';
  $handler->display->display_options['fields']['field_heading_background']['table'] = 'field_data_field_heading_background';
  $handler->display->display_options['fields']['field_heading_background']['field'] = 'field_heading_background';
  $handler->display->display_options['fields']['field_heading_background']['label'] = '';
  $handler->display->display_options['fields']['field_heading_background']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_heading_background']['empty'] = '<img src="/sites/all/themes/wholefoods/images/blank-marquee.gif" width="1680" height="521" />';
  $handler->display->display_options['fields']['field_heading_background']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_heading_background']['settings'] = array(
    'image_style' => 'marquee_other',
    'image_link' => '',
  );
  $export['marquee_page'] = $view;

  $view = new view();
  $view->name = 'marquee_scroller';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Marquee Scroller';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '8';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'nid' => 'nid',
    'field_marquee_media' => 'field_marquee_media',
    'field_marquee_url' => 'field_marquee_url',
    'field_heading_copy' => 'field_heading_copy',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_marquee_media' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_marquee_url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_heading_copy' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: File Usage: File */
  $handler->display->display_options['relationships']['node_to_file']['id'] = 'node_to_file';
  $handler->display->display_options['relationships']['node_to_file']['table'] = 'file_usage';
  $handler->display->display_options['relationships']['node_to_file']['field'] = 'node_to_file';
  $handler->display->display_options['relationships']['node_to_file']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_marquee_media']['id'] = 'field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['table'] = 'field_data_field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['field'] = 'field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['label'] = '';
  $handler->display->display_options['fields']['field_marquee_media']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_marquee_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_marquee_media']['type'] = 'styles_media_marquee';
  $handler->display->display_options['fields']['field_marquee_media']['settings'] = array(
    'file_view_mode' => 'file_styles_marqee',
  );
  /* Field: Content: Marquee Button */
  $handler->display->display_options['fields']['field_marquee_url']['id'] = 'field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['table'] = 'field_data_field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['field'] = 'field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['label'] = '';
  $handler->display->display_options['fields']['field_marquee_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_marquee_url']['alter']['text'] = '<a href="[field_marquee_url-url]">[field_marquee_url-title]</a>';
  $handler->display->display_options['fields']['field_marquee_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_marquee_url']['type'] = 'link_separate';
  /* Field: Content: Heading Copy */
  $handler->display->display_options['fields']['field_heading_copy']['id'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['table'] = 'field_data_field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['field'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['label'] = '';
  $handler->display->display_options['fields']['field_heading_copy']['element_label_colon'] = FALSE;
  /* Field: File: Path */
  $handler->display->display_options['fields']['uri']['id'] = 'uri';
  $handler->display->display_options['fields']['uri']['table'] = 'file_managed';
  $handler->display->display_options['fields']['uri']['field'] = 'uri';
  $handler->display->display_options['fields']['uri']['relationship'] = 'node_to_file';
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_marquee_media_1']['id'] = 'field_marquee_media_1';
  $handler->display->display_options['fields']['field_marquee_media_1']['table'] = 'field_data_field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media_1']['field'] = 'field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_marquee_media_1']['alter']['text'] = '[uri]';
  $handler->display->display_options['fields']['field_marquee_media_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_media_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_marquee_media_1']['type'] = 'media';
  $handler->display->display_options['fields']['field_marquee_media_1']['settings'] = array(
    'file_view_mode' => 'default',
  );
  /* Sort criterion: Global: PHP */
  $handler->display->display_options['sorts']['php']['id'] = 'php';
  $handler->display->display_options['sorts']['php']['table'] = 'views';
  $handler->display->display_options['sorts']['php']['field'] = 'php';
  $handler->display->display_options['sorts']['php']['use_php_setup'] = 1;
  $handler->display->display_options['sorts']['php']['php_setup'] = '$static = wholefoods_get_marquee_nodes(8);';
  $handler->display->display_options['sorts']['php']['php_sort'] = '$key_1 = array_search($row1->nid, $static);
$key_2 = array_search($row2->nid, $static);

if ($key_1 < $key_2)
  return -1;
return 1;
';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'php';
  $handler->display->display_options['arguments']['nid']['default_argument_options']['code'] = 'return 0;';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['nid']['validate']['type'] = 'php';
  $handler->display->display_options['arguments']['nid']['validate_options']['code'] = '$handler->argument = implode(\'+\', wholefoods_get_marquee_nodes(8));
return TRUE;
';
  $handler->display->display_options['arguments']['nid']['break_phrase'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'marquee' => 'marquee',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_marquee_media']['id'] = 'field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['table'] = 'field_data_field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['field'] = 'field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media']['label'] = '';
  $handler->display->display_options['fields']['field_marquee_media']['alter']['path'] = 'node/[nid]';
  $handler->display->display_options['fields']['field_marquee_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_marquee_media']['type'] = 'styles_media_marquee';
  $handler->display->display_options['fields']['field_marquee_media']['settings'] = array(
    'file_view_mode' => 'file_styles_marqee',
  );
  /* Field: Content: Marquee Button */
  $handler->display->display_options['fields']['field_marquee_url']['id'] = 'field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['table'] = 'field_data_field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['field'] = 'field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url']['label'] = '';
  $handler->display->display_options['fields']['field_marquee_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_marquee_url']['alter']['text'] = '<a href="[field_marquee_url-url]">[field_marquee_url-title]</a>';
  $handler->display->display_options['fields']['field_marquee_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_marquee_url']['type'] = 'link_separate';
  /* Field: Content: Heading Copy */
  $handler->display->display_options['fields']['field_heading_copy']['id'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['table'] = 'field_data_field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['field'] = 'field_heading_copy';
  $handler->display->display_options['fields']['field_heading_copy']['label'] = '';
  $handler->display->display_options['fields']['field_heading_copy']['element_label_colon'] = FALSE;
  /* Field: File: Path */
  $handler->display->display_options['fields']['uri']['id'] = 'uri';
  $handler->display->display_options['fields']['uri']['table'] = 'file_managed';
  $handler->display->display_options['fields']['uri']['field'] = 'uri';
  $handler->display->display_options['fields']['uri']['relationship'] = 'node_to_file';
  /* Field: Content: Media */
  $handler->display->display_options['fields']['field_marquee_media_1']['id'] = 'field_marquee_media_1';
  $handler->display->display_options['fields']['field_marquee_media_1']['table'] = 'field_data_field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media_1']['field'] = 'field_marquee_media';
  $handler->display->display_options['fields']['field_marquee_media_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_marquee_media_1']['alter']['text'] = '[uri]';
  $handler->display->display_options['fields']['field_marquee_media_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_media_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_marquee_media_1']['type'] = 'media';
  $handler->display->display_options['fields']['field_marquee_media_1']['settings'] = array(
    'file_view_mode' => 'default',
  );
  /* Field: Marquee Button Text */
  $handler->display->display_options['fields']['field_marquee_url_1']['id'] = 'field_marquee_url_1';
  $handler->display->display_options['fields']['field_marquee_url_1']['table'] = 'field_data_field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url_1']['field'] = 'field_marquee_url';
  $handler->display->display_options['fields']['field_marquee_url_1']['ui_name'] = 'Marquee Button Text';
  $handler->display->display_options['fields']['field_marquee_url_1']['label'] = '';
  $handler->display->display_options['fields']['field_marquee_url_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_marquee_url_1']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_marquee_url_1']['type'] = 'link_title_plain';
  /* Field: Content: Item Type */
  $handler->display->display_options['fields']['field_estore_item_type']['id'] = 'field_estore_item_type';
  $handler->display->display_options['fields']['field_estore_item_type']['table'] = 'field_data_field_estore_item_type';
  $handler->display->display_options['fields']['field_estore_item_type']['field'] = 'field_estore_item_type';
  $handler->display->display_options['fields']['field_estore_item_type']['label'] = '';
  $handler->display->display_options['fields']['field_estore_item_type']['element_label_colon'] = FALSE;
  /* Field: Content: eStore Item */
  $handler->display->display_options['fields']['field_estore_item']['id'] = 'field_estore_item';
  $handler->display->display_options['fields']['field_estore_item']['table'] = 'field_data_field_estore_item';
  $handler->display->display_options['fields']['field_estore_item']['field'] = 'field_estore_item';
  $handler->display->display_options['fields']['field_estore_item']['label'] = '';
  $handler->display->display_options['fields']['field_estore_item']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_estore_item']['type'] = 'text_plain';

  /* Display: Recipe Landing Page */
  $handler = $view->new_display('block', 'Recipe Landing Page', 'recipe');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: File Usage: File */
  $handler->display->display_options['relationships']['node_to_file']['id'] = 'node_to_file';
  $handler->display->display_options['relationships']['node_to_file']['table'] = 'file_usage';
  $handler->display->display_options['relationships']['node_to_file']['field'] = 'node_to_file';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = '';
  $handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_value'] = '$view = views_embed_view(\'marquee_page\', \'block\');
$static->default = $view';
  $handler->display->display_options['fields']['php']['php_output'] = '<?php
//dsm($data);
//$view = views_embed_view(\'marquee_page\', \'block\', $data->nid);
$nid = wholefoods_get_marquee_by_page($data->nid);
//dsm($nid);
$mview = views_embed_view(\'marquee_page\', \'heading_image\', $nid);
//dsm($mview);
print $mview;
?>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'recipe' => 'recipe',
  );
  /* Filter criterion: Content: Featured In Promo? (field_is_promoted_featured) */
  $handler->display->display_options['filters']['field_is_promoted_featured_value']['id'] = 'field_is_promoted_featured_value';
  $handler->display->display_options['filters']['field_is_promoted_featured_value']['table'] = 'field_data_field_is_promoted_featured';
  $handler->display->display_options['filters']['field_is_promoted_featured_value']['field'] = 'field_is_promoted_featured_value';
  $handler->display->display_options['filters']['field_is_promoted_featured_value']['value'] = array(
    1 => '1',
  );
  $export['marquee_scroller'] = $view;

  return $export;
}
