<?php
/**
 * @file
 * feature_marquee.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function feature_marquee_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'estore marquee'.
  $permissions['estore marquee'] = array(
    'name' => 'estore marquee',
    'roles' => array(
      'Global Content Admin' => 'Global Content Admin',
      'administrator' => 'administrator',
    ),
    'module' => 'wfm_marquee',
  );

  return $permissions;
}
