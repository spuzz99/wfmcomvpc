<?php
/**
 * @file
 * feature_marquee.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_marquee_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feature_marquee_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function feature_marquee_image_default_styles() {
  $styles = array();

  // Exported image style: marqee.
  $styles['marqee'] = array(
    'label' => 'marqee',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1680,
          'height' => 748,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: marquee_other.
  $styles['marquee_other'] = array(
    'label' => 'marquee_other',
    'effects' => array(
      2 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 1680,
          'height' => 521,
          'anchor' => 'center-center',
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function feature_marquee_node_info() {
  $items = array(
    'marquee' => array(
      'name' => t('Marquee'),
      'base' => 'node_content',
      'description' => t('Content in the scrolling homepage marquee. Composed of global, regional, metro and store content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
