<?php
/**
 * @file
 * product_recalls.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function product_recalls_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_recalls_page_context';
  $context->description = 'Context to display the Product Recalls view on the Product Recalls page.';
  $context->tag = 'Products';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'node/6828' => 'node/6828',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-fd2a6c0255cf3ee31b1c0243bf5a47ed' => array(
          'module' => 'views',
          'delta' => 'fd2a6c0255cf3ee31b1c0243bf5a47ed',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context to display the Product Recalls view on the Product Recalls page.');
  t('Products');
  $export['product_recalls_page_context'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_recalls_single_view';
  $context->description = 'Context to display a single Product Recall';
  $context->tag = 'Products';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'product_recall' => 'product_recall',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-cb00699a33fc5f65fa84987068ec89d9' => array(
          'module' => 'views',
          'delta' => 'cb00699a33fc5f65fa84987068ec89d9',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context to display a single Product Recall');
  t('Products');
  $export['product_recalls_single_view'] = $context;

  return $export;
}
