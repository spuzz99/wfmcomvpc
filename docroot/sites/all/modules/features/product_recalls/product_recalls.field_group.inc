<?php
/**
 * @file
 * product_recalls.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function product_recalls_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_recall_basics|node|product_recall|form';
  $field_group->group_name = 'group_product_recall_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_recall';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_product_recall';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '102',
    'children' => array(
      0 => 'body',
      1 => 'field_recall_date',
      2 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_product_recall_basics|node|product_recall|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_recall|node|product_recall|form';
  $field_group->group_name = 'group_product_recall';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_recall';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Product Recall',
    'weight' => '0',
    'children' => array(
      0 => 'locations_acl',
      1 => 'group_product_recall_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_product_recall|node|product_recall|form'] = $field_group;

  return $export;
}
