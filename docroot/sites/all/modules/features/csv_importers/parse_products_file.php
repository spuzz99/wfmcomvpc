<?php

$row = 1;
$lines = array();
$head = array();
$outlines = array();
$categories = array('brand' => array(), 'specialdiets' => array(), 'certifications' => array(), 'brand_other' => array());
if (($handle = fopen("products.csv", "r")) !== FALSE) {
  while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
    $extra_lines = array();
    $num = count($data);
    ++$row;
    if ($row == 2) {
      foreach ($data as $k => $v) {
        $head[$v] = $k;
      }
      print_r($data);
      $outlines[] = $data;
      continue;
    }

    $pid = $data[0];
    if ($pid == 32) {
      print_r($data);
    }

    foreach ($categories as $cat => $val) {
      if (isset($data[$head[$cat]])) {

        $lines = explode(',', $data[$head[$cat]]);
        /*
        if ($cat == 'brand')
          print_r($lines);
        */
        if (count($lines) > 1) {
          $data[$head[$cat]] = trim($lines[0], ' ');
          $counter = 0;
          foreach ($lines as $k => $v) {
            if ($counter > 0) {
              $extra_line = $data;
              $extra_line[$head[$cat]] = trim($v, ' ');
							$extra_line = preg_replace('/[^(\x20-\x7F)]*/','', $extra_line);
              $extra_lines[] = $extra_line;
            }
            ++$counter;
          }
        }

        foreach ($lines as $k => $v) {
          $v = trim($v, ' ');
          if (strlen($v) < 1)
            continue;
          $categories[$cat][md5($v)] = $v;
        }

      }
    }

    $outlines[] = $data;
    foreach ($extra_lines as $v) {
      $outlines[] = $v;
    }
  }
  fclose($handle);
}

$fp = fopen('new_products' . '.csv', 'w');
foreach ($outlines as $fields) {
  fputcsv($fp, $fields);
}

fclose($fp);

foreach ($categories as $k => $v) {
  $fp = fopen($k . '.csv', 'w');
  foreach ($v as $key => $fields) {
    fputcsv($fp, array('pid' => $key, 'title' => $fields));
  }
  fclose($fp);
}
