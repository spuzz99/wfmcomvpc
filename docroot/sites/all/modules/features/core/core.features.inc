<?php
/**
 * @file
 * core.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ckeditor_styles" && $api == "ckeditor_style_rules") {
    return array("version" => "1");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function core_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function core_image_default_styles() {
  $styles = array();

  // Exported image style: article_thumbnail.
  $styles['article_thumbnail'] = array(
    'effects' => array(
      39 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 180,
          'height' => 180,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'article_thumbnail',
  );

  // Exported image style: draggable_thumb.
  $styles['draggable_thumb'] = array(
    'effects' => array(
      8 => array(
        'name' => 'coloractions_convert',
        'data' => array(
          'format' => 'image/png',
          'quality' => 75,
        ),
        'weight' => 1,
      ),
      9 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 75,
          'height' => 75,
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
      11 => array(
        'name' => 'canvasactions_definecanvas',
        'data' => array(
          'RGB' => array(
            'HEX' => 'ffffff',
          ),
          'under' => 1,
          'exact' => array(
            'width' => 75,
            'height' => 75,
            'xpos' => 'center',
            'ypos' => 'center',
          ),
          'relative' => array(
            'leftdiff' => '',
            'rightdiff' => '',
            'topdiff' => '',
            'bottomdiff' => '',
          ),
        ),
        'weight' => 3,
      ),
    ),
    'label' => 'draggable_thumb',
  );

  // Exported image style: featured_video.
  $styles['featured_video'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 911,
          'height' => 674,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'featured_video',
  );

  // Exported image style: header_hero.
  $styles['header_hero'] = array(
    'effects' => array(
      53 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 430,
          'height' => 286,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'header_hero',
  );

  // Exported image style: hero_image.
  $styles['hero_image'] = array(
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 260,
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'hero_image',
  );

  // Exported image style: related_content_right.
  $styles['related_content_right'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 213,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'related_content_right',
  );

  // Exported image style: related_image.
  $styles['related_image'] = array(
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 210,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'related_image',
  );

  // Exported image style: small_pod.
  $styles['small_pod'] = array(
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 260,
          'height' => 160,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'small_pod',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function core_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Use <em>pages</em> as a generic template for landing pages (e.g. Healthy Eating, Products, etc.) and basic pages (e.g. Terms of Use, Privacy Policy, etc.).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'video' => array(
      'name' => t('Video'),
      'base' => 'node_content',
      'description' => t('Video content'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_styles_default_styles().
 */
function core_styles_default_styles() {
  $styles = array();

  // Exported style: featured_video

  $styles['file']['styles']['featured_video'] = array(
    'label' => 'featured_video',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'featured_video' => array(
          'default preset' => 'original',
          'preset' => 'featured_video',
        ),
      ),
      'audio' => array(
        'featured_video' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'featured_video' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'featured_video' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'featured_video' => array(
          'default preset' => 'unlinked_thumbnail',
          'preset' => 'unlinked_featured_video',
        ),
      ),
    ),
  );
  // Exported style: header_hero

  $styles['file']['styles']['header_hero'] = array(
    'label' => 'header_hero',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'header_hero' => array(
          'default preset' => 'original',
          'preset' => 'header_hero',
        ),
      ),
      'audio' => array(
        'header_hero' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'header_hero' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'header_hero' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'header_hero' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: product_hero

  $styles['file']['styles']['product_hero'] = array(
    'label' => 'product_hero',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'product_hero' => array(
          'default preset' => 'original',
          'preset' => 'product_hero',
        ),
      ),
      'audio' => array(
        'product_hero' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'product_hero' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'product_hero' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'product_hero' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: promo

  $styles['file']['styles']['promo'] = array(
    'label' => 'promo',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'promo' => array(
          'default preset' => 'original',
          'preset' => 'promo',
        ),
      ),
      'audio' => array(
        'promo' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'promo' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'promo' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'promo' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  // Exported style: small_pod

  $styles['file']['styles']['small_pod'] = array(
    'label' => 'small_pod',
    'description' => '',
    'preset_info' => array(
      'image' => array(
        'small_pod' => array(
          'default preset' => 'original',
          'preset' => 'small_pod',
        ),
      ),
      'audio' => array(
        'small_pod' => array(
          'default preset' => 'original',
        ),
      ),
      'video' => array(
        'small_pod' => array(
          'default preset' => 'original',
        ),
      ),
      'default' => array(
        'small_pod' => array(
          'default preset' => 'original',
        ),
      ),
      'media_youtube' => array(
        'small_pod' => array(
          'default preset' => 'unlinked_thumbnail',
        ),
      ),
    ),
  );
  return $styles;
}

/**
 * Implements hook_styles_default_presets_alter().
 */
function core_styles_default_presets_alter(&$presets) {
  $styles = styles_default_styles();
  if ($styles['file']['styles']['featured_video']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['featured_video'] = array(
      'default preset' => 'original',
      'preset' => 'featured_video',
    );

    $presets['file']['containers']['audio']['styles']['featured_video'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['featured_video'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['featured_video'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['featured_video'] = array(
      'default preset' => 'unlinked_thumbnail',
      'preset' => 'unlinked_featured_video',
    );

  }
  if ($styles['file']['styles']['header_hero']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['header_hero'] = array(
      'default preset' => 'original',
      'preset' => 'header_hero',
    );

    $presets['file']['containers']['audio']['styles']['header_hero'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['header_hero'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['header_hero'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['header_hero'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['file']['styles']['product_hero']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['product_hero'] = array(
      'default preset' => 'original',
      'preset' => 'product_hero',
    );

    $presets['file']['containers']['audio']['styles']['product_hero'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['product_hero'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['product_hero'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['product_hero'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['file']['styles']['promo']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['promo'] = array(
      'default preset' => 'original',
      'preset' => 'promo',
    );

    $presets['file']['containers']['audio']['styles']['promo'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['promo'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['promo'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['promo'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
  if ($styles['file']['styles']['small_pod']['storage'] == STYLES_STORAGE_DEFAULT) {
    $presets['file']['containers']['image']['styles']['small_pod'] = array(
      'default preset' => 'original',
      'preset' => 'small_pod',
    );

    $presets['file']['containers']['audio']['styles']['small_pod'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['video']['styles']['small_pod'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['default']['styles']['small_pod'] = array(
      'default preset' => 'original',
    );

    $presets['file']['containers']['media_youtube']['styles']['small_pod'] = array(
      'default preset' => 'unlinked_thumbnail',
    );

  }
}
