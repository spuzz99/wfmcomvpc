(function($) {
  $(function() {

    var mobile = 'wholefoods' !== Drupal.settings.WholeFoods.themeDefault
      , heading = $('#node_page_form_group_heading .fieldset-wrapper')
      , box = $('#node_page_form_group_heading_marquee_box', heading)
      , boxMobile = $('#node_page_form_group_heading_marquee_box_mobile', heading)
      , content = $('#node_page_form_group_heading_marquee_content', heading);
    $('#edit-field-page-heading-layout input').change(function() {
      switch ($(this).val()) {
        case 'none':
          $('> fieldset', heading).hide();
          break;

        case 'marquee_box':
          content.hide();
          box.add(boxMobile).show();
          if (!mobile && !boxMobile.hasClass('collapsed') || mobile && boxMobile.hasClass('collapsed')) {
            $('a.fieldset-title', boxMobile).click();
          }
          break;

        case 'marquee_content':
          box.add(boxMobile).hide();
          content.show();
          break;
      }
    }).filter(':checked').change();

    var body = $('#node_page_form_group_body .fieldset-wrapper')
      , copy = $('#node_page_form_group_body_copy', body)
      , copyMobile = $('#node_page_form_group_body_mobile', copy)
      , list = $('#node_page_form_group_body_list', body)
      , free = $('#node_page_form_group_body_free_form', body);
    $('#edit-field-page-body-layout input').change(function() {
      switch ($(this).val()) {
        case 'none':
          $('> fieldset', body).hide();
          break;

        case 'copy':
          list.add(free).hide();
          copy.show();
          if (!mobile && !copyMobile.hasClass('collapsed') || mobile && copyMobile.hasClass('collapsed')) {
            $('a.fieldset-title', copyMobile).click();
          }
          break;

        case 'list':
          copy.add(free).hide();
          list.show();
          break;

        case 'free_form':
          list.add(copy).hide();
          free.show();
          break;
      }
    }).filter(':checked').change();

    var expand = $('#edit-field-body-list-expand input')
      , expanded = $('.field-name-field-body-list-expanded-copy, .field-name-field-body-list-expanded-image')
      , links = $('.field-name-field-body-list-cta-link-node, .field-name-field-body-list-cta-link-ext');
    expand.change(function() {
      $(this).is(':checked') ? (links.hide(), expanded.show()) : (expanded.hide(), links.show());
    }).change();

    Drupal.behaviors.pageNodeEditExpandList = {
      attach: function () {
        expand.change();
      }
    };

  });
})(jQuery);
