<?php

/**
 * The name of the filter function to filter and order marquee's shown.
 *
 * This should be updated when/if a new method is  ever created.
 */
define('WHOLEFOODS_MARQUEE_FILTER_FUNCTION', '_wholefoods_filter_marquee_nodes_level');

/**
 * The field name on nodes that holds node references to marquees.
 */
define('WHOLEFOODS_MARQUEE_FIELD_NAME', 'field_marquee');

/**
 * The global headquarters content type machine name.
 */
define('WHOLEFOODS_TYPE_HEADQUARTERS', 'national_offices');

/**
 * Names of the cateorigies used in the list passed to filter functions.
 */
define('CATEGORY_STORE', 'store');
define('CATEGORY_METRO', 'metro');
define('CATEGORY_GLOBAL', 'global');
define('CATEGORY_REGION', 'region');
define('CATEGORY_NATIONAL', 'national_office');


/**
 * Processing method for wholefoods_get_marquee_by_page.
 *
 * @see wholefoods_get_marquee_by_page
 */
function _wholefoods_get_marquee_by_page($useful_node = NULL, $path = NULL) {
  $node = $useful_node;

  if ($useful_node !== NULL) {
    if (arg(0) == 'node' && is_numeric(arg(1))) {
      $node = node_load(arg(1));
      if ($node === FALSE) {
        continue;
      }

      if (isset($node->field_heading_background[LANGUAGE_NONE][0])) {
        $useful_node = $node;
      }
    }
  }
  else {
    module_load_include('module', 'blog', 'blog');
    $blog = blogs_get_current_blog_nid(FALSE);

    if ($blog !== NULL) {
      $useful_node = node_load($blog);
    }
  }

  $menu = menu_get_active_trail();

  while (!empty($menu) && $useful_node == NULL) {
    $current = array_pop($menu);
    if (empty($link_path)) {
      continue;
    }
    $link_path = $current['link_path'];
    $link_parts = explode('/', $link_path);

    if (count($link_parts) < 2) {
      continue;
    }

    if ($link_parts[0] != 'node' || !is_numeric($link_parts[1])) {
      continue;
    }

    $node = node_load($link_parts[1]);
    if ($node === FALSE) {
      continue;
    }

    if (isset($node->field_heading_background[LANGUAGE_NONE][0])) {
      $useful_node = $node;
      break;
    }
  }

  // If nothing has been found, return NULL.
  if ($useful_node === NULL) {
    return _wholefoods_get_marquee_by_page_via_path($useful_node, $path);
  }
  return $useful_node->nid;
}


function _wholefoods_get_marquee_by_page_via_path($useful_node = NULL, $path = NULL) {
  if ($path == NULL) {
    if (arg(0) == 'node') {
      $path = drupal_lookup_path('alias', 'node/' . arg(1));
    }
    else {
      $path = '';
      $i = 0;
      while (arg($i) != NULL) {
        $path .= '/' . arg($i);
        ++$i;
      }
    }
  }

  $exploded_path = explode('/', $path);
  $last_part = array_pop($exploded_path);
  $path = implode('/', $exploded_path);

  if ($path != '') {
    $previous = drupal_lookup_path('source', $path);
    if ($previous) {
      $node = explode('/', $previous);
      $node = array_pop($node);
      $node = node_load($node);
      if ($node) {
        return _wholefoods_get_marquee_by_page($node, $path);
      }
    }
    return _wholefoods_get_marquee_by_page_via_path(NULL, $path);
  }
  return 0;
}


/**
 * Processing method for wholefoods_get_global_node.
 *
 * @see wholefoods_get_global_node
 */
function _wholefoods_get_global_node($use_global = FALSE) {
  $global_node = &drupal_static(__FUNCTION__);
  $type = $use_global ? 'global' : 'local';
  if (!isset($global_node)) {
    $global_node['local'] = NULL;
    $global_node['global'] = NULL;
  }
  if ($global_node[$type] == NULL) {
    $global_nid = NULL;
    if (!$use_global && $global_nid == NULL) {
      $user_store = store_get_user_store();
      if ($user_store != NULL) {
        // Look by store.
        if (!empty($user_store->field_national_office)) {
          $global_nid = $user_store->field_national_office[$user_store->language][0]['nid'];
        }
      }
      // Try by TLD.
      if ($global_nid == NULL) {
        // Get TLD.
        $host = $_SERVER['HTTP_HOST'];
        $host_parts = explode('.', $host);
        $tld = array_pop($host_parts);
        $sub_domain = NULL;
        if (count($host_parts) > 1) {
          $sub_domain = array_shift($host_parts);
        }
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', WHOLEFOODS_TYPE_HEADQUARTERS)
            ->fieldCondition('field_top_level_domain', 'value', $tld, '=')
            ->range(0, 1)
            ->addMetaData('account', user_load(1));
        $result = $query->execute();
        if (array_key_exists('node', $result) && count($result['node'])) {
          $global_nid = array_shift($result['node'])->nid;
        }
      }
      if (!$global_nid) {
        $global_nid = variable_get('default_national_office');
      }
    }
    if ($global_nid == NULL) {
      $vocabulary = taxonomy_vocabulary_machine_name_load('locations_acl');
      $global_terms = taxonomy_get_tree($vocabulary->vid, 0, 1);
      $global_term = reset($global_terms);
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', WHOLEFOODS_TYPE_HEADQUARTERS)
          ->fieldCondition('locations_acl', 'tid', $global_term->tid, '=')
          ->range(0, 1)
          ->addMetaData('account', user_load(1));
      $result = $query->execute();
      $global_nid = array_shift($result['node'])->nid;
    }
    $global_node[$type] = node_load($global_nid);
  }

  return $global_node[$type];
}

/**
 * Return national node for given store NID, or when none is provide
 * deduce appropriate national node based upon TLD or SmartIP.
 *
 * @param $store_nid
 *  store_nid - the store's unique three letter code
 *
 * @return object
 *  a national node object
 * 
 * @see wholefoods_get_national_node
 */
function _wholefoods_get_national_node($store_nid = NULL) {
    $national_nid = NULL;
    // first try to get national office from store if provided
    if (!empty($store_nid)) { // if $store_nid is provided
      //dd(date("D M j G:i:s") .':helpers.inc $national_nid defined in call');
      $store = node_load($store_nid);
      if (!empty($store->field_national_office)) {
          $national_nid = $store->field_national_office[$user_store->language][0]['nid'];
      }
    }
    else{  // otherwise try to discover 
      // first by TLD.
      // dd('helpers.inc $national_nid being set by TLD');
      $host = $_SERVER['HTTP_HOST'];
      $host_parts = explode('.', $host);
      $tld = array_pop($host_parts);
      $sub_domain = NULL;
      if (count($host_parts) > 1) {
          $sub_domain = array_shift($host_parts);
      }
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', WHOLEFOODS_TYPE_HEADQUARTERS)
            ->fieldCondition('field_top_level_domain', 'value', $tld, '=')
            ->range(0, 1)
            ->addMetaData('account', user_load(1));
      $result = $query->execute();
      if (array_key_exists('node', $result) && count($result['node'])) {
          $national_nid = array_shift($result['node'])->nid;
          //dd($national_nid, date("D M j G:i:s") .':helpers.inc $national_nid set by TLD');
      }
      // If TLD fails fall back to set variable
      if (!$national_nid) {
        //dd('helpers.inc $national_nid being set by variable');
        $national_nid = variable_get('default_national_office');
        //dd($national_nid, date("D M j G:i:s") .':helpers.inc $national_nid set by variable');
      }
    } // we should have a national_NID by now
    
    $national_node = node_load($national_nid);
    //dd($national_node->title, 'helpers.inc $national_node->title for NID ' .$national_node->nid);
  return $national_node;
}


/**
 * Gets lists of marquee items from
 * 1) Global Node
 * 2) National Node
 *    If User has Store selected then also from
 * 3) Store node itself
 * 4) Metro and 5) Region nodes which are references by Store node.
 *
 * If Store is defined then 2) National Node is overriden with National Node referenced in Store
 * If National Node is Global Node then it's not used
 * @param $limit
 * @return mixed
 */
function _wholefoods_get_marquee_nodes($limit) {
  static $result;

  if (!isset($result[$limit])) {
    $store = store_get_user_store();
    // List of noeds to be filled in later
    $list = array(
      CATEGORY_STORE => array(),
      CATEGORY_METRO => array(),
      CATEGORY_REGION => array(),
      CATEGORY_NATIONAL => array(),
      CATEGORY_GLOBAL => array(),
    );
    $node_list = array();
    if ($store !== NULL) {
      // Get marquees for store
      $node_list = array(
        CATEGORY_METRO => '0',
        CATEGORY_REGION => '0',
        CATEGORY_NATIONAL => '0',
      );
      foreach ($node_list as $node_key => $node_value) {
        $node_references = $items = field_get_items('node', $store, 'field_' . $node_key, $store->language);
        if (!empty($node_references)) {
          $node_value = array_shift($node_references);
          $node_value = node_load($node_value['nid']);
        }
        $node_list[$node_key] = $node_value;
      }
      $node_list[CATEGORY_STORE] = $store;
    }
    else {
      $node_list[CATEGORY_NATIONAL] = wholefoods_get_global_node();
    }
    $node_list[CATEGORY_GLOBAL] = wholefoods_get_global_node(TRUE);
    if ($node_list[CATEGORY_GLOBAL] == $node_list[CATEGORY_NATIONAL]) {
      $node_list[CATEGORY_NATIONAL] = '0';
    }
    foreach ($node_list as $type => $node) {
      $marquees = _wholefoods_get_marquee_from_node($node);
      if ($marquees !== NULL) {
        $list[$type] = $marquees;
      }
    }
    $final_list = _wholefoods_filter_marquee_nodes(WHOLEFOODS_MARQUEE_FILTER_FUNCTION, $list, $limit);
    $result[$limit] = $final_list;
  }

  return $result[$limit];
}


/**
 * Get an array of all marquees from a node IN ORDER.
 *
 * @param
 *   node - The node (object OR nid) to pull marquees off.
 *
 * @return
 *   array - An array of nid's for marquee nodes IN ORDER.
 */
function _wholefoods_get_marquee_from_node($node) {
  static $item_list;

  if (!is_object($node)) {
    $node = node_load($node);
  }
  if ($node == NULL) {
    return NULL;
  }
  if (!isset($item_list[$node->nid])) {
    $item_list[$node->nid] = array();
    $node_marquees = field_get_items('node', $node, WHOLEFOODS_MARQUEE_FIELD_NAME, $node->language);
    $node_active_marquees = array();
    if (!empty($node_marquees)) {
      foreach ($node_marquees as $k => $nid) {
        $marquee = node_load($nid['nid']);
        if ($marquee && $marquee->status == 1) {
          $node_active_marquees[] = $nid['nid'];
        }
      }
    }
    if (!empty($node_active_marquees)) {
      $item_list[$node->nid] = $node_active_marquees;
    }
  }

  return $item_list[$node->nid];
}


/**
 * Run a list of marquee's through a filter function to limit the number of
 * total marquees as well as to order them based on a pre-defined pattern.
 *
 * @param
 *   callback - The name of a function to run the list of marquee's through.
 * @param
 *   array - A two dimensional array of marquees with the first dimension
 *           being the category the marquee falls under and the second being
 *           the lists of marquees.
 * @param
 *   int - A top limit of marquees to return. Is passed to the $function.
 *
 * @return
 *   array - A flat array of marquee nid's to be be displayed.
 */
function _wholefoods_filter_marquee_nodes($function, array $list, $limit = 5) {
  if (!function_exists($function)) {
    return array();
  }
  // Filter out any items from the $list that are not published.
  $all_marquees = array();
  foreach ($list as $sub_list) {
    foreach ($sub_list as $nid) {
      $all_marquees[] = $nid;
    }
  }
  if (empty($all_marquees)) {
    return array();
  }
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'marquee')
      ->propertyCondition('status', 1)
      ->propertyCondition('nid', $all_marquees)
      // Run the query as user 1
      ->addMetaData('account', user_load(1));
  $result = $query->execute();
  foreach ($list as $category => $sub_list) {
    foreach ($sub_list as $delta => $nid) {
      if (!array_key_exists($nid, $result['node'])) {
        unset($list[$category][$delta]);
      }
    }
  }

  return $function($list, $limit);
}


/**
 * The default filter function for a list of marquees.
 *
 * The list sent to this function will look like this...
 * $list = array(
 *   CATEGORY_STORE => array(1, 2, 3, 4, 8),
 *   CATEGORY_METRO => array(),
 *   CATEGORY_REGION => array(3, 5, 6, 16, 22),
 *   CATEGORY_GLOBAL => array(12, 13, 14, 15, 16),
 * )
 *
 * Each category contains an array of NIDs for marquee nodes that are assigned
 * to the current store, metro, region and global nodes IN ORDER. Each category
 * should contain the maximum number of marequee NIDs available to that node, to
 * a maximum of $limit.
 *
 * This function should decide how many of each category should be spit back
 * out, and in what order. For example.. 1 Global, 1 Regional, 1 Metro and,
 * 2 Local (in that order) from the example input should return an array such as...
 *
 * array(12, 3, 5, 1, 2)
 *
 * Notice there are two regional NIDs, as there are no metro we backfill from
 * the front.
 *
 * @param
 *   array - A two dimensional array of marquees with the first dimension
 *           being the category the marquee falls under and the second being
 *           the lists of marquees.
 * @param
 *   int - A top limit of marquees to return.
 *
 * @return
 *   array - A flat array of marquee nid's to be be displayed.
 */
function _wholefoods_filter_marquee_nodes_standard(array $list, $limit = 5) {
  // This arangement will be 1 Global, 1 Regional, 1 Metro and 2 Local
  // Start from the back.
  $sort = array(
    CATEGORY_STORE => 2,
    CATEGORY_METRO => 1,
    CATEGORY_REGION => 1,
    CATEGORY_NATIONAL => 1,
    CATEGORY_GLOBAL => 1,
  );

  $final = array();
  $total = 0;
  foreach ($sort as $sk => $sv) {
    $l = ($sv + ($total - count($final)));
    $slist = array();
    for ($i = 0; $i < $l; ++$i) {
      $item = array_shift($list[$sk]);

      if (!$item) {
        break;
      }

      if (in_array($item, $final)) {
        continue;
      }

      array_push($slist, $item);
    }
    if (!empty($slist)) {
      $final = array_merge($slist, $final);
    }
    $total += $sv;
  }

  return $final;
}


function _wholefoods_filter_marquee_nodes_level(array $list, $limit = 8) {
  $new_list = array();
  foreach ($list as $k => $v) {
    foreach ($v as $nid) {
      $node = node_load($nid);
      if (isset($node->field_marquee_priority[LANGUAGE_NONE][0])) {
        $priority = $node->field_marquee_priority[LANGUAGE_NONE][0]['value'];
      }
      else {
        $priority = 1;
      }
      $new_list[$priority][] = $node;
    }
  }
  ksort($new_list);
  $result = array();
  foreach ($new_list as $priority => $priority_list) {
    if (count($result) >= $limit) {
      break;
    }
    shuffle($priority_list);
    foreach ($priority_list as $node) {
      if (count($result) >= $limit) {
        break;
      }
      $result[] = $node->nid;
    }
  }

  return $result;
}
