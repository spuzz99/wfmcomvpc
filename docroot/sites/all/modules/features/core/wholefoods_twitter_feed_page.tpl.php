<?php
/**
 * @file
 * Theme file to display twitter posts.
 *
 */
$field = field_get_items('node',
                         $store,
                         'field_store_twitter',
                         LANGUAGE_NONE);

$twitter_profile_img = '';
$twitter = NULL;
if (is_array($field)) {
  $field = array_pop($field);
  $twitter = $field['value'];
  $twitter_profile_img = '<img src="https://api.twitter.com/1/users/profile_image?screen_name=' . $twitter . '&size=bigger" />';
}
?>

<div class="social-feed-container twitter_<?php print $store->nid; ?>" ref="<?php print $store->nid; ?>">
  <div class="social-feed-header">
    <div id="profile_image">
      <?php print $twitter_profile_img; ?>
    </div>
    <div id="twitter-user-meta">
      <h2><?php print $store->title; ?></h2>
<?php if ($twitter !== NULL): ?>
      <h3>@<?php print $twitter ?></h3>
      <div id="profile_description">
        <?php print $profile_description; ?>
      </div>
  <?php endif; ?>
    </div>
  </div>
  <?php $i = 0; ?>

  <?php foreach ($posts as $tweet): ?>
  <div class="tweet-content">
    <div class="pic"><img src="<?php print $tweet->profile_image_url; ?>"/></div>
  <div class="title"><h4><a href="https://twitter.com/<?php print $tweet->from_user; ?>/status/<?php print $tweet->id; ?>"><?php print $tweet->from_user_name; ?><span>@<?php print $tweet->from_user; ?></span></a></h4></div>
  <div class="tweet-text"><?php print $tweet->text; ?></div>
<?php $time = strtotime($tweet->created_at); ?>
<?php $time = REQUEST_TIME - $time; ?>
<?php if ($time <= 120): ?>
<?php $parse_time = t('Just Now'); ?>
<?php elseif ($time < 3600): ?>
<?php $parse_time = t('!time minutes ago', array('!time' => round($time / 60))); ?>
<?php elseif ($time < 12800): ?>
<?php $parse_time = format_plural(round($time / 3600), '1 hour ago', '@count hours ago'); ?>
<?php else: ?>
<?php $parse_time = date('j M', strtotime($tweet->created_at)); ?>
<?php endif; ?>
  <div class="tweet-time"><?php print $parse_time; ?></div>
  </div>
  <?php endforeach; ?>
</div>
