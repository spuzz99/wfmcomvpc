<?php
/**
 * @file
 * local.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function local_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'local_vendor_profiles';
  $context->description = '';
  $context->tag = 'Local Vendors';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'local-vendor-profiles/*' => 'local-vendor-profiles/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-local_vendor_profiles-block' => array(
          'module' => 'views',
          'delta' => 'local_vendor_profiles-block',
          'region' => 'content',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Local Vendors');
  $export['local_vendor_profiles'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'local_vendor_profiles_landing';
  $context->description = '';
  $context->tag = 'Local Vendors';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'locally-grown' => 'locally-grown',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-local_vendor_profiles-header' => array(
          'module' => 'views',
          'delta' => 'local_vendor_profiles-header',
          'region' => 'content_top',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Local Vendors');
  $export['local_vendor_profiles_landing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'online_ordering';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'online-ordering' => 'online-ordering',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'wfm_blocks-wfm_blocks_oo_header' => array(
          'module' => 'wfm_blocks',
          'delta' => 'wfm_blocks_oo_header',
          'region' => 'content_top',
          'weight' => '-10',
        ),
        'wfm_blocks-online_ordering_body' => array(
          'module' => 'wfm_blocks',
          'delta' => 'online_ordering_body',
          'region' => 'content_top',
          'weight' => '0',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['online_ordering'] = $context;

  return $export;
}
