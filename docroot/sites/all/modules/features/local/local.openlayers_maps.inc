<?php
/**
 * @file
 * local.openlayers_maps.inc
 */

/**
 * Implements hook_openlayers_maps().
 */
function local_openlayers_maps() {
  $export = array();

  $openlayers_maps = new stdClass();
  $openlayers_maps->disabled = FALSE; /* Edit this to true to make a default openlayers_maps disabled initially */
  $openlayers_maps->api_version = 1;
  $openlayers_maps->name = 'local_vendors_map';
  $openlayers_maps->title = 'Local Vendors Map';
  $openlayers_maps->description = 'This is a map of local vendors.';
  $openlayers_maps->data = array(
    'width' => 'auto',
    'height' => '400px',
    'image_path' => 'sites/all/modules/contrib/openlayers/themes/default_dark/img/',
    'css_path' => 'sites/all/modules/contrib/openlayers/themes/default_dark/style.css',
    'proxy_host' => '',
    'hide_empty_map' => 0,
    'center' => array(
      'initial' => array(
        'centerpoint' => '-101.337890625, 39.027714094843',
        'zoom' => '4',
      ),
      'restrict' => array(
        'restrictextent' => 0,
        'restrictedExtent' => '',
      ),
    ),
    'behaviors' => array(
      'openlayers_behavior_keyboarddefaults' => array(),
      'openlayers_behavior_attribution' => array(
        'seperator' => '',
      ),
      'openlayers_behavior_layerswitcher' => array(
        'ascending' => 1,
        'roundedCorner' => 0,
        'roundedCornerColor' => '#222222',
      ),
      'openlayers_behavior_navigation' => array(
        'zoomWheelEnabled' => 1,
        'zoomBoxEnabled' => 1,
        'documentDrag' => 0,
      ),
      'openlayers_behavior_panzoombar' => array(
        'zoomWorldIcon' => 0,
        'panIcons' => 1,
      ),
      'openlayers_behavior_popup' => array(
        'layers' => array(
          'local_vendor_map_openlayers_7' => 'local_vendor_map_openlayers_7',
          'local_vendor_map_openlayers_10' => 'local_vendor_map_openlayers_10',
          'local_vendor_map_openlayers_6' => 'local_vendor_map_openlayers_6',
          'local_vendor_map_openlayers_8' => 'local_vendor_map_openlayers_8',
          'local_vendor_map_openlayers_11' => 'local_vendor_map_openlayers_11',
          'local_vendor_map_openlayers_9' => 'local_vendor_map_openlayers_9',
          'local_vendor_map_openlayers_5' => 'local_vendor_map_openlayers_5',
          'local_vendor_map_openlayers_2' => 'local_vendor_map_openlayers_2',
          'local_vendor_map_openlayers_1' => 'local_vendor_map_openlayers_1',
          'local_vendor_map_openlayers_3' => 'local_vendor_map_openlayers_3',
        ),
      ),
    ),
    'default_layer' => 'mapquest_osm',
    'layers' => array(
      'mapquest_osm' => 'mapquest_osm',
      'mapquest_openaerial' => 'mapquest_openaerial',
      'local_vendor_map_openlayers_9' => 'local_vendor_map_openlayers_9',
      'local_vendor_map_openlayers_11' => 'local_vendor_map_openlayers_11',
      'local_vendor_map_openlayers_5' => 'local_vendor_map_openlayers_5',
      'local_vendor_map_openlayers_2' => 'local_vendor_map_openlayers_2',
      'local_vendor_map_openlayers_3' => 'local_vendor_map_openlayers_3',
      'local_vendor_map_openlayers_1' => 'local_vendor_map_openlayers_1',
      'local_vendor_map_openlayers_8' => 'local_vendor_map_openlayers_8',
      'local_vendor_map_openlayers_6' => 'local_vendor_map_openlayers_6',
      'local_vendor_map_openlayers_7' => 'local_vendor_map_openlayers_7',
      'local_vendor_map_openlayers_10' => 'local_vendor_map_openlayers_10',
    ),
    'layer_weight' => array(
      'local_vendor_map_openlayers_9' => '0',
      'local_vendor_map_openlayers_11' => '0',
      'local_vendor_map_openlayers_5' => '0',
      'local_vendor_map_openlayers_2' => '0',
      'local_vendor_map_openlayers_3' => '0',
      'local_vendor_map_openlayers_1' => '0',
      'local_vendor_map_openlayers_8' => '0',
      'local_vendor_map_openlayers_6' => '0',
      'store_locations_openlayers_1' => '0',
      'openlayers_geojson_picture_this' => '0',
      'store_map_openlayers_1' => '0',
      'local_vendor_map_openlayers_7' => '0',
      'local_vendor_map_openlayers_10' => '0',
      'geofield_formatter' => '0',
    ),
    'layer_styles' => array(
      'geofield_formatter' => '0',
      'openlayers_geojson_picture_this' => '0',
      'store_locations_openlayers_1' => '0',
      'store_map_openlayers_1' => '0',
      'local_vendor_map_openlayers_7' => 'marker_whole_body',
      'local_vendor_map_openlayers_10' => 'marker_grocery',
      'local_vendor_map_openlayers_6' => 'marker_meat_dairy',
      'local_vendor_map_openlayers_8' => 'marker_fruit_veggies',
      'local_vendor_map_openlayers_11' => 'marker_store_location',
      'local_vendor_map_openlayers_9' => 'marker_seafood',
      'local_vendor_map_openlayers_5' => 'marker_coffee_tea',
      'local_vendor_map_openlayers_2' => 'marker_lplp',
      'local_vendor_map_openlayers_1' => 'marker_baked_goods',
      'local_vendor_map_openlayers_3' => 'marker_wine_beer',
    ),
    'layer_styles_select' => array(
      'geofield_formatter' => '0',
      'openlayers_geojson_picture_this' => '0',
      'store_locations_openlayers_1' => '0',
      'store_map_openlayers_1' => '0',
      'local_vendor_map_openlayers_7' => 'marker_whole_body',
      'local_vendor_map_openlayers_10' => 'marker_grocery',
      'local_vendor_map_openlayers_6' => 'marker_meat_dairy',
      'local_vendor_map_openlayers_8' => 'marker_fruit_veggies',
      'local_vendor_map_openlayers_11' => 'marker_store_location',
      'local_vendor_map_openlayers_9' => 'marker_seafood',
      'local_vendor_map_openlayers_5' => 'marker_coffee_tea',
      'local_vendor_map_openlayers_2' => 'marker_lplp',
      'local_vendor_map_openlayers_1' => 'marker_baked_goods',
      'local_vendor_map_openlayers_3' => 'marker_wine_beer',
    ),
    'layer_activated' => array(
      'local_vendor_map_openlayers_7' => 'local_vendor_map_openlayers_7',
      'local_vendor_map_openlayers_10' => 'local_vendor_map_openlayers_10',
      'local_vendor_map_openlayers_6' => 'local_vendor_map_openlayers_6',
      'local_vendor_map_openlayers_8' => 'local_vendor_map_openlayers_8',
      'local_vendor_map_openlayers_11' => 'local_vendor_map_openlayers_11',
      'local_vendor_map_openlayers_9' => 'local_vendor_map_openlayers_9',
      'local_vendor_map_openlayers_5' => 'local_vendor_map_openlayers_5',
      'local_vendor_map_openlayers_2' => 'local_vendor_map_openlayers_2',
      'local_vendor_map_openlayers_1' => 'local_vendor_map_openlayers_1',
      'local_vendor_map_openlayers_3' => 'local_vendor_map_openlayers_3',
      'geofield_formatter' => 0,
      'openlayers_geojson_picture_this' => 0,
      'store_locations_openlayers_1' => 0,
      'store_map_openlayers_1' => 0,
    ),
    'layer_switcher' => array(
      'local_vendor_map_openlayers_7' => 'local_vendor_map_openlayers_7',
      'local_vendor_map_openlayers_10' => 'local_vendor_map_openlayers_10',
      'local_vendor_map_openlayers_6' => 'local_vendor_map_openlayers_6',
      'local_vendor_map_openlayers_8' => 'local_vendor_map_openlayers_8',
      'local_vendor_map_openlayers_11' => 'local_vendor_map_openlayers_11',
      'local_vendor_map_openlayers_9' => 'local_vendor_map_openlayers_9',
      'local_vendor_map_openlayers_5' => 'local_vendor_map_openlayers_5',
      'local_vendor_map_openlayers_2' => 'local_vendor_map_openlayers_2',
      'local_vendor_map_openlayers_1' => 'local_vendor_map_openlayers_1',
      'local_vendor_map_openlayers_3' => 'local_vendor_map_openlayers_3',
      'geofield_formatter' => 0,
      'openlayers_geojson_picture_this' => 0,
      'store_locations_openlayers_1' => 0,
      'store_map_openlayers_1' => 0,
    ),
    'projection' => '900913',
    'displayProjection' => '4326',
    'styles' => array(
      'default' => 'default',
      'select' => 'default_select',
      'temporary' => 'default',
    ),
  );
  $export['local_vendors_map'] = $openlayers_maps;

  return $export;
}
