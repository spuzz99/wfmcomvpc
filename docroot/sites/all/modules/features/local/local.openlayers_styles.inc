<?php
/**
 * @file
 * local.openlayers_styles.inc
 */

/**
 * Implements hook_openlayers_styles().
 */
function local_openlayers_styles() {
  $export = array();

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_baked_goods';
  $openlayers_styles->title = 'Baked Goods';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-baked-goods.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_baked_goods'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_coffee_tea';
  $openlayers_styles->title = 'Coffee/Tea';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-coffee.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_coffee_tea'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_fruit_veggies';
  $openlayers_styles->title = 'Fruit/Veggies';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-fruit-veggies.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_fruit_veggies'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_grocery';
  $openlayers_styles->title = 'Grocery';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-grocery.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_grocery'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_lplp';
  $openlayers_styles->title = 'LPLP';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-lplp.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_lplp'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_meat_dairy';
  $openlayers_styles->title = 'Meat/Dairy';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-meat.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_meat_dairy'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_seafood';
  $openlayers_styles->title = 'Seafood';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-seafood.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_seafood'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_store_location';
  $openlayers_styles->title = 'Store Location';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-store.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_store_location'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_whole_body';
  $openlayers_styles->title = 'Whole Body';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-whole-body.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_whole_body'] = $openlayers_styles;

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'marker_wine_beer';
  $openlayers_styles->title = 'Wine/Beer';
  $openlayers_styles->description = '';
  $openlayers_styles->data = array(
    'externalGraphic' => 'sites/all/themes/wholefoods/images/local-beer.png',
    'pointRadius' => 12,
    'fillColor' => '#EE9900',
    'strokeColor' => '#EE9900',
    'strokeWidth' => 1,
    'fillOpacity' => 1,
    'strokeOpacity' => 1,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'labelAlign' => 'cm',
  );
  $export['marker_wine_beer'] = $openlayers_styles;

  return $export;
}
