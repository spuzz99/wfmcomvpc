<?php
/**
 * @file
 * store_page_content.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function store_page_content_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_page';
  $context->description = 'Context for store main pages';
  $context->tag = 'Store';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'store' => 'store',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-9921a8226dcbc528f004a3704cd5a7ff' => array(
          'module' => 'views',
          'delta' => '9921a8226dcbc528f004a3704cd5a7ff',
          'region' => 'content_top',
          'weight' => 0,
        ),
        'views-promo_blocks-custom_location' => array(
          'module' => 'views',
          'delta' => 'promo_blocks-custom_location',
          'region' => 'content_top',
          'weight' => 1,
        ),
        'webform-client-block-96' => array(
          'module' => 'webform',
          'delta' => 'client-block-96',
          'region' => 'content',
          'weight' => 10,
        ),
        'views-store_page_content-about_store' => array(
          'module' => 'views',
          'delta' => 'store_page_content-about_store',
          'region' => 'content',
          'weight' => 5,
        ),
        'views-store_page_content-store_social' => array(
          'module' => 'views',
          'delta' => 'store_page_content-store_social',
          'region' => 'content',
          'weight' => 0,
        ),
        'views-a1e65372696b4ddcfc5bbd5d00f89f1d' => array(
          'module' => 'views',
          'delta' => 'a1e65372696b4ddcfc5bbd5d00f89f1d',
          'region' => 'content_sidebar',
          'weight' => 0,
        ),
        'views-50ecd3063d8c028709a4ced19ebbad62' => array(
          'module' => 'views',
          'delta' => '50ecd3063d8c028709a4ced19ebbad62',
          'region' => 'content_sidebar',
          'weight' => 5,
        ),
        'views-600a01353180e279d27bc2830c94660b' => array(
          'module' => 'views',
          'delta' => '600a01353180e279d27bc2830c94660b',
          'region' => 'content_sidebar',
          'weight' => 10,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context for store main pages');
  t('Store');
  $export['store_page'] = $context;

  return $export;
}
