<?php
/**
 * @file
 * Code for the Roles And Permissions feature.
 */

include_once 'roles_and_permissions.features.inc';
if (!class_exists('RapAccess')) {
  module_load_include('inc', 'roles_and_permissions', '/includes/RapAccess');
}

// Taxonomy vocabulary machine name from which a RAP tree must be got.
define('RAP_VOCABULARY', 'locations_acl');

// Field machine name from which the RAP will check an access limits for nodes.
define('RAP_FIELD', 'locations_acl');

// Field machine name from which the RAP will check an access limits for users.
define('RAP_FIELD_USER', 'locations_acl_user');

// Used to reference stores in content types like event or promo. Should have allowed values limitations
define('RAP_FIELD_STORE', 'field_locations');


/**
 * Implements hook_permission().
 */
function roles_and_permissions_permission() {
  $perms = array();
  $field = field_info_field(RAP_FIELD);
  if (isset($field['bundles']['node'])) {
    // Generate standard node permissions for all applicable node types.
    foreach (node_permissions_get_configured_types() as $type) {
      if (in_array($type, $field['bundles']['node'])) {
        $perms += roles_and_permissions_list_permissions($type);
      }
    }
  }
  $perms['bypass locations acl while creating content'] = array(
    'title' => t('Bypass locations acl while creating content'),
    'description' => t('Allow to fill field with values of all levels (not only current user Locations ACL).'),
    'restrict access' => TRUE,
  );
  // @WTF Remove this g-zero, no longer using Marquees since Brand Camp
  $perms['can use g-zero'] = array(
    'title' => t('Can Use G-Zero In Marquees'),
    'description' => t('Can set marquee priority to 0 to allow over-riding of other marquees'),
    'restrict access' => TRUE,
  );
  $perms['can use free-form'] = array(
    'title' => t('Can Use Free-Form Template'),
    'description' => t('Can use the Free-Form template and Free-Form promotion fields'),
    'restrict access' => TRUE,
  );
  $perms['reference all blogs'] = array(
    'title' => t('Can associate blog posts to any blogs'),
    'description' => t('Can associate blog posts to any blogs'),
    'restrict access' => TRUE,
  );

  return $perms;
}


/**
 * Implements hook_menu().
 */
function roles_and_permissions_menu() {
  $rap_taxonomy_autocomplete = array(
    'title' => 'Autocomplete taxonomy',
    'page callback' => 'roles_and_permissions_taxonomy_autocomplete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'includes/roles_and_permissions.pages.inc',
  );
  return array(
    'taxonomy/autocomplete/' . RAP_FIELD => $rap_taxonomy_autocomplete,
    'taxonomy/autocomplete/' . RAP_FIELD_USER => $rap_taxonomy_autocomplete,
    'taxonomy/autocomplete/field_locations' => $rap_taxonomy_autocomplete,
  );
}


/**
 * Implements hook_feeds_plugins().
 *
 * Initially loads the taxonomy with required terms i.e. stores etc.
 * This can be deleted after deployment
 *
 */
function roles_and_permissions_feeds_plugins() {
  $info = array();
  $info['RAPFeedsHTTPSFetcher'] = array(
    'name' => t('HTTPS Fetcher'),
    'description' => t('(from Roles And Permissions module)'),
    'help' => t('Download content from a https URL.'),
    'handler' => array(
      'parent' => 'FeedsHTTPFetcher',
      'class' => 'RAPFeedsHTTPSFetcher',
      'file' => 'RAPFeedsHTTPSFetcher.inc',
      'path' => drupal_get_path('module', 'roles_and_permissions') . '/plugins/feeds',
    ),
  );
  return $info;
}


/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * if you land on the node/xxx/edit form, it will hide the save button
 * and show message that you can't change this node
 * 
 * Disables the submit button for users without permissions to edit a node 
 * if they access the node edit page via a direct url.
 */
function roles_and_permissions_form_node_form_alter(&$form, &$form_state, $form_id) {
  if (!roles_and_permissions_access($form['#node'], 'save')) {
    // Hide button.
    $form['actions']['#access'] = FALSE;
    // Stop any other submission handlers.
    $form['#submit'] = array();
    // Show message for user.
    drupal_set_message(t('Your permissions does not allow to change this node.'), 'warning');
  }

  // check if you are on a store content type
  if (isset($form['#node']->type) && 'store' == $form['#node']->type && isset($form[RAP_FIELD])
      && !user_access('edit any store content in bypass ' . RAP_VOCABULARY)
      && !user_access('edit own store content in bypass ' . RAP_VOCABULARY)) {
    $form[RAP_FIELD]['#access'] = FALSE;  //don't display RAP_FIELD
  }

  // if there is a store field, only let users with perms change it
  if (isset($form['#node']->field_store_name)) {
    if (!user_access('edit field_store_name') && !user_access('edit own field_store_name')) {
      $form['title']['#type'] = 'item';
      $form['title']['#markup'] = $form['title']['#default_value'];
    }
  }
  if (isset($form['locations_acl']) && is_array($form['locations_acl'])) {
    $form['locations_acl'][LANGUAGE_NONE]['#title'] = '<strong>'
      . t('Permissions (Store name, TLC, metro name, region, or two-letter region code)') . '</strong><br>'
      . t('Select the stores, metros, or regions that should have access to edit this @bundle.
        Separate multiple locations by a comma. Ex: “Lamar (LMR)” or “Lamar (LMR), Gateway (GWY)', array('@bundle' => $form['#node']->type));
  }
}


/**
 * Implements hook_form_alter().
 */
function roles_and_permissions_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'user_profile_form':
      $form['locations_acl'][LANGUAGE_NONE]['#element_validate'] = array('roles_and_permissions_autocomplete_validate');
      $form['#validate'][] = 'roles_and_permissions_user_form_validate';
      break;

    case 'views_form_admin_people_page':
      $form['bundle_user']['_append::' . RAP_FIELD_USER]['#access'] = FALSE;
      $form['#validate'][] = 'roles_and_permissions_vbo_form_validate';
      break;
  }
}


/**
 * Custom validation handler for user_profile_form.
 */
function roles_and_permissions_user_form_validate($form, &$form_state) {
  $pseudo_entity = (object) $form_state['values'];
  _roles_and_permissions_form_validate('user', $pseudo_entity, $form, $form_state);
}


/**
 * Custom validation handler for views_form_admin_people_page (VBO).
 */
function roles_and_permissions_vbo_form_validate($form, &$form_state) {
  $pseudo_entity = $form_state['entities']['user'];
  _roles_and_permissions_form_validate('user', $pseudo_entity, $form, $form_state);
}


/**
 * Implements hook_node_validate().
 */
function roles_and_permissions_node_validate($node, $form, &$form_state) {
  if (_roles_and_permissions_form_validate('node', $node, $form, $form_state)
      && !roles_and_permissions_access($node, 'save')) {
    form_set_error('', t("You can not save this node because you don't have sufficient privileges."));
  }
}


/**
 * Helper function to validate forms for node and user entities.
 * 
 * Enforces the use of existing taxonomy terms rather than creating new taxonomy terms.
 * Disallows new terms autocreation via taxonomy autocomplete field too
 */
function _roles_and_permissions_form_validate($entity_type, $pseudo_entity, $form, &$form_state) {
  $field = 'user' == $entity_type ? RAP_FIELD_USER : RAP_FIELD;
  $rap = new RapAccess();
  $user_tids = $rap->get('userTids');
  $denied_terms = array();
  $is_error = false;

  foreach ($pseudo_entity->{$field}[LANGUAGE_NONE] as $term) {
    if (!in_array($term['tid'], $user_tids)) {
      $denied_terms[$term['tid']] = $term['name'];
    }
  }

  if (count($denied_terms)) {
    _roles_and_permissions_form_set_error_acl($form, $field, 'Store / Region', $denied_terms);
    $is_error = true;
  }

  // Check for possible store binding (additional field present in events, promos etc.)
  $denied_terms = array();
  if ($pseudo_entity->{RAP_FIELD_STORE}) {
    $field = RAP_FIELD_STORE;
    foreach ($pseudo_entity->{$field}[LANGUAGE_NONE] as $term) {
      if (!in_array($term['tid'], $user_tids)) {
        $denied_terms[] = $term['name'];
      }
    }

    if (count($denied_terms)) {
      _roles_and_permissions_form_set_error_acl($form, $field, 'Store association', $denied_terms);
      $is_error = true;
    }
  };

  return !$is_error;
}

function _roles_and_permissions_form_set_error_acl($form, $field, $fieldName = '', $values) {
  form_set_error($field, t($fieldName . ': You do not have permission to add "@values" to this field. '
    . 'Please contact !email if you need access to "@values" content', array(
      '@field' => $form[$field][LANGUAGE_NONE]['#title'],
      '@values' => implode(', ', $values),
      '!email' => l('website.updates@wholefoods.com', 'mailto:website.updates@wholefoods.com'),
    )));
}

/**
 * Helper function to generate standard node permission list for a given type.
 *
 * @param $type
 *   The machine-readable name of the node type.
 *
 * @return array
 *   An array of permission names and descriptions.
 */
function roles_and_permissions_list_permissions($type) {
  $info = node_type_get_type($type);

  // Build standard list of node permissions for this type.
  $perms = array(
    "edit own $type content in bypass " . RAP_VOCABULARY => array(
      'title' => t('%type_name: Edit own content in bypass user location', array('%type_name' => $info->name)),
    ),
    "edit any $type content in bypass " . RAP_VOCABULARY => array(
      'title' => t('%type_name: Edit any content in bypass user location', array('%type_name' => $info->name)),
    ),
    "delete own $type content in bypass " . RAP_VOCABULARY => array(
      'title' => t('%type_name: Delete own content in bypass user location', array('%type_name' => $info->name)),
    ),
    "delete any $type content in bypass " . RAP_VOCABULARY => array(
      'title' => t('%type_name: Delete any content in bypass user location', array('%type_name' => $info->name)),
    ),
  );

  return $perms;
}


/**
 * Implements hook_node_access().
 * 
 * Checks to see if user has edit access to node. Evaluates the 
 * node value to be object or string and looks for a possible 
 * RAP custom function if defined.
 */
function roles_and_permissions_node_access($node, $op, $account) {
  $result = NODE_ACCESS_IGNORE;
  $func_name = 'roles_and_permissions_node_access_';
  $type = '';
  if (!is_object($node)) {
    $type = $node;
  }
  else {
    $type = $node->type;
  }

  $func_name .= $type;

  if (function_exists($func_name)) {
    $result = $func_name($node, $op, $account);
  }
  if (NODE_ACCESS_DENY != $result) {
    $rap = new RapAccess($node, $account);
    $result = $rap->access($op);
  }
  return $result;
}


/**
 * Function to identify a RAP access for user to do some operation with node.
 *
 * @param object $node
 *   A node object
 * @param string $op
 *   Operation. The next values are available: create, update, delete, save.
 * @param object $account
 *   User account. If empty the global $user will be used.
 *
 * @return boolean
 *   TRUE, if account have access to current operation with current node and
 *   FALSE otherwise.
 */
function roles_and_permissions_access($node, $op, $account = NULL) {
  $type = is_string($node) ? $node : $node->type;
  $field = field_info_field(RAP_FIELD);

  if (isset($field['bundles']['node']) && in_array($type, $field['bundles']['node'])) {
    $rap = new RapAccess($node, $account);
    if (NODE_ACCESS_DENY == $rap->access($op)) {
      return FALSE;
    }
  }
  return TRUE;
}


function roles_and_permissions_node_access_page($node, $op, $account) {
  if ($op == 'update') {
    if (!user_access('edit any page content', $account)) {
      return NODE_ACCESS_DENY;
    }


    if ($node->uid == $account->uid)
      return NODE_ACCESS_IGNORE;

    // Test if the node is owned by a global content admin.
    // If yes, test if $account is a global content admin.
    $owner = user_load($node->uid);
    $owner_is_global = in_array('Global Content Admin', $owner->roles);
    if ($owner_is_global) {
      if (in_array('Global Content Admin', $account->roles)) {
        return NODE_ACCESS_ALLOW;
      }
      else {
        return NODE_ACCESS_DENY;
      }
    }
  }
  return NODE_ACCESS_IGNORE;
}

/**
 * Implements hook_taxonomy_term_presave().
 * in autocreate, it makes sure the parent is WFM if parent is not specified
 */
function roles_and_permissions_taxonomy_term_presave($term) {
  if (RAP_VOCABULARY == $term->vocabulary_machine_name && 'WFM' != $term->name) {
    if (!$term->parent || (1 == count($term->parent) && !$term->parent[0])) {
      $parents = taxonomy_get_term_by_name('WFM', RAP_VOCABULARY);
      if (count($parents)) {
        $parent = array_pop($parents);
        $term->parent = $parent->tid;
      }
    }
  }
}


/**
 * Implements hook_feeds_after_save().
 *
 * This hook is available in feeds 2.0-alpha8 and higher.
 * Feeds 2.0-alpha5 needs to be patched to work properly.
 */
function roles_and_permissions_feeds_after_save(FeedsSource $source, $entity) {
  if ('taxonomy_term' == $entity->feeds_item->entity_type && RAP_VOCABULARY == $entity->vocabulary_machine_name
      && 'Whole Foods Market' == $entity->field_bu_type[LANGUAGE_NONE][0]['value']
      && user_access('create store content')) {
    $nid = _roles_and_permissions_get_store_nid_for_term($entity);
    if ($nid) {
      $node = node_load($nid);
      if (!isset($node->{RAP_FIELD}[LANGUAGE_NONE]) || $node->{RAP_FIELD}[LANGUAGE_NONE][0]['tid'] != $entity->tid) {
        foreach (array('field_data_', 'field_revision_') as $prefix) {
          $mkey = $mapping = array(
            'entity_type' => 'node',
            'bundle' => $node->type,
            'deleted' => 0,
            'entity_id' => $node->nid,
            'revision_id' => $node->vid,
            'language' => LANGUAGE_NONE,
            'delta' => 0,
          );
          $mapping += array(RAP_FIELD . '_tid' => $entity->tid);
          db_merge($prefix . RAP_FIELD)->key($mkey)->fields($mapping)->execute();
        }
        $key = $fields = array('nid' => $node->nid, 'tid' => $entity->tid);
        $fields += array('sticky' => 0, 'created' => REQUEST_TIME);
        db_merge('taxonomy_index')->key($key)->fields($fields)->execute();
      }
    }
    else {
      _roles_and_permissions_create_store_from_term($entity);
    }
  }
}


/**
 * Helper function to get store NID for Locations ACL Term.
 *
 * @wtf Can probably be removed after import - only used in import
 *
 * @param object $term
 *   Term object from Locations ACL vocabulary.
 *
 * @return int
 *   node ID for store if it exists.
 */
function _roles_and_permissions_get_store_nid_for_term($term) {
  return db_select('node', 'n')
        ->fields('n', array('nid'))
        ->condition('type', 'store')
        ->condition('title', $term->description)
        ->orderBy('created', 'DESC')
        ->range(0, 1)
        ->execute()
        ->fetchField();
}


/**
 * Helper function to create and save store node from locations ACL term.
 *
 * @param object $term
 *   Term object from Locations ACL vocabulary.
 */
function _roles_and_permissions_create_store_from_term($term) {
  $node = new stdClass();
  $node->type = 'store';
  $node->title = $term->description;
  $node->language = LANGUAGE_NONE;
  node_object_prepare($node);
  $node->{RAP_FIELD} = array(LANGUAGE_NONE => array(array('tid' => $term->tid)));
  $node->log = 'Created by roles_and_permissions module.';
  node_save($node);
}


/**
 * Form element validate handler for taxonomy term autocomplete element.
 * Copied from taxonomy.module
 */
function roles_and_permissions_autocomplete_validate($element, &$form_state) {
  // Autocomplete widgets do not send their tids in the form, so we must detect
  // them here and process them independently.
  $value = array();
  if ($tags = $element['#value']) {
    // Collect candidate vocabularies.
    $field = field_widget_field($element, $form_state);
    $vocabularies = array();
    foreach ($field['settings']['allowed_values'] as $tree) {
      if ($vocabulary = taxonomy_vocabulary_machine_name_load($tree['vocabulary'])) {
        $vocabularies[$vocabulary->vid] = $vocabulary;
      }
    }

    // Translate term names into actual terms.
    $typed_terms = drupal_explode_tags($tags);
    if (count($typed_terms) <= 1) {
      foreach ($typed_terms as $typed_term) {
        // See if the term exists in the chosen vocabulary and return the tid;
        // otherwise, create a new 'autocreate' term for insert/update.
        if ($possibilities = taxonomy_term_load_multiple(array(), array('name' => trim($typed_term), 'vid' => array_keys($vocabularies)))) {
          $term = array_pop($possibilities);
        }
        else {
          $vocabulary = reset($vocabularies);
          $term = array(
            'tid' => 'autocreate',
            'vid' => $vocabulary->vid,
            'name' => $typed_term,
            'vocabulary_machine_name' => $vocabulary->machine_name,
          );
        }
        $value[] = (array)$term;
      }
    }
    else {
      form_set_error($element['#field_name'], t("The field !field_label doesn't allow to use multiple values.", array('!field_label' => $element['#title'])));
    }
  }
  form_set_value($element, $value, $form_state);
}


/**
 * Get token by authentication process.
 */
function roles_and_permission_get_token($client_id, $secret) {
  $url = 'https://api.wholefoodsmarket.com/oauth20/token';
  $prepared_url = $url . '?client_id=' . $client_id . '&client_secret=' . $secret . '&grant_type=client_credentials';
  $result = drupal_http_request($prepared_url);
  if (!is_object($result)) {
    return FALSE;
  }
  $parsed_doc = new SimpleXMLElement($result->data);

  return (string)$parsed_doc->access_token->token;
}

/**
 * Install Functions
 * A list of functions executed only once upon roles_and_permissions module being enabled
 */

/**
 * Import or update regions into Locations vocabulary from file.
 *
 * @param string $xml_path
 *   Path to xml file with regions that must be imported. If not set,
 *   the default file regions.xml in module dir will be used.
 *
 * @WTF - Remove if / when no longer needed. Is region import one time execution?
 */
function roles_and_permissions_import_regions($xml_path = NULL) {
  // Create WFM term if not exists.
  $vocabulary = taxonomy_vocabulary_machine_name_load(RAP_VOCABULARY);
  $def_terms = taxonomy_get_term_by_name('WFM', RAP_VOCABULARY);
  $us_terms = taxonomy_get_term_by_name('US', RAP_VOCABULARY);
  if (count($def_terms)) {
    $def_term = reset($def_terms);
  }
  else {
    $def_term = new stdClass();
    $def_term->vid = $vocabulary->vid;
    $def_term->name = 'WFM';
    taxonomy_term_save($def_term);
  }
  if (count($us_terms)) {
    $us_term = reset($us_terms);
  }
  else {
    $us_term = new stdClass();
    $us_term->vid = $vocabulary->vid;
    $us_term->name = 'US';
    $us_term->parent = $def_term->tid;
    $us_term->field_bu_type[LANGUAGE_NONE][0]['value'] = 'National Office';
    taxonomy_term_save($us_term);
  }

  // Create regions in Locations vocabulary if they are not exist.
  $xml_file = is_null($xml_path) ? drupal_get_path('module', 'roles_and_permissions') . '/regions.xml' : $xml_path;
  if (file_exists($xml_file)) {
    $regions = simplexml_load_file($xml_file);
    if (isset($regions->region) && count($regions->region)) {
      foreach ($regions->region as $xmlterm) {
        $existing_terms = taxonomy_get_term_by_name($xmlterm->name, RAP_VOCABULARY);
        if (count($existing_terms)) {
          $term = reset($existing_terms);
        }
        else {
          $term = new stdClass();
        }
        $term->name = $xmlterm->name;
        $term->description = $xmlterm->description;
        $term->vid = $vocabulary->vid;
        if (in_array($term->name, array('US', 'UK'))) {
          $term->parent = $def_term->tid;
          $term->field_bu_type[LANGUAGE_NONE][0]['value'] = 'National Office';
        }
        else {
          $term->parent = $us_term->tid;
          $term->field_bu_type[LANGUAGE_NONE][0]['value'] = 'region';
        }
        taxonomy_term_save($term);
        unset($term);
      }
    }
  }
}

/**
 * Import or update metro into Locations vocabulary using Sage api v2.
 */
function roles_and_permissions_import_metro() {
  $token = roles_and_permission_get_token('ATlB6Ny8dalhyvs4lHAGXOWOWOt2N2Jf', 'uEGzC616cyaOkiAD');
  $url = 'https://api.wholefoodsmarket.com/v2/stores?access_token=' . $token . '&limit=1000';

  $vocabulary = taxonomy_vocabulary_machine_name_load(RAP_VOCABULARY);
  $def_terms = taxonomy_get_term_by_name('WFM', RAP_VOCABULARY);
  $def_terms = reset($def_terms);

  $result = drupal_http_request($url);
  if (is_object($result)) {
    $data = json_decode($result->data);
    // Create metro in Locations vocabulary if they are not exist.
    if (is_array($data)) {
      foreach ($data as $metro_term) {
        if (isset($metro_term->metro_area) && !empty($metro_term->metro_area)) {
          $existing_terms = taxonomy_get_term_by_name($metro_term->metro_area, RAP_VOCABULARY);
          if (count($existing_terms)) {
            $term = reset($existing_terms);
          }
          else {
            $term = new stdClass();
          }
          $parent_tid = $def_terms->tid;
          $parent_term = taxonomy_get_term_by_name($metro_term->region, RAP_VOCABULARY);
          if (count($parent_term)) {
            $parent_term = reset($parent_term);
            $parent_tid = $parent_term->tid;
          }
          $term->name = $metro_term->metro_area;
          $term->vid = $vocabulary->vid;
          $term->parent = $parent_tid;
          $term->field_bu_type[LANGUAGE_NONE][0]['value'] = 'metro';
          taxonomy_term_save($term);
          unset($term);
        }
      }
    }
  }
}

/**
 * Helper function to create new instance or update existing instance.
 *
 * @param array $instance
 *   Instance that needs to be created or updated.
 * @param array $existing_instances
 *   All existing instances in array.
 */
function _roles_and_permissions_create_or_update_instance($instance, &$existing_instances) {
  if (isset($existing_instances[$instance['entity_type']][$instance['bundle']][$instance['field_name']])) {
    $existing_instance = $existing_instances[$instance['entity_type']][$instance['bundle']][$instance['field_name']];
    if ($instance + $existing_instance != $existing_instance) {
      field_update_instance($instance);
    }
  }
  else {
    field_create_instance($instance);
    $existing_instances[$instance['entity_type']][$instance['bundle']][$instance['field_name']] = $instance;
  }
}

/**
 * Creates needed fields and binds them into all node types and user.
 */
function roles_and_permissions_create_fields($content_types = array()) {
  if (!is_array($content_types)) {
    $content_types = array($content_types);
  }
  $vocabulary = taxonomy_vocabulary_machine_name_load(RAP_VOCABULARY);
  $instance = array(
    'description' => 'Locations ACL',
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'weight' => 99,
    ),
    'display' => array('default' => array('type' => 'hidden')),
  );
  $fields = array(
    RAP_FIELD => array(
      'field_name' => RAP_FIELD,
      'module' => 'taxonomy',
      'type' => 'taxonomy_term_reference',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'locked' => TRUE,
      'settings' => array(
        'required' => $vocabulary->required ? TRUE : FALSE,
        'allowed_values' => array(
          array(
            'vocabulary' => $vocabulary->machine_name,
            'parent' => 0,
          ),
        ),
      ),
      'field_permissions' => array('type' => 2),
    ),
    RAP_FIELD_USER => array(
      'field_name' => RAP_FIELD_USER,
      'module' => 'taxonomy',
      'type' => 'taxonomy_term_reference',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'locked' => TRUE,
      'settings' => array(
        'required' => $vocabulary->required ? TRUE : FALSE,
        'allowed_values' => array(
          array(
            'vocabulary' => $vocabulary->machine_name,
            'parent' => 0,
          ),
        ),
      ),
      'field_permissions' => array('type' => 2),
    ),
  );

  // additional field "field_locations" only for events and promos
  $fields['field_locations'] = $fields[RAP_FIELD];
  $fields['field_locations']['field_name'] = 'field_locations';
  $fields['field_locations']['locked'] = 0;

  field_info_cache_clear();

  // Load all the existing fields and instance up-front so that we don't
  // have to rebuild the cache all the time.
  $existing_fields = field_info_fields();
  $existing_instances = field_info_instances();

  foreach ($fields as $field) {
    // Create or update field.
    if (isset($existing_fields[$field['field_name']])) {
      $existing_field = $existing_fields[$field['field_name']];
      if ($field + $existing_field != $existing_field) {
        field_update_field($field);
      }
    }
    else {
      field_create_field($field);
      $existing_fields[$field['field_name']] = $field;
    }

    // Create or update field instance.
    if ($field['field_name'] == 'field_locations') {
      if (!count($content_types)) {
        $instance['entity_type'] = 'node';
        $instance['field_name'] = 'field_locations';
        $instance['required'] = FALSE;
        $instance['label'] = 'Store';
        foreach (array('event', 'promo', 'department_article') as $type) {
          $instance['bundle'] = $type;
          _roles_and_permissions_create_or_update_instance($instance, $existing_instances);
        }
      }
    }
    elseif (RAP_FIELD_USER == $field['field_name'] && !count($content_types)) {
      $instance['entity_type'] = 'user';
      $instance['field_name'] = RAP_FIELD_USER;
      $instance['label'] = 'Store / region ACL';
      $instance['bundle'] = 'user';
      _roles_and_permissions_create_or_update_instance($instance, $existing_instances);
    }
    else {
      if (count($content_types)) {
        $node_types = array();
        foreach ($content_types as $type) {
          $node_types[] = (object) array('type' => $type);
        }
      }
      else {
        $node_types = node_type_get_types();
      }
      $instance['entity_type'] = 'user';
      $instance['field_name'] = RAP_FIELD;  //@wtf shouldn't this be RAP_FIELD_USER?
      $instance['label'] = 'User Store';
      $instance['bundle'] = 'user';
      _roles_and_permissions_create_or_update_instance($instance, $existing_instances);
      $instance['entity_type'] = 'node';
      $instance['field_name'] = RAP_FIELD;
      $instance['required'] = TRUE;
      $instance['label'] = 'Store / Region';
      foreach ($node_types as $node_type) {
        $instance['bundle'] = $node_type->type;
        _roles_and_permissions_create_or_update_instance($instance, $existing_instances);
      }
    }
  }
}

/**
 * Implements hook_node_type_insert().
 * If you add a new content type, this calls a function to add
 * the Location ACL field
 */
function roles_and_permissions_node_type_insert($info) {
  roles_and_permissions_create_fields($info->type);
}
