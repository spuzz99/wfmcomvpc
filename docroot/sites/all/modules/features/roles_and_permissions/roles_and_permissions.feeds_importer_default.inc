<?php
/**
 * @file
 * roles_and_permissions.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function roles_and_permissions_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'locations_acl_taxonomy_xml';
  $feeds_importer->config = array(
    'name' => 'Locations ACL Taxonomy XML',
    'description' => 'Locations ACL taxonomy importer',
    'fetcher' => array(
      'plugin_key' => 'RAPFeedsHTTPSFetcher',
      'config' => array(
        'auto_detect_feeds' => TRUE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '$.[*]',
        'sources' => array(
          'jsonpath_parser:0' => 'tlc',
          'jsonpath_parser:1' => 'name',
          'jsonpath_parser:2' => 'region',
          'jsonpath_parser:4' => 'metro_area',
          'jsonpath_parser:3' => 'facility',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:0' => 0,
            'jsonpath_parser:1' => 0,
            'jsonpath_parser:2' => 0,
            'jsonpath_parser:4' => 0,
            'jsonpath_parser:3' => 0,
          ),
        ),
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsTermProcessor',
      'config' => array(
        'bundle' => 'locations_acl',
        'vocabulary' => 'locations_acl',
        'force_update' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:0',
            'target' => 'name',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'description',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'parent',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'jsonpath_parser:4',
            'target' => 'parent',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'field_bu_type',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => FALSE,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['locations_acl_taxonomy_xml'] = $feeds_importer;

  return $export;
}
