<?php
/**
 * @file
 * roles_and_permissions.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function roles_and_permissions_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'term_view_http_response';
  $handler->task = 'term_view';
  $handler->subtask = '';
  $handler->handler = 'http_response';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Locations ACL term',
    'contexts' => array(),
    'relationships' => array(),
    'code' => '301',
    'destination' => 'taxonomy/term/%term:tid/edit',
    'access' => array(
      'plugins' => array(
        1 => array(
          'name' => 'entity_bundle:taxonomy_term',
          'settings' => array(
            'type' => array(
              'locations_acl' => 'locations_acl',
            ),
          ),
          'context' => 'argument_term_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $export['term_view_http_response'] = $handler;

  return $export;
}
