<?php

/**
 * @file
 *   Roles and Permissions Migration pages for users migration.
 */

/**
 * Page callback: Form constructor for the RAP Content Migration page.
 */
function roles_and_permissions_migration_users_admin_form($form, $form_state) {
  drupal_set_title(t('Roles And Permissions Migration'));
  $form = array();
  $roles = user_roles(TRUE);
  $form['roles'] = array(
    '#type' => 'select',
    '#title' => t('User Roles to Migrate'),
    '#options' => array('all' => t('--All--')) + $roles,
    '#default_value' => isset($form_state['values']['roles']) ? $form_state['values']['roles'] : 'all',
    '#multiple' => TRUE,
  );
  $form['new_role'] = array(
    '#type' => 'select',
    '#title' => t('Add new role for selected users'),
    '#description' => t('Add this role to user if he hasn\'t yet this role. The following changes will be provided only for those users that must be migrated (at least one no empty Locations ACL field must be filled in user\'s data).'),
    '#options' => array('no' => t('--No--')) + $roles,
    '#default_value' => isset($form_state['values']['new_role']) ? $form_state['values']['new_role'] : 'no',
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Start Migration'),
  );
  return $form;
}


/**
 * Form submission handler for node_filter_form().
 */
function roles_and_permissions_migration_users_admin_form_submit($form, &$form_state) {
  $roles = isset($form_state['values']['roles']['all']) ? array() : $form_state['values']['roles'];
  $new_role = $form_state['values']['new_role'] == 'no' ? NULL : $form_state['values']['new_role'];
  $message = roles_and_permissions_migration_users_process($roles, $new_role);
  drupal_set_message($message['message'], $message['status']);
}
