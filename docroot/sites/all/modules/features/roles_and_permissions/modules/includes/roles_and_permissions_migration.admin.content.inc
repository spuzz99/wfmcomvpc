<?php

/**
 * @file
 *   Roles and Permissions Migration pages for content migration.
 */

/**
 * Page callback: Form constructor for the RAP Content Migration page.
 */
function roles_and_permissions_migration_admin_form($form, $form_state) {
  drupal_set_title(t('Roles and Permissions Migration'));
  $form = array();
  $types = array();
  $terms = array();
  $vocabulary = taxonomy_vocabulary_machine_name_load(RAP_VOCABULARY);
  $required_types = roles_and_permissions_migration_required_types();
  $excluded_types = roles_and_permissions_migration_excluded_types();

  if (!isset($form_state['values']['default_tid'])) {
    $def_terms = taxonomy_get_term_by_name('WFM', RAP_VOCABULARY);
    if (count($def_terms)) {
      $def_term = array_pop($def_terms);
      $def_tid = $def_term->tid;
    }
    else {
      $def_tid = 0;
    }
  }
  else {
    $def_tid = $form_state['values']['default_tid'];
  }

  // Get all content types wich have our field.
  $field = field_info_field(RAP_FIELD);
  if (isset($field['bundles']['node'])) {
    foreach (node_permissions_get_configured_types() as $type) {
      if (in_array($type, $field['bundles']['node'])) {
        $info = node_type_get_type($type);
        if (!isset($excluded_types[$type]) && !isset($required_types[$type])) {
          $types[$type] = $info->name;
        }
        if (isset($excluded_types[$type])) {
          $excluded_types[$type] = $info->name;
        }
        if (isset($required_types[$type])) {
          $required_types[$type] = $info->name;
        }
      }
    }
  }

  foreach (taxonomy_get_tree($vocabulary->vid, 0, 1) as $term) {
    $terms[$term->tid] = $term->name;
  }

  $form['default_tid'] = array(
    '#type' => 'select',
    '#title' => t('Default Location'),
    '#description' => t('Default (parent) Location for empty Location ID values.'),
    '#options' => $terms,
    '#default_value' => $def_tid,
    '#required' => TRUE,
  );
  $form['types_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types to Migrate'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['types_wrapper']['types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types to Migrate'),
    '#title_display' => 'invisible',
    '#options' => $types,
    '#default_value' => array_keys($types),
  );
  $form['types_wrapper']['required'] = array(
    '#type' => 'fieldset',
    '#title' => t('Required Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['types_wrapper']['required']['required_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types to Migrate'),
    '#title_display' => 'invisible',
    '#options' => $required_types,
    '#required' => TRUE,
    '#default_value' => array_keys($required_types),
    '#disabled' => TRUE,
    '#description' => t('This content types will be included anyway.'),
  );
  $form['types_wrapper']['excluded'] = array(
    '#type' => 'fieldset',
    '#title' => t('Excluded Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['types_wrapper']['excluded']['excluded_types'] = array(
    '#title' => t('Content Types to Exclude From Migrate'),
    '#title_display' => 'invisible',
    '#type' => 'checkboxes',
    '#options' => $excluded_types,
    '#required' => FALSE,
    '#disabled' => TRUE,
    '#description' => t('This content types will be excluded anyway.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Start Migration'),
  );
  return $form;
}


/**
 * Form submission handler for node_filter_form().
 */
function roles_and_permissions_migration_admin_form_submit($form, &$form_state) {
  $operations = array();

  $selected_types = $form_state['values']['types'];
  $required_types = $form_state['values']['required_types'];
  $excluded_types = array_merge($required_types, drupal_map_assoc(array_keys($form_state['values']['excluded_types'])));
  $tid = $form_state['values']['default_tid'];

  foreach ($selected_types as $type => $selected) {
    if (!$selected) {
      $excluded_types[$type] = $type;
      unset($selected_types[$type]);
    }
  }
  $counter = 0;
  // Map Stores, National offices, Metros and Regions first.
  foreach (roles_and_permissions_migration_get_all_nids($required_types) as $chunk) {
    $operations[] = array('roles_and_permissions_migration_batch_process', array($chunk, $tid));
    $counter += count($chunk);
  }
  // Map other content types except $excluded_types and not selected in form.
  foreach (roles_and_permissions_migration_get_all_nids($selected_types, $excluded_types) as $chunk) {
    $operations[] = array('roles_and_permissions_migration_batch_process', array($chunk, $tid));
    $counter += count($chunk);
  }
  $_SESSION['roles_and_permissions_current_count'] = 0;
  $_SESSION['roles_and_permissions_counter'] = $counter;
  $batch = array(
    'operations' => $operations,
    'title' => t('Roles and Permissions WFM Migration.'),
    'init_message' => t('Migration process is starting...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Batch has encountered an error.'),
    'finished' => 'roles_and_permissions_migration_batch_finished',
    'file' => drupal_get_path('module', 'roles_and_permissions_migration')
        . 'includes/roles_and_permissions_migration.batch.inc',
  );
  batch_set($batch);
}
