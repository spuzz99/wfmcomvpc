<?php
/**
 * @file
 * wfm_monster_nav.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wfm_monster_nav_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The Monster Nav.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Main menu');
  t('The Monster Nav.');


  return $menus;
}
