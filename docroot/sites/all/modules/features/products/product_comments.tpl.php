<?php
/**
 * @file
 * Theme file for the product comments.
 *
 * Available Variables:
 *  - $pid - The product ID stored at WFM.
 *  - $title - The product title
 */

// Add the setup JS that we'll be needing.
?>
<script type="text/javascript">
  var commentId="P<?php print $pid; ?>";
  var productId="P<?php print $pid; ?>";
  var productTitle="<?php print $title; ?>";
</script>

<h3 id="comments-section-head"><?php print t('Discussion'); ?></h3>
<a id="comments"></a>
<div id="pluck_comments">
  <div id="pluckComments">
    <script type="text/javascript">
	    var opts = {}
	    pluckAppProxy.embedApp("pluck_comments", {
  			plckCommentOnKeyType: "article", //required
  			plckCommentOnKey: "P<?php print $pid; ?>", //required
        plckItemsPerPage: "15",
        plckSort: "TimeStampDescending"
	    }, opts);
    </script>
  </div>
</div>