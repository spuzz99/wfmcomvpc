<?php
/**
 * @file
 * products.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function products_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__file_styles_prodline_hero_300x260__file_field_styles_file_prodline_hero_300x260';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = '';
  $export['image__file_styles_prodline_hero_300x260__file_field_styles_file_prodline_hero_300x260'] = $file_display;

  return $export;
}
