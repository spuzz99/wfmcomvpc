<?php
/**
 * @file
 * products.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function products_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basics|node|product|form';
  $field_group->group_name = 'group_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_product';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '38',
    'children' => array(
      0 => 'body',
      1 => 'field_allergen_info',
      2 => 'field_image',
      3 => 'field_ingredients',
      4 => 'field_product_certification',
      5 => 'field_product_line',
      6 => 'field_product_size',
      7 => 'field_special_diet',
      8 => 'field_product_category',
      9 => 'field_product_pid',
      10 => 'field_featured',
      11 => 'field_exclusive_until',
      12 => 'field_product_brand',
      13 => 'field_product_certifier',
      14 => 'field_product_nutrition_info',
      15 => 'field_product_origin',
      16 => 'field_product_shelf_life',
      17 => 'field_product_upc',
      18 => 'locations_acl',
      19 => 'remove_from_search_field',
      20 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_basics|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_body_content|node|product_line|form';
  $field_group->group_name = 'group_body_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_line';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_product_lines';
  $field_group->data = array(
    'label' => 'Body Content',
    'weight' => '12',
    'children' => array(
      0 => 'body',
      1 => 'field_image',
      2 => 'field_logo',
      3 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_body_content|node|product_line|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_left|node|product|default';
  $field_group->group_name = 'group_left';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'left',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_allergen_info',
      2 => 'field_ingredients',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'left',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_left|node|product|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_related|node|product_line|form';
  $field_group->group_name = 'group_page_related';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_line';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_product_lines';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '13',
    'children' => array(
      0 => 'field_related_content',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_page_related|node|product_line|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pc_basics|node|product_certification|form';
  $field_group->group_name = 'group_pc_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_certification';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_product_certification';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '102',
    'children' => array(
      0 => 'field_logo',
      1 => 'locations_acl',
      2 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_pc_basics|node|product_certification|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_certification|node|product_certification|form';
  $field_group->group_name = 'group_product_certification';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_certification';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Product Certification',
    'weight' => '0',
    'children' => array(
      0 => 'group_pc_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_product_certification|node|product_certification|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_lines|node|product_line|form';
  $field_group->group_name = 'group_product_lines';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_line';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Product Lines',
    'weight' => '0',
    'children' => array(
      0 => 'locations_acl',
      1 => 'group_body_content',
      2 => 'group_page_related',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_product_lines|node|product_line|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product|node|product|form';
  $field_group->group_name = 'group_product';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Product',
    'weight' => '38',
    'children' => array(
      0 => 'group_basics',
      1 => 'group_related_content',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_product|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_related_content|node|product|form';
  $field_group->group_name = 'group_related_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_product';
  $field_group->data = array(
    'label' => 'Related Content',
    'weight' => '40',
    'children' => array(
      0 => 'field_related_content',
      1 => 'field_related_recipes',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_related_content|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_right|node|product|default';
  $field_group->group_name = 'group_right';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'right',
    'weight' => '1',
    'children' => array(
      0 => 'field_product_certification',
      1 => 'field_special_diet',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'right',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_right|node|product|default'] = $field_group;

  return $export;
}
