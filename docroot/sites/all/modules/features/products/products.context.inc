<?php
/**
 * @file
 * products.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function products_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_category';
  $context->description = '';
  $context->tag = 'products';
  $context->conditions = array(
    'taxonomy_term' => array(
      'values' => array(
        'product_category' => 'product_category',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-product_category_header-block' => array(
          'module' => 'views',
          'delta' => 'product_category_header-block',
          'region' => 'content_top',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('products');
  $export['product_category'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_landing';
  $context->description = '';
  $context->tag = 'Products';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about-our-products' => 'about-our-products',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'search_api_page-product_search' => array(
          'module' => 'search_api_page',
          'delta' => 'product_search',
          'region' => 'content_top',
          'weight' => '-77',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Products');
  $export['product_landing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_lines';
  $context->description = '';
  $context->tag = 'Products';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about-our-products/our-product-lines' => 'about-our-products/our-product-lines',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ea63982c2cba834a786f0becb1af741c' => array(
          'module' => 'views',
          'delta' => 'ea63982c2cba834a786f0becb1af741c',
          'region' => 'content',
          'weight' => NULL,
        ),
        'views-product_lines-prodline' => array(
          'module' => 'views',
          'delta' => 'product_lines-prodline',
          'region' => 'content',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Products');
  $export['product_lines'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_lines_individual_pages';
  $context->description = 'Pulls together the views that make up the individual product line pages';
  $context->tag = 'Products';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'product_line' => 'product_line',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ba77cd001da557bfdc9b509af2430fa3' => array(
          'module' => 'views',
          'delta' => 'ba77cd001da557bfdc9b509af2430fa3',
          'region' => 'content',
          'weight' => '',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Products');
  t('Pulls together the views that make up the individual product line pages');
  $export['product_lines_individual_pages'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_page';
  $context->description = '';
  $context->tag = 'Products';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'product' => 'product',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-product_header-product_header' => array(
          'module' => 'views',
          'delta' => 'product_header-product_header',
          'region' => 'content_top',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Products');
  $export['product_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_search_page';
  $context->description = '';
  $context->tag = 'Products';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'products/search' => 'products/search',
        'products/search/*' => 'products/search/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'search_api_page-product_search' => array(
          'module' => 'search_api_page',
          'delta' => 'product_search',
          'region' => 'content_top',
          'weight' => '-77',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Products');
  $export['product_search_page'] = $context;

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sales_items';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'sales-item' => 'sales-item',
        'sales-flyer' => 'sales-flyer',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'products-products_flyers_selector' => array(
          'module' => 'products',
          'delta' => 'products_flyers_selector',
          'region' => 'content',
          'weight' => '-10',
        ),
        'products-sale_items' => array(
          'module' => 'products',
          'delta' => 'sale_items',
          'region' => 'content',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['sales_items'] = $context;

  return $export;
}
