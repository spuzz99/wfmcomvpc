
(function ($) {
  Drupal.behaviors.searchSort = {
    attach: function (context, settings) {
      var sort_search_selector = $('#edit-search-sort-form--2', context);
      sort_search_selector.change(function() {
        if ($(this).val() == 0) {
          $(location).attr('href', Drupal.settings.wfm_search.key);
        }
        else if ($(this).val() == 1) {
          $(location).attr('href', Drupal.settings.wfm_search.key + '?sort=created&order=desc');
        }
      });
    }
  };
})(jQuery);

