<?php
/**
 * @file
 * wfm_search.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wfm_search_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_search_sidebar';
  $context->description = 'Display blocks in sidebar of site search pages';
  $context->tag = 'search';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'site_search/*' => 'site_search/*',
        'recipe/search/*' => 'recipe/search/*',
        'products/search/*' => 'products/search/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'wfm_search-wfm_search_sort' => array(
          'module' => 'wfm_search',
          'delta' => 'wfm_search_sort',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-blogs-latest_blog_posts' => array(
          'module' => 'views',
          'delta' => 'blogs-latest_blog_posts',
          'region' => 'sidebar_second',
          'weight' => '0',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Display blocks in sidebar of site search pages');
  t('search');
  $export['site_search_sidebar'] = $context;

  return $export;
}
