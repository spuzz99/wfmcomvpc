<?php
/**
 * @file
 * wfm_search.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wfm_search_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use search_api_autocomplete for search_api_page_global_search'.
  $permissions['use search_api_autocomplete for search_api_page_global_search'] = array(
    'name' => 'use search_api_autocomplete for search_api_page_global_search',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_api_autocomplete',
  );

  return $permissions;
}
