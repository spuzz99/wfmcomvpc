<?php

/**
 * Implements hook_views_default_views().
 */
function wfm_search_views_default_views() {
  $views = array();
  $view = new view();
  $view->name = 'sponsored_search_results';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'sponsored_search_result';
  $view->human_name = 'Sponsored search results';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Sponsored search results';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filter';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'views_bulk_operations' => 'views_bulk_operations',
    'term' => 'term',
    'changed' => 'changed',
    'ssrid' => 'ssrid',
    'ssrid_1' => 'ssrid_1',
  );
  $handler->display->display_options['style_options']['default'] = 'changed';
  $handler->display->display_options['style_options']['info'] = array(
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'term' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ssrid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ssrid_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Sponsored search result: Sponsored node */
  $handler->display->display_options['relationships']['nid']['id'] = 'nid';
  $handler->display->display_options['relationships']['nid']['table'] = 'sponsored_search_result';
  $handler->display->display_options['relationships']['nid']['field'] = 'nid';
  /* Field: Bulk operations: Sponsored search result */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'sponsored_search_result';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['relationship'] = 'nid';
  $handler->display->display_options['fields']['path']['label'] = 'Page';
  $handler->display->display_options['fields']['path']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['path']['alter']['path'] = '[path]';
  /* Field: Sponsored search result: Term */
  $handler->display->display_options['fields']['term']['id'] = 'term';
  $handler->display->display_options['fields']['term']['table'] = 'sponsored_search_result';
  $handler->display->display_options['fields']['term']['field'] = 'term';
  /* Field: Sponsored search result: Changed */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'sponsored_search_result';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Modified';
  $handler->display->display_options['fields']['changed']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['changed']['date_format'] = 'blog_date';
  /* Field: Sponsored search result: Sponsored search result ID */
  $handler->display->display_options['fields']['ssrid']['id'] = 'ssrid';
  $handler->display->display_options['fields']['ssrid']['table'] = 'sponsored_search_result';
  $handler->display->display_options['fields']['ssrid']['field'] = 'ssrid';
  $handler->display->display_options['fields']['ssrid']['label'] = 'Operations';
  $handler->display->display_options['fields']['ssrid']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['ssrid']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['ssrid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['ssrid']['alter']['path'] = 'admin/config/search/sponsored/manage/[ssrid]';
  $handler->display->display_options['fields']['ssrid']['separator'] = '';
  /* Field: Sponsored search result: Sponsored search result ID */
  $handler->display->display_options['fields']['ssrid_1']['id'] = 'ssrid_1';
  $handler->display->display_options['fields']['ssrid_1']['table'] = 'sponsored_search_result';
  $handler->display->display_options['fields']['ssrid_1']['field'] = 'ssrid';
  $handler->display->display_options['fields']['ssrid_1']['label'] = '';
  $handler->display->display_options['fields']['ssrid_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['ssrid_1']['alter']['text'] = 'Delete';
  $handler->display->display_options['fields']['ssrid_1']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['ssrid_1']['alter']['path'] = 'admin/config/search/sponsored/manage/[ssrid_1]/delete';
  $handler->display->display_options['fields']['ssrid_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['ssrid_1']['separator'] = '';
  /* Filter criterion: Sponsored search result: Term */
  $handler->display->display_options['filters']['term']['id'] = 'term';
  $handler->display->display_options['filters']['term']['table'] = 'sponsored_search_result';
  $handler->display->display_options['filters']['term']['field'] = 'term';
  $handler->display->display_options['filters']['term']['operator'] = 'contains';
  $handler->display->display_options['filters']['term']['exposed'] = TRUE;
  $handler->display->display_options['filters']['term']['expose']['operator_id'] = 'term_op';
  $handler->display->display_options['filters']['term']['expose']['label'] = 'Term';
  $handler->display->display_options['filters']['term']['expose']['operator'] = 'term_op';
  $handler->display->display_options['filters']['term']['expose']['identifier'] = 'term';
  $handler->display->display_options['filters']['term']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    10 => 0,
    9 => 0,
    6 => 0,
    3 => 0,
  );
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'admin/config/search/sponsored';
  $views[$view->name] = $view;
  return $views;
}
