<?php
/**
 * @file
 * wfm_corporate.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wfm_corporate_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_core_value_basics|node|core_value|form';
  $field_group->group_name = 'group_core_value_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'core_value';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_core_value';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '9',
    'children' => array(
      0 => 'body',
      1 => 'field_value_media',
      2 => 'locations_acl',
      3 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_core_value_basics|node|core_value|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_core_value|node|core_value|form';
  $field_group->group_name = 'group_core_value';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'core_value';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Core Value',
    'weight' => '0',
    'children' => array(
      0 => 'group_core_value_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_core_value|node|core_value|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_faq_basics|node|faq|form';
  $field_group->group_name = 'group_faq_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'faq';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_faq';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '9',
    'children' => array(
      0 => 'body',
      1 => 'field_faq_taxonomy',
      2 => 'locations_acl',
      3 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_faq_basics|node|faq|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_faq|node|faq|form';
  $field_group->group_name = 'group_faq';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'faq';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'FAQ',
    'weight' => '0',
    'children' => array(
      0 => 'group_faq_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_faq|node|faq|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_national_offices_basics|node|national_offices|form';
  $field_group->group_name = 'group_national_offices_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'national_offices';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_national_offices';
  $field_group->data = array(
    'label' => 'Basics',
    'weight' => '21',
    'children' => array(
      0 => 'field_office_address',
      1 => 'field_office_telephone',
      2 => 'field_office_voicemail',
      3 => 'field_office_fax',
      4 => 'field_office_desc',
      5 => 'field_service',
      6 => 'field_department',
      7 => 'field_marquee',
      8 => 'field_store_twitter',
      9 => 'field_facebook_url',
      10 => 'field_top_level_domain',
      11 => 'locations_acl',
      12 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_national_offices_basics|node|national_offices|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_national_offices|node|national_offices|form';
  $field_group->group_name = 'group_national_offices';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'national_offices';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'National Offices',
    'weight' => '0',
    'children' => array(
      0 => 'group_pod_1',
      1 => 'group_pod_2',
      2 => 'group_pod_3',
      3 => 'group_national_offices_basics',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
      ),
    ),
  );
  $export['group_national_offices|node|national_offices|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pod_1|node|national_offices|form';
  $field_group->group_name = 'group_pod_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'national_offices';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_national_offices';
  $field_group->data = array(
    'label' => 'Featured Event',
    'weight' => '23',
    'children' => array(
      0 => 'field_pod_1_content',
      1 => 'field_pod_1_cta',
      2 => 'field_pod_1_image',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_pod_1|node|national_offices|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pod_2|node|national_offices|form';
  $field_group->group_name = 'group_pod_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'national_offices';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_national_offices';
  $field_group->data = array(
    'label' => 'Featured Sale',
    'weight' => '24',
    'children' => array(
      0 => 'field_pod_2_content',
      1 => 'field_pod_2_cta',
      2 => 'field_pod_2_image',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_pod_2|node|national_offices|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pod_3|node|national_offices|form';
  $field_group->group_name = 'group_pod_3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'national_offices';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_national_offices';
  $field_group->data = array(
    'label' => 'Featured Recipe',
    'weight' => '25',
    'children' => array(
      0 => 'field_pod_3_content',
      1 => 'field_pod_3_cta',
      2 => 'field_pod_3_image',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_pod_3|node|national_offices|form'] = $field_group;

  return $export;
}
