<?php
/**
 * @file
 * wfm_corporate.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wfm_corporate_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wfm_corporate_node_info() {
  $items = array(
    'national_offices' => array(
      'name' => t('National Offices'),
      'base' => 'node_content',
      'description' => t('World Headquarters and U.S. national offices'),
      'has_title' => '1',
      'title_label' => t('Office Name'),
      'help' => '',
    ),
    'person' => array(
      'name' => t('Person'),
      'base' => 'node_content',
      'description' => t('People to feature or reference within the site. People are primarily those employees that work for Whole Foods.'),
      'has_title' => '1',
      'title_label' => t('Full Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
