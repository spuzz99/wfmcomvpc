<?php
/**
 * @file
 * Contains the class definitions for adding video content from YouTube as nodes
 * in Drupal that point to their YouTube counterparts.
 */

define('WFM_CHANNEL_FEED', 'https://gdata.youtube.com/feeds/api/users/wholefoodsmarket/uploads');


class VideoMigrateList extends MigrateListJSON {
  static $_number = NULL;
  private $_idCache;

  protected function getIDsFromJSON(array $data) {
    $key = 'key=' . 'AI39si5hJMt13nDol1L3qCU7oSb4l-N9xTnyBiqufgRn8Qs9yJvha4lgCt12MJf-N9W-NT66OAmAybcDl6T3RKHNOaG2-p4saw';
    usleep(500);

    $first = FALSE;
    if (VideoMigrateList::$_number == NULL) {
      VideoMigrateList::$_number = 1;
      $first = TRUE;
    }

    $return_array = array();


    if (!isset($data['feed']['entry']))
      return array();

    foreach ($data['feed']['entry'] as $value) {
      $id_string = array_pop($value['id']);
      $id_array = explode(':', $id_string);
      $id = array_pop($id_array);
      $return_array[$id] = $id;
    }

    $totals = (int) $data['feed']['openSearch$totalResults']['$t'];
    VideoMigrateList::$_number += 25;

    if (VideoMigrateList::$_number < $totals) {
      $migrate_list = new VideoMigrateList(WFM_CHANNEL_FEED . '?v=2&alt=json&max-results=50&key' . $key . '&start-index=' . VideoMigrateList::$_number, array("content-type" => "application/json"));
      $return_array = array_merge($return_array, $migrate_list->getIdList());
    }

    if ($first) {
      VideoMigrateList::$_number = NULL;
    }

    return $return_array;
  }
}

class VideoMigrateItem extends MigrateItemJSON {
  protected function loadJSONUrl($item_url) {
    sleep(1);
    if (empty($this->httpOptions)) {
      $json = file_get_contents($item_url);
    }
    else {
      $response = drupal_http_request($item_url, $this->httpOptions);
      $json = $response->data;
    }
    return json_decode($json);
  }
}

class VideoMigrateSource extends MigrateSourceList {
}

class MigrateVideoFieldHandler extends MigrateFieldHandler {
  public function __construct() {
    $this->registerTypes(array('media'));
  }

  static function arguments() {
    return array();
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    $migration = Migration::currentMigration();
    $arguments = (isset($values['arguments']))? $values['arguments']: array();
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);
    $delta = 0;
    foreach ($values as $value) {
      $return[$language][$delta]['fid'] = $value;
      $delta++;
    }
    if (!isset($return)) {
      $return = NULL;
    }
    return $return;
  }
}


class VideoMigration extends Migration {
  public function complete($node, stdClass $row) {
    return;
  }

  public function prepareRow($row) {
    try {
      $title = get_object_vars($row->entry->title);
      $entry = get_object_vars($row->entry);
      $row->title = $title['$t'];

      $provider = media_internet_get_provider($row->entry->content->src);
      $file = $provider->save();

      $row->ytid = $row->vid_id;
      $title = get_object_vars($row->entry->title);
      $entry = get_object_vars($row->entry);
      $row->title = $title['$t'];
      $row->video = $file->fid;
      $media = get_object_vars($entry['media$group']);
      $desc = get_object_vars($media['media$description']);
      $desc = $desc['$t'];
      $row->body = $desc;

      $row->duration = (int) $media['yt$duration']->seconds;
      $row->rating_avg = (float) $entry['gd$rating']->average;
      $row->rating_count = (float) $entry['gd$rating']->numRaters;
      $row->favorite_count = (int) $entry['yt$statistics']->favoriteCount;
      $row->view_count = (int) $entry['yt$statistics']->viewCount;
      $row->like_count = (int) $entry['yt$rating']->numLikes;
      $row->dislike_count = (int) $entry['yt$rating']->numDislikes;
      $row->published = get_object_vars($entry['published']);
      $row->published = strtotime($row->published['$t']);
      $row->updated = get_object_vars($entry['updated']);
      $row->updated = strtotime($row->updated['$t']);

    } catch (Exception $e) {
      return FALSE;
    }

    return TRUE;
  }

  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('videos'));
    $key = 'key=' . 'AI39si5hJMt13nDol1L3qCU7oSb4l-N9xTnyBiqufgRn8Qs9yJvha4lgCt12MJf-N9W-NT66OAmAybcDl6T3RKHNOaG2-p4saw';
    $migrate_list = new VideoMigrateList(WFM_CHANNEL_FEED . '?v=2.1&alt=json&max-results=50&start-index=1&' . $key, array("content-type" => "application/json"));
    $migrate_item = new VideoMigrateItem('https://gdata.youtube.com/feeds/api/videos/:id?v=2&alt=json&' . $key, array("content-type" => "application/json"));
    $migrate_source_list = new MigrateSourceList($migrate_list, $migrate_item);

    $this->map = new MigrateSQLMap($this->machineName,
                                   array(
                                         'vid_id' => array(
                                                           'type' => 'varchar',
                                                           'length' => 32)
                                         ),
                                   MigrateDestinationNode::getKeySchema()
                                   );

    $this->source = $migrate_source_list;
    $this->destination = new MigrateDestinationNode('video');

    $args = MigrateVideoFieldHandler::arguments();
    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'body')->arguments(array('format' => 'full_html'));
    $this->addFieldMapping('field_video_ytid', 'ytid');
    $this->addFieldMapping('field_video', 'video')->arguments($args);
    $this->addFieldMapping('field_video_duration', 'duration');
    $this->addFieldMapping('field_video_rating_avg', 'rating_avg');
    $this->addFieldMapping('field_video_rating_count', 'rating_count');
    $this->addFieldMapping('field_video_favorite_count', 'favorite_count');
    $this->addFieldMapping('field_video_view_count', 'view_count');
    $this->addFieldMapping('field_video_like_count', 'like_count');
    $this->addFieldMapping('field_video_dislike_count', 'dislike_count');
    $this->addFieldMapping('field_video_youtube_published', 'published');
    $this->addFieldMapping('field_video_youtube_updated', 'updated');
  }

}
