<?php
/**
 * @file
 * Contains the class definitions for adding playlistsfrom YouTube as taxonomy
 * in Drupal that link the YouTube video nodes together.
 */

class PlaylistMigrateList extends MigrateListJSON {
  static $_number = NULL;

  protected function getIDsFromJSON(array $data) {
    $return_array = array();

    foreach ($data['feed']['entry'] as $value) {
      $id_string = array_pop($value['id']);
      $id_array = explode(':', $id_string);
      $id = array_pop($id_array);
      $return_array[] = $id;
    }

    return $return_array;
  }
}

class PlaylistMigrateItem extends MigrateItemJSON {
  protected function loadJSONUrl($item_url) {
    sleep(1);
    if (empty($this->httpOptions)) {
      $json = file_get_contents($item_url);
    }
    else {
      $response = drupal_http_request($item_url, $this->httpOptions);
      $json = $response->data;
    }
    return json_decode($json);
  }
}

class PlaylistMigrateSource extends MigrateSourceList {
}

class PlaylistMigration extends Migration {
  public function postImport() {
    $terms = taxonomy_get_tree(10, 0, NULL, TRUE);
    foreach ($terms as $t_tid => $term) {
      if (!isset($term->field_playlist_order[LANGUAGE_NONE]))
        continue;

      $print = FALSE;

      foreach ($term->field_playlist_order[LANGUAGE_NONE] as $k => $v) {
        $nid = $v['nid'];
        $node = node_load($nid);
        $playlists = array();
        if (isset($node->field_video_playlists[LANGUAGE_NONE])) {
          foreach ($node->field_video_playlists[LANGUAGE_NONE] as $delta => $tid) {
            $playlists[] = $tid['tid'];
          }
        }

        if (in_array($term->tid, $playlists)) {
          continue;
        }

        $playlists[] = $term->tid;

        foreach ($playlists as $delta => $tid) {
          $node->field_video_playlists[LANGUAGE_NONE][$delta]['tid'] = $tid;
        }

        node_save($node);
      }
    }
  }

  public function prepareRow($row) {
    $title = get_object_vars($row->feed->title);
    $title = $title['$t'];
    $row->title = $title;
    $the_title = $title;
    $entry = get_object_vars($row->feed);

    $nodes = array();

    foreach ($row->feed->entry as $id => $value) {
      $title = get_object_vars($value->title);
      $entry = get_object_vars($value);
      $media = get_object_vars($entry['media$group']);
      $video_id = get_object_vars($media['yt$videoid']);
      $video_id = $video_id['$t'];

      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'video')
        ->fieldCondition('field_video_ytid', 'value', $video_id)
        ->range(0, 1)
        ->addMetaData('account', user_load(1));

      $result = $query->execute();

      if (isset($result['node'])) {
        // Build a list in the correct positions.
        // Add tag to field.. or maybe save this and do it after.
        $result = array_pop($result['node']);
        $delta = get_object_vars($entry['yt$position']);
        $nodes[$delta['$t']]['nid'] = $result->nid;
      }
      else {
        //        echo "Can't Find " . $title['$t'] . " (" . $video_id . ") \n";
      }

    }

    $row->nodes = $nodes;
    return TRUE;
  }

  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('playlist'));
    $key = 'key=' . 'AI39si5hJMt13nDol1L3qCU7oSb4l-N9xTnyBiqufgRn8Qs9yJvha4lgCt12MJf-N9W-NT66OAmAybcDl6T3RKHNOaG2-p4saw';
    $migrate_list = new PlaylistMigrateList('http://gdata.youtube.com/feeds/api/users/wholefoodsmarket/playlists?v=2.1&alt=json&max-results=50&start-index=1&' . $key, array("content-type" => "application/json"));
    $migrate_item = new PlaylistMigrateItem('https://gdata.youtube.com/feeds/api/playlists/:id?v=2.1&alt=json&' . $key, array("content-type" => "application/json"));
    $migrate_source_list = new MigrateSourceList($migrate_list, $migrate_item);

    $this->map = new MigrateSQLMap(
      $this->machineName,
      array('pid_id' => array('type' => 'varchar', 'length' => 32)),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->source = $migrate_source_list;
    $this->destination = new MigrateDestinationTerm('playlist');

    $this->addFieldMapping('name', 'title');
    $this->addFieldMapping('field_playlist_order', 'nodes');
  }

}