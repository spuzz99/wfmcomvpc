<?php

function wfm_migrate_migrate_api() {

  $api = array(
    'api' => 2,

    // Migrations can be organized into groups. The key used here will be the
    // machine name of the group, which can be used in Drush:
    //  drush migrate-import --group=recipesv2
    // The title is a required argument which is displayed for the group in the
    'groups' => array(
      'recipesv2' => array(
        'title' => t('Recipes Import using SAGE API v2'),
      ),
    ),
    'migrations' => array(
      'recipev2' => array(
        'class_name' => 'RecipeV2Migration',
        'group_name' => 'recipesv2',

      ),
    ),
  );
  return $api;
}