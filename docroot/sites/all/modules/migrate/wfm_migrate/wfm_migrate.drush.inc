<?php

use Wfm\Api\SageClient\Recipe;

/**
 * Implements hook_drush_command().
 * @return array
 */
function wfm_migrate_drush_command() {
  $items = array();
  $items['mongo-mapper'] = array(
    'description' => 'Fill map table for initial SAGE v2 recipe import.',
    'callback' => '_wfm_migrate_mongo_mapper',
  );
  return $items;
}

/**
 * Callback function for drush command mongo-mapper.
 * @throws Exception
 */
function _wfm_migrate_mongo_mapper() {
  if (!class_exists('MongoLogger')) {
    $path = drupal_get_path('module', 'wfm_migrate');
    require_once($path . '/includes/logger.inc');
  }
  $log = New MongoLogger();

  $log->logMe('Beginning Mongo Mapper Process at ' . date(DATE_RFC2822, time()));

  try {
    // Truncate mapping table.
    $map_table = 'migrate_map_recipev2';
    db_truncate($map_table)->execute();

    // Caching mechanism for auth token.
    $cache = new WfmApiDrupalCache(array('cache_bin' => 'cache_wfm_api'));
    // Set up recipe api object.
    $apiRecipe = new Recipe(
      WFMVariables::get('wfmapi_key'),
      WFMVariables::get('wfmapi_secret'),
      'https://' . WFMVariables::get('wfmapi_host'),
      WFMVariables::get('wfmapi_version'),
      $cache
    );

    $log->logMe('Getting recipe ids from SAGE...');

    // Get array of CARL ids and Mongo Ids.
    $recipeIds = $apiRecipe->getAllRecipeIds();

    $log->logme(count($recipeIds) . ' recipes found in SAGE API.');

    foreach ($recipeIds as $recipeId) {
      $nid = _wfm_migrate_get_node_id_from_rid($recipeId['id']);
      if ($nid) {
        _wfm_migrate_insert_row($nid, $recipeId['_id'], $map_table);
      }
      else {
        $log->logme('Mongo id: ' . $recipeId['_id'] . ' has no associated node id.', 'notice');
      }
    }
    $log->logMe($map_table . ' table done importing data at ' . date(DATE_RFC2822, time()));

    $recipeNodeCount = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('type', 'recipe')
      ->execute()
      ->rowCount();

    $recipeImportCount = db_select($map_table . '', 'm')
      ->fields('m', array('sourceid1'))
      ->execute()
      ->rowCount();

    $log->logMe($recipeNodeCount . ' recipe nodes found in database. ' . $recipeImportCount . ' entries in mapping table.');
  }
  catch(Exception $e) {
    $log->logMe($e, 'error');
    die;
  }
}


/**
 * Get node id from recipe CARL mysql id.
 * @param int $rid
 * @return int node id
 */
function _wfm_migrate_get_node_id_from_rid($rid) {
  $result = db_query('SELECT f.entity_id FROM {field_data_field_rid} f WHERE f.field_rid_value = :rid',
    array(':rid' => $rid)
  );

  $entity = $result->fetchAssoc();
  return $entity['entity_id'];
}

/**
 * @param int $nid
 * @param string $mongoId
 * @throws Exception
 */
function _wfm_migrate_insert_row($nid, $mongoId, $table) {
  $result = db_insert($table)
    ->fields(array(
      'sourceid1' => $mongoId,
      'destid1' => $nid,
      'needs_update' => 0,
      'rollback_action' => 0,
      'last_imported' => 0,
      'hash' => 0
    ));
  $result->execute();
}