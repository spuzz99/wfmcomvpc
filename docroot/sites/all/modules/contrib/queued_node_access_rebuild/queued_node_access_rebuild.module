<?php

/**
 * Implements hook_menu().
 */
function queued_node_access_rebuild_menu() {
  $items = array();

  $items['admin/config/content/queued-node-access-rebuild'] = array(
    'title' => 'Queued node access rebuild',
    'description' => t('Manage queued node access rebuild settings.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('queued_node_access_rebuild_admin_form'),
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

/**
 * Administrative settings form.
 */
function queued_node_access_rebuild_admin_form() {
  $form = array();

  $form['queued_node_access_rebuild'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable queued node access rebuild'),
    '#default_value' => variable_get('queued_node_access_rebuild', FALSE),
  );

  $form['queued_node_access_rebuild_time'] = array(
    '#type' => 'textfield',
    '#title' => 'Queue processing time',
    '#default_value' => variable_get('queued_node_access_rebuild_time', 60),
    '#description' => t('The number of seconds that the node access rebuild queue will be processed per queue-cron run.'),
  );

  return system_settings_form($form);
}

/**
 * Validation for administrative settings form.
 */
function queued_node_access_rebuild_admin_form_validate($form, &$form_state) {
  $rebuild_time = $form_state['values']['queued_node_access_rebuild_time'];
  if (!is_numeric($rebuild_time) || $rebuild_time <= 0) {
    form_set_error('queued_node_access_rebuild_time', t('Queue processing time must be a number of seconds (greater than 0).'));
  }
}

/**
 * Implements hook_cron().
 */
function queued_node_access_rebuild_cron() {
  // Populate the queue with nodes needing access rebuild
  if (node_access_needs_rebuild() && variable_get('queued_node_access_rebuild', FALSE)) {
    queued_node_access_rebuild_build_queue();
  }
  // Check if the current queue has been fully processed
  elseif (variable_get('queued_node_access_rebuild_processing', FALSE)) {
    queued_node_access_rebuild_finished();
  }
}

/**
 * Build the queue.
 */
function queued_node_access_rebuild_build_queue() {
  variable_set('queued_node_access_rebuild_processing', FALSE);
  $nids = queued_node_access_rebuild_get_nodes();
  $count = count($nids);

  if (!empty($nids)) {
    // If the current queue isn't empty, delete it.
    $queue = DrupalQueue::get('queued_node_access_rebuild');
    if ($queue->numberOfItems() != 0) {
      $queue->deleteQueue();
    }
    // Add nodes to the queue.
    queued_node_access_rebuild_add($nids);
    variable_set('queued_node_access_rebuild_processing', TRUE);
    node_access_needs_rebuild(FALSE);
    watchdog('queued_node_access_rebuild', '%count nodes queued for node access rebuild.', array('%count' => $count), WATCHDOG_NOTICE);
  }
}

/**
 * Get list of nodes to queue.
 */
function queued_node_access_rebuild_get_nodes(array $types = array()) {

  $query = db_select('node', 'n')->fields('n', array('nid'));
  if (!empty($types)) {
    $query->condition('n.type', $types, 'IN');
  }
  $nids = $query->execute()->fetchCol();
  return $nids;
}

/**
 * Cleanup after queue completion.
 */
function queued_node_access_rebuild_finished() {
  $queue = DrupalQueue::get('queued_node_access_rebuild');
  if ($queue->numberOfItems() == 0) {
    variable_set('queued_node_access_rebuild_processing', FALSE);
    db_query("DELETE FROM {node_access} WHERE nid NOT IN (SELECT nid FROM {node})");
    watchdog('queued_node_access_rebuild', 'Node access rebuild complete.', NULL, WATCHDOG_NOTICE);
  }
}

/**
 * Processor function for queued_node_access_rebuild queue.
 */
function queued_node_access_rebuild_process_queue($nid) {
  if ($node = node_load($nid)) {
    node_access_acquire_grants($node);
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function queued_node_access_rebuild_cron_queue_info() {
  $queues['queued_node_access_rebuild'] = array(
    'worker callback' => 'queued_node_access_rebuild_process_queue',
    'time' => variable_get('queued_node_access_rebuild_time', 60),
  );
  return $queues;
}

/**
 * Utility function for developers to add nodes to the node access rebuild queue.
 *
 * @param [Array] $nids
 */
function queued_node_access_rebuild_add($nids) {
  $queue = DrupalQueue::get('queued_node_access_rebuild');
  $count = count($nids);
  foreach($nids as $nid) {
    $queue->createItem($nid);
  }
  variable_set('queued_node_access_rebuild_processing', TRUE);
  watchdog('queued_node_access_rebuild', '%count nodes queued for node access rebuild.', array('%count' => $count), WATCHDOG_NOTICE);
}
