<?php

class AcquiaSearchApiSolrHttpTransport extends SearchApiSolrHttpTransport {
  private $_authRequest = NULL;

  protected function getAuthRequest() {
    if ($this->_authRequest == NULL) {
      module_load_include('php', 'acquia_search_authenticator', 'AcquiaSearchAuthenticatorRequest');
      $this->_authRequest = new AcquiaSearchAuthenticatorRequest();
    }

    return $this->_authRequest;
  }

  /**
   * Modify the url and add headers appropriate to authenticate to Acquia Search.
   *
   * @return
   *  The nonce used in the request.
   */
  protected function prepareRequest(&$url, &$options, $use_data = TRUE) {
    $auth = $this->getAuthRequest();
    return $auth->prepareRequest($url, $options, $use_data);
  }

  /**
   * Validate the hmac for the response body.
   *
   * @return
   *  The response object.
   */
  protected function authenticateResponse($response, $nonce, $url) {
    $auth = $this->getAuthRequest();
    return $auth->authenticateResponse($response, $nonce, $url);
  }

  /**
   * Helper method for making an HTTP request.
   */
  protected function performHttpRequest($method, $url, $timeout, $rawPost = NULL, $contentType = NULL) {
    $options = array(
      'method' => $method,
      'timeout' => $timeout && $timeout > 0 ? $timeout : $this->getDefaultTimeout(),
      'headers' => array(),
    );

    if ($this->http_auth) {
      $options['headers']['Authorization'] = $this->http_auth;
    }
    if ($timeout) {
      $options['timeout'] = $timeout;
    }
    if ($rawPost) {
      $options['data'] = $rawPost;
    }
    if ($contentType) {
      $options['headers']['Content-Type'] = $contentType;
    }

    $nonce = $this->prepareRequest($url, $options);
    $response = drupal_http_request($url, $options);
    $type = isset($response->headers['content-type']) ? $response->headers['content-type'] : 'text/xml';
    $body = isset($response->data) ? $response->data : NULL;
    return new Apache_Solr_HttpTransport_Response($response->code, $type, $body);
  }
}
