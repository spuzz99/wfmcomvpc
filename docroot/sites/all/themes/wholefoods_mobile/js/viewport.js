/**
 * This is depcrecated for now. user scalable enabled to all devices according
 * to the ticket QD-1300.
 */
(function() {
  var meta = document.createElement('meta');
  meta.name = 'viewport';
  meta.content = /android/i.test(navigator.userAgent)
      ? 'width=640px'
      : 'width=640px, user-scalable=no';
  document.getElementsByTagName('head')[0].appendChild(meta);
})();
