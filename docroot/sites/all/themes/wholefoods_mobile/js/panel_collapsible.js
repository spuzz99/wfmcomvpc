(function($) {
  $(function() {
    $('.panel-pane.collapsible').each(function() {
      var target = $(this)
        , collapsed = target.hasClass('collapsed')
        , title = $('h2.pane-title', target)
        , height = title.outerHeight()
        , button = $('<a/>').attr('href', '').addClass('toggle');
      height && button.css('lineHeight', height + 'px');
      collapsed ? button.text('+') : button.text('−');
      title.click(function() {
        button.click();
      });
      button.click(function() {
        var target = $(this)
          , content = target.next()
          , collapsed = target.parent().hasClass('collapsed');
        title.addClass('animating');
        collapsed ? content.slideDown(function() {
          target.text('−').parent().removeClass('collapsed');
          title.removeClass('animating');
        }) : content.slideUp(function() {
          target.text('+').parent().addClass('collapsed');
          title.removeClass('animating');
        });

        return false;
      }).insertAfter(title);
    });
  });
})(jQuery);
