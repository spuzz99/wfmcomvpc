<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?> class="wholefoods-mobile-theme">
<head<?php print $rdf->profile; ?>>
  <meta name="viewport" content="width=640" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="format-detection" content="telephone=no">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php print _wholefoods_mobile_render_apple_touch_icons($variables); ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php print $gtm_code; ?>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
