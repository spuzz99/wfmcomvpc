<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php
  $rating = $row->field_field_recipe_rating[0]['raw']['value'];
  $percentage = $rating / 5 * 100;
?>

<?php if ($rating): ?>
  <div class="stars" style="width: <?php echo $percentage; ?>%">
      <?php
        echo t('Recipe Rating: !rating',
          array(
            '!rating' => $rating,
          )
        );
      ?>
  </div><!-- .stars -->
<?php endif; ?>
