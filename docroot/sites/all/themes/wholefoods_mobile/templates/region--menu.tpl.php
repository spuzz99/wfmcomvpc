<?php
/**
 * @file
 * Implementation to display a menu region.
 */

?>
<div class="<?php print $classes ?>" id="region-<?php print $region ?>">
  <div class="region-inner region-<?php print $region ?>-inner">
    <?php if ($main_menu): ?>
    <nav class="navigation">
      <?php print render($main_menu); ?>
    </nav>
    <?php endif; ?>
    <?php print $content; ?>
  </div>
</div>
