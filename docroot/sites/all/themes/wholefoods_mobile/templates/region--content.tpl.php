<?php
/**
 * @file
 * Implementation to display a content region.
 */
?>
<section class="<?php print $classes ?>" id="section-<?php print $region ?>">
  <div class="region-inner region-<?php print $region ?>-inner">
    <a id="main-content"></a>
    <?php print $content; ?>
  </div>
</section>
