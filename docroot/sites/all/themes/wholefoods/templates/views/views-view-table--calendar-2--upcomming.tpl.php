<?php $date_format_string = 'g:i A'; ?>
<div class="events">
  <?php foreach ($result as $id => $event): ?>
  <?php $row = $rows[$id]; ?>
  <?php $date = $event->field_field_event_date[0]['raw']; ?>
  <a id="node-<?php print $event->nid . '-' . $date['value']; ?>"></a>
  <div class="event node-<?php print $event->nid; ?>">
    <div class="date-block">
      <div class="weekday">
        <?php print date('D', $date['value']); ?>
      </div>
      <div class="monthday">
        <?php print date('d', $date['value']); ?>
      </div>
    </div>
    <div class="detail-block">
      <div class="brief">
        <div class="title">
          <?php print $row['title']; ?>
        </div>

        <div class="time-loc">
          <?php
          $start_date_formatted = date('Y-m-d H:i:s', $event->field_data_field_event_date_field_event_date_value);
          $end_date_formatted = date('Y-m-d H:i:s', $event->field_data_field_event_date_field_event_date_value2);
          if (date_is_all_day($start_date_formatted, $end_date_formatted, 'minute', 15)) {
            $date_string = "All Day";
          } else {
            $date_string = date($date_format_string, $date['value']);
            if ($date['value'] != $date['value2']) {
              $date_string .= ' - ' . date($date_format_string, $date['value2']);
            }
          }
          ?>
          <?php print $date_string ?> @ <?php print $row['field_location_name']; ?>
        </div>

        <div class="expand">
          <a href="">+</a>
        </div>

      </div>


      <div class="detail">
			        <div class="collapse">
							          <a href="">-</a>
												        </div>

        <div class="body">
          <div class="title">
            <?php print $row['title']; ?>
          </div>
          <div class="links">
            <?php print theme("wholefoods_event_links", array('nid' => $event->nid)); ?>
						<?php print theme('wholefoods_sharelinks'); ?>
          </div>

          <div class="time-loc">
            <div>
              <?php print date('l, F jS', $date['value']); ?>
            </div>

            <?php print $date_string; ?><br>
						<?php if ($row['field_price']): ?>
            <?php print $row['field_location_name']; ?>, <?php print $row['field_price']; ?>
						<?php else: ?>
						<?php print $row['field_location_name']; ?>
						<?php endif; ?>
          </div>
          <div class="clearfix"></div>
          <div class="description">
            <?php print $row['body']; ?>
          </div>

        </div>
        <div class="image">
          <?php print $row['field_event_image']; ?>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <?php endforeach; ?>
</div>
