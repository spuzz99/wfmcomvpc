<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div class="recipe-promo-container">
    <div class="<?php print $classes_array[$id]; ?>">
      <?php print $row; ?>
    </div>


    <div class="recipe-promo-nav">
      <a class="recipe-prev1" href="#">Prev</a> <a class="recipe-next1" href="#">Next</a>
      <ul class="recipe-promo-pager-nav"></ul>
    </div>
  </div>
<?php endforeach; ?>
