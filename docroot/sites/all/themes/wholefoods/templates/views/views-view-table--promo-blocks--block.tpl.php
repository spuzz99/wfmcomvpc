<?php
// Rows include...
//   title
//   field_marquee_media
//   field_marquee_url
//   field_heading_copy
//
// Uncomment to see the data structure...
// dsm($rows);
?>

<?php foreach ($rows as $row): ?>
  <div class="promo-container">
    <div class="promo-row">
      <div class="promo-content promo-ref" ref="<?php print $row['nid']; ?>">
        <div class="promo-media">
          <?php print $row['field_promo_media']; ?>
        </div>
        <h2><?php print $row['field_heading_copy']; ?></h2>

        <div class="promo-intro-subheading"><?php print $row['field_page_intro_subheading']; ?></div>
        <?php print $row['body']; ?>

        <?php if (!empty($row['php_1'])): ?>
          <div class="promo-share"><?php print $row['php_1']; ?></div>
        <?php endif; ?>

        <div class="promo_cta"><?php print $row['field_link_copy']; ?></div>

        <div class="promo-nav">
          <a class="prev1" href="#"><?php print t('Prev'); ?></a> <a class="next1" href="#"><?php print t('Next'); ?></a>
          <ul class="promo-pager-nav"></ul>
        </div>

      </div>
    </div>
  </div>
<?php endforeach; ?>

