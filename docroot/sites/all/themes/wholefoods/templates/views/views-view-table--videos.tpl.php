<?php
//print_r($rows);
foreach ($variables['view']->result as $k => $v) {
  $vid = $v->field_field_video[0]['raw']['file']->uri;
  $vid = explode('/', $vid);
  $vid = array_pop($vid);
  $query = array('autoplay' => '1', 'rel' => '0', 'border' => '0');
  $uri = url('http://www.youtube.com/v/' . $vid, array('query' => $query, 'external' => TRUE, 'https' => TRUE));
	$body = addslashes(check_plain($v->_field_data['nid']['entity']->body[LANGUAGE_NONE][0]['value']));
  $rows[$k]['field_video'] = '<a href="' . $uri . '" rel="lightvideo" title="'.$body.'">' . $rows[$k]['field_video'] . '</a>';
}

$grid = array();
$grid_row = 1;
$grid_col = 1;

foreach ($rows as $k => $row) {
  $grid[$grid_row][$grid_col] = array('id' => $k, 'content' => $row, 'class' => '');

  if ($grid_col == 3) {
    $grid_row++;
    $grid_col = 1;
  }
  else {
    $grid_col++;
  }
}
?>
<?php foreach ($grid as $row): ?>
	<?php $classes = array() ?>
		<?php
			if (count($row) == 3) {
				$classes[] = 'col-width-one';
        $style_name = 'small_pod';
			}
			elseif (count($row) == 2) {
				$classes[] = 'col-width-two';
        $style_name = 'promo_pod_large';
			}
			else { $classes[] = 'col-width-three';
        $style_name = 'promo_pod_large';
			}
		?>
		<div class="views-row <?php print implode(' ', $classes); ?>">
			<?php foreach ($row as $index => $item): ?>

			<div class="views-col <?php if ($index == 1) { print ' views-row-first'; } if ($index == count($row)) { print ' views-row-last'; } print ' ' . $item['class']; ?>">


    		<?php foreach ($item['content'] as $field => $content): ?>

        <?php if ($field == 'field_video_duration'): ?>
        <?php $min = floor(((int) $content / 60)); ?>
        <?php $seconds = (int) $content - ($min * 60); ?>
        <?php $content = str_pad($min, 1, '0', STR_PAD_LEFT) . ":" . str_pad($seconds, 2, '0', STR_PAD_LEFT); ?>
        <?php endif; ?>
        <div class="<?php print $field_classes[$field][$item['id']]; ?>"><?php print $content; ?></div>
				<?php endforeach; ?>
			</div>
			<?php endforeach; ?>
  		  <div class="clearfix"></div>
		</div><?php //end of views-row div?>

<?php endforeach; ?>
