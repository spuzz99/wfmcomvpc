<?php
$markers = array();

foreach ($rows as $id => $value) {
  $item = $result[$id];
  $address_parts_keys = array('thoroughfare', 'locality', 'administrative_area', 'postal_code', 'country');
  $address_parts = array();
  foreach ($address_parts_keys as $k) {
    if (!isset($item->field_field_postal_address[0])) {
    } else {
      if ($item->field_field_postal_address[0]['raw'][$k] != NULL)
        $address_parts[$k] = check_plain($item->field_field_postal_address[0]['raw'][$k]);
    }
  }

  $address = implode(', ', $address_parts);
  $markers[$id] = array(
    'lat' => $item->field_field_geo_data[0]['raw']['lat'],
    'lon' => $item->field_field_geo_data[0]['raw']['lon'],
    'title' => $value['title'],
    'raw_title' => check_plain($item->node_title),
    'description' => $value['title'] . $value['field_postal_address'],
    'address' => $address,
    'telephone_number' => (strlen($value['field_phone_number']) > 0) ? "<br />Phone Number: " . $value['field_phone_number'] : '',
    'links' => '<div class="store-links"><a href="#directions" onClick="javascript:insertMapDirections('.$id.')">' . t('Get Directions') . '</a>' . '<br />' .
    l(t('More Info'), 'node/' . $item->nid) . '</div>',
		'id' => $item->nid,
  );
}

$store_nid = NULL;
$store = NULL;
if (isset($_GET['store'])) {
  $store_nid = check_plain($_GET['store']);
}

if ($store_nid !== NULL) {
  $store = node_load($store_nid);
  if ($store !== NULL) {
    if ($store->type != 'store') {
      $store = NULL;
    }
  }
}

?>

<script>
var markers_list = <?php print json_encode($markers); ?>;
var current_store = null;
<?php if ($store !== NULL): ?>
current_store = <?php print json_encode(array('id' => $store->nid, 'lat' => $store->field_geo_data[LANGUAGE_NONE][0]['lat'], 'lon' => $store->field_geo_data[LANGUAGE_NONE][0]['lon'])); ?>;
<?php endif; ?>
</script>
<div id="map_left_bar">
  <?php if ($store !== NULL): ?>
  <div id="current_store">
  <h2><?php print t('Current Store'); ?></h2>
  <?php print views_embed_view('store_map', 'selected_store', $store->nid); ?>
  </div>

  <div id="nearby_stores">
    <h2><?php print t('Nearby Stores'); ?></h2>
<?php $arg1 = implode(':', array($store->field_geo_data[LANGUAGE_NONE][0]['lat'], $store->field_geo_data[LANGUAGE_NONE][0]['lon'])); ?>

    <div class="content">
    <div id="nearest_35_miles" class="nearest_stores">
      <h3><?php print t('Stores within 35 miles:'); ?></h3>
<?php print views_embed_view('store_lookup_by_address', 'within_range', $arg1, 35, $store->nid); ?>
    </div>
    <div id="nearest_100_miles" class="nearest_stores">
      <h3><?php print t('Stores within 100 miles:'); ?></h3>
<?php print views_embed_view('store_lookup_by_address', 'within_range', $arg1, 100, $store->nid); ?>
    </div>
    <div id="nearest_200_miles" class="nearest_stores">
      <h3><?php print t('Stores within 200 miles:'); ?></h3>
<?php print views_embed_view('store_lookup_by_address', 'within_range', $arg1, 200, $store->nid); ?>
    </div>
    <div id="nearest_500_miles" class="nearest_stores">
      <h3><?php print t('Stores within 500 miles:'); ?></h3>
<?php print views_embed_view('store_lookup_by_address', 'within_range', $arg1, 500, $store->nid); ?>
    </div>
      <div class="links">
  <a id="nearest_35_miles_button" href="" onClick="" class="nearest_store_button" within='35'><?php print t('Stores within 35 miles?'); ?></a>
  <a id="nearest_100_miles_button" href="" onClick="" class="nearest_store_button" within='100'><?php print t('Stores within 100 miles?'); ?></a>
  <a id="nearest_200_miles_button" href="" onClick="" class="nearest_store_button" within='200'><?php print t('Stores within 200 miles?'); ?></a>
  <a id="nearest_500_miles_button" href="" onClick="" class="nearest_store_button" within='500'><?php print t('Stores within 500 miles?'); ?></a>
  </div>
    </div>
  </div>
  <?php endif; ?>
	<a id="directions" name="directions"></a>
  <div id="map_directions">
    <h2><?php print t('Directions'); ?></h2>
    <div class="content">
      <form id="directions_form">
			<?php $val = ''; ?>
			<?php if ($store): ?>
			  <?php $postal = $store->field_postal_address[LANGUAGE_NONE][0]; ?>
				<?php $val = $postal['thoroughfare'] . ' '
				. $postal['locality'] . ' '
				. $postal['administrative_area'] . ' '
				.$postal['postal_code'] . ' '
				.$postal['country']; ?>
			<?php endif; ?>
        <label><?php print t('Start'); ?></label><input type="text" id="origin_box"></input>
         <label><?php print t('End'); ?></label><input type="text" id="destination_box" value="<?php print $val; ?>"></input>
        <label></label><input type="submit" value="<?php print t('Go'); ?>"></input>
      </form>
      <div id="map_directions_result">
      </div>
    </div>
  </div>
</div>

<?php
$nid = arg(1);
$the_node = node_load($nid);
?>

<div id="map_right_bar">
  <ul>
    <li>
      <a href="javascript:window.print();" class="print">print</a>
    </li>
    <li>
      <a href="mailto:?subject=<?php print urlencode($the_node->title); ?>&body=<?php print urlencode(url('node/' . $nid, array('absolute' => TRUE))); ?>">
      <?php print t('email'); ?></a>
    </li>
    <li>
      <?php echo theme('wholefoods_sharelinks'); ?>
    </li>
  </ul>
  <div id="map_canvas"></div>
</div>
