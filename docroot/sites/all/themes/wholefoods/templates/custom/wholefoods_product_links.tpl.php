<?php
global $user;
$node = node_load($nid);
?>

<ul>
  <li>
  <?php if ($user->uid > 0): ?>
    <?php print l(t('add to shopping list'), 'modals/nojs/add_to_shopping_list/' . $nid, array('attributes' => array('class' => array('add-to-shopping-list', 'ctools-use-modal')))); ?>
  <?php else: ?>
    <?php print l(t('add to shopping list'), 'user/janrain_register', array('attributes' => array('class' => array('iframe', 'fancy')))); ?>
  <?php endif; ?>
  </li>
  <li>
    <?php print l(t('print'), 'modals/nojs/print/' . $nid, array('attributes' => array('class' => array('print', 'ctools-use-modal')))); ?>
  </li>
  <li>
    <a class="email" href="mailto:?subject=<?php print urlencode($node->title); ?>&body=<?php print urlencode(url('node/' . $nid, array('absolute' => TRUE))); ?>">
      <?php print t('email'); ?>
    </a>
  </li>
</ul>
