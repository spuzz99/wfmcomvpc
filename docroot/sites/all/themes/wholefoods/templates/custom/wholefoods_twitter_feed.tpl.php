<?php
/**
 * @file
 * Theme file to display twitter posts.
 *
 */
?>

<?php drupal_add_js('//platform.twitter.com/widgets.js', array('type'=>'external', 'scope'=>'footer')); ?>
<h2>Tweets</h2>
<div class="social-feed-container">
  <div class="tweets-container">
    <?php foreach ($posts as $tweet): ?>
    <?php //define some vars
      if(isset($tweet->retweeted_status)) {
        $username = check_plain($tweet->retweeted_status->user->name);
        $shortname = check_plain($tweet->retweeted_status->user->screen_name);
        $userimage = check_plain($tweet->retweeted_status->user->profile_image_url);
        $tweetid = check_plain($tweet->retweeted_status->id_str);
        $tweetbody = filter_xss($tweet->retweeted_status->text);
        $retweetuser = check_plain($tweet->user->name);
        $retweetusershort = check_plain($tweet->user->screen_name);       
      }
      else {
        $username = check_plain($tweet->user->name);
        $shortname = check_plain($tweet->user->screen_name);
        $userimage = check_plain($tweet->user->profile_image_url);
        $tweetid = check_plain($tweet->id_str);
        $tweetbody = filter_xss($tweet->text);      
      }
    ?>
    <div class="tweet-content">
      <div class="tweet-img">
        <img alt="" src="<?php echo $userimage; ?>" />
      </div><!--tweet-img-->
      <div class="title">
        <h4 class="handle"><a href="https://twitter.com/<?php echo $shortname; ?>"><?php echo $username; ?></a></h4>
        <span class="timeago">
          <a href="https://twitter.com/<?php echo $shortname; ?>/status/<?php echo $tweetid; ?>">
            <?php echo $tweet->timeago; ?></span>
          </a>       
      </div><!--title-->
      <span class="screen_name">@<?php echo $shortname; ?></span>
      <p><?php echo $tweetbody; ?></p>
      <?php if(isset($tweet->retweeted_status)) {
        ?>
        <p class="retweet"><span class="retweeticon"></span>Retweeted by <a href="https://twitter.com/<?php echo $retweetusershort; ?>"><?php echo $retweetuser; ?></a></p>
        <?php
      }
      ?>
    </div><!--tweet-content-->
    <div class="tweet-actions">
      <a class="reply" title="Reply" href="https://twitter.com/intent/tweet?in_reply_to=<?php echo $tweet->id; ?>">Reply</a>
      <a class="retweet" title="Retweet" href="https://twitter.com/intent/retweet?tweet_id=<?php echo $tweet->id; ?>">Retweet</a>
      <a class="favorite" title="Favorite" href="https://twitter.com/intent/favorite?tweet_id=<?php echo $tweet->id; ?>">Favorite</a>
    </div><!--tweet-actions-->  
    <?php endforeach; ?>
  </div><!--tweet-container-->
  <p class="follow"><a href="https://twitter.com/intent/user?screen_name=<?php echo $posts[0]->user->screen_name; ?>">Follow <?php echo $posts[0]->user->screen_name; ?> on Twitter</a></p>
</div><!--social-feed-container-->
