
<?php if ($rating != 0): ?>
<div id="user-ratings">
<div class="recipe_rating">
<p>
<script type="text/javascript">
	var opts = {}
	pluckAppProxy.embedApp("pluck_itemRatings", {
		plckItemRatingOnKeyType: "article", 
		plckItemRatingOnKey: "<?php print $rating; ?>", 
		plckItemRatingDisplayType: "all"
	}, opts);
</script>
</p>
</div>
</div>
<!--user-ratings -->
<?php endif; ?>