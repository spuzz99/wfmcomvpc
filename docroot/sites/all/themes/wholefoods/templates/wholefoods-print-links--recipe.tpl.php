<?php
	global $theme;
	$themePath = drupal_get_path('theme', $theme);
?>
<form id="print-box" action="#">
   <div class="full_recipe_image">
   	<img src="/<?php print($themePath);?>/images/print/recipe-image.jpg" width="101" height="129" />
   	<input checked='checked' name="print-type" type="radio" value="recipe_with_image">Full Recipe (+ Image)</input>
   	<ul>
   		<li>Title /</li>
   		<li>Image /</li>
   		<li>Description /</li>
   		<li>Ingredients</li>
   		<li>Directions /</li>
   		<li>Nutrition /</li>
   		<li>Tags /</li>
   		<li>Notes</li>
   	</ul>
   </div>
   <div class="full_recipe">
   	<img src="/<?php print($themePath);?>/images/print/recipe-no-image.jpg" width="101" height="129" />
   	<input name="print-type" type="radio" value="recipe_no_image">Full Recipe (No Image)</input>
   	<ul>
   		<li>Title /</li>
   		<li> Description /</li>
   		<li>Ingredients</li>
   		<li>Directions /</li>
   		<li>Nutrition /</li>
   		<li>Tags /</li>
   		<li>Notes</li>
   	</ul>
   </div>
   <div class="recipe_card">
   	<img src="/<?php print($themePath);?>/images/print/recipe-card.jpg" width="101" height="129" />
 	<input name="print-type" type="radio" value="recipe_3x5">3 X 5 Recipe Card</input>
 	<ul>
   		<li>Title /</li>
   		<li>Ingredients /</li>
   		<li>Directions</li>
   	</ul>
   </div>
   <?php print l('Continue', '#', array('attributes' => array('class' => array('orange-link')))); ?>
</form>
<?php
$path = drupal_lookup_path('alias', 'node/' . $node->nid);
$path = '/print/' . $path;
?>
<script>
jQuery(document).ready(function($) {
  var form = $('#print-box');

  $('a', form).click(function() {
    var selected = $('input:checked', form);
    var win = window.open(Drupal.settings.WholeFoods.print.nodePath + '?css=' + selected.val(), "print");
    Drupal.CTools.Modal.dismiss();
    win.focus();
    return false;
  });
});
</script>
