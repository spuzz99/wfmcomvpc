if (typeof OpenLayers != 'undefined') {
    OpenLayers.Control.LayerSwitcher.prototype.redraw =
        function() {
            //if the state hasn't changed since last redraw, no need
            // to do anything. Just return the existing div.
            if (!this.checkRedraw()) {
                return this.div;
            }

            //clear out previous layers
            this.clearLayersArray("base");
            this.clearLayersArray("data");

            var containsOverlays = false;
            var containsBaseLayers = false;

            // Save state -- for checking layer if the map state changed.
            // We save this before redrawing, because in the process of redrawing
            // we will trigger more visibility changes, and we want to not redraw
            // and enter an infinite loop.
            var len = this.map.layers.length;
            this.layerStates = new Array(len);
            for (var i=0; i <len; i++) {
                var layer = this.map.layers[i];
                this.layerStates[i] = {
                    'name': layer.name,
                    'visibility': layer.visibility,
                    'inRange': layer.inRange,
                    'id': layer.id
                };
            }

            var layers = this.map.layers.slice();
            if (!this.ascending) { layers.reverse(); }
            for(var i=0, len=layers.length; i<len; i++) {
                var layer = layers[i];
                var baseLayer = layer.isBaseLayer;

                if (layer.displayInLayerSwitcher) {

                    if (baseLayer) {
                        containsBaseLayers = true;
                    } else {
                        containsOverlays = true;
                    }

                    // only check a baselayer if it is *the* baselayer, check data
                    //  layers if they are visible
                    var checked = (baseLayer) ? (layer == this.map.baseLayer)
                        : layer.getVisibility();

                    // create input element
                    var inputElem = document.createElement("input");
                    inputElem.id = this.id + "_input_" + layer.name.replace(' ', '_');
                    inputElem.name = (baseLayer) ? this.id + "_baseLayers" : layer.name;
                    inputElem.type = (baseLayer) ? "radio" : "checkbox";
                    inputElem.value = layer.name;
                    inputElem.checked = checked;
                    inputElem.defaultChecked = checked;

                    if (!baseLayer && !layer.inRange) {
                        inputElem.disabled = true;
                    }
                    var context = {
                        'inputElem': inputElem,
                        'layer': layer,
                        'layerSwitcher': this
                    };
                    OpenLayers.Event.observe(inputElem, "mouseup",
                                             OpenLayers.Function.bindAsEventListener(this.onInputClick,
                                                                                     context)
                                            );

                    // create span
                    var labelSpan = document.createElement("span");
                    OpenLayers.Element.addClass(labelSpan, "labelSpan");
                    if (!baseLayer && !layer.inRange) {
                        labelSpan.style.color = "gray";
                    }
                    labelSpan.innerHTML = layer.name;
                    labelSpan.style.verticalAlign = (baseLayer) ? "bottom"
                        : "baseline";
                    OpenLayers.Event.observe(labelSpan, "click",
                                             OpenLayers.Function.bindAsEventListener(this.onInputClick,
                                                                                     context)
                                            );
                    // create line break
                    var br = document.createElement("br");


                    var groupArray = (baseLayer) ? this.baseLayers
                        : this.dataLayers;
                    groupArray.push({
                        'layer': layer,
                        'inputElem': inputElem,
                        'labelSpan': labelSpan
                    });


                    var groupDiv = (baseLayer) ? this.baseLayersDiv
                        : this.dataLayersDiv;
                    groupDiv.appendChild(inputElem);
                    groupDiv.appendChild(labelSpan);
                    groupDiv.appendChild(br);
                }
            }

            // if no overlays, dont display the overlay label
            this.dataLbl.style.display = (containsOverlays) ? "" : "none";

            // if no baselayers, dont display the baselayer label
            this.baseLbl.style.display = (containsBaseLayers) ? "" : "none";

            var vendor_map = jQuery('.view-local-vendor-map');
            if (typeof vendor_map != 'undefined') {
                if (jQuery('#control_box').length == 0) {
                    jQuery('.view-content', vendor_map).prepend('<div id="control_box"></div>');
                }
                var control = jQuery('.olControlLayerSwitcher');
                control.appendTo(jQuery('#control_box'));

                control.attr('style', '');
                jQuery('.layersDiv').show();
                var dataLayers = jQuery('.dataLayersDiv');
                jQuery('br', dataLayers).remove();
                var i = 0;
                var list = new Array();
                jQuery('input', dataLayers).each(function() {
                    var item = new Array();
                    jQuery(this).hide();
                    item['input'] = this;
                    item['clicked'] = (jQuery(this).is(':checked')) ? 'checked' : 'not-checked';
                    item['label'] = jQuery('.labelSpan:eq(' + i + ')', dataLayers);
                    list[i] = item;
                    ++i;
                });
                dataLayers.append('<ul id="control_data_layers"></ul>');
                jQuery('input', dataLayers).remove();
                jQuery('span', dataLayers).remove();

                var count = list.length;
                for (i = 0; i < count; ++i) {
                    var label;
                    label = list[i]['label'].text();
                    label = label.replace("/", "_");
                    label = label.replace(" ", "_").toLowerCase();
                    jQuery('#control_data_layers', dataLayers).append('<li id="control_layer_' + label + '" class="'+list[i]['clicked']+'"></li>');
                    jQuery('#control_layer_' + label).append(list[i]['input']);
                    jQuery('#control_layer_' + label).append(list[i]['label']);
                }
            }
            return this.div;
        }
}