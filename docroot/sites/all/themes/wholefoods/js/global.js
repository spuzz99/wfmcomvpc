var slider_obj = [];

function pagerFactory(idx, slide) {
  return '<li><a href="#">'+(idx+1)+'</a></li>';
}

function _hide_extra_pods($) {
  if($.trim($('.view-store-departments .row-last .col-3.col-last .torn-pod-content').html()).length == 0){
    $('.view-store-departments .row-last .col-3.col-last .torn-pod-content').hide();
    $('.view-store-departments .row-last .col-3.col-last .torn-pod-header').hide();
    $('.view-store-departments .row-last .col-3.col-last .torn-pod-footer').hide();
  }

  if($.trim($('.view-store-departments .row-last .col-2 .torn-pod-content').html()).length == 0){
    $('.view-store-departments .row-last .col-2 .torn-pod-content').hide();
    $('.view-store-departments .row-last .col-2 .torn-pod-header').hide();
    $('.view-store-departments .row-last .col-2 .torn-pod-footer').hide();
  }

  if($.trim($('.view-store-services .row-last .col-3.col-last .torn-pod-content').html()).length == 0){
    $('.view-store-services .row-last .col-3.col-last .torn-pod-content').hide();
    $('.view-store-services .row-last .col-3.col-last .torn-pod-header').hide();
    $('.view-store-services .row-last .col-3.col-last .torn-pod-footer').hide();
  }

  if($.trim($('.view-store-services .row-last .col-2 .torn-pod-content').html()).length == 0) {
    $('.view-store-services .row-last .col-2 .torn-pod-content').hide();
    $('.view-store-services .row-last .col-2 .torn-pod-header').hide();
    $('.view-store-services .row-last .col-2 .torn-pod-footer').hide();
  }
}

function equalHeight($, group) {
  var tallest = 0;
  group.each(function() {
    var thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest);
}

var _init_flexslider = function() {
  jQuery('.flexslider').flexslider({
    animation: 'slide',
    pauseOnHover: true,
	pauseOnAction: true,
    slideShow: false,
    directionNav: false,
//	selector: ".promo-row > .promo-nav",
    start: function(slider){
      jQuery('.pager-nav').html(slider.controlNav);
      slider_obj.s_main = slider;
      slider_obj.s_curr = parseInt(slider.currentSlide, 10);
      slider_obj.s_count = slider.count - 1;
      jQuery('.marquee-nav .next1, .marquee-nav .prev1').show();
    },
    after: function(slider){
      jQuery('.pager-nav').html(slider.controlNav);
      slider_obj.s_curr = parseInt(slider.currentSlide, 10);
    }
  });


//Remove nav pager if there is only one slide
  var numItems = jQuery('.promo-nav').length;
    if (numItems < 2) {
    jQuery('.promo-nav').hide();
  };

  var recipeNumItems = jQuery('.recipe-promo-nav').length;
    if (recipeNumItems < 2) {
    jQuery('.recipe-promo-nav').hide();
  };

  jQuery('.marquee-content').delegate('.marquee-nav .next1', 'click', function(){
    if (slider_obj.s_curr === slider_obj.s_count) {
      slider_obj.s_main.flexAnimate(0);
    } else {
      slider_obj.s_main.flexAnimate(slider_obj.s_curr + 1);
    }
  });

  jQuery('.marquee-content').delegate('.marquee-nav .prev1', 'click', function(){
    if (slider_obj.s_curr === 0) {
      slider_obj.s_main.flexAnimate(slider_obj.s_count);
    } else {
      slider_obj.s_main.flexAnimate(slider_obj.s_curr - 1);
    }
  });

  jQuery('.marquee-content').delegate('.pager-nav a', 'click', function(){
    if (!jQuery(this).hasClass('active')) {
      slider_obj.s_main.flexAnimate(jQuery(this).parent('.pager-nav').children('a').index(jQuery(this)));
    }
  });
};

var _change_modal = function() {
  if (jQuery(window).width() < 1000) {
    jQuery('a.ctools-use-modal').each(function() {
      if (jQuery(this).attr('href') == '/modals/nojs/find_store') {
        jQuery(this).replaceWith('<a href="/stores/list">'+jQuery(this).text()+'</a>');
      }
    });
  }
}

var _init_video = function () {
    var width = jQuery(window).width();
    var iframes = jQuery('.view-videos iframe, .promo-media iframe');
    var f_width = 910 - 20;
    var f_height = 672;
    var scale = (width - 50) / 910;
    if (width < 910) {
      iframes.each(function() {
        if (typeof jQuery(this).attr("oheight") == 'undefined') {
          jQuery(this).attr("oheight", jQuery(this).attr("height"));
        }
        var o_height = jQuery(this).attr("oheight");

        if (typeof jQuery(this).attr("owidth") == 'undefined') {
          jQuery(this).attr("owidth", jQuery(this).attr("width"));
        }
        var o_width = jQuery(this).attr("owidth");
        if (width < o_width) {
          var scale = (width - 50) / o_width;
          jQuery(this).attr("width", (o_width * scale));
          jQuery(this).attr("height", (o_height * scale));
        }
      });
    }

  var featured = jQuery("#flvvideo");
  //reSizeForIE8();

  if (scale < 1) {
    featured.attr('width', f_width * scale);
    featured.attr('height', f_height * scale);
  }
  else {
    featured.attr('width', f_width);
    featured.attr('height', f_height);
  }
}

var _set_recipe_top = function() {
  if(jQuery('body').width() > 999){
    jQuery('.recipeSpotlight').css('margin-top', '-' + (jQuery('#block-views-marquee-page-block').outerHeight() - 60) + 'px');
  } else {
    jQuery('.recipeSpotlight').css('margin-top', '-' + (jQuery('#block-views-marquee-page-block').outerHeight() - 50) + 'px');
  }
};

jQuery(document).ready(function($){
  checkVersion();

  if (jQuery(window).width() > 999) {
	  _init_flexslider();
		onResize();
  } else {
    _init_video();
  }



/* the following blocks with cycle cause loading issues in IE */
if (jQuery('#block-views-recipe-promo-block').length) {
  jQuery('#block-views-recipe-promo-block .view-content:first').cycle({
    fx: 'scrollHorz',
    pause: 1,
    prev: '.recipe-prev1',
    next: '.recipe-next1',
    pager: '.recipe-promo-pager-nav',
    pagerAnchorBuilder: pagerFactory,
    timeout: 5000
  });
}

if (jQuery('#block-views-promo-blocks-block .view-content').length) {
  jQuery('#block-views-promo-blocks-block .view-content').cycle({
    fx: 'scrollHorz',
    pause: 1,
    prev: '.prev1',
    next: '.next1',
    pager: '.promo-pager-nav',
    pagerAnchorBuilder: pagerFactory,
    timeout: 5000
  });
}

/* temporary fix to pause Cycle when videos are played - a better solution is needed */

$("#block-views-recipe-promo-block .promo-media").mouseenter(function() {
  $('#block-views-recipe-promo-block .view-content').cycle('pause');
});

$("#block-views-promo-blocks-block .promo-media").mouseenter(function() {
  $('#block-views-promo-blocks-block .view-content').cycle('pause');
});

  /*$('#main').delegate('.promo-media', 'click', function(){
    $('#block-views-promo-blocks-block .view-content:first').cycle('pause');
  });*/


  // control the drop down selector on the Store landing page
  $("#webform-client-form-96 select").change(function() {
      $("fieldset#webform-component-user-content").addClass('field-display');
      $("#edit-actions").addClass('field-display');
  });

  // redisplay the form when there are submission errors on the Store landing page
  var selectedOption = $("#webform-client-form-96 select option:selected").text();
  if( selectedOption !== '- Select -'){
      $("fieldset#webform-component-user-content").addClass('field-display');
      $("#edit-actions").addClass('field-display');
  }

  $('.advanced-search-link').toggle(function(){
    $(this).addClass('on');
    $(this).text("Basic Search");
  }, function(){
    $(this).removeClass('on');
    $(this).text('Advanced Search');
  });

//Collapsible blocks trigger (isolated to blog and blog post)
  $(".collapse").parents(".block").addClass('collapse-block');
  $(".collapse").parent(".content").hide();
  $(".collapse").parent(".content").prevAll("h2").click(function() {
    $(this).toggleClass('open');
    $(this).nextAll(".content").slideToggle(200);
  });


  //navigation fix when window height is 800px or smaller
  //$(window).resize(function(){
  $(window).resize( onResize );
  function onResize() {
    if ($(window).width() < 1000 && !$('html').hasClass('undernav-active')){
      _change_modal();
    }

  //force Android to behave
    if ( navigator.userAgent.match(/Android/i)){
      Drupal.WholeFoods.Responsive._initUnderMonsterNav();
      _change_modal();
    }

    _init_video();

  };
  
  //hides extra pod on department page if not in use
  _hide_extra_pods($);

  //hides about store header if no content is present
  if($.trim($('#block-views-store-page-content-about-store .content .view-id-store_page_content .view-content .views-row-last').html()) === ""){
    $('#block-views-store-page-content-about-store h2').hide();
  }

  if($.trim($('.views-field-field-serve-count .field-content').html()) === ""){
    $('.views-field-field-serve-count').hide();
  }

  //sets promo pods to equal height on homepage
  $('.pod_blogs, .pod_facebook_feed, .pod_twitter_feed').addClass('social');
  $('body.front .view-promo-blocks .col-width-one:first-child .views-col').addClass('promos');

  equalHeight($, $(".social .torn-pod-content"));
  equalHeight($, $(".promos .torn-pod-content"));

  //wraps div around recipe promo block on recipe home page
  $('#block-views-recipe-promo-block').wrap('<div class="recipeSpotlight"></div>');

  $('#center_content img').removeAttr('height').removeAttr('width');

  if ($('body').hasClass('page-node-41')) {
    _set_recipe_top();

    setTimeout(_set_recipe_top, 1000);
    setTimeout(_set_recipe_top, 3000);

    $(window).resize(function(){
      _set_recipe_top();
    });
  }

	//Atempted fix for IE8 bug 1139. At one point a 20sec delayed call to the onResize function was attempted. Although the call was
	//made it did not fix the issue. A resizeTo function was also tried. However IE would throw errors because it was not user
	//initiated.
	//Check for IE8 or lower
	function getInternetExplorerVersion(){
		var rv = -1; // Return value assumes failure.
		if (navigator.appName == 'Microsoft Internet Explorer'){
			var ua = navigator.userAgent;
			var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			if (re.exec(ua) != null){
				rv = parseFloat( RegExp.$1 );
			}
			return rv;
		}
	}

	var IE8 = false;
	//var reSizeIE8 = 0;
	function checkVersion(){
		var ver = getInternetExplorerVersion();
			if ( ver > -1 ){
				if( ver = 8.0 ){
					IE8 = true;
					//reSizeForIE8();
//remove modal frame for ie8
//jQuery('#block-system-user-menu a').removeClass('iframe');
//jQuery('#block-system-user-menu a').removeClass('fancy');
//reload for ie8
var reloaded = false;
var loc=""+document.location;
loc = loc.indexOf("?reloaded=")!=-1?loc.substring(loc.indexOf("?reloaded=")+10,loc.length):"";
loc = loc.indexOf("&")!=-1?loc.substring(0,loc.indexOf("&")):loc;
reloaded = loc!=""?(loc=="true"):reloaded;

function reloadOnceOnly() {
	if ($('body').hasClass('page-node-14')) {
    if (!reloaded)
        window.location.replace(window.location+"?reloaded=true");
								}
						}
reloadOnceOnly(); //You can call this via the body tag if desired
				}
			}
	}

	function reSizeForIE8(){
			for (reSizeIE8=0; reSizeIE8<50; reSizeIE8++)
  			{
				if (jQuery(window).width() > 999){
					//onResize();
				}
			}
	}

});//close document ready


/**
*
* Show/Hide for Product Recalls
*
**/

jQuery(document).ready(function() {
  jQuery('div.target').css('display','none');
  jQuery("div.view-product-recalls-list-block div.views-field-title a").click(function() {
    jQuery(this).parents('.views-field-title').next().find('div.target').slideToggle();
    return false;
  });

/**
*
* Show/Hide for Investor Relations pages
*
**/

  jQuery('div.toggled').css('display','none');
  jQuery('a.toggler').click(function(e) {
		jQuery(this).parent().next('div.toggled').slideToggle();
		e.preventDefault();
	});

});
/**
*
*  Adjust width of marquee pager on home page
*
**/
jQuery(document).ready(function() {
  var slides = jQuery('ul.slides').children('li').length - 2;

  if(slides >= 8){
    jQuery('a.prev1').css('left', '30%');
    jQuery('a.next1').css('right', '30%');
  }
});

// Add event listener for Janrain password recover window
// using attachEvent where addEventListener not supported (IE8)
jQuery(document).ready(function() {

  function passwordSent() {
    jQuery.fancybox.close();
    jQuery("#main").prepend('<div class="messages status">'+Drupal.t('E-mail with password recover link will be sent to address you have provided.')+'</div>');
  }

  if (!window.addEventListener) {
	window.attachEvent(
	  "onmessage",
	  function (e) {
      if (e.data && typeof e.data === 'string' && e.data.match(/Drupal\.janrainCapture\.closeRecoverPassword/)) {
        passwordSent();
      }
	  },
	  false);
  }
  else {
    window.addEventListener(
      "message",
      function (event) {
        if ('data' in event && typeof event.data === 'string' && event.data.match(/Drupal\.janrainCapture\.closeRecoverPassword/)) {
          passwordSent();
        }
      },
      false);
  }
});


Drupal.behaviors.showFlexsliderSingleSlide = {
  attach: function (context) {
    jQuery('.flexslider ul.slides li:only-child').addClass('only-slide-ie8');
  }
};
