function janrainReturnExperience() {
    var span = document.getElementById('traditionalWelcomeName');
    var name = janrain.capture.ui.getReturnExperienceData("displayName");
    if (span && name) {
        span.innerHTML = "Welcome back, " + name + "!";
    }
}

function janrainCaptureWidgetOnLoad() {
    janrain.capture.ui.start();
    janrain.capture.ui.createCaptureSession(access_token);
}