<?php

class BehatXmlReportAggregator extends BehatReportAggregator {

  const REPORT_PATH = 'reports';
  const XML_REPORT_PATH = 'reports/xml';
  const HTML_REPORT_PATH = 'reports/html';
  const REPORT_FILE_NAME = 'aggregate-behat-errors.html';

  function __construct($profile) {
    $this->selenium_errors = array(
      'Failed connect to ondemand.saucelabs.com:80; Operation timed out',
    );
    $this->profile = $profile;
  }

  private function load_xml_files() {
    $path = getcwd() . DIRECTORY_SEPARATOR . self::XML_REPORT_PATH . DIRECTORY_SEPARATOR . $this->profile;
      
    if (file_exists($path)) {
      $xml_report_files = glob($path . '/*.xml'); 
  
      if (is_array($xml_report_files) && count($xml_report_files) > 0) {        
        return $xml_report_files;
      }
    }
    return false;
  }

  private function load_all_xml_reports() {
    $reports = array();
    if ($report_file_names = $this->load_xml_files()) {
      foreach($report_file_names as $report_file) {
        if ($report = $this->xml_report_to_array($report_file)) {
          $reports[] = $report;
        }
      }
    }
    if (count($reports) > 0) {
      return $reports;
    }
    return false;
  }

  private function xml_report_to_array($file) {
    if (file_exists($file)) {
      if ($report = simplexml_load_file($file)) {
        return $report;
      }
    }
    return false;
  }

  private function load_all_failed_tests()  {
    $reports = $this->load_all_xml_reports();
    
    $failed_tests = array();
    
    if ($reports) {
      foreach($reports as $report) {        
        if ($failed = $this->get_failed_tests($report)) {
          $failed_tests = array_merge($failed_tests, $failed);
        }
      }
      return $failed_tests;
    }
    return false;
  }

  private function load_and_sort_all_failed_tests() {
    $result = false;
    $i = 0;
    $j = 0;
    $errors = array();
    $failures = array();

    if ($failed_tests = $this->load_all_failed_tests()) {
      $result = new stdClass;
      foreach($failed_tests as $failed_test) {
        $feature = $this->parse_failed_test_attribute($failed_test, 'classname');
        $scenario = $this->parse_failed_test_attribute($failed_test, 'name');
        $msg = $this->parse_exception($failed_test);
        if ($this->is_error($msg)) {
          $errors[$i]['feature'] = $feature;
          $errors[$i]['scenario'] = $scenario;
          $errors[$i]['msg'] = $msg;
          $i++;
        }
        else {
          $stacktrace = $this->parse_stacktrace($failed_test);
          $failures[$j]['feature'] =  $feature;
          $failures[$j]['scenario'] = $scenario;
          $failures[$j]['msg'] = $msg;
          $failures[$j]['stacktrace'] = $stacktrace;
          $j++;
        }
      }
      $result->errors = $errors;
      $result->failures = $failures;
    }
    
    return $result;
  }

  private function get_failed_tests($report) {
    if ($failed_tests = $report->xpath('testcase[count(failure) > 0]')) {
      return $failed_tests;
    }
    return false;
  }

  private function write_report($content) {
    if (!file_exists(getcwd() . DIRECTORY_SEPARATOR . self::HTML_REPORT_PATH . DIRECTORY_SEPARATOR . $this->profile)) {
      mkdir(getcwd() . DIRECTORY_SEPARATOR . self::HTML_REPORT_PATH . DIRECTORY_SEPARATOR . $this->profile, 0777, true);
    }    
    file_put_contents(getcwd() . DIRECTORY_SEPARATOR . self::HTML_REPORT_PATH . DIRECTORY_SEPARATOR . $this->profile . DIRECTORY_SEPARATOR . self::REPORT_FILE_NAME, $content);
  }

  private function parse_stacktrace($error) {
    $stacktrace = (string) $error;
    $end = strpos($stacktrace, 'Stack trace:');
    
    $stacktrace = substr($stacktrace, 0, $end);
    return $stacktrace;
  }

  private function format_stacktrace($error) {
    return '<li class="behat-error-stacktrace">' . $error . '</li>' . "\n";
  }

  private function parse_failed_test_attribute($failed_test_node, $attribute) {
    $attributes = $failed_test_node->attributes();
    return (string) $attributes[$attribute];
  }
  
  private function parse_exception($failed_test_node) {
    $attributes = $failed_test_node->failure->attributes();
    return (string) $attributes['message'];
  }

  private function format_message($message, $type) {
    $msg = '<li class="behat-' . $type . '-message"><strong>Feature: ' . $message['feature'] . ' - Scenario: ' . $message['scenario'] . '</strong> ' . $message['msg'];
    if (!empty($message['stacktrace'])) {
      $msg .= '<div class="stacktrace">' . $message['stacktrace'] . '</div>';
    }
    $msg .= '</li>' . "\n";
    return $msg;
  }

  private function is_error($msg) {
    foreach($this->selenium_errors as $error) {
      if (strpos($msg, $error) !== false) {
        return true;
      }
    }
    return false;
  }

  public function aggregate_xml_reports_and_generate_html_report() {
    
    if ($result = $this->load_and_sort_all_failed_tests()) {
      $report_content = $this->report_header();
      $report_content .= '<h3>Failures</h3><ol>';
      
      if (is_array($result->failures) && count($result->failures) > 0) {
        foreach($result->failures as $failure) {
          $report_content .= $this->format_message($failure, 'failure');
        }      
      }
      else {
        $report_content .= "<li>No failures!</li>";
      }
  
      $report_content .= "</ol><h3>Errors</h3><ol>";
  
      if (is_array($result->errors) && count($result->errors) > 0) {
        foreach($result->errors as $error) {
          $report_content .= $this->format_message($error, 'error');
        }
      }
      else {
        $report_content .= "<li>No errors!</li>";
      }
  
      $report_content .= "</ol>";
      
      $report_content .= $this->report_footer();
  
      $this->write_report($report_content); 
    }
  }

  private function report_header() {
    $header = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns ="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
      <title>Behat Test Suite</title>
      <style type="text/css">
        li {
          margin-bottom: 12px;
        }
        .stacktrace {
          padding: 15px;
          font-size: 11px ;
          font-family: "Lucida Console", Monaco, monospace;
        }
      </style>
    </head>
    <body>
      <h2>Profile: ' . $this->profile . '</h2>';

    return $header;
  }

  private function report_footer() {
    $footer = '</body></html>';
    return $footer;
  }
}
?>