Feature: Customer Service
  To verify the customer service page is working
  As an unauthenticated user
  With and without a store chosen
  I need to test the shit out of all the options

Background:
  Given I am on "/customer-service"
  
@customer-service @unauth @nojs @quick @help
Scenario: Page Content is Good
  Then I should see "Customer Service" in the "breadcrumb" region
  Then I should see the "Customer Service" heading in the "content" region
  Then I should see the link "Product Ingredient FAQs" with href "/about-our-products/product-faq" in the "content" region
  Then I should see the link "Investor FAQs" with href "/company-info/investor-relations/investor-faq" in the "content" region

#numbers are store node ids. Store names stopped working for some reason.
@customer-service @unauth @javascript
Scenario: State Select Loads Correct Stores
  Given I select "Store Feedback" from "edit-submitted-choose-topic" in the "content" region
  When I select "Colorado" from "edit-submitted-topic-block-state-select" in the "content" region
  When I additionally select "43216" from "edit-submitted-topic-block-store-select"
  When I select "California" from "edit-submitted-topic-block-state-select"
  When I additionally select "6484" from "edit-submitted-topic-block-store-select"
  When I additionally select "6489" from "edit-submitted-topic-block-store-select"
  When I additionally select "6600" from "edit-submitted-topic-block-store-select"
  When I select "Louisiana" from "edit-submitted-topic-block-state-select"
  When I additionally select "6488" from "edit-submitted-topic-block-store-select"
  When I select "Massachusetts" from "edit-submitted-topic-block-state-select"
  When I additionally select "6701" from "edit-submitted-topic-block-store-select"
  When I select "Nevada" from "edit-submitted-topic-block-state-select"
  When I additionally select "6610" from "edit-submitted-topic-block-store-select"
  When I additionally select "6696" from "edit-submitted-topic-block-store-select"
  When I select "North Carolina" from "edit-submitted-topic-block-state-select"
  When I additionally select "27443" from "edit-submitted-topic-block-store-select"
  When I select "Tennessee" from "edit-submitted-topic-block-state-select"
  When I additionally select "6554" from "edit-submitted-topic-block-store-select"
  When I additionally select "418296" from "edit-submitted-topic-block-store-select"
  When I select "Virginia" from "edit-submitted-topic-block-state-select"
  When I additionally select "6507" from "edit-submitted-topic-block-store-select"
  When I select "Washington" from "edit-submitted-topic-block-state-select"
  When I additionally select "6699" from "edit-submitted-topic-block-store-select"

@customer-service @unauth @javascript
Scenario: Correct form elements are visible on page load
  Then I should see the elements: "#edit-submitted-choose-topic"
  And I should not see the elements: ".header-content, #edit-submitted-topic-block, #phone-number-box, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body, #webform-component-product-upc"

@customer-service @unauth @javascript
Scenario Outline: Topic Select Displays Correct Form Fields
  Given I select "<topic>" from "edit-submitted-choose-topic"
  Then I should see the elements: "<visible-elements>"
  Then I should not see the elements: "<invisible-elements>"

  Examples:
    | topic | visible-elements | invisible-elements |
    | Online Ordering | .header-content, #edit-submitted-topic-block, #phone-number-box, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select | #webform-component-product-upc |
    | Product Request | .header-content, #edit-submitted-topic-block, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select | #webform-component-product-upc, #phone-number-box |
    | Store Feedback | .header-content, #edit-submitted-topic-block, #phone-number-box, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select | #webform-component-product-upc |
    | 365 Everyday Products | .header-content, #edit-submitted-topic-block, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body, #webform-component-product-upc, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select | #phone-number-box |
    | Company Feedback | .header-content, #edit-submitted-topic-block, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select | #webform-component-product-upc, #phone-number-box |
    | Potential Vendor | .header-content, #edit-submitted-topic-block, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body | #webform-component-product-upc, #phone-number-box, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select |
    | Product Standards | .header-content, #edit-submitted-topic-block, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body | #webform-component-product-upc, #phone-number-box, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select |
    | Store Location Request | #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body | .header-content, #webform-component-product-upc, #phone-number-box, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select |
    | Website Issues | .header-content, #edit-submitted-topic-block, #webform-component-name, #webform-component-phone, #edit-submitted-email, #webform-component-body | #webform-component-product-upc, #phone-number-box, #edit-submitted-topic-block-store-select, #edit-submitted-topic-block-state-select |

@customer-service @unauth @javascript
Scenario Outline: Selecting a store displays the correct phone number
  Given I select "Store Feedback" from "edit-submitted-choose-topic"
  And I additionally select "Texas" from "edit-submitted-topic-block-state-select"
  And I additionally select "<store-option>" from "edit-submitted-topic-block-store-select"
  Then I should see "<phone-number>"

  Examples:
  | store-option | phone-number |
  | Lamar - 525 N Lamar Blvd. | (512) 542-2200 |
  | Gateway - 9607 Research Blvd. | 512.345.5003 |
  | Domain - 11920 Domain Drive   | (512) 831-3981 |
  | Arbor Trails - 4301 W. William Cannon | (512) 358-2460 |

@customer-service @unauth @javascript
Scenario: Customer service form submits correctly.
  Given I select "Company Feedback" from "edit-submitted-choose-topic"
  And I select "Texas" from "edit-submitted-topic-block-state-select"
  And I select "Lamar - 525 N Lamar Blvd." from "edit-submitted-topic-block-store-select"
  And I enter "WFM Test" for "edit-submitted-name"
  And I enter "noreply@wholefoods.com" for "edit-submitted-email"
  And I enter "Test message from automated testing, please disregard" for "edit-submitted-body"
  And I press the "Send comment" button
  Then I should see "Thank you, your submission has been received."