Feature: Individual Recipe Page - JS
  As an authenticated user
  I need to add and remove recipes from my recipe box
  so I can keep track of recipes that match my current tastes.

#dropped the Background even though there is some code duplication
#errors in Background are suppressed and hard to debug.

  @recipe @auth @javascript
  Scenario: Add a random recipe to the to Desktop Recipe Box and remove it
    Given That the Window is Full Screen
    Then I login as a test user
    Given That my desktop recipe box is empty
    Given I am on "recipes"
    Then I click a random element identified by ".view-display-id-featured .views-row"
    When I click "add to recipe box"
    Then I should see the link "remove from recipe box"
    When I click "remove from recipe box"
    Then I should see the link "add to recipe box"

  @recipe @auth @javascript @box
  Scenario: Add random recipe to Desktop Recipe Box, verify it shows up in my recipe box, and remove it
    Given That the Window is Full Screen
    Then I login as a test user
    Given That my desktop recipe box is empty
    Given I am on "recipes"
    Then I click a random element identified by ".view-display-id-featured .views-row"
    When I click "add to recipe box"
    When I click "view saved recipes"
    And I wait for "5000" milliseconds
    Then I should see the randomly added recipe identified by "#content ul li a"
    Given That my desktop recipe box is empty
    Then I should not see an "#block-users-user-recipe-self .content ul li a" element
