Feature: About Our Products Page  
  In order to assure basic functionality of the products page
  As an unauthenticated user 
  I need to assure all page blocks are displayed correctly 
  
Background:
  Given I am on "/about-our-products"
  
@products @nojs @quick
Scenario: Check basic blocks of the page are loading 
  Then I should see at least "2" "#block-views-generic-page-items-marqee .view-display-id-marqee .field-content" 
  And I should see an "#block-search-api-page-product-search" element
  And I should see the heading "Product Search"
  And I should see an "#search-api-page-search-form-product-search" element
  And I should see an "#block-views-promo-blocks-block" element 
  And I should see an "#block-views-generic-page-items-list-copy" element
  And I should see at least "5" "#block-views-generic-page-items-list-copy .list-row .brief"
  And I should see an "#sidebar-second" element 
  
@products @product-search @javascript @search @refactor
Scenario: Product search 
  Given That the Window is Full Screen  
  Then I perform a "paginated" "product" search for "organic"
  Given I am on "/about-our-products"
  Then I perform a "non-paginated" "product" search for "vinegar"
  
@products @product-search @javascript @search
Scenario: Blank query product search
  Then I perform a negative "product" search for ""