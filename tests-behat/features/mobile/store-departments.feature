Feature: Store Departments
  To ensure the mobile store departments load in entirety
  As an unauthenticated user
  I need to verify all page elements load correctly

Background:
  Given I am on "/store-departments"

@mobile @product-faqs @nojs @unauth @quick
Scenario: Product FAQs page - required page elements are visible
  Then I should see the heading "Store Departments" in the "content" region
  Then the ".view-store-departments .view-content" element should contain at least "9" ".views-row a" elements in the "content" region

@mobile @product-faqs @nojs @unauth @quick
Scenario: All product FAQ links are visible
  Then I should see the link "Bakery" with href "/department/bakery" in the "content" region
  Then I should see the link "Beer" with href "/department/beer" in the "content" region
  Then I should see the link "Bulk" with href "/department/bulk" in the "content" region
  Then I should see the link "Cheese" with href "/department/cheese" in the "content" region
  Then I should see the link "Coffee & Tea" with href "/department/coffee-tea" in the "content" region
  Then I should see the link "Floral" with href "/department/floral" in the "content" region
  Then I should see the link "Grocery" with href "/department/grocery" in the "content" region
  Then I should see the link "Meat & Poultry" with href "/department/meat-poultry" in the "content" region
  Then I should see the link "Prepared Foods" with href "/department/prepared-foods" in the "content" region
  Then I should see the link "Produce" with href "/department/produce" in the "content" region
  Then I should see the link "Seafood" with href "/department/seafood" in the "content" region  
  Then I should see the link "Wine" with href "/department/wine" in the "content" region
  Then I should see the link "Whole Body" with href "/department/whole-body" in the "content" region
  Then I should see the link "Pets" with href "/department/pets" in the "content" region

@mobile @product-faqs @nojs @unauth @quick
Scenario: Individual store department page - required page elements are visible
  Then I click a random element identified by ".view-id-store_departments .view-content .views-row .views-field .field-content a" in the "content" region
  Then I should be on the selected page
  Then the page element identified by ".pane-content .field-name-body .field-items .field-item" has at least "100" words in the "content" region

