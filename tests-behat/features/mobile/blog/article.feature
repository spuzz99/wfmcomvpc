Feature: Blog Page
  In order to ensure mobile access to individual blog posts
  I need to visit a random blog page
  And be certain that all page elements load

Background:
  Given I am on "/blog/whole-story"
  And I view a random blog post in the "content" region

@mobile @unauth @nojs @quick @blog
Scenario: All Individual Mobile Blog Page Elements are Visible
  Then I should see the text "Whole Story" in the "content" region
  Then I should see the text "The Official Whole Foods Market® Blog" in the "content" region
  Then the page element identified by "h1.blog-title a" has content of minimum length "11" in the "content" region
  Then the page element identified by "h2.blog-subtitle" has content of minimum length "37" in the "content" region
  Then there should be at least "2" words in the ".views-field-title .field-content" container in the "content" region
  Then there should be at least "100" words in the ".views-field-body .field-content" container in the "content" region
  #post date
  Then I should see the regex "/([A-Z])\w+\s\d{1,2},\s\d{4}/i" identified by ".views-field-created .field-content" in the "content" region
  #post author
  Then I should see the regex "/([A-Z])\w+\s([A-Z])\w+|\s|-/i" identified by ".views-field-field-blog-author .field-content a" in the "content" region