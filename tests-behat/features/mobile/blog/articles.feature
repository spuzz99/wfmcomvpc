Feature: Blog Page
  In order to ensure mobile access to the Whole Story Blog
  I need to visit the blog page
  And be certain that blog posts load

Background:
  Given I am on "/blog/whole-story"

@mobile @unauth @nojs @quick @blog
Scenario: Blog title and subtitle are visible
  Then I should see the text "Whole Story" in the "content" region
  Then I should see the text "The Official Whole Foods Market® Blog" in the "content" region

@mobile @unauth @nojs @quick
Scenario: Blog posts are visible
  Then the ".view-display-id-mobile_blog_pages" element should contain at least "5" ".views-row .post h3 a" elements in the "content" region
  Then the ".view-display-id-mobile_blog_pages" element should contain at least "5" ".views-row .post .meta .date-field" elements in the "content" region
  Then the ".view-display-id-mobile_blog_pages" element should contain at least "5" ".views-row .post .meta .author" elements in the "content" region  
  Then the ".view-display-id-mobile_blog_pages" element should contain at least "5" ".views-row .body" elements in the "content" region

@mobile @unauth @nojs @quick
Scenario: Older and Newer Links work
  When I click "older »"
  Then the url and querystring should match "/blog/whole-story?page=1"
  Then I should see the link "« newer" in the "content" region
  When I click "« newer"
  Then the url and querystring should match "/blog/whole-story"