Feature: Mobile Navigation
  To ensure the mobile navigation loads in entirety
  As an unauthenticated user
  I need to verify all menu elements load correctly

Background:
  Given I am on "/mobile/home"

@mobile @nav @nojs @unauth @quick
Scenario:
  Then I should see the link "Recipes" with href "/mobile/recipes" in the "menu" region
  Then I should see the link "Deals" with href "/mobile/coupons" in the "menu" region
  Then I should see the link "Blog" with href "/blogs" in the "menu" region
  Then I should see the link "Online Ordering" with href "http://shop.wholefoodsmarket.com/store/ShopAt.aspx" in the "menu" region
  Then I should see the link "Shopping Lists" with href "/mobile/shopping-lists" in the "menu" region
  Then I should see the link "Product FAQs" with href "/about-our-products/product-faq" in the "menu" region
  Then I should see the link "Store Departments" with href "/store-departments" in the "menu" region
  Then I should see the link "Special Diets" with href "/healthy-eating/special-diets" in the "menu" region
  Then I should see the link "My Recipe Box" with href "/myrecipebox" in the "menu" region
  Then I should see the link "Our Apps" with href "/apps" in the "menu" region
  Then I should see the link "Careers" with href "/careers" in the "menu" region
  Then I should see the link "Core Values" with href "/mission-values/core-values" in the "menu" region