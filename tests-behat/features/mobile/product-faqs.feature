Feature: Product FAQs
  To ensure the mobile product FAQs load in entirety
  As an unauthenticated user
  I need to verify all page elements load correctly

Background:
  Given I am on "/about-our-products/product-faq"

@mobile @product-faqs @nojs @unauth @quick
Scenario: Product FAQs page - required page elements are visible
  Then I should see the heading "Product Ingredient FAQs" in the "content" region
  Then I should see the heading "Our Private Label Brands" in the "content" region
  Then the page element identified by ".pane-content .view .view-header p" has at least "50" words in the "content" region
  Then the ".view-content .faq-pages-list ul" element should contain at least "9" "li a" elements in the "content" region

@mobile @product-faqs @nojs @unauth @quick
Scenario: All product FAQ links are visible
  Then I should see the link "FAQ on GMOs" with href "/about-our-products/product-faq/gmos" in the "content" region
  Then I should see the link "Quality FAQs" with href "/about-our-products/product-faq/quality" in the "content" region
  Then I should see the link "Ingredients FAQs" with href "/about-our-products/product-faq/ingredients" in the "content" region
  Then I should see the link "Product Selection FAQs" with href "/about-our-products/product-faq/product-selection" in the "content" region
  Then I should see the link "Food Allergy FAQs" with href "/about-our-products/product-faq/food-allergies" in the "content" region
  Then I should see the link "Milk and Eggs FAQs" with href "/about-our-products/product-faq/milk-and-eggs" in the "content" region
  Then I should see the link "Olive Oil FAQs" with href "/about-our-products/product-faq/olive-oil" in the "content" region
  Then I should see the link "Peanut Butter FAQs" with href "/about-our-products/product-faq/peanut-butter" in the "content" region
  Then I should see the link "Supplements FAQs" with href "/about-our-products/product-faq/supplements" in the "content" region

@mobile @product-faqs @nojs @unauth @quick
Scenario: Individual product FAQ page - required page elements are visible
  Then I click a random element identified by ".view-content .faq-pages-list ul li a" in the "content" region
  Then I should be on the selected page
  Then the page element identified by ".view-content .views-row .views-field-field-body-mobile .field-content" has at least "150" words in the "content" region
