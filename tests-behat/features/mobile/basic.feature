Feature: Mobile Features Available on all devices
  In order to ensure mobile functionality
  I need to visit all pages
  And be certain that they load

Background:
  Given I am on the homepage

@mobile @unauth @javascript @quick @homepage
Scenario: Header is Complete
  Then I should see the link "Sign in or Create account" in the "header" region
  Then I should see the link "Find Stores" in the "header" region
  Then I should see the link "Search Site" in the "header" region

@mobile @unauth @javascript @quick @homepage
Scenario: Menu is Complete
  Then I should see the link "Recipes" in the "menu" region
  Then I should see the link "Deals" in the "menu" region
  Then I should see the link "Blog" in the "menu" region
  Then I should see the link "Online Ordering" in the "menu" region
  Then I should see the link "more" in the "menu" region

@mobile @unauth @javascript @quick @homepage
Scenario: Footer is Complete
  Then I should see the link "Contact Us" in the "footer" region
  Then I should see the link "Privacy Policy" in the "footer" region
  Then I should see the link "Terms of Use" in the "footer" region
  Then I should see the link "Facebook" in the "footer" region
  Then I should see the link "Twitter" in the "footer" region
  Then I should see the link "Pinterest" in the "footer" region
  Then I should see the link "Google Plus" in the "footer" region
  Then I should see the link "Instagram" in the "footer" region
  Then I should see the link "YouTube" in the "footer" region

@mobile @unauth @javascript @quick @homepage
Scenario: Home Page Loads - No Store Selected
  Given I have no store selected
  Then I should see the heading "Select a store" in the "content" region

# disabled - this test works fine when viewed in a regular desktop browser but
# hangs on mobile because the 'choose location' browser promt blocks execution
# until a user either accepts or declines location tracking. No easy solutions
# on SauceLabs
@mobile @unauth @javascript @quick @homepage @disabled
Scenario: Home Page Loads - Store Selected
  Given that I search for stores in a random state
  And I wait for "3000" milliseconds
  Then the "div.view-content div.item-list ul" element should contain at least "1" "li.views-row" elements in the "content" region
  Given that I select a random store
  Then I should selected store name in the "header" region