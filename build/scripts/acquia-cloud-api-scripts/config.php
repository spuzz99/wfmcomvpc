<?php

/**
 * @file
 * Configuration settings for Acquia Cloud CI/CD Scripts.
 */

// Used for all scripts.
$site = 'prod:wholefoods';
$database = 'wholefoods';

// Used for `database-cleanup.php`.
$db_backup_cleanup_environments = array(
  'dev',
  'test',
);

// Used for `database-get.php`.
$db_download_environment = 'test';
