Feature: Basic Site Load
  In order to assure basic functionality
  As an unauthenticated user
  I need to visit top-level pages
  And be certain that they load

@basic @unauth @javascript @quick
Scenario: Header loads correctly
  Given That the Window is Full Screen
  Given I am on "/healthy-eating"
  Then I should see the link "find a store" with href "/stores/list" in the "header" region
  Then I should see the link "SIGN IN / REGISTER" with href "/signin" in the "header" region
  #Then I should see the link "Create Account" with href "/user/janrain_register" in the "header" region

@basic @unauth @nojs @quick
Scenario: Home Page Loads
  Given I am on "/healthy-eating"
  Then I should see the text "Welcome" in the "header" region
  #And I should see the Global Marquees
  #And I should see the Go Local block

@basic @unauth @nojs @quick
Scenario: Customer Service Page Loads
  Given I am on "/customer-service"
  Then I should see the heading "Customer Service" in the "content" region

@basic @unauth @nojs @quick
Scenario: Recipe Page Loads
  Given I am on "/recipes"
  Then the "edit-search-box" field should contain "Search our"
  Then I should see the link "Featured" in the "content" region
  Then I should see the link "Newest" in the "content" region
  Then the "div#quicktabs-tabpage-recipes-0 div.view-recipes div.view-content" element should contain at least "6" ".views-row" elements in the "content" region

@basic @unauth @nojs @quick
Scenario: Healthy Eating Page Loads
  Given I am on "/healthy-eating"
  Then I should see the text "HEALTHY EATING" in the "marquee" region

@basic @unauth @nojs @quick
Scenario: Products Page Loads
  Given I am on "/about-our-products"
  Then I should see the text "About Our Products" in the "marquee" region
  Then I should see the text "Product Search" in the "contentTop" region
  Then the "div#block-views-generic-page-items-list-copy div.content div.view-generic-page-items div.view-content" element should contain at least "5" ".list-row" elements in the "content" region
  Then I should see the text "Organic Food FAQ" in the "sidebar" region
  Then the "div.view-content div.item-list" element should contain at least "2" ".views-row" elements in the "sidebar" region

@basic @unauth @nojs @quick @current
Scenario: Online Ordering Page Loads
  Given I am on "/online-ordering"
  Then I should see the text "Online Ordering" in the ".block-wfm-blocks .content h2" container in the "contentTop" region
  When I select "Texas" from "edit-state" in the "content" region
  When I additionally select "Lamar - 525 N Lamar Blvd." from "edit-store" in the "content" region
  Then I should see the continue button in the "#store-select-form" container
  Then the "div.online-ordering-landing" element should contain at least "1" ".oo-pod" elements in the "contentTop" region

@basic @unauth @nojs @quick
Scenario: Mission & Values Page Loads
  Given I am on "/mission-values"
  Then I should see the text "Mission & Values" in the "content" region
  Then the "div#mission-values-wrapper" element should contain at least "6" ".slides" elements in the "content" region

@basic @unauth @nojs @quick
Scenario: Blog Page Loads
  Given I am on "/blog/whole-story"
  Then I should see the text "Whole Story" in the "contentTop" region
  Then the "div#block-views-blogs-blog-pages div.view-blogs div.view-content" element should contain at least "5" ".views-row" elements in the "content" region

@basic @unauth @nojs @quick
Scenario: Store Departments Page Loads
  Given I am on "/store-departments"
  Then I should see the text "Store Departments" in the "marquee" region
  Then I should see the text "Store Departments" in the "breadcrumb" region
  Then I should see the link "Bakery" in the "contentBottom" region
  Then I should see the link "Beer" in the "contentBottom" region
  Then I should see the link "Bulk" in the "contentBottom" region
  Then I should see the link "Cheese" in the "contentBottom" region
  Then I should see the link "Coffee & Tea" in the "contentBottom" region
  Then I should see the link "Floral" in the "contentBottom" region
  Then I should see the link "Grocery" in the "contentBottom" region
  Then I should see the link "Meat & Poultry" in the "contentBottom" region
  Then I should see the link "Prepared Foods" in the "contentBottom" region
  Then I should see the link "Produce" in the "contentBottom" region
  Then I should see the link "Seafood" in the "contentBottom" region
  Then I should see the link "Wine" in the "contentBottom" region
  Then I should see the link "Whole Body" in the "contentBottom" region
  Then I should see the link "Pets" in the "contentBottom" region

@basic @search @nojs @quick
Scenario: View search results
  Given I am on the homepage
  Given for "keys_16" I enter "Cheese"
  When I press "op"
  Then I should be on "/site_search/Cheese"
  And I should see many search results
  
@basic @unauth @javascript
Scenario: Forums Page Loads
  Given I am on "/forums"
  Then I should see the Pluck forum