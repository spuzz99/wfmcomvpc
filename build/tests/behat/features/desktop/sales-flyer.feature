Feature: Sales Flyer
  To ensure Sales Flyer is working
  As an unauthenticated user
  I need to verify that the state and store selectors load correctly

@sales-flyer @unauth @nojs @quick
Scenario: Sales Flyer Page Properly Loads
  Given I am on "/sales-flyer"
  Then I should see a heading in the "salesAndCouponsHeaderBlock" region
  And I should not see an "#quicktabs-container-custom_quicktab_sales_coupons" element

@sales-flyer @unauth @nojs @quick
Scenario: Sales Flyer Display Based On Selected Store
  Given my selected store is "LMR"
  And I am on "/sales-flyer"
  Then I should see a heading in the "salesAndCouponsHeaderBlock" region
  And I should see sales items in the "salesAndCouponsQuicktab" region
  And I should see the text "Lamar" in the "breadcrumb" region
  And I should see the link "Download Sales Flyer as PDF"

@sales-flyer @unauth @nojs @quick
Scenario: Sales Flyer Display Based On Store In URL
  Given I am on "/sales-flyer/lamar"
  Then I should see a heading in the "salesAndCouponsHeaderBlock" region
  And I should see sales items in the "salesAndCouponsQuicktab" region
  And I should see the text "Lamar" in the "breadcrumb" region
  And I should see the link "Download Sales Flyer as PDF"

@sales-flyer @unauth @javascript @tom
  Scenario: Toggling between sales and coupons works
    Given I am on "/sales-flyer/lamar"
    When I click "quicktabs-tab-custom_quicktab_sales_coupons-1"
    Then I should see coupons in the "salesAndCouponsQuicktab" region