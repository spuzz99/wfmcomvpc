Feature: Footer
To verify the footer loads 
As an unauthenticated user
I need to see the headings for each footer section
I need to see the links for each footer section
I need to see the link urls are correct

@footer @unauth @nojs @quick
Scenario: Footer Loads Correctly
  Given I am on homepage
  Then I should see "3" "div.footer-column" elements in the "#footer" container
  #Shopping
  Then I should see the text "Shopping" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Find a Store" with href "/stores/list" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Coupons" with href "/coupons" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Sales Flyers" with href "/sales-flyer" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "The Whole Deal®" with href "/about-our-products/whole-deal" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Catering & Online Ordering" with href "/online-ordering" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Gift Cards" with href "/buy-gift-cards" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Store Departments" with href "/store-departments" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Store Events" with href "/events" in count "1" of "div.footer-column" element in the "#footer" container
  #Eating & Cooking
  Then I should see the text "Eating & Cooking" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Healthy Eating" with href "/healthy-eating" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Featured & Top-Rated Recipes" with href "/recipes" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Recipe Collections" with href "/recipe-collections" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Special Diets" with href "/healthy-eating/special-diets" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Food Guides" with href "/recipes/food-guides" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Cooking & Entertainment Guides" with href "/recipes/cooking-entertainment-guides" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Parents & Kids" with href "/healthy-eating/kid-friendly" in count "1" of "div.footer-column" element in the "#footer" container
  #Community & Values
  Then I should see the text "Community & Values" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Our Core Values" with href "/mission-values/core-values" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Community Giving" with href "/mission-values/caring-communities/community-giving" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Local Producer Loan Program" with href "/mission-values/caring-communities/local-producer-loan-program" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Whole Trade® Program" with href "/mission-values/whole-trade-program" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Environmental Stewardship" with href "/mission-values/environmental-stewardship" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Organic Farming" with href "/mission-values/organic/about-organic-farming" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Animal Welfare" with href "/mission-values/animal-welfare" in count "1" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Seafood Sustainability" with href "/mission-values/seafood-sustainability" in count "1" of "div.footer-column" element in the "#footer" container
  #About Our Products
  Then I should see the text "About Our Products" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Quality Standards" with href "/about-our-products/quality-standards" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Food Safety" with href "/about-our-products/food-safety" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "GMOs: Your Right to Know" with href "/gmo-your-right-know" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Our Product Lines" with href "/about-our-products/our-product-lines" in count "2" of "div.footer-column" element in the "#footer" container
  #Careers
  Then I should see the text "Careers" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Find and Apply for Jobs" with href "/careers/find-and-apply-jobs" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Why We're a Great Place to Work" with href "/careers/why-were-great-place-work" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Tasty Jobs at Whole Foods Market" with href "/careers/tasty-jobs" in count "2" of "div.footer-column" element in the "#footer" container
  #Company
  Then I should see the text "Company" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "About Whole Foods Market" with href "/company-info" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Info for Potential Suppliers" with href "/company-info/information-potential-suppliers" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Investor Relations" with href "/company-info/investor-relations" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Whole Foods Market Newsroom" with href "http://media.wholefoodsmarket.com" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Customer Service" with href "/customer-service" in count "2" of "div.footer-column" element in the "#footer" container
  #Videos
  Then I should see the text "Videos" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Featured & Recent Videos" with href "/videos" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Healthy Recipe How-Tos" with href "/healthy-eating/quick-simple-recipe-videos" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Healthy Cooking Techniques" with href "/healthy-eating/healthy-cooking-videos" in count "2" of "div.footer-column" element in the "#footer" container
  #Blogs
  Then I should see the text "Blogs" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "The Whole Story" with href "/blog/whole-story" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Co-CEO John Mackey's Blog" with href "/blog/john-mackeys-blog" in count "2" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Co-CEO Walter Robb's Blog" with href "/blog/walter-robbs-blog" in count "2" of "div.footer-column" element in the "#footer" container

  #Connect with Us
  Then I should see the text "Connect with Us" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://www.facebook.com/wholefoods" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://twitter.com/wholefoods/" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "http://pinterest.com/wholefoods/" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "https://plus.google.com/108900531664537360582/posts" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "http://instagram.com/wholefoods" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the href "http://www.youtube.com/wholefoods" in count "3" of "div.footer-column" element in the "#footer" container
  #Newsletter Subscription
  Then I should see the text "Newsletter Subscription" in count "3" of "div.footer-column" element in the "#footer" container
  #More from Whole Foods Market
  Then I should see the text "More from Whole Foods Market" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Whole Planet Foundation" with href "https://www.wholeplanetfoundation.org" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Whole Kids Foundation" with href "https://www.wholekidsfoundation.org" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see the link "Whole Journeys travel planning" with href "http://www.wholejourneys.com" in count "3" of "div.footer-column" element in the "#footer" container
  #Then I should see the link "Dark Rye online magazine" with href "http://www.darkrye.com" in count "3" of "div.footer-column" element in the "#footer" container

  @footer @unauth @javascript @quick @test
  Scenario: Select A Store Function in Footer Works
    #Select a Store
  Given I am on homepage
  Then I should see the text "Select a Store" in count "3" of "div.footer-column" element in the "#footer" container
  Then I should see a "disabled" element "#footer-store-locator-store" in the "footer" region
  When I select "Texas" from "footer-store-locator-state" in the "footer" region
  When I additionally select "Lamar" from "footer-store-locator-store" in the "footer" region
  Then I should see a "enabled" element "#footer-store-locator-submit" in the "footer" region
