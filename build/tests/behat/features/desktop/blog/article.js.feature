Feature: Blog landing page   
  In order to assure basic functionality of the blog articles
  As an unauthenticated user 
  I need to be able to go the blog page and view the articles on individual article pages
  
Background:
  Given I am on "blog/whole-story"
  Then I click a random element identified by "h2.blog-title a" in the "content" region
  Then I should be on the selected page with title identified by "h2.blog-title" in the "content" region

@blog @javascript @dontfail
Scenario Outline: Check that side bar tabs are expanding and collapsing
  When I click on the element with css selector "<heading>"
  Then "<content>" should be visible
  When I click on the element with css selector "<heading>"
  Then "<content>" should not be visible
  
  Examples:
    | heading | content |
    | #block-views-blogs-blog-related-posts h2 | #block-views-blogs-blog-related-posts .content |

@blog @javascript @comments
Scenario: Check Comment form validation -- missing comment
  Given That the Window is Full Screen
  Given I enter "Foo Bar" for "edit-name--2"
  Given I enter "foo.bar@foobar.com" for "edit-mail--2"
  Given I enter "" for "edit-comment-body-und-0-value--2"  
  Given I press the submit button with value "Save" in the "form.comment-form" container in the "content" region
  Then I should see the error message containing "Comment field is required."
  
@blog @javascript @comments
Scenario: Check Comment form validation -- blank form
  Given That the Window is Full Screen
  Given I enter "" for "edit-name--2"
  Given I enter "" for "edit-mail--2"
  Given I enter "" for "edit-comment-body-und-0-value--2"
  Given I press the submit button with value "Save" in the "form.comment-form" container in the "content" region
  Then I should see the following <error messages>
    | error messages |
    | Your name field is required. |
    | E-mail field is required. |
    | Comment field is required. |
    
@blog @javascript @comments
Scenario: Check Comment form validation -- no name
  Given That the Window is Full Screen
  Given I enter "" for "edit-name--2"
  Given I enter "foo.bar@foobar.com" for "edit-mail--2"
  Given I enter "This is my comment" for "edit-comment-body-und-0-value--2"
  Given I press the submit button with value "Save" in the "form.comment-form" container in the "content" region
  Then I should see the following <error messages>
    | error messages |
    | Your name field is required. |
    
@blog @javascript @comments
Scenario: Check Comment form validation -- try submitting form after submitting with errors once
  Given That the Window is Full Screen
  Given I enter "" for "edit-name--2"
  Given I enter "foo.bar@foobar.com" for "edit-mail--2"
  Given I enter "This is my comment" for "edit-comment-body-und-0-value--2"
  Given I press the submit button with value "Save" in the "form.comment-form" container in the "content" region
  Then I should see the following <error messages>
    | error messages |
    | Your name field is required. |
  And I press "edit-submit"
  Then I should see the following <error messages>
    | error messages |
    | Your name field is required. |