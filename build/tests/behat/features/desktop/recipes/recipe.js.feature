Feature: Individual Recipe Page - JS
  In order to ensure individual recipe pages load
  As an unauthenticated user
  I need to verify all page blocks are displayed correctly

Background:
  Given I am not logged in
  Given That the Window is Full Screen
  Given I am on "/recipe/sonoma-chicken-salad"

@recipe @javascript @unauth
Scenario: Can I Search in Sidebar With Results
  #Given I fill in "search_box" with "chicken" in the "sidebar" region
  Given I enter "chicken" into "search_box" in the "sidebar" region
  Given I press "Search" in the "sidebar" region
  Then I should be on "/recipe/search/chicken" 
  Then I should see search results for "chicken" in the "content" region

@recipe @javascript @unauth
Scenario: Can I Search in Sidebar No Results
  Given I fill in "search_box" with "pebkac-pebkac-pebkac" in the "sidebar" region
  Given I press "Search" in the "sidebar" region
  Then I should be on "/recipe/search/pebkac-pebkac-pebkac" 
  Then I should see no search results for "pebkac-pebkac-pebkac" in the "content" region

@recipe @javascript @unauth
Scenario: Are Sidebar Links Correct
  When I click the first link in the container "id" "block-views-related-recipes-featured" in the "sidebar" region
  Then I should see the link "Recipes" in the "breadcrumb" region
  Then I should see an image in container ".recipe-image .field-type-image img" in the "contentTop" region  
  Then I should see the text "Ingredients" in the "content" region

@recipe @javascript @unauth
Scenario: Pluck Ratings Loaded
  Then I should see Pluck Ratings

@recipe @javascript @unauth
Scenario: Recipe Discussions Load
  Then the page should contain "1" "div" tag with ID or class "class" "plck-app-container-loaded" in the "contentBottom" region

@recipe @javascript @unauth
Scenario: Recipe Discussion Sign Up Button Loads Janrain
  When I click on the element with css selector "a.pluck-login-comment-signup-button"
  Then I should see the text "SIGN IN OR CREATE AN ACCOUNT" in the "#content" container
  Then restart session

@recipe @javascript @unauth
Scenario: Recipe Discussion Login Button Loads Janrain
  When I click on the element with css selector "a.pluck-login-comment-submit-button"  
  Then I should see the text "SIGN IN OR CREATE AN ACCOUNT" in the "#content" container
  Then restart session

@recipe @javascript @unauth @print
Scenario: Can I Print Full Page With Image
  When I click "print" in the "contentTop" region
  Then I should see the text "Full Recipe (+ Image)" in the "#print-box .full_recipe_image" container
  When I ammend radio button with value "recipe_with_image" to be "recipe_with_image&suppressPrint=true" and then select it
  When I click "Continue"
  Then I switch to window "print"
  #hit escape key to suppress print dialog if the above method fails to do so  
  Given I manually press "27"
  Then the url and querystring should match "/print/recipe/sonoma-chicken-salad?css=recipe_with_image&suppressPrint=true"

@recipe @javascript @unauth @print
Scenario: Can I Print Full Page Without Image
  When I click "print" in the "contentTop" region
  Then I should see the text "Full Recipe (No Image)" in the "#print-box .full_recipe" container
  When I ammend radio button with value "recipe_no_image" to be "recipe_no_image&suppressPrint=true" and then select it
  When I click "Continue"
  Then I switch to window "print"
  #hit escape key to suppress print dialog if the above method fails to do so
  Given I manually press "27"
  Then the url and querystring should match "/print/recipe/sonoma-chicken-salad?css=recipe_no_image&suppressPrint=true"

@recipe @javascript @unauth @print
Scenario: Can I Print 3x5
  When I click "print" in the "contentTop" region
  Then I should see the text "3 X 5 Recipe Card" in the "#print-box .recipe_card" container
  When I ammend radio button with value "recipe_3x5" to be "recipe_3x5&suppressPrint=true" and then select it 
  When I click "Continue"
  Then I switch to window "print"
  #hit escape key to suppress print dialog if the above method fails to do so
  Given I manually press "27"
  Then the url and querystring should match "/print/recipe/sonoma-chicken-salad?css=recipe_3x5&suppressPrint=true"
