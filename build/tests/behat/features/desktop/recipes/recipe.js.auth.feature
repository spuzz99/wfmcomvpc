Feature: Individual Recipe Page - JS
  In order to ensure individual recipe pages load
  As an authenticated user
  I need to verify all page blocks are displayed correctly

#dropped the Background even though there is some code duplication
#errors in Background are suppressed and hard to debug.

@recipe @auth @javascript @disabled
Scenario: Add to Desktop Recipe Box and Remove From Desktop Recipe Box
  Given That the Window is Full Screen 
  Given I am not logged in
  Given I am on "recipes"
  When I click "Sign In" in the "header" region
  Then I login as a test user
  Given That my desktop recipe box is empty
  Given I am on "recipes"
  Then I click a random element identified by ".views-row .views-field-title .field-content a" in the "content" region
  Then I should be on the selected page with title identified by ".view-content .views-row .torn-pod-content .views-field-title .field-content h2 span" in the "contentTop" region
  When I click "add to recipe box"
  Then I should see the link "remove from recipe box"
  When I click "remove from recipe box"
  Then I should see the link "add to recipe box"

@recipe @auth @javascript @disabled 
Scenario: Add to Desktop Recipe Box View Saved Recipes Remove From Desktop Recipe Box
  Given That the Window is Full Screen 
  Given I am not logged in
  Given I am on "recipes"
  When I click "Sign In" in the "header" region
  Then I login as a test user
  Given That my desktop recipe box is empty
  Given I am on "recipes"
  Then I click a random element identified by ".views-row .views-field-title .field-content a" in the "content" region
  Then I should be on the selected page with title identified by ".view-content .views-row .torn-pod-content .views-field-title .field-content h2 span" in the "contentTop" region
  When I click "add to recipe box"
  When I click "view saved recipes"
  Then I should be on "user?qt-user_profile_self=2"
  Then I should see the randomly added recipe identified by "#block-users-user-recipe-self ul li a" in the "content" region
  Given That my desktop recipe box is empty
  Then I should not see an "#block-users-user-recipe-self .content ul li a" element