Feature: Store Locator Page 
  In order to assure basic functionality of the locator page
  As an unauthenticated user 
  I need to be able to find stores based on postal code, or city and state/province
  And select a store as "my store"
  
Background:
  Given That the Window is Full Screen
  Given I am on "/stores/list"

@locator @javascript
Scenario: Check the form submission button is visible 
  Then "#edit-submit-store-lookup-by-address" should be visible

@locator @javascript
Scenario: Locator search using postal code in united states 
  Given for "edit-field-geo-data-latlon-address" I enter "78701"
  When I press "edit-submit-store-lookup-by-address"
  Then I should see the text "525 N Lamar Blvd." in the ".storefront-address .street-block .thoroughfare" container in the "content" region

@locator @javascript
Scenario: Locator search using city and state in United States
  Given for "edit-field-geo-data-latlon-address" I enter "austin tx"
  When I press "edit-submit-store-lookup-by-address"
  Then I should see the text "525 N Lamar Blvd." in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  And I should see the text "4301 W. William Cannon" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  And I should see the text "11920 Domain Drive" in the ".storefront-address .street-block .thoroughfare" container in the "content" region

@locator @javascript
Scenario: Locator search entering junk as query
  Given for "edit-field-geo-data-latlon-address" I enter "foobar"
  When I press "edit-submit-store-lookup-by-address"
  And I should see the text "No results found. Please enter postal code, or city and state/province in the search box above." in the ".view-store-lookup-by-address .view-empty" container in the "content" region

@locator @javascript
Scenario: Locator search using a Canadian postal code
  Given for "edit-field-geo-data-latlon-address" I enter "V5Z 1C5"
  When I press "edit-submit-store-lookup-by-address"
  Then I should see the text "510 West 8th Avenue" in the ".storefront-address .street-block .thoroughfare" container in the "content" region

@locator @javascript
Scenario: Locator search using a Canadian city and province
  Given for "edit-field-geo-data-latlon-address" I enter "Vancouver BC"
  When I press "edit-submit-store-lookup-by-address"
  Then I should see the text "510 West 8th Avenue" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  And I should see the text "2285 West 4th Avenue" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  And I should see the text "1675 Robson Street" in the ".storefront-address .street-block .thoroughfare" container in the "content" region

@locator @javascript
Scenario: Locator search using postal code in UK
  Given for "edit-field-geo-data-latlon-address" I enter "NW1 7PN"
  When I press "edit-submit-store-lookup-by-address"
  Then I should see the text "49 Parkway" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  And I should see the text "20 Glasshouse Street" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  And I should see the text "63-97 Kensington High Street" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  And I should see the text "32-40 Stoke Newington Church St" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  And I should see the text "2-6 Fulham Broadway" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  
@locator @javascript
Scenario: Locator search using city in Uk
  Given for "edit-field-geo-data-latlon-address" I enter "London"
  When I press "edit-submit-store-lookup-by-address"
  Then I should see the text "49 Parkway" in the ".storefront-address .street-block .thoroughfare" container in the "content" region
  Then I should see the text "305-311 Lavender Hill" in the ".storefront-address .street-block .thoroughfare" container in the "content" region

@locator @javascript
Scenario: Check list more stores by state button works
  When I click "US Stores By State"
  Then I should be on "/stores/list/state"
  When I click "Show More Stores"
  Then ".show_more_stores" should not be visible
  And "#block-views-store-locations-by-state-state .pager" should be visible

@locator @javascript
Scenario: Check some of the pagination links of stores listed by state 
  When I click "US Stores By State"
  Then I should be on "/stores/list/state"
  Then I click "Show More Stores"
  When I click "Go to page 2"
  Then I should be on "/stores/list/state?page=1"
  And I should see at least "5" "#block-views-store-locations-by-state-state .views-row"
  When I click "Go to page 3"
  Then I should be on "/stores/list/state?page=2"
  And I should see at least "5" "#block-views-store-locations-by-state-state .views-row"
  #When I click "Go to last page"
  #And I should see at least "1" "#block-views-store-locations-by-state-state .views-row"

@locator @javascript
Scenario: Select "Make This My Store button", then try changing selection
  Given I select a random store
  Then I should see the selected store name in the "header" region
  Given I select a random store
  Then I should see the selected store name in the "header" region  
 
@locator @javascript
Scenario: Go to paginated page and select store and check it stays set 
  When I click "Load more stores"
  And I should see at least "10" ".view-store-lookup-by-address .view-content .views-row"
  Then I click a random element identified by ".torn-pod-content .views-field-title .field-content a" in the "content" region
  Then I should be on the selected page with title identified by ".torn-pod-content .store-title" in the "contentTop" region