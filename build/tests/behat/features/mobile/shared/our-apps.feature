Feature: Our Apps
  To ensure the mobile Our Apps page loads in entirety
  As an unauthenticated user
  I need to verify all page elements load correctly

Background:
  Given I am on "/apps"

@mobile @our-apps @javascript @unauth @quick
Scenario: Our Apps page - required page elements are visible
  Then I should see the heading "Our Apps" in the "content" region
  Then I should see the heading "Whole Foods Market Recipes" in the "content" region
  Then I should see the heading "Whole Foods Market Missions" in the "content" region
  Then I should see the heading "Whole Kids Foundation Awesome Eats™" in the "content" region
  Then the page element identified by ".view-generic-page-items .view-content .views-row .views-field-field-body-mobile .field-content" has at least "250" words in the "content" region
  Then I should see a link with href "app/whole-foods-market-recipes/id320029256" in the "content" region
  Then I should see a link with href "app/awesome-eats/id504890965" in the "content" region
  Then I should see a link with href "app/whole-foods-market-missions/id386304254" in the "content" region