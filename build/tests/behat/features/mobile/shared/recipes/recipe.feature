Feature: Individual Recipe Page
  In order to essure mobile recipe page functionality
  I need to load random recipe pages
  And be certain that all page elements load correctly

Background:
  Given I am on "/mobile/recipes"
  Given I fill in "#edit-search-box" with a random search term
  And I press "Search"
  Then I click a random element identified by ".panel-panel .inside .views-field-title .field-content a" in the "content" region
  Then I should be on the selected page with title identified by "h2.page-title" in the "content" region

@mobile @unauth @nojs @quick @recipe
Scenario: Recipe Page Loads All Page Elements
  Then I should see the link "Summary" in the "content" region
  Then I should see the link "Ingredients" in the "content" region
  Then I should see the link "Method" in the "content" region
  Then there should be at least "1" words in the ".views-field-field-serves .field-content" container in the "content" region
  Then I should see an image in the ".views-field-field-hero-image .field-content" container
  Then there should be at least "15" words in the ".views-field-body .field-content" container in the "content" region
  Then I should see the text "Special Diets" in the "content" region
  Then I should see the text "Nutritional Info" in the "content" region
  Then I should see the text "Amount Per Serving" in the "content" region  
  Then I should see the heading "Share This Recipe" in the "content" region
  Then the ".views-field-field-nutritional-info table" element should contain at least "7" "tr" elements in the "content" region
  Then the Share This block is visible and complete in the "content" region
  Then the ".views-field-field-ingredients .field-content .item-list ul" element should contain at least "5" "li" elements in the "content" region
  Then there should be at least "25" words in the ".views-field-field-recipe-directions .field-content" container in the "content" region
