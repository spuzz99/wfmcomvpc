<?php

class BehatHtmlReportAggregator extends BehatReportAggregator {

  const HTML = 'html';
    
  public function __construct() {
    $this->html_reports = array();
  }
  
  public function aggregate_individual_html_profile_reports() {
        
    foreach (new DirectoryIterator(self::HTML_REPORT_PATH) as $file_info) {  
      if ($this->is_report_file($file_info)) {
        $html = $this->load_report_contents($file_info->getFilename());
        $body = $this->parse_body_content($html);
        $this->html_reports[] = $body;
      }
    } 
    
    $this->write_report();
  }

  private function parse_body_content($html) {
            
    $dom = new DOMDocument;
    @$dom->loadHTML($html);
    $bodies = $dom->getElementsByTagName('body');
    assert($bodies->length === 1);
    $body = $bodies->item(0);
    
    $body = $dom->saveHTML($body);
    
    $body = str_replace('<body>', '', $body);
    $body = str_replace('</body>', '', $body);
    return $body;    
  }
  
  private function load_report_contents($profile) {
    $html = file_get_contents(getcwd() . DIRECTORY_SEPARATOR . self::HTML_REPORT_PATH . DIRECTORY_SEPARATOR . $profile . DIRECTORY_SEPARATOR . self::REPORT_FILE_NAME);
    return $html;
  }
  
  private function is_report_file($file_info) {
    if ($file_info->isDot() || $file_info->getFilename() == '.DS_Store' || !$file_info->isDir()) {
      return false;
    }
    return true;
  }
  
  private function write_report() {
    if (!file_exists(getcwd() . DIRECTORY_SEPARATOR . self::HTML_REPORT_PATH)) {
      mkdir(getcwd() . DIRECTORY_SEPARATOR . self::HTML_REPORT_PATH, 0777, true);
    }
    
    $content = $this->report_header();
    foreach($this->html_reports as $report) {
      $content .= $report;
    }
    $content .= $this->report_footer();
    file_put_contents(getcwd() . DIRECTORY_SEPARATOR . self::HTML_REPORT_PATH . DIRECTORY_SEPARATOR . self::REPORT_FILE_NAME, $content);
  }
  
  
  private function report_header() {
    $header = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns ="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
      <title>Behat Test Suite</title>
      <style>
        li {
          margin-bottom: 12px;
        }
        .stacktrace {
          padding: 15px;
          font-size: 11px ;
          font-family: "Lucida Console", Monaco, monospace;
        }
      </style>
    </head>
    <body>
      <h1>Behat Aggregated Smoke Test Report</h1>';

    return $header;
  }

  private function report_footer() {
    $footer = '</body></html>';
    return $footer;
  }
}
?>