<?php
define('REPORT_ARG_HTML', 'html');
define('REPORT_ARG_XML', 'xml');

require_once('behat-report-aggregator.php');
require_once('behat-xml-report-aggregator.php');
require_once('behat-html-report-aggregator.php');

$shortopts = "p::r::";  //optional -p for 'profile'
$longopts  = array("profile::", "report::");  //optional --profile, optional --report
$args = getopt($shortopts, $longopts);

if (!isset($args['report']) || empty($args['report']) 
  || (isset($args['report']) && $args['report'] != REPORT_ARG_HTML && $args['report'] != REPORT_ARG_XML)) {
    exit("\nYou must provide a '--report=html|xml' argument to indicate which report(s) you wish to aggregate. \n\n");
}
elseif ($args['report'] == REPORT_ARG_XML) {
  if (!isset($args['profile']) || empty($args['profile'])) {
    exit("\nYou must provide a '--profile=xxxxxxxx-xxxxxx-xx' argument that matches whichever profile's (from *.behat.*.yml) tests that you are wanting to aggregate\n\n");
  }
  else {
    $bra = new BehatXmlReportAggregator($args['profile']);
    $bra->aggregate_xml_reports_and_generate_html_report();
  }    
}
elseif ($args['report'] == REPORT_ARG_HTML) {  
  $bra = new BehatHtmlReportAggregator();
  $bra->aggregate_individual_html_profile_reports();
}
?>