#!/bin/bash
# Codeship - Setup Commands

# printenv

###
# ./build/codeship/codeship_setup.sh
###

# Set php version through phpenv. 5.3, 5.4 and 5.5 available
phpenv local 5.5

# Set ruby version through rvm. Codeship default is 2.1.2
# rvm use ruby-2.1.2 --install

# Custom php configuration
echo "xdebug.max_nesting_level=200" >> /home/rof/.phpenv/versions/5.5/etc/php.ini
echo "sendmail_path=`which true`" >> /home/rof/.phpenv/versions/5.5/etc/php.ini

# Install extentions through Pear
# pear install pear/PHP_CodeSniffer

# Install extensions through Pecl
# yes yes | pecl install memcache

# Phpenv rehashing
phpenv rehash

# remove Codeship's phantomjs
rm -rf ~/.phantomjs

# Install dependencies through Composer
COMPOSER_HOME=${HOME}/cache/composer
composer install --prefer-source --working-dir=build --no-interaction

# Add build bin dir to Codeship's $PATH
export PATH=$PATH:$HOME/clone/build/bin

# Setup Phing properties for Codeship
cp build/codeship/codeship.build.properties build/phing/build.properties
