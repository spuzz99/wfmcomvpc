#!/bin/bash
# Codeship - Configure Test Pipelines

###
# ./build/codeship/codeship_test.sh
###

# Installs Drupal from ACQUIA db sql dump
# Disabling until finding a better way to deal with sending db over the net
# See build/scripts/acquia-cloud-api-scripts for script and congif details
#
# DB_DOWNLOAD_URL=$(php -f build/scripts/acquia-cloud-api-scripts/database-get.php)
# if [ -z "{DB_DOWNLOAD_URL}" ]; then
#     echo "DB_DOWNLOAD_URL is not set"
#     exit 1
# fi
# echo ${DB_DOWNLOAD_URL}
# curl -L ${DB_DOWNLOAD_URL} -o docroot/backup.sql.gz
# cd $HOME/clone/docroot
# drush st
# drush sql-drop -y
# gunzip < backup.sql.gz | drush sql-cli
# cd $HOME/clone

# Overrides for Behat
export BEHAT_PARAMS='{"extensions":{"Behat\\MinkExtension":{"base_url":"http://127.0.0.1:8000/"}}}'

# Move custom routing file into place since we don't have Apache and clearn URLS
cp build/codeship/.ht.router.php docroot/.ht.router.php

# Start the PHP Server
cd docroot
nohup bash -c 'php -S 127.0.0.1:8000 ".ht.router.php" 2>&1 &' && sleep 1; cat nohup.out
cd ../

# Run all tests
# ./build/bin/phing -verbose -f build/phing/build.xml validate-test:all

# Run behat tests
# ./build/bin/phing -verbose -f build/phing/build.xml test:behat

# Run code sniffing tools
./build/bin/phing -f build/phing/build.xml validate:phpcs